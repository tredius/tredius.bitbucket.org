/*!
  * =============================================================
  * Ender: open module JavaScript framework (https://ender.no.de)
  * Build: ender build .
  * =============================================================
  */

/*!
  * Ender: open module JavaScript framework (client-lib)
  * copyright Dustin Diaz & Jacob Thornton 2011-2012 (@ded @fat)
  * http://ender.no.de
  * License MIT
  */
(function (context) {

  // a global object for node.js module compatiblity
  // ============================================

  context['global'] = context

  // Implements simple module system
  // losely based on CommonJS Modules spec v1.1.1
  // ============================================

  var modules = {}
    , old = context['$']
    , oldEnder = context['ender']
    , oldRequire = context['require']
    , oldProvide = context['provide']

  function require (identifier) {
    // modules can be required from ender's build system, or found on the window
    var module = modules['$' + identifier] || window[identifier]
    if (!module) throw new Error("Ender Error: Requested module '" + identifier + "' has not been defined.")
    return module
  }

  function provide (name, what) {
    return (modules['$' + name] = what)
  }

  context['provide'] = provide
  context['require'] = require

  function aug(o, o2) {
    for (var k in o2) k != 'noConflict' && k != '_VERSION' && (o[k] = o2[k])
    return o
  }

  /**
   * main Ender return object
   * @constructor
   * @param {Array|Node|string} s a CSS selector or DOM node(s)
   * @param {Array.|Node} r a root node(s)
   */
  function Ender(s, r) {
    var elements
      , i

    this.selector = s
    // string || node || nodelist || window
    if (typeof s == 'undefined') {
      elements = []
      this.selector = ''
    } else if (typeof s == 'string' || s.nodeName || (s.length && 'item' in s) || s == window) {
      elements = ender._select(s, r)
    } else {
      elements = isFinite(s.length) ? s : [s]
    }
    this.length = elements.length
    for (i = this.length; i--;) this[i] = elements[i]
  }

  /**
   * @param {function(el, i, inst)} fn
   * @param {Object} opt_scope
   * @returns {Ender}
   */
  Ender.prototype['forEach'] = function (fn, opt_scope) {
    var i, l
    // opt out of native forEach so we can intentionally call our own scope
    // defaulting to the current item and be able to return self
    for (i = 0, l = this.length; i < l; ++i) i in this && fn.call(opt_scope || this[i], this[i], i, this)
    // return self for chaining
    return this
  }

  Ender.prototype.$ = ender // handy reference to self


  function ender(s, r) {
    return new Ender(s, r)
  }

  ender['_VERSION'] = '0.4.3-dev'

  ender.fn = Ender.prototype // for easy compat to jQuery plugins

  ender.ender = function (o, chain) {
    aug(chain ? Ender.prototype : ender, o)
  }

  ender._select = function (s, r) {
    if (typeof s == 'string') return (r || document).querySelectorAll(s)
    if (s.nodeName) return [s]
    return s
  }


  // use callback to receive Ender's require & provide and remove them from global
  ender.noConflict = function (callback) {
    context['$'] = old
    if (callback) {
      context['provide'] = oldProvide
      context['require'] = oldRequire
      context['ender'] = oldEnder
      if (typeof callback == 'function') callback(require, provide, this)
    }
    return this
  }

  if (typeof module !== 'undefined' && module.exports) module.exports = ender
  // use subscript notation as extern for Closure compilation
  context['ender'] = context['$'] = ender

}(this));

(function () {

  var module = { exports: {} }, exports = module.exports;

  /***************************************************************
    * Traversty: A DOM collection management and traversal utility
    * (c) Rod Vagg (@rvagg) 2012
    * https://github.com/rvagg/traversty
    * License: MIT
    */
  
  !(function (name, definition) {
    if (typeof module !== 'undefined') module.exports = definition()
    else if (typeof define === 'function' && define.amd) define(name, definition)
    else this[name] = definition()
  }('traversty', function () {
  
    var context = this
      , old = context.traversty
      , doc = window.document
      , html = doc.documentElement
      , toString = Object.prototype.toString
      , Ap = Array.prototype
      , slice = Ap.slice
        // feature test to find native matchesSelector()
      , matchesSelector = (function (el, pfx, name, i, ms) {
          while (i < pfx.length)
            if (el[ms = pfx[i++] + name]) return ms
        }(html, [ 'msM', 'webkitM', 'mozM', 'oM', 'm' ], 'atchesSelector', 0))
  
      , Kfalse = function () { return false }
  
      , isNumber = function (o) {
          return toString.call(o) === '[object Number]'
        }
  
      , isString = function (o) {
          return toString.call(o) === '[object String]'
        }
  
      , isFunction = function (o) {
          return toString.call(o) === '[object Function]'
        }
  
      , isUndefined = function (o) {
          return o === void 0
        }
  
      , isElement = function (o) {
          return o && o.nodeType === 1
        }
  
        // figure out which argument, if any, is our 'index'
      , getIndex = function (selector, index) {
          return isUndefined(selector) && !isNumber(index) ? 0 :
            isNumber(selector) ? selector : isNumber(index) ? index : null
        }
  
        // figure out which argument, if any, is our 'selector'
      , getSelector = function (selector) {
          return isString(selector) ? selector : '*'
        }
  
      , nativeSelectorFind = function (selector, el) {
          return slice.call(el.querySelectorAll(selector), 0)
        }
  
      , nativeSelectorMatches = function (selector, el) {
          return selector === '*' || el[matchesSelector](selector)
        }
  
      , selectorFind = nativeSelectorFind
  
      , selectorMatches = nativeSelectorMatches
  
        // used in the case where our selector engine does out-of-order element returns for
        // grouped selectors, e.g. '.class, tag', we need our elements in document-order
        // so we do it ourselves if need be
      , createUnorderedEngineSelectorFind = function(engineSelect, selectorMatches) {
          return function (selector, el) {
            if (/,/.test(selector)) {
              var ret = [], i = -1, els = el.getElementsByTagName('*')
              while (++i < els.length) {
                if (isElement(els[i]) && selectorMatches(selector, els[i])) ret.push(els[i])
              }
              return ret
            }
            return engineSelect(selector, el)
          }
        }
  
        // is 'element' underneath 'container' somewhere
      , isAncestor = 'compareDocumentPosition' in html
          ? function (element, container) {
              return (container.compareDocumentPosition(element) & 16) == 16
            }
          : 'contains' in html
            ? function (element, container) {
                container = container.nodeType === 9 || container == window ? html : container
                return container !== element && container.contains(element)
              }
            : function (element, container) { // old smelly browser
                while (element = element.parentNode) if (element === container) return 1
                return 0
              }
  
        // return an array containing only unique elements
      , unique = function (ar) {
          var a = [], i = -1, j, has
          while (++i < ar.length) {
            j = -1
            has = false
            while (++j < a.length) {
              if (a[j] === ar[i]) {
                has = true
                break
              }
            }
            if (!has) a.push(ar[i])
          }
          return a
        }
  
        // for each element of 'els' execute 'fn' to get an array of elements to collect
      , collect = function (els, fn) {
          var ret = [], res, i = 0, j, l = els.length, l2
          while (i < l) {
            j = 0
            l2 = (res = fn(els[i], i++)).length
            while (j < l2) ret.push(res[j++])
          }
          return ret
        }
  
       // generic DOM navigator to move multiple elements around the DOM
     , move = function (els, method, selector, index, filterFn) {
          index = getIndex(selector, index)
          selector = getSelector(selector)
          return collect(els
            , function (el, elind) {
                var i = index || 0, ret = []
                if (!filterFn)
                  el = el[method]
                while (el && (index === null || i >= 0)) {
                  // ignore non-elements, only consider selector-matching elements
                  // handle both the index and no-index (selector-only) cases
                  if (isElement(el)
                      && (!filterFn || filterFn === true || filterFn(el, elind))
                      && selectorMatches(selector, el)
                      && (index === null || i-- === 0)) {
                    // this concat vs push is to make sure we add elements to the result array
                    // in reverse order when doing a previous(selector) and up(selector)
                    index === null && method !== 'nextSibling' ? ret = [el].concat(ret) : ret.push(el)
                  }
                  el = el[method]
                }
                return ret
              }
          )
        }
  
        // given an index & length, return a 'fixed' index, fixes non-numbers & neative indexes
      , eqIndex = function (length, index, def) {
          if (index < 0) index = length + index
          if (index < 0 || index >= length) return null
          return !index && index !== 0 ? def : index
        }
  
        // collect elements of an array that match a filter function
      , filter = function (els, fn) {
          var arr = [], i = 0, l = els.length
          for (; i < l; i++)
            fn(els[i], i) && arr.push(els[i])
          return arr
        }
  
        // create a filter function, for use by filter(), is() & not()
        // allows the argument to be an element, a function or a selector
      , filterFn = function (slfn) {
          var to
          return isElement(slfn)
            ? function (el) { return el === slfn }
            : (to = typeof slfn) == 'function'
              ? function (el, i) { return slfn.call(el, i) }
              : to == 'string' && slfn.length
                ? function (el) { return selectorMatches(slfn, el) }
                : Kfalse
        }
  
        // fn = !fn
      , inv = function (fn) {
          return function () {
            return !fn.apply(this, arguments)
          }
        }
  
      , traversty = (function () {
          function T(els) {
            this.length = 0
            if (els) {
              els = unique(!els.nodeType && !isUndefined(els.length) ? els : [ els ])
              var i = this.length = els.length
              while (i--) this[i] = els[i]
            }
          }
  
          T.prototype = {
              down: function (selector, index) {
                index = getIndex(selector, index)
                selector = getSelector(selector)
                return traversty(collect(this
                  , function (el) {
                      var f = selectorFind(selector, el)
                      return index === null ? f : ([ f[index] ] || [])
                    }
                  ))
              }
  
            , up: function (selector, index) {
                return traversty(move(this, 'parentNode', selector, index))
              }
  
            , parents: function () {
                return T.prototype.up.apply(this, arguments.length ? arguments : [ '*' ])
              }
  
            , closest: function (selector, index) {
                if (isNumber(selector)) {
                  index = selector
                  selector = '*'
                } else if (!isString(selector)) {
                  return traversty([])
                } else if (!isNumber(index)) {
                  index = 0
                }
                return traversty(move(this, 'parentNode', selector, index, true))
              }
  
            , previous: function (selector, index) {
                return traversty(move(this, 'previousSibling', selector, index))
              }
  
            , next: function (selector, index) {
                return traversty(move(this, 'nextSibling', selector, index))
              }
  
            , siblings: function (selector, index) {
                var self = this
                  , arr = slice.call(this, 0)
                  , i = 0, l = arr.length
                for (; i < l; i++) {
                  arr[i] = arr[i].parentNode.firstChild
                  while (!isElement(arr[i])) arr[i] = arr[i].nextSibling
                }
                if (isUndefined(selector))
                  selector = '*'
  
                return traversty(move(arr, 'nextSibling', selector || '*', index
                      , function (el, i) { return el !== self[i] } // filter
                    ))
              }
  
            , children: function (selector, index) {
                return traversty(move(T.prototype.down.call(this), 'nextSibling', selector || '*', index, true))
              }
  
            , first: function () {
                return T.prototype.eq.call(this, 0)
              }
  
            , last: function () {
                return T.prototype.eq.call(this, -1)
              }
  
            , eq: function (index) {
                return traversty(this.get(index))
              }
  
            , get: function (index) {
                return this[eqIndex(this.length, index, 0)]
              }
  
              // a crazy man wrote this, don't try to understand it, see the tests
            , slice: function (start, end) {
                var e = end, l = this.length, arr = []
                start = eqIndex(l, Math.max(-this.length, start), 0)
                e = eqIndex(end < 0 ? l : l + 1, end, l)
                end = e === null || e > l ? end < 0 ? 0 : l : e
                while (start !== null && start < end)
                  arr.push(this[start++])
                return traversty(arr)
              }
  
            , filter: function (slfn) {
                return traversty(filter(this, filterFn(slfn)))
              }
  
            , not: function (slfn) {
                return traversty(filter(this, inv(filterFn(slfn))))
              }
  
              // similar to filter() but cares about descendent elements
            , has: function (slel) {
                return traversty(filter(
                    this
                  , isElement(slel)
                      ? function (el) { return isAncestor(slel, el) }
                      : typeof slel == 'string' && slel.length
                        ? function (el) { return selectorFind(slel, el).length } //TODO: performance
                        : Kfalse
                ))
              }
  
              // same as filter() but return a boolean so quick-return after first successful find
            , is: function (slfn) {
                var i = 0, l = this.length
                  , fn = filterFn(slfn)
                for (; i < l; i++)
                  if (fn(this[i], i)) return true
                return false
              }
  
            , toArray: function () { return Ap.slice.call(this) }
  
            , size: function () { return this.length }
  
            , each: function (fn, ctx) {
                var i = 0, l = this.length
                for (; i < l; i++)
                  fn.call(ctx || this[i], this[i], i, this)
                return this
              }
  
              // quack like a duck (Array)
            , push: Ap.push
            , sort: Ap.sort
            , splice: Ap.splice
          }
  
          T.prototype.prev = T.prototype.previous
  
          function t(els) {
            return new T(isString(els) ? selectorFind(els, doc) : els)
          }
  
          // extend traversty functionality with custom methods
          t.aug = function (methods) {
            var key, method
            for (key in methods) {
              method = methods[key]
              if (typeof method == 'function') {
                T.prototype[key] = method
              }
            }
          }
  
  
          t.setSelectorEngine = function (s) {
            // feature testing the selector engine like a boss
            var ss, r, a, _selectorMatches, _selectorFind
              , e = doc.createElement('p')
              , select = s.select || s.sel || s
  
            e.innerHTML = '<a/><i/><b/>'
            a = e.firstChild
            try {
              // YO! I HEARD YOU LIKED NESTED TERNARY OPERATORS SO I COOKED SOME UP FOR YOU!
              // (one day I might loop this...)
  
              // check to see how we do a matchesSelector
              _selectorMatches = isFunction(s.matching)
                ? function (selector, el) { return s.matching([el], selector).length > 0 }
                : isFunction(s.is)
                  ? function (selector, el) { return s.is(el, selector) }
                  : isFunction(s.matchesSelector)
                    ? function (selector, el) { return s.matchesSelector(el, selector) }
                    : isFunction(s.match)
                      ? function (selector, el) { return s.match(el, selector) }
                      : isFunction(s.matches)
                        ? function (selector, el) { return s.matches(el, selector) }
                        : null
  
              if (!_selectorMatches) {
                // perhaps it's an selector(x).is(y) type selector?
                ss = s('a', e)
                _selectorMatches = isFunction(ss._is)
                  ? function (selector, el) { return s(el)._is(selector) } // original .is(), replaced by Enderbridge
                  : isFunction(ss.matching)
                    ? function (selector, el) { return s(el).matching(selector).length > 0 }
                    : isFunction(ss.is) && !ss.is.__ignore
                      ? function (selector, el) { return s(el).is(selector) }
                        : isFunction(ss.matchesSelector)
                          ? function (selector, el) { return s(el).matchesSelector(selector) }
                          : isFunction(ss.match)
                            ? function (selector, el) { return s(el).match(selector) }
                            : isFunction(ss.matches)
                              ? function (selector, el) { return s(el).matches(selector) }
                              : null
              }
  
              if (!_selectorMatches)
                  throw new Error('Traversty: couldn\'t find selector engine\'s `matchesSelector`')
  
              // verify that we have a working `matchesSelector`
              if (_selectorMatches('x,y', e) || !_selectorMatches('a,p', e))
                  throw new Error('Traversty: couldn\'t make selector engine\'s `matchesSelector` work')
  
              // basic select
              if ((r = select('b,a', e)).length !== 2) throw new Error('Traversty: don\'t know how to use this selector engine')
              // check to see if the selector engine has given us the results in document-order
              // and if not, work around it
              _selectorFind = r[0] === a ? select : createUnorderedEngineSelectorFind(select, _selectorMatches)
              // have we done enough to get a working `selectorFind`?
              if ((r = _selectorFind('b,a', e)).length !== 2 || r[0] !== a)
                throw new Error('Traversty: couldn\'t make selector engine work')
  
              selectorMatches = _selectorMatches
              selectorFind = _selectorFind
            } catch (ex) {
              if (isString(ex)) throw ex
              throw new Error('Traversty: error while figuring out how the selector engine works: ' + (ex.message || ex))
            } finally {
              e = null
            }
  
            return t
          }
  
          t.noConflict = function () {
            context.traversty = old
            return this
          }
  
          return t
        }())
   
    return traversty
  }));
  

  provide("traversty", module.exports);

  /*global ender:true*/
  
  (function ($) {
    var t = require('traversty')
      , integrated = false
      , integrate = function (meth) {
          // this crazyness is for lazy initialisation because we can't be guaranteed
          // that a selector engine has been installed *before* traversty in an ender build
          var fn = function (self, selector, index) {
              if (!integrated) {
                try {
                  t.setSelectorEngine($)
                } catch (ex) { } // ignore exception, we may have an ender build with no selector engine
                integrated = true
              }
              fn = meth == 'is'
                ? function (self, slfn) {
                    return t(self)[meth](slfn) // boolean
                  }
                : function (self, selector, index) {
                    return $(t(self)[meth](selector, index)) // collection
                  }
              return fn(self, selector, index)
            }
          return function (selector, index) { return fn(this, selector, index) }
        }
      , methods = 'up down next previous prev parents closest siblings children first last eq slice filter not is has'.split(' ')
      , b = {}, i = methods.length
  
    // does this build have an .is()? if so, shift it to _is() for traversty to use and
    // allow us to integrate a new is(), wrapped around it
    if ($.fn.is) $.fn._is = $.fn.is
    while (--i >= 0) b[methods[i]] = integrate(methods[i])
    $.ender(b, true)
    $.fn.is.__ignore = true
  }(ender))

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*!
    * Bowser - a browser detector
    * https://github.com/ded/bowser
    * MIT License | (c) Dustin Diaz 2011
    */
  !function (name, definition) {
    if (typeof define == 'function') define(definition)
    else if (typeof module != 'undefined' && module.exports) module.exports['browser'] = definition()
    else this[name] = definition()
  }('bowser', function () {
    /**
      * navigator.userAgent =>
      * Chrome:  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.57 Safari/534.24"
      * Opera:   "Opera/9.80 (Macintosh; Intel Mac OS X 10.6.7; U; en) Presto/2.7.62 Version/11.01"
      * Safari:  "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; en-us) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1"
      * IE:      "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C)"
      * Firefox: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0) Gecko/20100101 Firefox/4.0"
      * iPhone:  "Mozilla/5.0 (iPhone Simulator; U; CPU iPhone OS 4_3_2 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8H7 Safari/6533.18.5"
      * iPad:    "Mozilla/5.0 (iPad; U; CPU OS 4_3_2 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8H7 Safari/6533.18.5",
      * Android: "Mozilla/5.0 (Linux; U; Android 2.3.4; en-us; T-Mobile G2 Build/GRJ22) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"
      * Touchpad: "Mozilla/5.0 (hp-tabled;Linux;hpwOS/3.0.5; U; en-US)) AppleWebKit/534.6 (KHTML, like Gecko) wOSBrowser/234.83 Safari/534.6 TouchPad/1.0"
      */
  
    var ua = navigator.userAgent
      , t = true
      , ie = /msie/i.test(ua)
      , chrome = /chrome/i.test(ua)
      , safari = /safari/i.test(ua) && !chrome
      , iphone = /iphone/i.test(ua)
      , ipad = /ipad/i.test(ua)
      , touchpad = /touchpad/i.test(ua)
      , android = /android/i.test(ua)
      , opera = /opera/i.test(ua)
      , firefox = /firefox/i.test(ua)
      , gecko = /gecko\//i.test(ua)
      , seamonkey = /seamonkey\//i.test(ua)
      , webkitVersion = /version\/(\d+(\.\d+)?)/i
      , o
  
    function detect() {
  
      if (ie) return {
          msie: t
        , version: ua.match(/msie (\d+(\.\d+)?);/i)[1]
      }
      if (chrome) return {
          webkit: t
        , chrome: t
        , version: ua.match(/chrome\/(\d+(\.\d+)?)/i)[1]
      }
      if (touchpad) return {
          webkit: t
        , touchpad: t
        , version : ua.match(/touchpad\/(\d+(\.\d+)?)/i)[1]
      }
      if (iphone || ipad) {
        o = {
            webkit: t
          , mobile: t
          , ios: t
          , iphone: iphone
          , ipad: ipad
        }
        // WTF: version is not part of user agent in web apps
        if (webkitVersion.test(ua)) {
          o.version = ua.match(webkitVersion)[1]
        }
        return o
      }
      if (android) return {
          webkit: t
        , android: t
        , mobile: t
        , version: ua.match(webkitVersion)[1]
      }
      if (safari) return {
          webkit: t
        , safari: t
        , version: ua.match(webkitVersion)[1]
      }
      if (opera) return {
          opera: t
        , version: ua.match(webkitVersion)[1]
      }
      if (gecko) {
        o = {
            gecko: t
          , mozilla: t
          , version: ua.match(/firefox\/(\d+(\.\d+)?)/i)[1]
        }
        if (firefox) o.firefox = t
        return o
      }
      if (seamonkey) return {
          seamonkey: t
        , version: ua.match(/seamonkey\/(\d+(\.\d+)?)/i)[1]
      }
    }
  
    var bowser = detect()
  
    // Graded Browser Support
    // http://developer.yahoo.com/yui/articles/gbs
    if ((bowser.msie && bowser.version >= 7) ||
        (bowser.chrome && bowser.version >= 10) ||
        (bowser.firefox && bowser.version >= 4.0) ||
        (bowser.safari && bowser.version >= 5) ||
        (bowser.opera && bowser.version >= 10.0)) {
      bowser.a = t;
    }
  
    else if ((bowser.msie && bowser.version < 7) ||
        (bowser.chrome && bowser.version < 10) ||
        (bowser.firefox && bowser.version < 4.0) ||
        (bowser.safari && bowser.version < 5) ||
        (bowser.opera && bowser.version < 10.0)) {
      bowser.c = t
    } else bowser.x = t
  
    return bowser
  })
  

  provide("bowser", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /**
   * @constructor
   * @param {!{patterns: !Object, leftmin: !number, rightmin: !number}} language The language pattern file. Compatible with Hyphenator.js.
   */
  function Hypher(language) {
      var exceptions = [],
          i = 0;
      /**
       * @type {!Hypher.TrieNode}
       */
      this.trie = this.createTrie(language['patterns']);
  
      /**
       * @type {!number}
       * @const
       */
      this.leftMin = language['leftmin'];
  
      /**
       * @type {!number}
       * @const
       */
      this.rightMin = language['rightmin'];
  
      /**
       * @type {!Object.<string, !Array.<string>>}
       */
      this.exceptions = {};
  
      if (language['exceptions']) {
          exceptions = language['exceptions'].split(/,\s?/g);
  
          for (; i < exceptions.length; i += 1) {
              this.exceptions[exceptions[i].replace(/-/g, '')] = exceptions[i].split('-');
          }
      }
  }
  
  /**
   * @typedef {{_points: !Array.<number>}}
   */
  Hypher.TrieNode;
  
  /**
   * Creates a trie from a language pattern.
   * @private
   * @param {!Object} patternObject An object with language patterns.
   * @return {!Hypher.TrieNode} An object trie.
   */
  Hypher.prototype.createTrie = function (patternObject) {
      var size = 0,
          i = 0,
          c = 0,
          p = 0,
          chars = null,
          points = null,
          codePoint = null,
          t = null,
          tree = {
              _points: []
          },
          patterns;
  
      for (size in patternObject) {
          if (patternObject.hasOwnProperty(size)) {
              patterns = patternObject[size].match(new RegExp('.{1,' + (+size) + '}', 'g'));
  
              for (i = 0; i < patterns.length; i += 1) {
                  chars = patterns[i].replace(/[0-9]/g, '').split('');
                  points = patterns[i].split(/\D/);
                  t = tree;
  
                  for (c = 0; c < chars.length; c += 1) {
                      codePoint = chars[c].charCodeAt(0);
  
                      if (!t[codePoint]) {
                          t[codePoint] = {};
                      }
                      t = t[codePoint];
                  }
  
                  t._points = [];
  
                  for (p = 0; p < points.length; p += 1) {
                      t._points[p] = points[p] || 0;
                  }
              }
          }
      }
      return tree;
  };
  
  /**
   * Hyphenates a text.
   *
   * @param {!string} str The text to hyphenate.
   * @return {!string} The same text with soft hyphens inserted in the right positions.
   */
  Hypher.prototype.hyphenateText = function (str, minLength) {
      minLength = minLength || 4;
  
      // Regexp("\b", "g") splits on word boundaries,
      // compound separators and ZWNJ so we don't need
      // any special cases for those characters.
      var words = str.split(/\b/g);
  
      for (var i = 0; i < words.length; i += 1) {
          if (words[i].indexOf('/') !== -1) {
              // Don't insert a zero width space if the slash is at the beginning or end
              // of the text, or right after or before a space.
              if (i !== 0 && i !== words.length - 1 && !(/\s+\/|\/\s+/.test(words[i]))) {
                  words[i] += '\u200B';
              }
          } else if (words[i].length > minLength) {
              words[i] = this.hyphenate(words[i]).join('\u00AD');
          }
      }
      return words.join('');
  };
  
  /**
   * Hyphenates a word.
   *
   * @param {!string} word The word to hyphenate
   * @return {!Array.<!string>} An array of word fragments indicating valid hyphenation points.
   */
  Hypher.prototype.hyphenate = function (word) {
      var characters,
          characterPoints = [],
          originalCharacters,
          i,
          j,
          k,
          node,
          points = [],
          wordLength,
          nodePoints,
          nodePointsLength,
          m = Math.max,
          trie = this.trie,
          result = [''];
  
      if (this.exceptions.hasOwnProperty(word)) {
          return this.exceptions[word];
      }
  
      if (word.indexOf('\u00AD') !== -1) {
          return [word];
      }
  
      word = '_' + word + '_';
  
      characters = word.toLowerCase().split('');
      originalCharacters = word.split('');
      wordLength = characters.length;
  
      for (i = 0; i < wordLength; i += 1) {
          points[i] = 0;
          characterPoints[i] = characters[i].charCodeAt(0);
      }
  
      for (i = 0; i < wordLength; i += 1) {
          node = trie;
          for (j = i; j < wordLength; j += 1) {
              node = node[characterPoints[j]];
  
              if (node) {
                  nodePoints = node._points;
                  if (nodePoints) {
                      for (k = 0, nodePointsLength = nodePoints.length; k < nodePointsLength; k += 1) {
                          points[i + k] = m(points[i + k], nodePoints[k]);
                      }
                  }
              } else {
                  break;
              }
          }
      }
  
      for (i = 1; i < wordLength - 1; i += 1) {
          if (i > this.leftMin && i < (wordLength - this.rightMin) && points[i] % 2) {
              result.push(originalCharacters[i]);
          } else {
              result[result.length - 1] += originalCharacters[i];
          }
      }
  
      return result;
  };
  
  module.exports = Hypher;

  provide("hypher", module.exports);

  !function (doc, $) {
      var Hypher = require('hypher'),
          cache = {};
  
      $.ender({
          registerHyphenationLanguage: function (pattern, options) {
              var h = new Hypher(pattern, options);
  
              if (typeof pattern.id === 'string') {
                  pattern.id = [pattern.id];
              }
  
              for (var i = 0; i < pattern.id.length; i += 1) {
                  cache[pattern.id[i]] = h;
              }
          }
      });
  
      $.ender({
          hyphenate: function (language) {
              if (cache[language]) {
                  var i = 0,
                      j = 0,
                      len = this.length,
                      nodeLen = 0;
  
                  for (; i < len; i += 1) {
                      j = 0;
                      nodeLen = this[i].childNodes.length;
                      
                      for (; j < nodeLen; j += 1) {
                          if (this[i].childNodes[j].nodeType === 3) {
                              this[i].childNodes[j].nodeValue = cache[language].hyphenateText(this[i].childNodes[j].nodeValue);
                          }
                      }
                  }
              }
          }
      }, true);
  }(document, ender);
  

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*! Categorizr.js: Device Detection Scripts | https://github.com/Skookum/categorizr.js/blob/master/license.md */
  
  (function (name, context, definition) {
    if (typeof module !== 'undefined') module.exports = definition(name, context);
    else if (typeof define === 'function' && typeof define.amd  === 'object') define(definition);
    else context[name] = definition(name, context);
  }('categorizr', this, function(name, context) {
        // isBrowser implementation based on https://github.com/jquery/jquery/blob/master/src/core.js
    var key // used in a loop below
      , isBrowser = context != null && context == context.window
      , isNode = !isBrowser
      , is$ = isBrowser && context.$
      , eventEmitter = (function () {
          var e
          if (is$) e = context.$('').trigger
          return e
        }())
      , docElement = isNode ? null : document.documentElement
  
      , deviceTypes = 'Tv Desktop Tablet Mobile'.split(' ')
  
      , test = function (ua) {
                  // smart tv
          return  ua.match(/GoogleTV|SmartTV|Internet.TV|NetCast|NETTV|AppleTV|boxee|Kylo|Roku|DLNADOC|CE\-HTML/i) ? 'tv'
                  // tv-based gaming console
                : ua.match(/Xbox|PLAYSTATION.3|Wii/i) ? 'tv'
                  // tablet
                : ua.match(/iPad/i) || ua.match(/tablet/i) && !ua.match(/RX-34/i) || ua.match(/FOLIO/i) ? 'tablet'
                  // android tablet
                : ua.match(/Linux/i) && ua.match(/Android/i) && !ua.match(/Fennec|mobi|HTC.Magic|HTCX06HT|Nexus.One|SC-02B|fone.945/i) ? 'tablet'
                  // Kindle or Kindle Fire
                : ua.match(/Kindle/i) || ua.match(/Mac.OS/i) && ua.match(/Silk/i) ? 'tablet'
                  // pre Android 3.0 Tablet
                : ua.match(/GT-P10|SC-01C|SHW-M180S|SGH-T849|SCH-I800|SHW-M180L|SPH-P100|SGH-I987|zt180|HTC(.Flyer|\_Flyer)|Sprint.ATP51|ViewPad7|pandigital(sprnova|nova)|Ideos.S7|Dell.Streak.7|Advent.Vega|A101IT|A70BHT|MID7015|Next2|nook/i) || ua.match(/MB511/i) && ua.match(/RUTEM/i) ? 'tablet'
                  // unique Mobile User Agent
                : ua.match(/BOLT|Fennec|Iris|Maemo|Minimo|Mobi|mowser|NetFront|Novarra|Prism|RX-34|Skyfire|Tear|XV6875|XV6975|Google.Wireless.Transcoder/i) ? 'mobile'
                  // odd Opera User Agent - http://goo.gl/nK90K
                : ua.match(/Opera/i) && ua.match(/Windows.NT.5/i) && ua.match(/HTC|Xda|Mini|Vario|SAMSUNG\-GT\-i8000|SAMSUNG\-SGH\-i9/i) ? 'mobile'
                  // Windows Desktop
                : ua.match(/Windows.(NT|XP|ME|9)/) && !ua.match(/Phone/i) || ua.match(/Win(9|.9|NT)/i) ? 'desktop'
                  // Mac Desktop
                : ua.match(/Macintosh|PowerPC/i) && !ua.match(/Silk/i) ? 'desktop'
                  // Linux Desktop
                : ua.match(/Linux/i) && ua.match(/X11/i) ? 'desktop'
                  // Solaris, SunOS, BSD Desktop
                : ua.match(/Solaris|SunOS|BSD/i) ? 'desktop'
                  // Desktop BOT/Crawler/Spider
                : ua.match(/Bot|Crawler|Spider|Yahoo|ia_archiver|Covario-IDS|findlinks|DataparkSearch|larbin|Mediapartners-Google|NG-Search|Snappy|Teoma|Jeeves|TinEye/i) && !ua.match(/Mobile/i) ? 'desktop'
                  // assume it is a Mobile Device (mobile-first)
                : 'mobile'
        }
      , device = test( context.navigator ? context.navigator.userAgent
                      : context.request ? context.request.headers['user-agent']
                      : 'No User-Agent Provided' )
      , is = function (type) {
          return device === type
        }
      , categorizr = function () {
          var args = [].slice.call(arguments, 0)
  
          // previously categorizeType. arg1 = real, arg2 = fake
          if (args.length === 2 && device === args[0]) {
            device = args[1]
            _update()
          }
  
          // else set type
          else if (args.length === 1 && typeof args[0] === 'string') {
            // todo: can only set to registered deviceTypes
            device = args[0]
            _update()
          }
  
          // always return device. no args returns device
          return device
        }
  
    categorizr.is = is
    categorizr.test = test
  
    // set quick access properties
    // e.g. categorizr.isTv => false
    //      categorizr.isDesktop => true
    //      categorizr.isTablet => false
    //      categorizr.isMobile => false
    function _setDeviceBooleans () {
      var i = deviceTypes.length
      while (i--) {
        categorizr['is'+deviceTypes[i]] = is(deviceTypes[i].toLowerCase())
        if (is$) context.$['is'+deviceTypes[i]] = is(deviceTypes[i].toLowerCase())
      }
    }
  
    function _setClassName () {
      if (isBrowser) {
        docElement.className = docElement.className.replace(/(^|\s)desktop|tablet|tv|mobile(\s|$)/, '$1$2') + (' ' + device)
      }
    }
  
    function _update () {
      _setDeviceBooleans()
      _setClassName()
  
      // trigger deviceChange event
      if (eventEmitter) context.$(context).trigger('deviceChange', [{ type: device }])
    }
  
    // init
    _update()
  
    if (is$) {
      // put categorizr onto global $
      for (key in categorizr)
        if (Object.hasOwnProperty.call(categorizr, key))
          context.$[key == 'test' ? 'testUserAgent' : key == 'is' ? 'isDeviceType' : key] = categorizr[key]
      context.$.categorizr = categorizr
    }
  
    return categorizr;
  }));
  

  provide("categorizr", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*!
    * Reqwest! A general purpose XHR connection manager
    * (c) Dustin Diaz 2012
    * https://github.com/ded/reqwest
    * license MIT
    */
  !function (name, definition) {
    if (typeof module != 'undefined' && module.exports) module.exports = definition()
    else if (typeof define == 'function' && define.amd) define(definition)
    else this[name] = definition()
  }('reqwest', function () {
  
    var win = window
      , doc = document
      , twoHundo = /^20\d$/
      , byTag = 'getElementsByTagName'
      , readyState = 'readyState'
      , contentType = 'Content-Type'
      , requestedWith = 'X-Requested-With'
      , head = doc[byTag]('head')[0]
      , uniqid = 0
      , callbackPrefix = 'reqwest_' + (+new Date())
      , lastValue // data stored by the most recent JSONP callback
      , xmlHttpRequest = 'XMLHttpRequest'
  
    var isArray = typeof Array.isArray == 'function' ? Array.isArray : function (a) {
      return a instanceof Array
    }
    var defaultHeaders = {
        contentType: 'application/x-www-form-urlencoded'
      , requestedWith: xmlHttpRequest
      , accept: {
          '*':  'text/javascript, text/html, application/xml, text/xml, */*'
        , xml:  'application/xml, text/xml'
        , html: 'text/html'
        , text: 'text/plain'
        , json: 'application/json, text/javascript'
        , js:   'application/javascript, text/javascript'
        }
      }
    var xhr = win[xmlHttpRequest] ?
      function () {
        return new XMLHttpRequest()
      } :
      function () {
        return new ActiveXObject('Microsoft.XMLHTTP')
      }
  
    function handleReadyState(o, success, error) {
      return function () {
        if (o && o[readyState] == 4) {
          o.onreadystatechange = undefined;
          if (twoHundo.test(o.status)) {
            success(o)
          } else {
            error(o)
          }
        }
      }
    }
  
    function setHeaders(http, o) {
      var headers = o.headers || {}, h
      headers.Accept = headers.Accept || defaultHeaders.accept[o.type] || defaultHeaders.accept['*']
      // breaks cross-origin requests with legacy browsers
      if (!o.crossOrigin && !headers[requestedWith]) headers[requestedWith] = defaultHeaders.requestedWith
      if (!headers[contentType]) headers[contentType] = o.contentType || defaultHeaders.contentType
      for (h in headers) {
        headers.hasOwnProperty(h) && http.setRequestHeader(h, headers[h])
      }
    }
  
    function setCredentials(http, o) {
      if (typeof o.withCredentials !== "undefined" && typeof http.withCredentials !== "undefined") {
        http.withCredentials = !!o.withCredentials
      }
    }
  
    function generalCallback(data) {
      lastValue = data
    }
  
    function urlappend(url, s) {
      return url + (/\?/.test(url) ? '&' : '?') + s
    }
  
    function handleJsonp(o, fn, err, url) {
      var reqId = uniqid++
        , cbkey = o.jsonpCallback || 'callback' // the 'callback' key
        , cbval = o.jsonpCallbackName || reqwest.getcallbackPrefix(reqId)
        // , cbval = o.jsonpCallbackName || ('reqwest_' + reqId) // the 'callback' value
        , cbreg = new RegExp('((^|\\?|&)' + cbkey + ')=([^&]+)')
        , match = url.match(cbreg)
        , script = doc.createElement('script')
        , loaded = 0
        , isIE10 = navigator.userAgent.indexOf('MSIE 10.0') !== -1
  
      if (match) {
        if (match[3] === '?') {
          url = url.replace(cbreg, '$1=' + cbval) // wildcard callback func name
        } else {
          cbval = match[3] // provided callback func name
        }
      } else {
        url = urlappend(url, cbkey + '=' + cbval) // no callback details, add 'em
      }
  
      win[cbval] = generalCallback
  
      script.type = 'text/javascript'
      script.src = url
      script.async = true
      if (typeof script.onreadystatechange !== 'undefined' && !isIE10) {
        // need this for IE due to out-of-order onreadystatechange(), binding script
        // execution to an event listener gives us control over when the script
        // is executed. See http://jaubourg.net/2010/07/loading-script-as-onclick-handler-of.html
        //
        // if this hack is used in IE10 jsonp callback are never called
        script.event = 'onclick'
        script.htmlFor = script.id = '_reqwest_' + reqId
      }
  
      script.onload = script.onreadystatechange = function () {
        if ((script[readyState] && script[readyState] !== 'complete' && script[readyState] !== 'loaded') || loaded) {
          return false
        }
        script.onload = script.onreadystatechange = null
        script.onclick && script.onclick()
        // Call the user callback with the last value stored and clean up values and scripts.
        o.success && o.success(lastValue)
        lastValue = undefined
        head.removeChild(script)
        loaded = 1
      }
  
      // Add the script to the DOM head
      head.appendChild(script)
    }
  
    function getRequest(o, fn, err) {
      var method = (o.method || 'GET').toUpperCase()
        , url = typeof o === 'string' ? o : o.url
        // convert non-string objects to query-string form unless o.processData is false
        , data = (o.processData !== false && o.data && typeof o.data !== 'string')
          ? reqwest.toQueryString(o.data)
          : (o.data || null)
        , http
  
      // if we're working on a GET request and we have data then we should append
      // query string to end of URL and not post data
      if ((o.type == 'jsonp' || method == 'GET') && data) {
        url = urlappend(url, data)
        data = null
      }
  
      if (o.type == 'jsonp') return handleJsonp(o, fn, err, url)
  
      http = xhr()
      http.open(method, url, true)
      setHeaders(http, o)
      setCredentials(http, o)
      http.onreadystatechange = handleReadyState(http, fn, err)
      o.before && o.before(http)
      http.send(data)
      return http
    }
  
    function Reqwest(o, fn) {
      this.o = o
      this.fn = fn
  
      init.apply(this, arguments)
    }
  
    function setType(url) {
      var m = url.match(/\.(json|jsonp|html|xml)(\?|$)/)
      return m ? m[1] : 'js'
    }
  
    function init(o, fn) {
  
      this.url = typeof o == 'string' ? o : o.url
      this.timeout = null
  
      // whether request has been fulfilled for purpose
      // of tracking the Promises
      this._fulfilled = false
      // success handlers
      this._fulfillmentHandlers = []
      // error handlers
      this._errorHandlers = []
      // complete (both success and fail) handlers
      this._completeHandlers = []
      this._erred = false
      this._responseArgs = {}
  
      var self = this
        , type = o.type || setType(this.url)
  
      fn = fn || function () {}
  
      if (o.timeout) {
        this.timeout = setTimeout(function () {
          self.abort()
        }, o.timeout)
      }
  
      if (o.success) {
        this._fulfillmentHandlers.push(function () {
          o.success.apply(o, arguments)
        })
      }
  
      if (o.error) {
        this._errorHandlers.push(function () {
          o.error.apply(o, arguments)
        })
      }
  
      if (o.complete) {
        this._completeHandlers.push(function () {
          o.complete.apply(o, arguments)
        })
      }
  
      function complete(resp) {
        o.timeout && clearTimeout(self.timeout)
        self.timeout = null
        while (self._completeHandlers.length > 0) {
          self._completeHandlers.shift()(resp)
        }
      }
  
      function success(resp) {
        var r = resp.responseText
        if (r) {
          switch (type) {
          case 'json':
            try {
              resp = win.JSON ? win.JSON.parse(r) : eval('(' + r + ')')
            } catch (err) {
              return error(resp, 'Could not parse JSON in response', err)
            }
            break;
          case 'js':
            resp = eval(r)
            break;
          case 'html':
            resp = r
            break;
          case 'xml':
            resp = resp.responseXML;
            break;
          }
        }
  
        self._responseArgs.resp = resp
        self._fulfilled = true
        fn(resp)
        while (self._fulfillmentHandlers.length > 0) {
          self._fulfillmentHandlers.shift()(resp)
        }
  
        complete(resp)
      }
  
      function error(resp, msg, t) {
        self._responseArgs.resp = resp
        self._responseArgs.msg = msg
        self._responseArgs.t = t
        self._erred = true
        while (self._errorHandlers.length > 0) {
          self._errorHandlers.shift()(resp, msg, t)
        }
        complete(resp)
      }
  
      this.request = getRequest(o, success, error)
    }
  
    Reqwest.prototype = {
      abort: function () {
        this.request.abort()
      }
  
    , retry: function () {
        init.call(this, this.o, this.fn)
      }
  
      /**
       * Small deviation from the Promises A CommonJs specification
       * http://wiki.commonjs.org/wiki/Promises/A
       */
  
      /**
       * `then` will execute upon successful requests
       */
    , then: function (success, fail) {
        if (this._fulfilled) {
          success(this._responseArgs.resp)
        } else if (this._erred) {
          fail(this._responseArgs.resp, this._responseArgs.msg, this._responseArgs.t)
        } else {
          this._fulfillmentHandlers.push(success)
          this._errorHandlers.push(fail)
        }
        return this
      }
  
      /**
       * `always` will execute whether the request succeeds or fails
       */
    , always: function (fn) {
        if (this._fulfilled || this._erred) {
          fn(this._responseArgs.resp)
        } else {
          this._completeHandlers.push(fn)
        }
        return this
      }
  
      /**
       * `fail` will execute when the request fails
       */
    , fail: function (fn) {
        if (this._erred) {
          fn(this._responseArgs.resp, this._responseArgs.msg, this._responseArgs.t)
        } else {
          this._errorHandlers.push(fn)
        }
        return this
      }
    }
  
    function reqwest(o, fn) {
      return new Reqwest(o, fn)
    }
  
    // normalize newline variants according to spec -> CRLF
    function normalize(s) {
      return s ? s.replace(/\r?\n/g, '\r\n') : ''
    }
  
    function serial(el, cb) {
      var n = el.name
        , t = el.tagName.toLowerCase()
        , optCb = function (o) {
            // IE gives value="" even where there is no value attribute
            // 'specified' ref: http://www.w3.org/TR/DOM-Level-3-Core/core.html#ID-862529273
            if (o && !o.disabled)
              cb(n, normalize(o.attributes.value && o.attributes.value.specified ? o.value : o.text))
          }
  
      // don't serialize elements that are disabled or without a name
      if (el.disabled || !n) return;
  
      switch (t) {
      case 'input':
        if (!/reset|button|image|file/i.test(el.type)) {
          var ch = /checkbox/i.test(el.type)
            , ra = /radio/i.test(el.type)
            , val = el.value;
          // WebKit gives us "" instead of "on" if a checkbox has no value, so correct it here
          (!(ch || ra) || el.checked) && cb(n, normalize(ch && val === '' ? 'on' : val))
        }
        break;
      case 'textarea':
        cb(n, normalize(el.value))
        break;
      case 'select':
        if (el.type.toLowerCase() === 'select-one') {
          optCb(el.selectedIndex >= 0 ? el.options[el.selectedIndex] : null)
        } else {
          for (var i = 0; el.length && i < el.length; i++) {
            el.options[i].selected && optCb(el.options[i])
          }
        }
        break;
      }
    }
  
    // collect up all form elements found from the passed argument elements all
    // the way down to child elements; pass a '<form>' or form fields.
    // called with 'this'=callback to use for serial() on each element
    function eachFormElement() {
      var cb = this
        , e, i, j
        , serializeSubtags = function (e, tags) {
          for (var i = 0; i < tags.length; i++) {
            var fa = e[byTag](tags[i])
            for (j = 0; j < fa.length; j++) serial(fa[j], cb)
          }
        }
  
      for (i = 0; i < arguments.length; i++) {
        e = arguments[i]
        if (/input|select|textarea/i.test(e.tagName)) serial(e, cb)
        serializeSubtags(e, [ 'input', 'select', 'textarea' ])
      }
    }
  
    // standard query string style serialization
    function serializeQueryString() {
      return reqwest.toQueryString(reqwest.serializeArray.apply(null, arguments))
    }
  
    // { 'name': 'value', ... } style serialization
    function serializeHash() {
      var hash = {}
      eachFormElement.apply(function (name, value) {
        if (name in hash) {
          hash[name] && !isArray(hash[name]) && (hash[name] = [hash[name]])
          hash[name].push(value)
        } else hash[name] = value
      }, arguments)
      return hash
    }
  
    // [ { name: 'name', value: 'value' }, ... ] style serialization
    reqwest.serializeArray = function () {
      var arr = []
      eachFormElement.apply(function (name, value) {
        arr.push({name: name, value: value})
      }, arguments)
      return arr
    }
  
    reqwest.serialize = function () {
      if (arguments.length === 0) return ''
      var opt, fn
        , args = Array.prototype.slice.call(arguments, 0)
  
      opt = args.pop()
      opt && opt.nodeType && args.push(opt) && (opt = null)
      opt && (opt = opt.type)
  
      if (opt == 'map') fn = serializeHash
      else if (opt == 'array') fn = reqwest.serializeArray
      else fn = serializeQueryString
  
      return fn.apply(null, args)
    }
  
    reqwest.toQueryString = function (o) {
      var qs = '', i
        , enc = encodeURIComponent
        , push = function (k, v) {
            qs += enc(k) + '=' + enc(v) + '&'
          }
  
      if (isArray(o)) {
        for (i = 0; o && i < o.length; i++) push(o[i].name, o[i].value)
      } else {
        for (var k in o) {
          if (!Object.hasOwnProperty.call(o, k)) continue;
          var v = o[k]
          if (isArray(v)) {
            for (i = 0; i < v.length; i++) push(k, v[i])
          } else push(k, o[k])
        }
      }
  
      // spaces should be + according to spec
      return qs.replace(/&$/, '').replace(/%20/g, '+')
    }
  
    reqwest.getcallbackPrefix = function (reqId) {
      return callbackPrefix
    }
  
    // jQuery and Zepto compatibility, differences can be remapped here so you can call
    // .ajax.compat(options, callback)
    reqwest.compat = function (o, fn) {
      if (o) {
        o.type && (o.method = o.type) && delete o.type
        o.dataType && (o.type = o.dataType)
        o.jsonpCallback && (o.jsonpCallbackName = o.jsonpCallback) && delete o.jsonpCallback
        o.jsonp && (o.jsonpCallback = o.jsonp)
      }
      return new Reqwest(o, fn)
    }
  
    return reqwest
  });
  

  provide("reqwest", module.exports);

  !function ($) {
    var r = require('reqwest')
      , integrate = function(method) {
          return function() {
            var args = Array.prototype.slice.call(arguments, 0)
              , i = (this && this.length) || 0
            while (i--) args.unshift(this[i])
            return r[method].apply(null, args)
          }
        }
      , s = integrate('serialize')
      , sa = integrate('serializeArray')
  
    $.ender({
        ajax: r
      , serialize: r.serialize
      , serializeArray: r.serializeArray
      , toQueryString: r.toQueryString
    })
  
    $.ender({
        serialize: s
      , serializeArray: sa
    }, true)
  }(ender);
  

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*!
    * treeIt: Convert your JavaScript objects to HTML, no additional hassle
    * https://github.com/alessioalex/treeIt
    * License MIT (c) Alexandru Vladutu
    */
  
  (function() {
    "use strict";
  
    /**
     * Variable declarations
     */
    var root, treeIt, originalTreeIt, types, len, i;
  
    /**
     * Initializations
     */
    root           = this;
    originalTreeIt = root.treeIt;
    treeIt         = {};
    types          = [
      'Object', 'Array', 'Arguments', 'Function', 'String', 'Number', 'Date', 'RegExp'
    ];
  
    /**
     * Assign isType method to treeIt
     *
     * @param {String}
     */
    function createIsType(typeName) {
      treeIt['is' + typeName] = function(obj) {
        return Object.prototype.toString.call(obj) == '[object ' + typeName + ']';
      };
    }
  
    /**
     * isType methods:
     * isObject, isArguments, isFunction, isString, isNumber, isDate, isRegExp.
     */
    for (i = 0, len = types.length; i < len; i++) {
      createIsType(types[i]);
    }
  
    /**
     * Returns the object's keys, using the native Object.keys function if
     * available or the polyfill from MDN:
     * https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object/keys
     *
     * Example:
     *
     *    var myObj = { name: 'John', job: 'programmer' };
     *    // this will output ['name', 'job']
     *    alert(treeIt.getKeys(myObj));
     *
     * @param {Object}
     * @return {Array}
     */
    treeIt.getKeys = Object.keys || (function () {
      var hasOwnProp, hasDontEnumBug, dontEnums, dontEnumsLength;
  
      hasOwnProp = Object.prototype.hasOwnProperty;
      hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString');
      dontEnums = [
        'toString',
        'toLocaleString',
        'valueOf',
        'hasOwnProperty',
        'isPrototypeOf',
        'propertyIsEnumerable',
        'constructor'
      ];
      dontEnumsLength = dontEnums.length;
  
      return function (obj) {
        var result, prop, i;
  
        result = [];
  
        if ((typeof obj !== 'object') && (typeof obj !== 'function') || (obj === null))  {
          throw new TypeError('Object.keys called on non-object');
        }
  
        for (prop in obj) {
          if (hasOwnProp.call(obj, prop)) { result.push(prop); }
        }
  
        if (hasDontEnumBug) {
          for (i = 0; i < dontEnumsLength; i++) {
            if (hasOwnProp.call(obj, dontEnums[i])) {
              result.push(dontEnums[i]);
            }
          }
        }
  
        return result;
      };
    }());
  
    /**
     * Check if param is an array or an object.
     *
     * Example:
     *
     *    var count = 2;
     *    treeIt.hasBranches(count) === false;
     *    var props = { a: 1, b: 2 };
     *    treeIt.hasBranches(props) === true;
     *
     * @param {Object}
     * @return {Boolean}
     */
    treeIt.hasBranches = function(obj) {
      return (
        (typeof obj !== 'undefined' && obj !== null) &&
        (treeIt.isObject(obj) || treeIt.isArray(obj))
      );
    };
  
    /**
     * Returns an array with the param's keys.
     *
     * @param {Object} obj
     * @return {Array}
     */
    treeIt.getBranches = function(obj) {
      return (treeIt.hasBranches(obj)) ? treeIt.getKeys(obj) : [];
    };
  
    /**
     * Return default templates, Mustache style
     *
     * @return {String}
     */
    treeIt.getDefaultTemplates = function() {
      return {
        root       : '<ul>{{data}}</ul>',
        branch     : '<li class="branch">{{data}}</li>',
        branchRoot : '<span class="branchRoot">{{data}}</span>',
        leaf       : '<ul><li class="leaf">{{data}}</li></ul>'
      };
    };
  
    /**
     * Basic templating function that replaces '{{data}}' from the template `tmpl`
     * with `data` and returns the updates template.
     *
     * Example:
     *
     *    var tmpl = 'Hello {{data}}!';
     *    // this will output 'Hello world!'
     *    alert(treeIt.tmpl(tmpl, 'world'));
     *
     * @param {String} tmpl
     * @param {String} data
     * @return {String}
     */
    treeIt.tmpl = function(tmpl, data) {
      if (typeof data !== "undefined" && data !== null) {
        return tmpl.replace('{{data}}', data);
      } else {
        return '';
      }
    };
  
    /**
     * Return the html rendered template for a single item (that doesn't have
     * subbranches). Strings are rendered between '', to distinguish between
     * String and Number/Boolean.
     *
     * @param {String} data
     * @return {String}
     */
    treeIt.renderSingleLeaf = function(data) {
      if (treeIt.isString(data)) { data = "'" + data + "'"; }
      return treeIt.tmpl(treeIt.templates.leaf, data);
    };
  
    /**
     * Recursively traverse the `root` object's keys && sub-keys and return the
     * html code.
     *
     * @param {Object} root
     * @return {String}
     */
    treeIt.traverse = function(root) {
      var html, branches, i, len, branchHtml;
  
      html     = '';
      branches = treeIt.getBranches(root);
  
      if (!branches.length) {
        html += treeIt.renderSingleLeaf(root);
      } else {
        for (i = 0, len = branches.length; i < len; i++) {
          branchHtml = treeIt.tmpl(treeIt.templates.branchRoot, branches[i]);
          if (treeIt.hasBranches(root[branches[i]])) {
            branchHtml += treeIt.traverse(root[branches[i]]);
          } else {
            branchHtml += treeIt.renderSingleLeaf(root[branches[i]]);
          }
  
          html += treeIt.tmpl(treeIt.templates.branch, branchHtml);
        }
        html = treeIt.tmpl(treeIt.templates.root, html);
      }
  
      return html;
    };
  
    /**
     * Generate the HTML code for the `root` Object. If the second optional param
     * `templates` is passed, that will override the default templates.
     *
     * Example:
     *
     *    var data = [{
     *      name    : 'John Doe',
     *      married : false,
     *      age     : 25,
     *      job: {
     *        title     : 'programmer',
     *        location  : 'SF',
     *        time      : '8h/day',
     *        collegues : ['Mariah', 'Johnny']
     *      },
     *      hobbies: ['swimming', 'skating']
     *    }, {
     *      name    : 'Patrick',
     *      details : 'unknown'
     *    }];
     *
     *    var html = treeIt.generate(myObject);
     *    // ---------------------------------
     *
     *    html will contain the following code:
     *
     *    <ul>
     *      <li class="branch">
     *        <span class="branchRoot">0</span>
     *        <ul>
     *          <li class="branch">
     *            <span class="branchRoot">name</span>
     *            <ul><li class="leaf">'John Doe'</li></ul>
     *          </li>
     *          <li class="branch">
     *            <span class="branchRoot">married</span>
     *            <ul><li class="leaf">false</li></ul>
     *          </li>
     *          <li class="branch">
     *            <span class="branchRoot">age</span>
     *            <ul><li class="leaf">25</li></ul>
     *          </li>
     *          <li class="branch">
     *            <span class="branchRoot">job</span>
     *            <ul>
     *              <li class="branch">
     *                <span class="branchRoot">title</span>
     *                <ul><li class="leaf">'programmer'</li></ul>
     *              </li>
     *              <li class="branch">
     *                <span class="branchRoot">location</span>
     *                <ul><li class="leaf">'SF'</li></ul>
     *              </li>
     *              <li class="branch">
     *                <span class="branchRoot">time</span>
     *                <ul><li class="leaf">'8h/day'</li></ul>
     *              </li>
     *              <li class="branch">
     *                <span class="branchRoot">collegues</span>
     *                <ul>
     *                  <li class="branch">
     *                    <span class="branchRoot">0</span>
     *                    <ul><li class="leaf">'Mariah'</li></ul>
     *                  </li>
     *                  <li class="branch">
     *                    <span class="branchRoot">1</span>
     *                    <ul><li class="leaf">'Johnny'</li></ul>
     *                  </li>
     *                </ul>
     *              </li>
     *            </ul>
     *          </li>
     *          <li class="branch">
     *            <span class="branchRoot">hobbies</span>
     *            <ul>
     *              <li class="branch">
     *                <span class="branchRoot">0</span>
     *                <ul><li class="leaf">'swimming'</li></ul>
     *              </li>
     *              <li class="branch">
     *                <span class="branchRoot">1</span>
     *                <ul><li class="leaf">'skating'</li></ul>
     *              </li>
     *            </ul>
     *          </li>
     *        </ul>
     *      </li>
     *      <li class="branch">
     *        <span class="branchRoot">1</span>
     *        <ul>
     *          <li class="branch">
     *            <span class="branchRoot">name</span>
     *            <ul><li class="leaf">'Patrick'</li></ul>
     *          </li>
     *          <li class="branch">
     *            <span class="branchRoot">details</span>
     *            <ul><li class="leaf">'unknown'</li></ul>
     *          </li>
     *        </ul>
     *      </li>
     *    </ul>
     *
     * @param {Object} root
     * @param {Object} templates
     * @return {String}
     */
    treeIt.generate = function(root, templates) {
      var tmplKeys, i, len, res;
  
      treeIt.templates = treeIt.getDefaultTemplates();
  
      if (treeIt.isObject(templates)) {
        tmplKeys = treeIt.getBranches(templates);
        for (i = 0, len = tmplKeys.length; i < len; i++) {
          treeIt.templates[tmplKeys[i]] = templates[tmplKeys[i]];
        }
      }
  
      return treeIt.traverse(root);
    };
  
    /**
     * Run treeIt in *noConflict* mode, returning the `treeIt` variable to its
     * previous owner. Returns a reference to the treeIt object.
     */
    treeIt.noConflict = function() {
      root.treeIt = originalTreeIt;
      return treeIt;
    };
  
    /**
     * Check if it's a Node.js module and export it, else
     * assign treeIt object to the global context
     */
    if (typeof module !== 'undefined' && module.exports) {
      exports = module.exports = treeIt;
    } else {
      root.treeIt = treeIt;
    }
  }).call(this);
  

  provide("treeit", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*!
   * Copyright (c) 2010 Chris O'Hara <cohara87@gmail.com>
   *
   * Permission is hereby granted, free of charge, to any person obtaining
   * a copy of this software and associated documentation files (the
   * "Software"), to deal in the Software without restriction, including
   * without limitation the rights to use, copy, modify, merge, publish,
   * distribute, sublicense, and/or sell copies of the Software, and to
   * permit persons to whom the Software is furnished to do so, subject to
   * the following conditions:
   *
   * The above copyright notice and this permission notice shall be
   * included in all copies or substantial portions of the Software.
   *
   * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
   * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
   * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
   * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
  
  (function(exports) {
  
      var entities = {
          '&nbsp;': '\u00a0',
          '&iexcl;': '\u00a1',
          '&cent;': '\u00a2',
          '&pound;': '\u00a3',
          '&curren;': '\u20ac',
          '&yen;': '\u00a5',
          '&brvbar;': '\u0160',
          '&sect;': '\u00a7',
          '&uml;': '\u0161',
          '&copy;': '\u00a9',
          '&ordf;': '\u00aa',
          '&laquo;': '\u00ab',
          '&not;': '\u00ac',
          '&shy;': '\u00ad',
          '&reg;': '\u00ae',
          '&macr;': '\u00af',
          '&deg;': '\u00b0',
          '&plusmn;': '\u00b1',
          '&sup2;': '\u00b2',
          '&sup3;': '\u00b3',
          '&acute;': '\u017d',
          '&micro;': '\u00b5',
          '&para;': '\u00b6',
          '&middot;': '\u00b7',
          '&cedil;': '\u017e',
          '&sup1;': '\u00b9',
          '&ordm;': '\u00ba',
          '&raquo;': '\u00bb',
          '&frac14;': '\u0152',
          '&frac12;': '\u0153',
          '&frac34;': '\u0178',
          '&iquest;': '\u00bf',
          '&Agrave;': '\u00c0',
          '&Aacute;': '\u00c1',
          '&Acirc;': '\u00c2',
          '&Atilde;': '\u00c3',
          '&Auml;': '\u00c4',
          '&Aring;': '\u00c5',
          '&AElig;': '\u00c6',
          '&Ccedil;': '\u00c7',
          '&Egrave;': '\u00c8',
          '&Eacute;': '\u00c9',
          '&Ecirc;': '\u00ca',
          '&Euml;': '\u00cb',
          '&Igrave;': '\u00cc',
          '&Iacute;': '\u00cd',
          '&Icirc;': '\u00ce',
          '&Iuml;': '\u00cf',
          '&ETH;': '\u00d0',
          '&Ntilde;': '\u00d1',
          '&Ograve;': '\u00d2',
          '&Oacute;': '\u00d3',
          '&Ocirc;': '\u00d4',
          '&Otilde;': '\u00d5',
          '&Ouml;': '\u00d6',
          '&times;': '\u00d7',
          '&Oslash;': '\u00d8',
          '&Ugrave;': '\u00d9',
          '&Uacute;': '\u00da',
          '&Ucirc;': '\u00db',
          '&Uuml;': '\u00dc',
          '&Yacute;': '\u00dd',
          '&THORN;': '\u00de',
          '&szlig;': '\u00df',
          '&agrave;': '\u00e0',
          '&aacute;': '\u00e1',
          '&acirc;': '\u00e2',
          '&atilde;': '\u00e3',
          '&auml;': '\u00e4',
          '&aring;': '\u00e5',
          '&aelig;': '\u00e6',
          '&ccedil;': '\u00e7',
          '&egrave;': '\u00e8',
          '&eacute;': '\u00e9',
          '&ecirc;': '\u00ea',
          '&euml;': '\u00eb',
          '&igrave;': '\u00ec',
          '&iacute;': '\u00ed',
          '&icirc;': '\u00ee',
          '&iuml;': '\u00ef',
          '&eth;': '\u00f0',
          '&ntilde;': '\u00f1',
          '&ograve;': '\u00f2',
          '&oacute;': '\u00f3',
          '&ocirc;': '\u00f4',
          '&otilde;': '\u00f5',
          '&ouml;': '\u00f6',
          '&divide;': '\u00f7',
          '&oslash;': '\u00f8',
          '&ugrave;': '\u00f9',
          '&uacute;': '\u00fa',
          '&ucirc;': '\u00fb',
          '&uuml;': '\u00fc',
          '&yacute;': '\u00fd',
          '&thorn;': '\u00fe',
          '&yuml;': '\u00ff',
          '&quot;': '\u0022',
          '&lt;': '\u003c',
          '&gt;': '\u003e',
          '&apos;': '\u0027',
          '&minus;': '\u2212',
          '&circ;': '\u02c6',
          '&tilde;': '\u02dc',
          '&Scaron;': '\u0160',
          '&lsaquo;': '\u2039',
          '&OElig;': '\u0152',
          '&lsquo;': '\u2018',
          '&rsquo;': '\u2019',
          '&ldquo;': '\u201c',
          '&rdquo;': '\u201d',
          '&bull;': '\u2022',
          '&ndash;': '\u2013',
          '&mdash;': '\u2014',
          '&trade;': '\u2122',
          '&scaron;': '\u0161',
          '&rsaquo;': '\u203a',
          '&oelig;': '\u0153',
          '&Yuml;': '\u0178',
          '&fnof;': '\u0192',
          '&Alpha;': '\u0391',
          '&Beta;': '\u0392',
          '&Gamma;': '\u0393',
          '&Delta;': '\u0394',
          '&Epsilon;': '\u0395',
          '&Zeta;': '\u0396',
          '&Eta;': '\u0397',
          '&Theta;': '\u0398',
          '&Iota;': '\u0399',
          '&Kappa;': '\u039a',
          '&Lambda;': '\u039b',
          '&Mu;': '\u039c',
          '&Nu;': '\u039d',
          '&Xi;': '\u039e',
          '&Omicron;': '\u039f',
          '&Pi;': '\u03a0',
          '&Rho;': '\u03a1',
          '&Sigma;': '\u03a3',
          '&Tau;': '\u03a4',
          '&Upsilon;': '\u03a5',
          '&Phi;': '\u03a6',
          '&Chi;': '\u03a7',
          '&Psi;': '\u03a8',
          '&Omega;': '\u03a9',
          '&alpha;': '\u03b1',
          '&beta;': '\u03b2',
          '&gamma;': '\u03b3',
          '&delta;': '\u03b4',
          '&epsilon;': '\u03b5',
          '&zeta;': '\u03b6',
          '&eta;': '\u03b7',
          '&theta;': '\u03b8',
          '&iota;': '\u03b9',
          '&kappa;': '\u03ba',
          '&lambda;': '\u03bb',
          '&mu;': '\u03bc',
          '&nu;': '\u03bd',
          '&xi;': '\u03be',
          '&omicron;': '\u03bf',
          '&pi;': '\u03c0',
          '&rho;': '\u03c1',
          '&sigmaf;': '\u03c2',
          '&sigma;': '\u03c3',
          '&tau;': '\u03c4',
          '&upsilon;': '\u03c5',
          '&phi;': '\u03c6',
          '&chi;': '\u03c7',
          '&psi;': '\u03c8',
          '&omega;': '\u03c9',
          '&thetasym;': '\u03d1',
          '&upsih;': '\u03d2',
          '&piv;': '\u03d6',
          '&ensp;': '\u2002',
          '&emsp;': '\u2003',
          '&thinsp;': '\u2009',
          '&zwnj;': '\u200c',
          '&zwj;': '\u200d',
          '&lrm;': '\u200e',
          '&rlm;': '\u200f',
          '&sbquo;': '\u201a',
          '&bdquo;': '\u201e',
          '&dagger;': '\u2020',
          '&Dagger;': '\u2021',
          '&hellip;': '\u2026',
          '&permil;': '\u2030',
          '&prime;': '\u2032',
          '&Prime;': '\u2033',
          '&oline;': '\u203e',
          '&frasl;': '\u2044',
          '&euro;': '\u20ac',
          '&image;': '\u2111',
          '&weierp;': '\u2118',
          '&real;': '\u211c',
          '&alefsym;': '\u2135',
          '&larr;': '\u2190',
          '&uarr;': '\u2191',
          '&rarr;': '\u2192',
          '&darr;': '\u2193',
          '&harr;': '\u2194',
          '&crarr;': '\u21b5',
          '&lArr;': '\u21d0',
          '&uArr;': '\u21d1',
          '&rArr;': '\u21d2',
          '&dArr;': '\u21d3',
          '&hArr;': '\u21d4',
          '&forall;': '\u2200',
          '&part;': '\u2202',
          '&exist;': '\u2203',
          '&empty;': '\u2205',
          '&nabla;': '\u2207',
          '&isin;': '\u2208',
          '&notin;': '\u2209',
          '&ni;': '\u220b',
          '&prod;': '\u220f',
          '&sum;': '\u2211',
          '&lowast;': '\u2217',
          '&radic;': '\u221a',
          '&prop;': '\u221d',
          '&infin;': '\u221e',
          '&ang;': '\u2220',
          '&and;': '\u2227',
          '&or;': '\u2228',
          '&cap;': '\u2229',
          '&cup;': '\u222a',
          '&int;': '\u222b',
          '&there4;': '\u2234',
          '&sim;': '\u223c',
          '&cong;': '\u2245',
          '&asymp;': '\u2248',
          '&ne;': '\u2260',
          '&equiv;': '\u2261',
          '&le;': '\u2264',
          '&ge;': '\u2265',
          '&sub;': '\u2282',
          '&sup;': '\u2283',
          '&nsub;': '\u2284',
          '&sube;': '\u2286',
          '&supe;': '\u2287',
          '&oplus;': '\u2295',
          '&otimes;': '\u2297',
          '&perp;': '\u22a5',
          '&sdot;': '\u22c5',
          '&lceil;': '\u2308',
          '&rceil;': '\u2309',
          '&lfloor;': '\u230a',
          '&rfloor;': '\u230b',
          '&lang;': '\u2329',
          '&rang;': '\u232a',
          '&loz;': '\u25ca',
          '&spades;': '\u2660',
          '&clubs;': '\u2663',
          '&hearts;': '\u2665',
          '&diams;': '\u2666'
      };
  
      var decode = function (str) {
          if (!~str.indexOf('&')) return str;
  
          //Decode literal entities
          for (var i in entities) {
              str = str.replace(new RegExp(i, 'g'), entities[i]);
          }
  
          //Decode hex entities
          str = str.replace(/&#x(0*[0-9a-f]{2,5});?/gi, function (m, code) {
              return String.fromCharCode(parseInt(+code, 16));
          });
  
          //Decode numeric entities
          str = str.replace(/&#([0-9]{2,4});?/gi, function (m, code) {
              return String.fromCharCode(+code);
          });
  
          str = str.replace(/&amp;/g, '&');
  
          return str;
      }
  
      var encode = function (str) {
          str = str.replace(/&/g, '&amp;');
  
          //IE doesn't accept &apos;
          str = str.replace(/'/g, '&#39;');
  
          //Encode literal entities
          for (var i in entities) {
              str = str.replace(new RegExp(entities[i], 'g'), i);
          }
  
          return str;
      }
  
      exports.entities = {
          encode: encode,
          decode: decode
      }
  
      //This module is adapted from the CodeIgniter framework
      //The license is available at http://codeigniter.com/
  
      var never_allowed_str = {
          'document.cookie':              '[removed]',
          'document.write':               '[removed]',
          '.parentNode':                  '[removed]',
          '.innerHTML':                   '[removed]',
          'window.location':              '[removed]',
          '-moz-binding':                 '[removed]',
          '<!--':                         '&lt;!--',
          '-->':                          '--&gt;',
          '<![CDATA[':                    '&lt;![CDATA['
      };
  
      var never_allowed_regex = {
          'javascript\\s*:':              '[removed]',
          'expression\\s*(\\(|&\\#40;)':  '[removed]',
          'vbscript\\s*:':                '[removed]',
          'Redirect\\s+302':              '[removed]'
      };
  
      var non_displayables = [
          /%0[0-8bcef]/g,           // url encoded 00-08, 11, 12, 14, 15
          /%1[0-9a-f]/g,            // url encoded 16-31
          /[\x00-\x08]/g,           // 00-08
          /\x0b/g, /\x0c/g,         // 11,12
          /[\x0e-\x1f]/g            // 14-31
      ];
  
      var compact_words = [
          'javascript', 'expression', 'vbscript',
          'script', 'applet', 'alert', 'document',
          'write', 'cookie', 'window'
      ];
  
      exports.xssClean = function(str, is_image) {
  
          //Recursively clean objects and arrays
          if (typeof str === 'object') {
              for (var i in str) {
                  str[i] = exports.xssClean(str[i]);
              }
              return str;
          }
  
          //Remove invisible characters
          str = remove_invisible_characters(str);
  
          //Protect query string variables in URLs => 901119URL5918AMP18930PROTECT8198
          str = str.replace(/\&([a-z\_0-9]+)\=([a-z\_0-9]+)/i, xss_hash() + '$1=$2');
  
          //Validate standard character entities - add a semicolon if missing.  We do this to enable
          //the conversion of entities to ASCII later.
          str = str.replace(/(&\#?[0-9a-z]{2,})([\x00-\x20])*;?/i, '$1;$2');
  
          //Validate UTF16 two byte encoding (x00) - just as above, adds a semicolon if missing.
          str = str.replace(/(&\#x?)([0-9A-F]+);?/i, '$1;$2');
  
          //Un-protect query string variables
          str = str.replace(xss_hash(), '&');
  
          //Decode just in case stuff like this is submitted:
          //<a href="http://%77%77%77%2E%67%6F%6F%67%6C%65%2E%63%6F%6D">Google</a>
          try {
            str = decodeURIComponent(str);
          } catch (e) {
            // str was not actually URI-encoded
          }
  
          //Convert character entities to ASCII - this permits our tests below to work reliably.
          //We only convert entities that are within tags since these are the ones that will pose security problems.
          str = str.replace(/[a-z]+=([\'\"]).*?\1/gi, function(m, match) {
              return m.replace(match, convert_attribute(match));
          });
  
          //Remove invisible characters again
          str = remove_invisible_characters(str);
  
          //Convert tabs to spaces
          str = str.replace('\t', ' ');
  
          //Captured the converted string for later comparison
          var converted_string = str;
  
          //Remove strings that are never allowed
          for (var i in never_allowed_str) {
              str = str.replace(i, never_allowed_str[i]);
          }
  
          //Remove regex patterns that are never allowed
          for (var i in never_allowed_regex) {
              str = str.replace(new RegExp(i, 'i'), never_allowed_regex[i]);
          }
  
          //Compact any exploded words like:  j a v a s c r i p t
          // We only want to do this when it is followed by a non-word character
          for (var i in compact_words) {
              var spacified = compact_words[i].split('').join('\\s*')+'\\s*';
  
              str = str.replace(new RegExp('('+spacified+')(\\W)', 'ig'), function(m, compat, after) {
                  return compat.replace(/\s+/g, '') + after;
              });
          }
  
          //Remove disallowed Javascript in links or img tags
          do {
              var original = str;
  
              if (str.match(/<a/i)) {
                  str = str.replace(/<a\s+([^>]*?)(>|$)/gi, function(m, attributes, end_tag) {
                      attributes = filter_attributes(attributes.replace('<','').replace('>',''));
                      return m.replace(attributes, attributes.replace(/href=.*?(alert\(|alert&\#40;|javascript\:|charset\=|window\.|document\.|\.cookie|<script|<xss|base64\s*,)/gi, ''));
                  });
              }
  
              if (str.match(/<img/i)) {
                  str = str.replace(/<img\s+([^>]*?)(\s?\/?>|$)/gi, function(m, attributes, end_tag) {
                      attributes = filter_attributes(attributes.replace('<','').replace('>',''));
                      return m.replace(attributes, attributes.replace(/src=.*?(alert\(|alert&\#40;|javascript\:|charset\=|window\.|document\.|\.cookie|<script|<xss|base64\s*,)/gi, ''));
                  });
              }
  
              if (str.match(/script/i) || str.match(/xss/i)) {
                  str = str.replace(/<(\/*)(script|xss)(.*?)\>/gi, '[removed]');
              }
  
          } while(original != str);
  
          //Remove JavaScript Event Handlers - Note: This code is a little blunt.  It removes the event
          //handler and anything up to the closing >, but it's unlikely to be a problem.
          event_handlers = ['[^a-z_\-]on\\w*'];
  
          //Adobe Photoshop puts XML metadata into JFIF images, including namespacing,
          //so we have to allow this for images
          if (!is_image) {
              event_handlers.push('xmlns');
          }
  
          str = str.replace(new RegExp("<([^><]+?)("+event_handlers.join('|')+")(\\s*=\\s*[^><]*)([><]*)", 'i'), '<$1$4');
  
          //Sanitize naughty HTML elements
          //If a tag containing any of the words in the list
          //below is found, the tag gets converted to entities.
          //So this: <blink>
          //Becomes: &lt;blink&gt;
          naughty = 'alert|applet|audio|basefont|base|behavior|bgsound|blink|body|embed|expression|form|frameset|frame|head|html|ilayer|iframe|input|isindex|layer|link|meta|object|plaintext|style|script|textarea|title|video|xml|xss';
          str = str.replace(new RegExp('<(/*\\s*)('+naughty+')([^><]*)([><]*)', 'gi'), function(m, a, b, c, d) {
              return '&lt;' + a + b + c + d.replace('>','&gt;').replace('<','&lt;');
          });
  
          //Sanitize naughty scripting elements Similar to above, only instead of looking for
          //tags it looks for PHP and JavaScript commands that are disallowed.  Rather than removing the
          //code, it simply converts the parenthesis to entities rendering the code un-executable.
          //For example:    eval('some code')
          //Becomes:        eval&#40;'some code'&#41;
          str = str.replace(/(alert|cmd|passthru|eval|exec|expression|system|fopen|fsockopen|file|file_get_contents|readfile|unlink)(\s*)\((.*?)\)/gi, '$1$2&#40;$3&#41;');
  
          //This adds a bit of extra precaution in case something got through the above filters
          for (var i in never_allowed_str) {
              str = str.replace(i, never_allowed_str[i]);
          }
          for (var i in never_allowed_regex) {
              str = str.replace(new RegExp(i, 'i'), never_allowed_regex[i]);
          }
  
          //Images are handled in a special way
          if (is_image && str !== converted_string) {
              throw new Error('Image may contain XSS');
          }
  
          return str;
      }
  
      function remove_invisible_characters(str) {
          for (var i in non_displayables) {
              str = str.replace(non_displayables[i], '');
          }
          return str;
      }
  
      function xss_hash() {
          //TODO: Create a random hash
          return '!*$^#(@*#&';
      }
  
      function convert_attribute(str) {
          return str.replace('>','&gt;').replace('<','&lt;').replace('\\','\\\\');
      }
  
      //Filter Attributes - filters tag attributes for consistency and safety
      function filter_attributes(str) {
          out = '';
  
          str.replace(/\s*[a-z\-]+\s*=\s*(?:\042|\047)(?:[^\1]*?)\1/gi, function(m) {
              out += m.replace(/\/\*.*?\*\//g, '');
          });
  
          return out;
      }
  
      var Validator = exports.Validator = function() {}
  
      Validator.prototype.check = function(str, fail_msg) {
          this.str = str == null || (isNaN(str) && str.length == undefined) ? '' : str+'';
          this.msg = fail_msg;
          this._errors = [];
          return this;
      }
  
      //Create some aliases - may help code readability
      Validator.prototype.validate = Validator.prototype.check;
      Validator.prototype.assert = Validator.prototype.check;
  
      Validator.prototype.error = function(msg) {
          throw new Error(msg);
      }
  
      Validator.prototype.isEmail = function() {
          if (!this.str.match(/^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/)) {
              return this.error(this.msg || 'Invalid email');
          }
          return this;
      }
  
      //Will work against Visa, MasterCard, American Express, Discover, Diners Club, and JCB card numbering formats
      Validator.prototype.isCreditCard = function() {
          this.str = this.str.replace(/[^0-9]+/g, ''); //remove all dashes, spaces, etc.
          if (!this.str.match(/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/)) {
              return this.error(this.msg || 'Invalid credit card');
          }
          return this;
      }
  
      Validator.prototype.isUrl = function() {
          if (!this.str.match(/^(?:(?:ht|f)tp(?:s?)\:\/\/|~\/|\/)?(?:\w+:\w+@)?((?:(?:[-\w\d{1-3}]+\.)+(?:com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|edu|co\.uk|ac\.uk|it|fr|tv|museum|asia|local|travel|[a-z]{2}))|((\b25[0-5]\b|\b[2][0-4][0-9]\b|\b[0-1]?[0-9]?[0-9]\b)(\.(\b25[0-5]\b|\b[2][0-4][0-9]\b|\b[0-1]?[0-9]?[0-9]\b)){3}))(?::[\d]{1,5})?(?:(?:(?:\/(?:[-\w~!$+|.,=]|%[a-f\d]{2})+)+|\/)+|\?|#)?(?:(?:\?(?:[-\w~!$+|.,*:]|%[a-f\d{2}])+=?(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)(?:&(?:[-\w~!$+|.,*:]|%[a-f\d{2}])+=?(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)*)*(?:#(?:[-\w~!$ |\/.,*:;=]|%[a-f\d]{2})*)?$/i) || this.str.length > 2083) {
              return this.error(this.msg || 'Invalid URL');
          }
          return this;
      }
  
      Validator.prototype.isIP = function() {
          if (!this.str.match(/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/)) {
              return this.error(this.msg || 'Invalid IP');
          }
          return this;
      }
  
      Validator.prototype.isAlpha = function() {
          if (!this.str.match(/^[a-zA-Z]+$/)) {
              return this.error(this.msg || 'Invalid characters');
          }
          return this;
      }
  
      Validator.prototype.isAlphanumeric = function() {
          if (!this.str.match(/^[a-zA-Z0-9]+$/)) {
              return this.error(this.msg || 'Invalid characters');
          }
          return this;
      }
  
      Validator.prototype.isNumeric = function() {
          if (!this.str.match(/^-?[0-9]+$/)) {
              return this.error(this.msg || 'Invalid number');
          }
          return this;
      }
  
      Validator.prototype.isLowercase = function() {
          if (!this.str.match(/^[a-z0-9]+$/)) {
              return this.error(this.msg || 'Invalid characters');
          }
          return this;
      }
  
      Validator.prototype.isUppercase = function() {
          if (!this.str.match(/^[A-Z0-9]+$/)) {
              return this.error(this.msg || 'Invalid characters');
          }
          return this;
      }
  
      Validator.prototype.isInt = function() {
          if (!this.str.match(/^(?:-?(?:0|[1-9][0-9]*))$/)) {
              return this.error(this.msg || 'Invalid integer');
          }
          return this;
      }
  
      Validator.prototype.isDecimal = function() {
          if (!this.str.match(/^(?:-?(?:0|[1-9][0-9]*))?(?:\.[0-9]*)?$/)) {
              return this.error(this.msg || 'Invalid decimal');
          }
          return this;
      }
  
      Validator.prototype.isFloat = function() {
          return this.isDecimal();
      }
  
      Validator.prototype.notNull = function() {
          if (this.str === '') {
              return this.error(this.msg || 'Invalid characters');
          }
          return this;
      }
  
      Validator.prototype.isNull = function() {
          if (this.str !== '') {
              return this.error(this.msg || 'Invalid characters');
          }
          return this;
      }
  
      Validator.prototype.notEmpty = function() {
          if (this.str.match(/^[\s\t\r\n]*$/)) {
              return this.error(this.msg || 'String is whitespace');
          }
          return this;
      }
  
      Validator.prototype.equals = function(equals) {
          if (this.str != equals) {
              return this.error(this.msg || 'Not equal');
          }
          return this;
      }
  
      Validator.prototype.contains = function(str) {
          if (this.str.indexOf(str) === -1) {
              return this.error(this.msg || 'Invalid characters');
          }
          return this;
      }
  
      Validator.prototype.notContains = function(str) {
          if (this.str.indexOf(str) >= 0) {
              return this.error(this.msg || 'Invalid characters');
          }
          return this;
      }
  
      Validator.prototype.regex = Validator.prototype.is = function(pattern, modifiers) {
          if (typeof pattern !== 'function') {
              pattern = new RegExp(pattern, modifiers);
          }
          if (! this.str.match(pattern)) {
              return this.error(this.msg || 'Invalid characters');
          }
          return this;
      }
  
      Validator.prototype.notRegex = Validator.prototype.not = function(pattern, modifiers) {
          if (typeof pattern !== 'function') {
              pattern = new RegExp(pattern, modifiers);
          }
          if (this.str.match(pattern)) {
              this.error(this.msg || 'Invalid characters');
          }
          return this;
      }
  
      Validator.prototype.len = function(min, max) {
          if (this.str.length < min) {
              this.error(this.msg || 'String is too small');
          }
          if (typeof max !== undefined && this.str.length > max) {
              return this.error(this.msg || 'String is too large');
          }
          return this;
      }
  
      //Thanks to github.com/sreuter for the idea.
      Validator.prototype.isUUID = function(version) {
          if (version == 3 || version == 'v3') {
              pattern = /[0-9A-F]{8}-[0-9A-F]{4}-3[0-9A-F]{3}-[0-9A-F]{4}-[0-9A-F]{12}$/i;
          } else if (version == 4 || version == 'v4') {
              pattern = /[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i;
          } else {
              pattern = /[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i;
          }
          if (!this.str.match(pattern)) {
              return this.error(this.msg || 'Not a UUID');
          }
          return this;
      }
  
      Validator.prototype.isDate = function() {
          var intDate = Date.parse(this.str);
          if (isNaN(intDate)) { 
              return this.error(this.msg || 'Not a date'); 
          }
          return this;
      }
  
      Validator.prototype.isIn = function(options) {
          if (options && typeof options.indexOf === 'function') {
              if (!~options.indexOf(this.str)) {
                  return this.error(this.msg || 'Unexpected value');
              }
              return this;
          } else {
              return this.error(this.msg || 'Invalid in() argument');
          }
      }
  
      Validator.prototype.notIn = function(options) {
          if (options && typeof options.indexOf === 'function') {
              if (options.indexOf(this.str) !== -1) {
                  return this.error(this.msg || 'Unexpected value');
              }
              return this;
          } else {
              return this.error(this.msg || 'Invalid notIn() argument');
          }
      }
  
      Validator.prototype.min = function(val) {
          var number = parseFloat(this.str);
  
          if (!isNaN(number) && number < val) {
              return this.error(this.msg || 'Invalid number');
          }
  
          return this;
      }
  
      Validator.prototype.max = function(val) {
          var number = parseFloat(this.str);
          if (!isNaN(number) && number > val) {
              return this.error(this.msg || 'Invalid number');
          }
          return this;
      }
  
      Validator.prototype.isArray = function() {
          if (!Array.isArray(this.str)) {
              return this.error(this.msg || 'Not an array');
          }
          return this;
      }
  
      var Filter = exports.Filter = function() {}
  
      var whitespace = '\\r\\n\\t\\s';
  
      Filter.prototype.modify = function(str) {
          this.str = str;
      }
  
      //Create some aliases - may help code readability
      Filter.prototype.convert = Filter.prototype.sanitize = function(str) {
          this.str = str;
          return this;
      }
  
      Filter.prototype.xss = function(is_image) {
          this.modify(exports.xssClean(this.str, is_image));
          return this.str;
      }
  
      Filter.prototype.entityDecode = function() {
          this.modify(decode(this.str));
          return this.str;
      }
  
      Filter.prototype.entityEncode = function() {
          this.modify(encode(this.str));
          return this.str;
      }
  
      Filter.prototype.ltrim = function(chars) {
          chars = chars || whitespace;
          this.modify(this.str.replace(new RegExp('^['+chars+']+', 'g'), ''));
          return this.str;
      }
  
      Filter.prototype.rtrim = function(chars) {
          chars = chars || whitespace;
          this.modify(this.str.replace(new RegExp('['+chars+']+$', 'g'), ''));
          return this.str;
      }
  
      Filter.prototype.trim = function(chars) {
          chars = chars || whitespace;
          this.modify(this.str.replace(new RegExp('^['+chars+']+|['+chars+']+$', 'g'), ''));
          return this.str;
      }
  
      Filter.prototype.ifNull = function(replace) {
          if (!this.str || this.str === '') {
              this.modify(replace);
          }
          return this.str;
      }
  
      Filter.prototype.toFloat = function() {
          this.modify(parseFloat(this.str));
          return this.str;
      }
  
      Filter.prototype.toInt = function(radix) {
          radix = radix || 10;
          this.modify(parseInt(this.str), radix);
          return this.str;
      }
  
      //Any strings with length > 0 (except for '0' and 'false') are considered true,
      //all other strings are false
      Filter.prototype.toBoolean = function() {
          if (!this.str || this.str == '0' || this.str == 'false' || this.str == '') {
              this.modify(false);
          } else {
              this.modify(true);
          }
          return this.str;
      }
  
      //String must be equal to '1' or 'true' to be considered true, all other strings
      //are false
      Filter.prototype.toBooleanStrict = function() {
          if (this.str == '1' || this.str == 'true') {
              this.modify(true);
          } else {
              this.modify(false);
          }
          return this.str;
      }
  
      //Quick access methods
      exports.sanitize = exports.convert = function(str) {
          var filter = new exports.Filter();
          return filter.sanitize(str);
      }
  
      exports.check = exports.validate = exports.assert = function(str, fail_msg) {
          var validator = new exports.Validator();
          return validator.check(str, fail_msg);
      }
  
  })(typeof(exports) === 'undefined' ? window : exports);
  
  

  provide("ender-validator", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*!
    * domready (c) Dustin Diaz 2012 - License MIT
    */
  !function (name, definition) {
    if (typeof module != 'undefined') module.exports = definition()
    else if (typeof define == 'function' && typeof define.amd == 'object') define(definition)
    else this[name] = definition()
  }('domready', function (ready) {
  
    var fns = [], fn, f = false
      , doc = document
      , testEl = doc.documentElement
      , hack = testEl.doScroll
      , domContentLoaded = 'DOMContentLoaded'
      , addEventListener = 'addEventListener'
      , onreadystatechange = 'onreadystatechange'
      , readyState = 'readyState'
      , loaded = /^loade|c/.test(doc[readyState])
  
    function flush(f) {
      loaded = 1
      while (f = fns.shift()) f()
    }
  
    doc[addEventListener] && doc[addEventListener](domContentLoaded, fn = function () {
      doc.removeEventListener(domContentLoaded, fn, f)
      flush()
    }, f)
  
  
    hack && doc.attachEvent(onreadystatechange, fn = function () {
      if (/^c/.test(doc[readyState])) {
        doc.detachEvent(onreadystatechange, fn)
        flush()
      }
    })
  
    return (ready = hack ?
      function (fn) {
        self != top ?
          loaded ? fn() : fns.push(fn) :
          function () {
            try {
              testEl.doScroll('left')
            } catch (e) {
              return setTimeout(function() { ready(fn) }, 50)
            }
            fn()
          }()
      } :
      function (fn) {
        loaded ? fn() : fns.push(fn)
      })
  })

  provide("domready", module.exports);

  !function ($) {
    var ready = require('domready')
    $.ender({domReady: ready})
    $.ender({
      ready: function (f) {
        ready(f)
        return this
      }
    }, true)
  }(ender);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*!
    * @preserve Qwery - A Blazing Fast query selector engine
    * https://github.com/ded/qwery
    * copyright Dustin Diaz 2012
    * MIT License
    */
  
  (function (name, context, definition) {
    if (typeof module != 'undefined' && module.exports) module.exports = definition()
    else if (typeof context['define'] == 'function' && context['define']['amd']) define(definition)
    else context[name] = definition()
  })('qwery', this, function () {
    var doc = document
      , html = doc.documentElement
      , byClass = 'getElementsByClassName'
      , byTag = 'getElementsByTagName'
      , qSA = 'querySelectorAll'
      , useNativeQSA = 'useNativeQSA'
      , tagName = 'tagName'
      , nodeType = 'nodeType'
      , select // main select() method, assign later
  
      , id = /#([\w\-]+)/
      , clas = /\.[\w\-]+/g
      , idOnly = /^#([\w\-]+)$/
      , classOnly = /^\.([\w\-]+)$/
      , tagOnly = /^([\w\-]+)$/
      , tagAndOrClass = /^([\w]+)?\.([\w\-]+)$/
      , splittable = /(^|,)\s*[>~+]/
      , normalizr = /^\s+|\s*([,\s\+\~>]|$)\s*/g
      , splitters = /[\s\>\+\~]/
      , splittersMore = /(?![\s\w\-\/\?\&\=\:\.\(\)\!,@#%<>\{\}\$\*\^'"]*\]|[\s\w\+\-]*\))/
      , specialChars = /([.*+?\^=!:${}()|\[\]\/\\])/g
      , simple = /^(\*|[a-z0-9]+)?(?:([\.\#]+[\w\-\.#]+)?)/
      , attr = /\[([\w\-]+)(?:([\|\^\$\*\~]?\=)['"]?([ \w\-\/\?\&\=\:\.\(\)\!,@#%<>\{\}\$\*\^]+)["']?)?\]/
      , pseudo = /:([\w\-]+)(\(['"]?([^()]+)['"]?\))?/
      , easy = new RegExp(idOnly.source + '|' + tagOnly.source + '|' + classOnly.source)
      , dividers = new RegExp('(' + splitters.source + ')' + splittersMore.source, 'g')
      , tokenizr = new RegExp(splitters.source + splittersMore.source)
      , chunker = new RegExp(simple.source + '(' + attr.source + ')?' + '(' + pseudo.source + ')?')
  
    var walker = {
        ' ': function (node) {
          return node && node !== html && node.parentNode
        }
      , '>': function (node, contestant) {
          return node && node.parentNode == contestant.parentNode && node.parentNode
        }
      , '~': function (node) {
          return node && node.previousSibling
        }
      , '+': function (node, contestant, p1, p2) {
          if (!node) return false
          return (p1 = previous(node)) && (p2 = previous(contestant)) && p1 == p2 && p1
        }
      }
  
    function cache() {
      this.c = {}
    }
    cache.prototype = {
      g: function (k) {
        return this.c[k] || undefined
      }
    , s: function (k, v, r) {
        v = r ? new RegExp(v) : v
        return (this.c[k] = v)
      }
    }
  
    var classCache = new cache()
      , cleanCache = new cache()
      , attrCache = new cache()
      , tokenCache = new cache()
  
    function classRegex(c) {
      return classCache.g(c) || classCache.s(c, '(^|\\s+)' + c + '(\\s+|$)', 1)
    }
  
    // not quite as fast as inline loops in older browsers so don't use liberally
    function each(a, fn) {
      var i = 0, l = a.length
      for (; i < l; i++) fn(a[i])
    }
  
    function flatten(ar) {
      for (var r = [], i = 0, l = ar.length; i < l; ++i) arrayLike(ar[i]) ? (r = r.concat(ar[i])) : (r[r.length] = ar[i])
      return r
    }
  
    function arrayify(ar) {
      var i = 0, l = ar.length, r = []
      for (; i < l; i++) r[i] = ar[i]
      return r
    }
  
    function previous(n) {
      while (n = n.previousSibling) if (n[nodeType] == 1) break;
      return n
    }
  
    function q(query) {
      return query.match(chunker)
    }
  
    // called using `this` as element and arguments from regex group results.
    // given => div.hello[title="world"]:foo('bar')
    // div.hello[title="world"]:foo('bar'), div, .hello, [title="world"], title, =, world, :foo('bar'), foo, ('bar'), bar]
    function interpret(whole, tag, idsAndClasses, wholeAttribute, attribute, qualifier, value, wholePseudo, pseudo, wholePseudoVal, pseudoVal) {
      var i, m, k, o, classes
      if (this[nodeType] !== 1) return false
      if (tag && tag !== '*' && this[tagName] && this[tagName].toLowerCase() !== tag) return false
      if (idsAndClasses && (m = idsAndClasses.match(id)) && m[1] !== this.id) return false
      if (idsAndClasses && (classes = idsAndClasses.match(clas))) {
        for (i = classes.length; i--;) if (!classRegex(classes[i].slice(1)).test(this.className)) return false
      }
      if (pseudo && qwery.pseudos[pseudo] && !qwery.pseudos[pseudo](this, pseudoVal)) return false
      if (wholeAttribute && !value) { // select is just for existance of attrib
        o = this.attributes
        for (k in o) {
          if (Object.prototype.hasOwnProperty.call(o, k) && (o[k].name || k) == attribute) {
            return this
          }
        }
      }
      if (wholeAttribute && !checkAttr(qualifier, getAttr(this, attribute) || '', value)) {
        // select is for attrib equality
        return false
      }
      return this
    }
  
    function clean(s) {
      return cleanCache.g(s) || cleanCache.s(s, s.replace(specialChars, '\\$1'))
    }
  
    function checkAttr(qualify, actual, val) {
      switch (qualify) {
      case '=':
        return actual == val
      case '^=':
        return actual.match(attrCache.g('^=' + val) || attrCache.s('^=' + val, '^' + clean(val), 1))
      case '$=':
        return actual.match(attrCache.g('$=' + val) || attrCache.s('$=' + val, clean(val) + '$', 1))
      case '*=':
        return actual.match(attrCache.g(val) || attrCache.s(val, clean(val), 1))
      case '~=':
        return actual.match(attrCache.g('~=' + val) || attrCache.s('~=' + val, '(?:^|\\s+)' + clean(val) + '(?:\\s+|$)', 1))
      case '|=':
        return actual.match(attrCache.g('|=' + val) || attrCache.s('|=' + val, '^' + clean(val) + '(-|$)', 1))
      }
      return 0
    }
  
    // given a selector, first check for simple cases then collect all base candidate matches and filter
    function _qwery(selector, _root) {
      var r = [], ret = [], i, l, m, token, tag, els, intr, item, root = _root
        , tokens = tokenCache.g(selector) || tokenCache.s(selector, selector.split(tokenizr))
        , dividedTokens = selector.match(dividers)
  
      if (!tokens.length) return r
  
      token = (tokens = tokens.slice(0)).pop() // copy cached tokens, take the last one
      if (tokens.length && (m = tokens[tokens.length - 1].match(idOnly))) root = byId(_root, m[1])
      if (!root) return r
  
      intr = q(token)
      // collect base candidates to filter
      els = root !== _root && root[nodeType] !== 9 && dividedTokens && /^[+~]$/.test(dividedTokens[dividedTokens.length - 1]) ?
        function (r) {
          while (root = root.nextSibling) {
            root[nodeType] == 1 && (intr[1] ? intr[1] == root[tagName].toLowerCase() : 1) && (r[r.length] = root)
          }
          return r
        }([]) :
        root[byTag](intr[1] || '*')
      // filter elements according to the right-most part of the selector
      for (i = 0, l = els.length; i < l; i++) {
        if (item = interpret.apply(els[i], intr)) r[r.length] = item
      }
      if (!tokens.length) return r
  
      // filter further according to the rest of the selector (the left side)
      each(r, function (e) { if (ancestorMatch(e, tokens, dividedTokens)) ret[ret.length] = e })
      return ret
    }
  
    // compare element to a selector
    function is(el, selector, root) {
      if (isNode(selector)) return el == selector
      if (arrayLike(selector)) return !!~flatten(selector).indexOf(el) // if selector is an array, is el a member?
  
      var selectors = selector.split(','), tokens, dividedTokens
      while (selector = selectors.pop()) {
        tokens = tokenCache.g(selector) || tokenCache.s(selector, selector.split(tokenizr))
        dividedTokens = selector.match(dividers)
        tokens = tokens.slice(0) // copy array
        if (interpret.apply(el, q(tokens.pop())) && (!tokens.length || ancestorMatch(el, tokens, dividedTokens, root))) {
          return true
        }
      }
      return false
    }
  
    // given elements matching the right-most part of a selector, filter out any that don't match the rest
    function ancestorMatch(el, tokens, dividedTokens, root) {
      var cand
      // recursively work backwards through the tokens and up the dom, covering all options
      function crawl(e, i, p) {
        while (p = walker[dividedTokens[i]](p, e)) {
          if (isNode(p) && (interpret.apply(p, q(tokens[i])))) {
            if (i) {
              if (cand = crawl(p, i - 1, p)) return cand
            } else return p
          }
        }
      }
      return (cand = crawl(el, tokens.length - 1, el)) && (!root || isAncestor(cand, root))
    }
  
    function isNode(el, t) {
      return el && typeof el === 'object' && (t = el[nodeType]) && (t == 1 || t == 9)
    }
  
    function uniq(ar) {
      var a = [], i, j;
      o:
      for (i = 0; i < ar.length; ++i) {
        for (j = 0; j < a.length; ++j) if (a[j] == ar[i]) continue o
        a[a.length] = ar[i]
      }
      return a
    }
  
    function arrayLike(o) {
      return (typeof o === 'object' && isFinite(o.length))
    }
  
    function normalizeRoot(root) {
      if (!root) return doc
      if (typeof root == 'string') return qwery(root)[0]
      if (!root[nodeType] && arrayLike(root)) return root[0]
      return root
    }
  
    function byId(root, id, el) {
      // if doc, query on it, else query the parent doc or if a detached fragment rewrite the query and run on the fragment
      return root[nodeType] === 9 ? root.getElementById(id) :
        root.ownerDocument &&
          (((el = root.ownerDocument.getElementById(id)) && isAncestor(el, root) && el) ||
            (!isAncestor(root, root.ownerDocument) && select('[id="' + id + '"]', root)[0]))
    }
  
    function qwery(selector, _root) {
      var m, el, root = normalizeRoot(_root)
  
      // easy, fast cases that we can dispatch with simple DOM calls
      if (!root || !selector) return []
      if (selector === window || isNode(selector)) {
        return !_root || (selector !== window && isNode(root) && isAncestor(selector, root)) ? [selector] : []
      }
      if (selector && arrayLike(selector)) return flatten(selector)
      if (m = selector.match(easy)) {
        if (m[1]) return (el = byId(root, m[1])) ? [el] : []
        if (m[2]) return arrayify(root[byTag](m[2]))
        if (hasByClass && m[3]) return arrayify(root[byClass](m[3]))
      }
  
      return select(selector, root)
    }
  
    // where the root is not document and a relationship selector is first we have to
    // do some awkward adjustments to get it to work, even with qSA
    function collectSelector(root, collector) {
      return function (s) {
        var oid, nid
        if (splittable.test(s)) {
          if (root[nodeType] !== 9) {
            // make sure the el has an id, rewrite the query, set root to doc and run it
            if (!(nid = oid = root.getAttribute('id'))) root.setAttribute('id', nid = '__qwerymeupscotty')
            s = '[id="' + nid + '"]' + s // avoid byId and allow us to match context element
            collector(root.parentNode || root, s, true)
            oid || root.removeAttribute('id')
          }
          return;
        }
        s.length && collector(root, s, false)
      }
    }
  
    var isAncestor = 'compareDocumentPosition' in html ?
      function (element, container) {
        return (container.compareDocumentPosition(element) & 16) == 16
      } : 'contains' in html ?
      function (element, container) {
        container = container[nodeType] === 9 || container == window ? html : container
        return container !== element && container.contains(element)
      } :
      function (element, container) {
        while (element = element.parentNode) if (element === container) return 1
        return 0
      }
    , getAttr = function () {
        // detect buggy IE src/href getAttribute() call
        var e = doc.createElement('p')
        return ((e.innerHTML = '<a href="#x">x</a>') && e.firstChild.getAttribute('href') != '#x') ?
          function (e, a) {
            return a === 'class' ? e.className : (a === 'href' || a === 'src') ?
              e.getAttribute(a, 2) : e.getAttribute(a)
          } :
          function (e, a) { return e.getAttribute(a) }
      }()
    , hasByClass = !!doc[byClass]
      // has native qSA support
    , hasQSA = doc.querySelector && doc[qSA]
      // use native qSA
    , selectQSA = function (selector, root) {
        var result = [], ss, e
        try {
          if (root[nodeType] === 9 || !splittable.test(selector)) {
            // most work is done right here, defer to qSA
            return arrayify(root[qSA](selector))
          }
          // special case where we need the services of `collectSelector()`
          each(ss = selector.split(','), collectSelector(root, function (ctx, s) {
            e = ctx[qSA](s)
            if (e.length == 1) result[result.length] = e.item(0)
            else if (e.length) result = result.concat(arrayify(e))
          }))
          return ss.length > 1 && result.length > 1 ? uniq(result) : result
        } catch (ex) { }
        return selectNonNative(selector, root)
      }
      // no native selector support
    , selectNonNative = function (selector, root) {
        var result = [], items, m, i, l, r, ss
        selector = selector.replace(normalizr, '$1')
        if (m = selector.match(tagAndOrClass)) {
          r = classRegex(m[2])
          items = root[byTag](m[1] || '*')
          for (i = 0, l = items.length; i < l; i++) {
            if (r.test(items[i].className)) result[result.length] = items[i]
          }
          return result
        }
        // more complex selector, get `_qwery()` to do the work for us
        each(ss = selector.split(','), collectSelector(root, function (ctx, s, rewrite) {
          r = _qwery(s, ctx)
          for (i = 0, l = r.length; i < l; i++) {
            if (ctx[nodeType] === 9 || rewrite || isAncestor(r[i], root)) result[result.length] = r[i]
          }
        }))
        return ss.length > 1 && result.length > 1 ? uniq(result) : result
      }
    , configure = function (options) {
        // configNativeQSA: use fully-internal selector or native qSA where present
        if (typeof options[useNativeQSA] !== 'undefined')
          select = !options[useNativeQSA] ? selectNonNative : hasQSA ? selectQSA : selectNonNative
      }
  
    configure({ useNativeQSA: true })
  
    qwery.configure = configure
    qwery.uniq = uniq
    qwery.is = is
    qwery.pseudos = {}
  
    return qwery
  });
  

  provide("qwery", module.exports);

  (function ($) {
    var q = function () {
      var r
      try {
        r = require('qwery')
      } catch (ex) {
        r = require('qwery-mobile')
      } finally {
        return r
      }
    }()
  
    $.pseudos = q.pseudos
  
    $._select = function (s, r) {
      // detect if sibling module 'bonzo' is available at run-time
      // rather than load-time since technically it's not a dependency and
      // can be loaded in any order
      // hence the lazy function re-definition
      return ($._select = (function () {
        var b
        if (typeof $.create == 'function') return function (s, r) {
          return /^\s*</.test(s) ? $.create(s, r) : q(s, r)
        }
        try {
          b = require('bonzo')
          return function (s, r) {
            return /^\s*</.test(s) ? b.create(s, r) : q(s, r)
          }
        } catch (e) { }
        return q
      })())(s, r)
    }
  
    $.ender({
        find: function (s) {
          var r = [], i, l, j, k, els
          for (i = 0, l = this.length; i < l; i++) {
            els = q(s, this[i])
            for (j = 0, k = els.length; j < k; j++) r.push(els[j])
          }
          return $(q.uniq(r))
        }
      , and: function (s) {
          var plus = $(s)
          for (var i = this.length, j = 0, l = this.length + plus.length; i < l; i++, j++) {
            this[i] = plus[j]
          }
          this.length += plus.length
          return this
        }
      , is: function(s, r) {
          var i, l
          for (i = 0, l = this.length; i < l; i++) {
            if (q.is(this[i], s, r)) {
              return true
            }
          }
          return false
        }
    }, true)
  }(ender));
  

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*!
    * Bonzo: DOM Utility (c) Dustin Diaz 2012
    * https://github.com/ded/bonzo
    * License MIT
    */
  (function (name, context, definition) {
    if (typeof module != 'undefined' && module.exports) module.exports = definition()
    else if (typeof context['define'] == 'function' && context['define']['amd']) define(definition)
    else context[name] = definition()
  })('bonzo', this, function() {
    var win = window
      , doc = win.document
      , html = doc.documentElement
      , parentNode = 'parentNode'
      , specialAttributes = /^(checked|value|selected|disabled)$/i
      , specialTags = /^(select|fieldset|table|tbody|tfoot|td|tr|colgroup)$/i // tags that we have trouble inserting *into*
      , simpleScriptTagRe = /\s*<script +src=['"]([^'"]+)['"]>/
      , table = ['<table>', '</table>', 1]
      , td = ['<table><tbody><tr>', '</tr></tbody></table>', 3]
      , option = ['<select>', '</select>', 1]
      , noscope = ['_', '', 0, 1]
      , tagMap = { // tags that we have trouble *inserting*
            thead: table, tbody: table, tfoot: table, colgroup: table, caption: table
          , tr: ['<table><tbody>', '</tbody></table>', 2]
          , th: td , td: td
          , col: ['<table><colgroup>', '</colgroup></table>', 2]
          , fieldset: ['<form>', '</form>', 1]
          , legend: ['<form><fieldset>', '</fieldset></form>', 2]
          , option: option, optgroup: option
          , script: noscope, style: noscope, link: noscope, param: noscope, base: noscope
        }
      , stateAttributes = /^(checked|selected|disabled)$/
      , ie = /msie/i.test(navigator.userAgent)
      , hasClass, addClass, removeClass
      , uidMap = {}
      , uuids = 0
      , digit = /^-?[\d\.]+$/
      , dattr = /^data-(.+)$/
      , px = 'px'
      , setAttribute = 'setAttribute'
      , getAttribute = 'getAttribute'
      , byTag = 'getElementsByTagName'
      , features = function() {
          var e = doc.createElement('p')
          e.innerHTML = '<a href="#x">x</a><table style="float:left;"></table>'
          return {
            hrefExtended: e[byTag]('a')[0][getAttribute]('href') != '#x' // IE < 8
          , autoTbody: e[byTag]('tbody').length !== 0 // IE < 8
          , computedStyle: doc.defaultView && doc.defaultView.getComputedStyle
          , cssFloat: e[byTag]('table')[0].style.styleFloat ? 'styleFloat' : 'cssFloat'
          , transform: function () {
              var props = ['transform', 'webkitTransform', 'MozTransform', 'OTransform', 'msTransform'], i
              for (i = 0; i < props.length; i++) {
                if (props[i] in e.style) return props[i]
              }
            }()
          , classList: 'classList' in e
          , opasity: function () {
              return typeof doc.createElement('a').style.opacity !== 'undefined'
            }()
          }
        }()
      , trimReplace = /(^\s*|\s*$)/g
      , whitespaceRegex = /\s+/
      , toString = String.prototype.toString
      , unitless = { lineHeight: 1, zoom: 1, zIndex: 1, opacity: 1, boxFlex: 1, WebkitBoxFlex: 1, MozBoxFlex: 1 }
      , query = doc.querySelectorAll && function (selector) { return doc.querySelectorAll(selector) }
      , trim = String.prototype.trim ?
          function (s) {
            return s.trim()
          } :
          function (s) {
            return s.replace(trimReplace, '')
          }
  
  
    function isNode(node) {
      return node && node.nodeName && (node.nodeType == 1 || node.nodeType == 11)
    }
  
  
    function normalize(node, host, clone) {
      var i, l, ret
      if (typeof node == 'string') return bonzo.create(node)
      if (isNode(node)) node = [ node ]
      if (clone) {
        ret = [] // don't change original array
        for (i = 0, l = node.length; i < l; i++) ret[i] = cloneNode(host, node[i])
        return ret
      }
      return node
    }
  
  
    /**
     * @param {string} c a class name to test
     * @return {boolean}
     */
    function classReg(c) {
      return new RegExp("(^|\\s+)" + c + "(\\s+|$)")
    }
  
  
    /**
     * @param {Bonzo|Array} ar
     * @param {function(Object, number, (Bonzo|Array))} fn
     * @param {Object=} opt_scope
     * @param {boolean=} opt_rev
     * @return {Bonzo|Array}
     */
    function each(ar, fn, opt_scope, opt_rev) {
      var ind, i = 0, l = ar.length
      for (; i < l; i++) {
        ind = opt_rev ? ar.length - i - 1 : i
        fn.call(opt_scope || ar[ind], ar[ind], ind, ar)
      }
      return ar
    }
  
  
    /**
     * @param {Bonzo|Array} ar
     * @param {function(Object, number, (Bonzo|Array))} fn
     * @param {Object=} opt_scope
     * @return {Bonzo|Array}
     */
    function deepEach(ar, fn, opt_scope) {
      for (var i = 0, l = ar.length; i < l; i++) {
        if (isNode(ar[i])) {
          deepEach(ar[i].childNodes, fn, opt_scope)
          fn.call(opt_scope || ar[i], ar[i], i, ar)
        }
      }
      return ar
    }
  
  
    /**
     * @param {string} s
     * @return {string}
     */
    function camelize(s) {
      return s.replace(/-(.)/g, function (m, m1) {
        return m1.toUpperCase()
      })
    }
  
  
    /**
     * @param {string} s
     * @return {string}
     */
    function decamelize(s) {
      return s ? s.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase() : s
    }
  
  
    /**
     * @param {Element} el
     * @return {*}
     */
    function data(el) {
      el[getAttribute]('data-node-uid') || el[setAttribute]('data-node-uid', ++uuids)
      var uid = el[getAttribute]('data-node-uid')
      return uidMap[uid] || (uidMap[uid] = {})
    }
  
  
    /**
     * removes the data associated with an element
     * @param {Element} el
     */
    function clearData(el) {
      var uid = el[getAttribute]('data-node-uid')
      if (uid) delete uidMap[uid]
    }
  
  
    function dataValue(d) {
      var f
      try {
        return (d === null || d === undefined) ? undefined :
          d === 'true' ? true :
            d === 'false' ? false :
              d === 'null' ? null :
                (f = parseFloat(d)) == d ? f : d;
      } catch(e) {}
      return undefined
    }
  
  
    /**
     * @param {Bonzo|Array} ar
     * @param {function(Object, number, (Bonzo|Array))} fn
     * @param {Object=} opt_scope
     * @return {boolean} whether `some`thing was found
     */
    function some(ar, fn, opt_scope) {
      for (var i = 0, j = ar.length; i < j; ++i) if (fn.call(opt_scope || null, ar[i], i, ar)) return true
      return false
    }
  
  
    /**
     * this could be a giant enum of CSS properties
     * but in favor of file size sans-closure deadcode optimizations
     * we're just asking for any ol string
     * then it gets transformed into the appropriate style property for JS access
     * @param {string} p
     * @return {string}
     */
    function styleProperty(p) {
        (p == 'transform' && (p = features.transform)) ||
          (/^transform-?[Oo]rigin$/.test(p) && (p = features.transform + 'Origin')) ||
          (p == 'float' && (p = features.cssFloat))
        return p ? camelize(p) : null
    }
  
    var getStyle = features.computedStyle ?
      function (el, property) {
        var value = null
          , computed = doc.defaultView.getComputedStyle(el, '')
        computed && (value = computed[property])
        return el.style[property] || value
      } :
  
      (ie && html.currentStyle) ?
  
      /**
       * @param {Element} el
       * @param {string} property
       * @return {string|number}
       */
      function (el, property) {
        if (property == 'opacity' && !features.opasity) {
          var val = 100
          try {
            val = el['filters']['DXImageTransform.Microsoft.Alpha'].opacity
          } catch (e1) {
            try {
              val = el['filters']('alpha').opacity
            } catch (e2) {}
          }
          return val / 100
        }
        var value = el.currentStyle ? el.currentStyle[property] : null
        return el.style[property] || value
      } :
  
      function (el, property) {
        return el.style[property]
      }
  
    // this insert method is intense
    function insert(target, host, fn, rev) {
      var i = 0, self = host || this, r = []
        // target nodes could be a css selector if it's a string and a selector engine is present
        // otherwise, just use target
        , nodes = query && typeof target == 'string' && target.charAt(0) != '<' ? query(target) : target
      // normalize each node in case it's still a string and we need to create nodes on the fly
      each(normalize(nodes), function (t, j) {
        each(self, function (el) {
          fn(t, r[i++] = j > 0 ? cloneNode(self, el) : el)
        }, null, rev)
      }, this, rev)
      self.length = i
      each(r, function (e) {
        self[--i] = e
      }, null, !rev)
      return self
    }
  
  
    /**
     * sets an element to an explicit x/y position on the page
     * @param {Element} el
     * @param {?number} x
     * @param {?number} y
     */
    function xy(el, x, y) {
      var $el = bonzo(el)
        , style = $el.css('position')
        , offset = $el.offset()
        , rel = 'relative'
        , isRel = style == rel
        , delta = [parseInt($el.css('left'), 10), parseInt($el.css('top'), 10)]
  
      if (style == 'static') {
        $el.css('position', rel)
        style = rel
      }
  
      isNaN(delta[0]) && (delta[0] = isRel ? 0 : el.offsetLeft)
      isNaN(delta[1]) && (delta[1] = isRel ? 0 : el.offsetTop)
  
      x != null && (el.style.left = x - offset.left + delta[0] + px)
      y != null && (el.style.top = y - offset.top + delta[1] + px)
  
    }
  
    // classList support for class management
    // altho to be fair, the api sucks because it won't accept multiple classes at once
    if (features.classList) {
      hasClass = function (el, c) {
        return el.classList.contains(c)
      }
      addClass = function (el, c) {
        el.classList.add(c)
      }
      removeClass = function (el, c) {
        el.classList.remove(c)
      }
    }
    else {
      hasClass = function (el, c) {
        return classReg(c).test(el.className)
      }
      addClass = function (el, c) {
        el.className = trim(el.className + ' ' + c)
      }
      removeClass = function (el, c) {
        el.className = trim(el.className.replace(classReg(c), ' '))
      }
    }
  
  
    /**
     * this allows method calling for setting values
     *
     * @example
     * bonzo(elements).css('color', function (el) {
     *   return el.getAttribute('data-original-color')
     * })
     *
     * @param {Element} el
     * @param {function (Element)|string}
     * @return {string}
     */
    function setter(el, v) {
      return typeof v == 'function' ? v(el) : v
    }
  
    /**
     * @constructor
     * @param {Array.<Element>|Element|Node|string} elements
     */
    function Bonzo(elements) {
      this.length = 0
      if (elements) {
        elements = typeof elements !== 'string' &&
          !elements.nodeType &&
          typeof elements.length !== 'undefined' ?
            elements :
            [elements]
        this.length = elements.length
        for (var i = 0; i < elements.length; i++) this[i] = elements[i]
      }
    }
  
    Bonzo.prototype = {
  
        /**
         * @param {number} index
         * @return {Element|Node}
         */
        get: function (index) {
          return this[index] || null
        }
  
        // itetators
        /**
         * @param {function(Element|Node)} fn
         * @param {Object=} opt_scope
         * @return {Bonzo}
         */
      , each: function (fn, opt_scope) {
          return each(this, fn, opt_scope)
        }
  
        /**
         * @param {Function} fn
         * @param {Object=} opt_scope
         * @return {Bonzo}
         */
      , deepEach: function (fn, opt_scope) {
          return deepEach(this, fn, opt_scope)
        }
  
  
        /**
         * @param {Function} fn
         * @param {Function=} opt_reject
         * @return {Array}
         */
      , map: function (fn, opt_reject) {
          var m = [], n, i
          for (i = 0; i < this.length; i++) {
            n = fn.call(this, this[i], i)
            opt_reject ? (opt_reject(n) && m.push(n)) : m.push(n)
          }
          return m
        }
  
      // text and html inserters!
  
      /**
       * @param {string} h the HTML to insert
       * @param {boolean=} opt_text whether to set or get text content
       * @return {Bonzo|string}
       */
      , html: function (h, opt_text) {
          var method = opt_text
                ? html.textContent === undefined ? 'innerText' : 'textContent'
                : 'innerHTML'
            , that = this
            , append = function (el, i) {
                each(normalize(h, that, i), function (node) {
                  el.appendChild(node)
                })
              }
            , updateElement = function (el, i) {
                try {
                  if (opt_text || (typeof h == 'string' && !specialTags.test(el.tagName))) {
                    return el[method] = h
                  }
                } catch (e) {}
                append(el, i)
              }
          return typeof h != 'undefined'
            ? this.empty().each(updateElement)
            : this[0] ? this[0][method] : ''
        }
  
        /**
         * @param {string=} opt_text the text to set, otherwise this is a getter
         * @return {Bonzo|string}
         */
      , text: function (opt_text) {
          return this.html(opt_text, true)
        }
  
        // more related insertion methods
  
        /**
         * @param {Bonzo|string|Element|Array} node
         * @return {Bonzo}
         */
      , append: function (node) {
          var that = this
          return this.each(function (el, i) {
            each(normalize(node, that, i), function (i) {
              el.appendChild(i)
            })
          })
        }
  
  
        /**
         * @param {Bonzo|string|Element|Array} node
         * @return {Bonzo}
         */
      , prepend: function (node) {
          var that = this
          return this.each(function (el, i) {
            var first = el.firstChild
            each(normalize(node, that, i), function (i) {
              el.insertBefore(i, first)
            })
          })
        }
  
  
        /**
         * @param {Bonzo|string|Element|Array} target the location for which you'll insert your new content
         * @param {Object=} opt_host an optional host scope (primarily used when integrated with Ender)
         * @return {Bonzo}
         */
      , appendTo: function (target, opt_host) {
          return insert.call(this, target, opt_host, function (t, el) {
            t.appendChild(el)
          })
        }
  
  
        /**
         * @param {Bonzo|string|Element|Array} target the location for which you'll insert your new content
         * @param {Object=} opt_host an optional host scope (primarily used when integrated with Ender)
         * @return {Bonzo}
         */
      , prependTo: function (target, opt_host) {
          return insert.call(this, target, opt_host, function (t, el) {
            t.insertBefore(el, t.firstChild)
          }, 1)
        }
  
  
        /**
         * @param {Bonzo|string|Element|Array} node
         * @return {Bonzo}
         */
      , before: function (node) {
          var that = this
          return this.each(function (el, i) {
            each(normalize(node, that, i), function (i) {
              el[parentNode].insertBefore(i, el)
            })
          })
        }
  
  
        /**
         * @param {Bonzo|string|Element|Array} node
         * @return {Bonzo}
         */
      , after: function (node) {
          var that = this
          return this.each(function (el, i) {
            each(normalize(node, that, i), function (i) {
              el[parentNode].insertBefore(i, el.nextSibling)
            }, null, 1)
          })
        }
  
  
        /**
         * @param {Bonzo|string|Element|Array} target the location for which you'll insert your new content
         * @param {Object=} opt_host an optional host scope (primarily used when integrated with Ender)
         * @return {Bonzo}
         */
      , insertBefore: function (target, opt_host) {
          return insert.call(this, target, opt_host, function (t, el) {
            t[parentNode].insertBefore(el, t)
          })
        }
  
  
        /**
         * @param {Bonzo|string|Element|Array} target the location for which you'll insert your new content
         * @param {Object=} opt_host an optional host scope (primarily used when integrated with Ender)
         * @return {Bonzo}
         */
      , insertAfter: function (target, opt_host) {
          return insert.call(this, target, opt_host, function (t, el) {
            var sibling = t.nextSibling
            sibling ?
              t[parentNode].insertBefore(el, sibling) :
              t[parentNode].appendChild(el)
          }, 1)
        }
  
  
        /**
         * @param {Bonzo|string|Element|Array} node
         * @return {Bonzo}
         */
      , replaceWith: function (node) {
          bonzo(normalize(node)).insertAfter(this)
          return this.remove()
        }
  
        // class management
  
        /**
         * @param {string} c
         * @return {Bonzo}
         */
      , addClass: function (c) {
          c = toString.call(c).split(whitespaceRegex)
          return this.each(function (el) {
            // we `each` here so you can do $el.addClass('foo bar')
            each(c, function (c) {
              if (c && !hasClass(el, setter(el, c)))
                addClass(el, setter(el, c))
            })
          })
        }
  
  
        /**
         * @param {string} c
         * @return {Bonzo}
         */
      , removeClass: function (c) {
          c = toString.call(c).split(whitespaceRegex)
          return this.each(function (el) {
            each(c, function (c) {
              if (c && hasClass(el, setter(el, c)))
                removeClass(el, setter(el, c))
            })
          })
        }
  
  
        /**
         * @param {string} c
         * @return {boolean}
         */
      , hasClass: function (c) {
          c = toString.call(c).split(whitespaceRegex)
          return some(this, function (el) {
            return some(c, function (c) {
              return c && hasClass(el, c)
            })
          })
        }
  
  
        /**
         * @param {string} c classname to toggle
         * @param {boolean=} opt_condition whether to add or remove the class straight away
         * @return {Bonzo}
         */
      , toggleClass: function (c, opt_condition) {
          c = toString.call(c).split(whitespaceRegex)
          return this.each(function (el) {
            each(c, function (c) {
              if (c) {
                typeof opt_condition !== 'undefined' ?
                  opt_condition ? !hasClass(el, c) && addClass(el, c) : removeClass(el, c) :
                  hasClass(el, c) ? removeClass(el, c) : addClass(el, c)
              }
            })
          })
        }
  
        // display togglers
  
        /**
         * @param {string=} opt_type useful to set back to anything other than an empty string
         * @return {Bonzo}
         */
      , show: function (opt_type) {
          opt_type = typeof opt_type == 'string' ? opt_type : ''
          return this.each(function (el) {
            el.style.display = opt_type
          })
        }
  
  
        /**
         * @return {Bonzo}
         */
      , hide: function () {
          return this.each(function (el) {
            el.style.display = 'none'
          })
        }
  
  
        /**
         * @param {Function=} opt_callback
         * @param {string=} opt_type
         * @return {Bonzo}
         */
      , toggle: function (opt_callback, opt_type) {
          opt_type = typeof opt_type == 'string' ? opt_type : '';
          typeof opt_callback != 'function' && (opt_callback = null)
          return this.each(function (el) {
            el.style.display = (el.offsetWidth || el.offsetHeight) ? 'none' : opt_type;
            opt_callback && opt_callback.call(el)
          })
        }
  
  
        // DOM Walkers & getters
  
        /**
         * @return {Element|Node}
         */
      , first: function () {
          return bonzo(this.length ? this[0] : [])
        }
  
  
        /**
         * @return {Element|Node}
         */
      , last: function () {
          return bonzo(this.length ? this[this.length - 1] : [])
        }
  
  
        /**
         * @return {Element|Node}
         */
      , next: function () {
          return this.related('nextSibling')
        }
  
  
        /**
         * @return {Element|Node}
         */
      , previous: function () {
          return this.related('previousSibling')
        }
  
  
        /**
         * @return {Element|Node}
         */
      , parent: function() {
          return this.related(parentNode)
        }
  
  
        /**
         * @private
         * @param {string} method the directional DOM method
         * @return {Element|Node}
         */
      , related: function (method) {
          return this.map(
            function (el) {
              el = el[method]
              while (el && el.nodeType !== 1) {
                el = el[method]
              }
              return el || 0
            },
            function (el) {
              return el
            }
          )
        }
  
  
        /**
         * @return {Bonzo}
         */
      , focus: function () {
          this.length && this[0].focus()
          return this
        }
  
  
        /**
         * @return {Bonzo}
         */
      , blur: function () {
          this.length && this[0].blur()
          return this
        }
  
        // style getter setter & related methods
  
        /**
         * @param {Object|string} o
         * @param {string=} opt_v
         * @return {Bonzo|string}
         */
      , css: function (o, opt_v) {
          var p, iter = o
          // is this a request for just getting a style?
          if (opt_v === undefined && typeof o == 'string') {
            // repurpose 'v'
            opt_v = this[0]
            if (!opt_v) return null
            if (opt_v === doc || opt_v === win) {
              p = (opt_v === doc) ? bonzo.doc() : bonzo.viewport()
              return o == 'width' ? p.width : o == 'height' ? p.height : ''
            }
            return (o = styleProperty(o)) ? getStyle(opt_v, o) : null
          }
  
          if (typeof o == 'string') {
            iter = {}
            iter[o] = opt_v
          }
  
          if (ie && iter.opacity) {
            // oh this 'ol gamut
            iter.filter = 'alpha(opacity=' + (iter.opacity * 100) + ')'
            // give it layout
            iter.zoom = o.zoom || 1;
            delete iter.opacity;
          }
  
          function fn(el, p, v) {
            for (var k in iter) {
              if (iter.hasOwnProperty(k)) {
                v = iter[k];
                // change "5" to "5px" - unless you're line-height, which is allowed
                (p = styleProperty(k)) && digit.test(v) && !(p in unitless) && (v += px)
                try { el.style[p] = setter(el, v) } catch(e) {}
              }
            }
          }
          return this.each(fn)
        }
  
  
        /**
         * @param {number=} opt_x
         * @param {number=} opt_y
         * @return {Bonzo|number}
         */
      , offset: function (opt_x, opt_y) {
          if (opt_x && typeof opt_x == 'object' && (typeof opt_x.top == 'number' || typeof opt_x.left == 'number')) {
            return this.each(function (el) {
              xy(el, opt_x.left, opt_x.top)
            })
          } else if (typeof opt_x == 'number' || typeof opt_y == 'number') {
            return this.each(function (el) {
              xy(el, opt_x, opt_y)
            })
          }
          if (!this[0]) return {
              top: 0
            , left: 0
            , height: 0
            , width: 0
          }
          var el = this[0]
            , de = el.ownerDocument.documentElement
            , bcr = el.getBoundingClientRect()
            , scroll = getWindowScroll()
            , width = el.offsetWidth
            , height = el.offsetHeight
            , top = bcr.top + scroll.y - Math.max(0, de && de.clientTop, doc.body.clientTop)
            , left = bcr.left + scroll.x - Math.max(0, de && de.clientLeft, doc.body.clientLeft)
  
          return {
              top: top
            , left: left
            , height: height
            , width: width
          }
        }
  
  
        /**
         * @return {number}
         */
      , dim: function () {
          if (!this.length) return { height: 0, width: 0 }
          var el = this[0]
            , de = el.nodeType == 9 && el.documentElement // document
            , orig = !de && !!el.style && !el.offsetWidth && !el.offsetHeight ?
               // el isn't visible, can't be measured properly, so fix that
               function (t) {
                 var s = {
                     position: el.style.position || ''
                   , visibility: el.style.visibility || ''
                   , display: el.style.display || ''
                 }
                 t.first().css({
                     position: 'absolute'
                   , visibility: 'hidden'
                   , display: 'block'
                 })
                 return s
              }(this) : null
            , width = de
                ? Math.max(el.body.scrollWidth, el.body.offsetWidth, de.scrollWidth, de.offsetWidth, de.clientWidth)
                : el.offsetWidth
            , height = de
                ? Math.max(el.body.scrollHeight, el.body.offsetHeight, de.scrollWidth, de.offsetWidth, de.clientHeight)
                : el.offsetHeight
  
          orig && this.first().css(orig)
          return {
              height: height
            , width: width
          }
        }
  
        // attributes are hard. go shopping
  
        /**
         * @param {string} k an attribute to get or set
         * @param {string=} opt_v the value to set
         * @return {Bonzo|string}
         */
      , attr: function (k, opt_v) {
          var el = this[0]
          if (typeof k != 'string' && !(k instanceof String)) {
            for (var n in k) {
              k.hasOwnProperty(n) && this.attr(n, k[n])
            }
            return this
          }
          return typeof opt_v == 'undefined' ?
            !el ? null : specialAttributes.test(k) ?
              stateAttributes.test(k) && typeof el[k] == 'string' ?
                true : el[k] : (k == 'href' || k =='src') && features.hrefExtended ?
                  el[getAttribute](k, 2) : el[getAttribute](k) :
            this.each(function (el) {
              specialAttributes.test(k) ? (el[k] = setter(el, opt_v)) : el[setAttribute](k, setter(el, opt_v))
            })
        }
  
  
        /**
         * @param {string} k
         * @return {Bonzo}
         */
      , removeAttr: function (k) {
          return this.each(function (el) {
            stateAttributes.test(k) ? (el[k] = false) : el.removeAttribute(k)
          })
        }
  
  
        /**
         * @param {string=} opt_s
         * @return {Bonzo|string}
         */
      , val: function (s) {
          return (typeof s == 'string') ?
            this.attr('value', s) :
            this.length ? this[0].value : null
        }
  
        // use with care and knowledge. this data() method uses data attributes on the DOM nodes
        // to do this differently costs a lot more code. c'est la vie
        /**
         * @param {string|Object=} opt_k the key for which to get or set data
         * @param {Object=} opt_v
         * @return {Bonzo|Object}
         */
      , data: function (opt_k, opt_v) {
          var el = this[0], o, m
          if (typeof opt_v === 'undefined') {
            if (!el) return null
            o = data(el)
            if (typeof opt_k === 'undefined') {
              each(el.attributes, function (a) {
                (m = ('' + a.name).match(dattr)) && (o[camelize(m[1])] = dataValue(a.value))
              })
              return o
            } else {
              if (typeof o[opt_k] === 'undefined')
                o[opt_k] = dataValue(this.attr('data-' + decamelize(opt_k)))
              return o[opt_k]
            }
          } else {
            return this.each(function (el) { data(el)[opt_k] = opt_v })
          }
        }
  
        // DOM detachment & related
  
        /**
         * @return {Bonzo}
         */
      , remove: function () {
          this.deepEach(clearData)
          return this.detach()
        }
  
  
        /**
         * @return {Bonzo}
         */
      , empty: function () {
          return this.each(function (el) {
            deepEach(el.childNodes, clearData)
  
            while (el.firstChild) {
              el.removeChild(el.firstChild)
            }
          })
        }
  
  
        /**
         * @return {Bonzo}
         */
      , detach: function () {
          return this.each(function (el) {
            el[parentNode] && el[parentNode].removeChild(el)
          })
        }
  
        // who uses a mouse anyway? oh right.
  
        /**
         * @param {number} y
         */
      , scrollTop: function (y) {
          return scroll.call(this, null, y, 'y')
        }
  
  
        /**
         * @param {number} x
         */
      , scrollLeft: function (x) {
          return scroll.call(this, x, null, 'x')
        }
  
    }
  
  
    function cloneNode(host, el) {
      var c = el.cloneNode(true)
        , cloneElems
        , elElems
  
      // check for existence of an event cloner
      // preferably https://github.com/fat/bean
      // otherwise Bonzo won't do this for you
      if (host.$ && typeof host.cloneEvents == 'function') {
        host.$(c).cloneEvents(el)
  
        // clone events from every child node
        cloneElems = host.$(c).find('*')
        elElems = host.$(el).find('*')
  
        for (var i = 0; i < elElems.length; i++)
          host.$(cloneElems[i]).cloneEvents(elElems[i])
      }
      return c
    }
  
    function scroll(x, y, type) {
      var el = this[0]
      if (!el) return this
      if (x == null && y == null) {
        return (isBody(el) ? getWindowScroll() : { x: el.scrollLeft, y: el.scrollTop })[type]
      }
      if (isBody(el)) {
        win.scrollTo(x, y)
      } else {
        x != null && (el.scrollLeft = x)
        y != null && (el.scrollTop = y)
      }
      return this
    }
  
    function isBody(element) {
      return element === win || (/^(?:body|html)$/i).test(element.tagName)
    }
  
    function getWindowScroll() {
      return { x: win.pageXOffset || html.scrollLeft, y: win.pageYOffset || html.scrollTop }
    }
  
    function createScriptFromHtml(html) {
      var scriptEl = document.createElement('script')
        , matches = html.match(simpleScriptTagRe)
      scriptEl.src = matches[1]
      return scriptEl
    }
  
    /**
     * @param {Array.<Element>|Element|Node|string} els
     * @return {Bonzo}
     */
    function bonzo(els) {
      return new Bonzo(els)
    }
  
    bonzo.setQueryEngine = function (q) {
      query = q;
      delete bonzo.setQueryEngine
    }
  
    bonzo.aug = function (o, target) {
      // for those standalone bonzo users. this love is for you.
      for (var k in o) {
        o.hasOwnProperty(k) && ((target || Bonzo.prototype)[k] = o[k])
      }
    }
  
    bonzo.create = function (node) {
      // hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh
      return typeof node == 'string' && node !== '' ?
        function () {
          if (simpleScriptTagRe.test(node)) return [createScriptFromHtml(node)]
          var tag = node.match(/^\s*<([^\s>]+)/)
            , el = doc.createElement('div')
            , els = []
            , p = tag ? tagMap[tag[1].toLowerCase()] : null
            , dep = p ? p[2] + 1 : 1
            , ns = p && p[3]
            , pn = parentNode
            , tb = features.autoTbody && p && p[0] == '<table>' && !(/<tbody/i).test(node)
  
          el.innerHTML = p ? (p[0] + node + p[1]) : node
          while (dep--) el = el.firstChild
          // for IE NoScope, we may insert cruft at the begining just to get it to work
          if (ns && el && el.nodeType !== 1) el = el.nextSibling
          do {
            // tbody special case for IE<8, creates tbody on any empty table
            // we don't want it if we're just after a <thead>, <caption>, etc.
            if ((!tag || el.nodeType == 1) && (!tb || (el.tagName && el.tagName != 'TBODY'))) {
              els.push(el)
            }
          } while (el = el.nextSibling)
          // IE < 9 gives us a parentNode which messes up insert() check for cloning
          // `dep` > 1 can also cause problems with the insert() check (must do this last)
          each(els, function(el) { el[pn] && el[pn].removeChild(el) })
          return els
        }() : isNode(node) ? [node.cloneNode(true)] : []
    }
  
    bonzo.doc = function () {
      var vp = bonzo.viewport()
      return {
          width: Math.max(doc.body.scrollWidth, html.scrollWidth, vp.width)
        , height: Math.max(doc.body.scrollHeight, html.scrollHeight, vp.height)
      }
    }
  
    bonzo.firstChild = function (el) {
      for (var c = el.childNodes, i = 0, j = (c && c.length) || 0, e; i < j; i++) {
        if (c[i].nodeType === 1) e = c[j = i]
      }
      return e
    }
  
    bonzo.viewport = function () {
      return {
          width: ie ? html.clientWidth : self.innerWidth
        , height: ie ? html.clientHeight : self.innerHeight
      }
    }
  
    bonzo.isAncestor = 'compareDocumentPosition' in html ?
      function (container, element) {
        return (container.compareDocumentPosition(element) & 16) == 16
      } : 'contains' in html ?
      function (container, element) {
        return container !== element && container.contains(element);
      } :
      function (container, element) {
        while (element = element[parentNode]) {
          if (element === container) {
            return true
          }
        }
        return false
      }
  
    return bonzo
  }); // the only line we care about using a semi-colon. placed here for concatenation tools
  

  provide("bonzo", module.exports);

  (function ($) {
  
    var b = require('bonzo')
    b.setQueryEngine($)
    $.ender(b)
    $.ender(b(), true)
    $.ender({
      create: function (node) {
        return $(b.create(node))
      }
    })
  
    $.id = function (id) {
      return $([document.getElementById(id)])
    }
  
    function indexOf(ar, val) {
      for (var i = 0; i < ar.length; i++) if (ar[i] === val) return i
      return -1
    }
  
    function uniq(ar) {
      var r = [], i = 0, j = 0, k, item, inIt
      for (; item = ar[i]; ++i) {
        inIt = false
        for (k = 0; k < r.length; ++k) {
          if (r[k] === item) {
            inIt = true; break
          }
        }
        if (!inIt) r[j++] = item
      }
      return r
    }
  
    $.ender({
      parents: function (selector, closest) {
        if (!this.length) return this
        if (!selector) selector = '*'
        var collection = $(selector), j, k, p, r = []
        for (j = 0, k = this.length; j < k; j++) {
          p = this[j]
          while (p = p.parentNode) {
            if (~indexOf(collection, p)) {
              r.push(p)
              if (closest) break;
            }
          }
        }
        return $(uniq(r))
      }
  
    , parent: function() {
        return $(uniq(b(this).parent()))
      }
  
    , closest: function (selector) {
        return this.parents(selector, true)
      }
  
    , first: function () {
        return $(this.length ? this[0] : this)
      }
  
    , last: function () {
        return $(this.length ? this[this.length - 1] : [])
      }
  
    , next: function () {
        return $(b(this).next())
      }
  
    , previous: function () {
        return $(b(this).previous())
      }
  
    , appendTo: function (t) {
        return b(this.selector).appendTo(t, this)
      }
  
    , prependTo: function (t) {
        return b(this.selector).prependTo(t, this)
      }
  
    , insertAfter: function (t) {
        return b(this.selector).insertAfter(t, this)
      }
  
    , insertBefore: function (t) {
        return b(this.selector).insertBefore(t, this)
      }
  
    , siblings: function () {
        var i, l, p, r = []
        for (i = 0, l = this.length; i < l; i++) {
          p = this[i]
          while (p = p.previousSibling) p.nodeType == 1 && r.push(p)
          p = this[i]
          while (p = p.nextSibling) p.nodeType == 1 && r.push(p)
        }
        return $(r)
      }
  
    , children: function () {
        var i, l, el, r = []
        for (i = 0, l = this.length; i < l; i++) {
          if (!(el = b.firstChild(this[i]))) continue;
          r.push(el)
          while (el = el.nextSibling) el.nodeType == 1 && r.push(el)
        }
        return $(uniq(r))
      }
  
    , height: function (v) {
        return dimension.call(this, 'height', v)
      }
  
    , width: function (v) {
        return dimension.call(this, 'width', v)
      }
    }, true)
  
    /**
     * @param {string} type either width or height
     * @param {number=} opt_v becomes a setter instead of a getter
     * @return {number}
     */
    function dimension(type, opt_v) {
      return typeof opt_v == 'undefined'
        ? b(this).dim()[type]
        : this.css(type, opt_v)
    }
  }(ender));

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*!
    * Bean - copyright (c) Jacob Thornton 2011-2012
    * https://github.com/fat/bean
    * MIT license
    */
  !(function (name, context, definition) {
    if (typeof module != 'undefined' && module.exports) module.exports = definition(name, context);
    else if (typeof define == 'function' && typeof define.amd  == 'object') define(definition);
    else context[name] = definition(name, context);
  }('bean', this, function (name, context) {
    var win            = window
      , old            = context[name]
      , namespaceRegex = /[^\.]*(?=\..*)\.|.*/
      , nameRegex      = /\..*/
      , addEvent       = 'addEventListener'
      , removeEvent    = 'removeEventListener'
      , doc            = document || {}
      , root           = doc.documentElement || {}
      , W3C_MODEL      = root[addEvent]
      , eventSupport   = W3C_MODEL ? addEvent : 'attachEvent'
      , ONE            = {} // singleton for quick matching making add() do one()
  
      , slice          = Array.prototype.slice
      , str2arr        = function (s, d) { return s.split(d || ' ') }
      , isString       = function (o) { return typeof o == 'string' }
      , isFunction     = function (o) { return typeof o == 'function' }
  
        // events that we consider to be 'native', anything not in this list will
        // be treated as a custom event
      , standardNativeEvents =
          'click dblclick mouseup mousedown contextmenu '                  + // mouse buttons
          'mousewheel mousemultiwheel DOMMouseScroll '                     + // mouse wheel
          'mouseover mouseout mousemove selectstart selectend '            + // mouse movement
          'keydown keypress keyup '                                        + // keyboard
          'orientationchange '                                             + // mobile
          'focus blur change reset select submit '                         + // form elements
          'load unload beforeunload resize move DOMContentLoaded '         + // window
          'readystatechange message '                                      + // window
          'error abort scroll '                                              // misc
        // element.fireEvent('onXYZ'... is not forgiving if we try to fire an event
        // that doesn't actually exist, so make sure we only do these on newer browsers
      , w3cNativeEvents =
          'show '                                                          + // mouse buttons
          'input invalid '                                                 + // form elements
          'touchstart touchmove touchend touchcancel '                     + // touch
          'gesturestart gesturechange gestureend '                         + // gesture
          'textinput'                                                      + // TextEvent
          'readystatechange pageshow pagehide popstate '                   + // window
          'hashchange offline online '                                     + // window
          'afterprint beforeprint '                                        + // printing
          'dragstart dragenter dragover dragleave drag drop dragend '      + // dnd
          'loadstart progress suspend emptied stalled loadmetadata '       + // media
          'loadeddata canplay canplaythrough playing waiting seeking '     + // media
          'seeked ended durationchange timeupdate play pause ratechange '  + // media
          'volumechange cuechange '                                        + // media
          'checking noupdate downloading cached updateready obsolete '       // appcache
  
        // convert to a hash for quick lookups
      , nativeEvents = (function (hash, events, i) {
          for (i = 0; i < events.length; i++) events[i] && (hash[events[i]] = 1)
          return hash
        }({}, str2arr(standardNativeEvents + (W3C_MODEL ? w3cNativeEvents : ''))))
  
        // custom events are events that we *fake*, they are not provided natively but
        // we can use native events to generate them
      , customEvents = (function () {
          var isAncestor = 'compareDocumentPosition' in root
                ? function (element, container) {
                    return container.compareDocumentPosition && (container.compareDocumentPosition(element) & 16) === 16
                  }
                : 'contains' in root
                  ? function (element, container) {
                      container = container.nodeType === 9 || container === window ? root : container
                      return container !== element && container.contains(element)
                    }
                  : function (element, container) {
                      while (element = element.parentNode) if (element === container) return 1
                      return 0
                    }
            , check = function (event) {
                var related = event.relatedTarget
                return !related
                  ? related == null
                  : (related !== this && related.prefix !== 'xul' && !/document/.test(this.toString())
                      && !isAncestor(related, this))
              }
  
          return {
              mouseenter: { base: 'mouseover', condition: check }
            , mouseleave: { base: 'mouseout', condition: check }
            , mousewheel: { base: /Firefox/.test(navigator.userAgent) ? 'DOMMouseScroll' : 'mousewheel' }
          }
        }())
  
        // we provide a consistent Event object across browsers by taking the actual DOM
        // event object and generating a new one from its properties.
      , Event = (function () {
              // a whitelist of properties (for different event types) tells us what to check for and copy
          var commonProps  = str2arr('altKey attrChange attrName bubbles cancelable ctrlKey currentTarget ' +
                'detail eventPhase getModifierState isTrusted metaKey relatedNode relatedTarget shiftKey '  +
                'srcElement target timeStamp type view which propertyName')
            , mouseProps   = commonProps.concat(str2arr('button buttons clientX clientY dataTransfer '      +
                'fromElement offsetX offsetY pageX pageY screenX screenY toElement'))
            , mouseWheelProps = mouseProps.concat(str2arr('wheelDelta wheelDeltaX wheelDeltaY wheelDeltaZ ' +
                'axis')) // 'axis' is FF specific
            , keyProps     = commonProps.concat(str2arr('char charCode key keyCode keyIdentifier '          +
                'keyLocation location'))
            , textProps    = commonProps.concat(str2arr('data'))
            , touchProps   = commonProps.concat(str2arr('touches targetTouches changedTouches scale rotation'))
            , messageProps = commonProps.concat(str2arr('data origin source'))
            , stateProps   = commonProps.concat(str2arr('state'))
            , overOutRegex = /over|out/
              // some event types need special handling and some need special properties, do that all here
            , typeFixers   = [
                  { // key events
                      reg: /key/i
                    , fix: function (event, newEvent) {
                        newEvent.keyCode = event.keyCode || event.which
                        return keyProps
                      }
                  }
                , { // mouse events
                      reg: /click|mouse(?!(.*wheel|scroll))|menu|drag|drop/i
                    , fix: function (event, newEvent, type) {
                        newEvent.rightClick = event.which === 3 || event.button === 2
                        newEvent.pos = { x: 0, y: 0 }
                        if (event.pageX || event.pageY) {
                          newEvent.clientX = event.pageX
                          newEvent.clientY = event.pageY
                        } else if (event.clientX || event.clientY) {
                          newEvent.clientX = event.clientX + doc.body.scrollLeft + root.scrollLeft
                          newEvent.clientY = event.clientY + doc.body.scrollTop + root.scrollTop
                        }
                        if (overOutRegex.test(type)) {
                          newEvent.relatedTarget = event.relatedTarget
                            || event[(type == 'mouseover' ? 'from' : 'to') + 'Element']
                        }
                        return mouseProps
                      }
                  }
                , { // mouse wheel events
                      reg: /mouse.*(wheel|scroll)/i
                    , fix: function () { return mouseWheelProps }
                  }
                , { // TextEvent
                      reg: /^text/i
                    , fix: function () { return textProps }
                  }
                , { // touch and gesture events
                      reg: /^touch|^gesture/i
                    , fix: function () { return touchProps }
                  }
                , { // message events
                      reg: /^message$/i
                    , fix: function () { return messageProps }
                  }
                , { // popstate events
                      reg: /^popstate$/i
                    , fix: function () { return stateProps }
                  }
                , { // everything else
                      reg: /.*/
                    , fix: function () { return commonProps }
                  }
              ]
            , typeFixerMap = {} // used to map event types to fixer functions (above), a basic cache mechanism
  
            , Event = function (event, element, isNative) {
                if (!arguments.length) return
                event = event || ((element.ownerDocument || element.document || element).parentWindow || win).event
                this.originalEvent = event
                this.isNative       = isNative
                this.isBean         = true
  
                if (!event) return
  
                var type   = event.type
                  , target = event.target || event.srcElement
                  , i, l, p, props, fixer
  
                this.target = target && target.nodeType === 3 ? target.parentNode : target
  
                if (isNative) { // we only need basic augmentation on custom events, the rest expensive & pointless
                  fixer = typeFixerMap[type]
                  if (!fixer) { // haven't encountered this event type before, map a fixer function for it
                    for (i = 0, l = typeFixers.length; i < l; i++) {
                      if (typeFixers[i].reg.test(type)) { // guaranteed to match at least one, last is .*
                        typeFixerMap[type] = fixer = typeFixers[i].fix
                        break
                      }
                    }
                  }
  
                  props = fixer(event, this, type)
                  for (i = props.length; i--;) {
                    if (!((p = props[i]) in this) && p in event) this[p] = event[p]
                  }
                }
              }
  
          // preventDefault() and stopPropagation() are a consistent interface to those functions
          // on the DOM, stop() is an alias for both of them together
          Event.prototype.preventDefault = function () {
            if (this.originalEvent.preventDefault) this.originalEvent.preventDefault()
            else this.originalEvent.returnValue = false
          }
          Event.prototype.stopPropagation = function () {
            if (this.originalEvent.stopPropagation) this.originalEvent.stopPropagation()
            else this.originalEvent.cancelBubble = true
          }
          Event.prototype.stop = function () {
            this.preventDefault()
            this.stopPropagation()
            this.stopped = true
          }
          // stopImmediatePropagation() has to be handled internally because we manage the event list for
          // each element
          // note that originalElement may be a Bean#Event object in some situations
          Event.prototype.stopImmediatePropagation = function () {
            if (this.originalEvent.stopImmediatePropagation) this.originalEvent.stopImmediatePropagation()
            this.isImmediatePropagationStopped = function () { return true }
          }
          Event.prototype.isImmediatePropagationStopped = function () {
            return this.originalEvent.isImmediatePropagationStopped && this.originalEvent.isImmediatePropagationStopped()
          }
          Event.prototype.clone = function (currentTarget) {
            //TODO: this is ripe for optimisation, new events are *expensive*
            // improving this will speed up delegated events
            var ne = new Event(this, this.element, this.isNative)
            ne.currentTarget = currentTarget
            return ne
          }
  
          return Event
        }())
  
        // if we're in old IE we can't do onpropertychange on doc or win so we use doc.documentElement for both
      , targetElement = function (element, isNative) {
          return !W3C_MODEL && !isNative && (element === doc || element === win) ? root : element
        }
  
        /**
          * Bean maintains an internal registry for event listeners. We don't touch elements, objects
          * or functions to identify them, instead we store everything in the registry.
          * Each event listener has a RegEntry object, we have one 'registry' for the whole instance.
          */
      , RegEntry = (function () {
          // each handler is wrapped so we can handle delegation and custom events
          var wrappedHandler = function (element, fn, condition, args) {
              var call = function (event, eargs) {
                    return fn.apply(element, args ? slice.call(eargs, event ? 0 : 1).concat(args) : eargs)
                  }
                , findTarget = function (event, eventElement) {
                    return fn.__beanDel ? fn.__beanDel.ft(event.target, element) : eventElement
                  }
                , handler = condition
                    ? function (event) {
                        var target = findTarget(event, this) // deleated event
                        if (condition.apply(target, arguments)) {
                          if (event) event.currentTarget = target
                          return call(event, arguments)
                        }
                      }
                    : function (event) {
                        if (fn.__beanDel) event = event.clone(findTarget(event)) // delegated event, fix the fix
                        return call(event, arguments)
                      }
              handler.__beanDel = fn.__beanDel
              return handler
            }
  
          , RegEntry = function (element, type, handler, original, namespaces, args, root) {
              var customType     = customEvents[type]
                , isNative
  
              if (type == 'unload') {
                // self clean-up
                handler = once(removeListener, element, type, handler, original)
              }
  
              if (customType) {
                if (customType.condition) {
                  handler = wrappedHandler(element, handler, customType.condition, args)
                }
                type = customType.base || type
              }
  
              this.isNative      = isNative = nativeEvents[type] && !!element[eventSupport]
              this.customType    = !W3C_MODEL && !isNative && type
              this.element       = element
              this.type          = type
              this.original      = original
              this.namespaces    = namespaces
              this.eventType     = W3C_MODEL || isNative ? type : 'propertychange'
              this.target        = targetElement(element, isNative)
              this[eventSupport] = !!this.target[eventSupport]
              this.root          = root
              this.handler       = wrappedHandler(element, handler, null, args)
            }
  
          // given a list of namespaces, is our entry in any of them?
          RegEntry.prototype.inNamespaces = function (checkNamespaces) {
            var i, j, c = 0
            if (!checkNamespaces) return true
            if (!this.namespaces) return false
            for (i = checkNamespaces.length; i--;) {
              for (j = this.namespaces.length; j--;) {
                if (checkNamespaces[i] == this.namespaces[j]) c++
              }
            }
            return checkNamespaces.length === c
          }
  
          // match by element, original fn (opt), handler fn (opt)
          RegEntry.prototype.matches = function (checkElement, checkOriginal, checkHandler) {
            return this.element === checkElement &&
              (!checkOriginal || this.original === checkOriginal) &&
              (!checkHandler || this.handler === checkHandler)
          }
  
          return RegEntry
        }())
  
      , registry = (function () {
          // our map stores arrays by event type, just because it's better than storing
          // everything in a single array.
          // uses '$' as a prefix for the keys for safety and 'r' as a special prefix for
          // rootListeners so we can look them up fast
          var map = {}
  
            // generic functional search of our registry for matching listeners,
            // `fn` returns false to break out of the loop
            , forAll = function (element, type, original, handler, root, fn) {
                var pfx = root ? 'r' : '$'
                if (!type || type == '*') {
                  // search the whole registry
                  for (var t in map) {
                    if (t.charAt(0) == pfx) {
                      forAll(element, t.substr(1), original, handler, root, fn)
                    }
                  }
                } else {
                  var i = 0, l, list = map[pfx + type], all = element == '*'
                  if (!list) return
                  for (l = list.length; i < l; i++) {
                    if ((all || list[i].matches(element, original, handler)) && !fn(list[i], list, i, type)) return
                  }
                }
              }
  
            , has = function (element, type, original, root) {
                // we're not using forAll here simply because it's a bit slower and this
                // needs to be fast
                var i, list = map[(root ? 'r' : '$') + type]
                if (list) {
                  for (i = list.length; i--;) {
                    if (!list[i].root && list[i].matches(element, original, null)) return true
                  }
                }
                return false
              }
  
            , get = function (element, type, original, root) {
                var entries = []
                forAll(element, type, original, null, root, function (entry) {
                  return entries.push(entry)
                })
                return entries
              }
  
            , put = function (entry) {
                var has = !entry.root && !this.has(entry.element, entry.type, null, false)
                  , key = (entry.root ? 'r' : '$') + entry.type
                ;(map[key] || (map[key] = [])).push(entry)
                return has
              }
  
            , del = function (entry) {
                forAll(entry.element, entry.type, null, entry.handler, entry.root, function (entry, list, i) {
                  list.splice(i, 1)
                  entry.removed = true
                  if (list.length === 0) delete map[(entry.root ? 'r' : '$') + entry.type]
                  return false
                })
              }
  
              // dump all entries, used for onunload
            , entries = function () {
                var t, entries = []
                for (t in map) {
                  if (t.charAt(0) == '$') entries = entries.concat(map[t])
                }
                return entries
              }
  
          return { has: has, get: get, put: put, del: del, entries: entries }
        }())
  
        // we need a selector engine for delegated events, use querySelectorAll if it exists
        // but for older browsers we need Qwery, Sizzle or similar
      , selectorEngine
      , setSelectorEngine = function (e) {
          if (!arguments.length) {
            selectorEngine = doc.querySelectorAll
              ? function (s, r) {
                  return r.querySelectorAll(s)
                }
              : function () {
                  throw new Error('Bean: No selector engine installed') // eeek
                }
          } else {
            selectorEngine = e
          }
        }
  
        // we attach this listener to each DOM event that we need to listen to, only once
        // per event type per DOM element
      , rootListener = function (event, type) {
          if (!W3C_MODEL && type && event && event.propertyName != '_on' + type) return
  
          var listeners = registry.get(this, type || event.type, null, false)
            , l = listeners.length
            , i = 0
  
          event = new Event(event, this, true)
          if (type) event.type = type
  
          // iterate through all handlers registered for this type, calling them unless they have
          // been removed by a previous handler or stopImmediatePropagation() has been called
          for (; i < l && !event.isImmediatePropagationStopped(); i++) {
            if (!listeners[i].removed) listeners[i].handler.call(this, event)
          }
        }
  
        // add and remove listeners to DOM elements
      , listener = W3C_MODEL
          ? function (element, type, add) {
              // new browsers
              element[add ? addEvent : removeEvent](type, rootListener, false)
            }
          : function (element, type, add, custom) {
              // IE8 and below, use attachEvent/detachEvent and we have to piggy-back propertychange events
              // to simulate event bubbling etc.
              var entry
              if (add) {
                registry.put(entry = new RegEntry(
                    element
                  , custom || type
                  , function (event) { // handler
                      rootListener.call(element, event, custom)
                    }
                  , rootListener
                  , null
                  , null
                  , true // is root
                ))
                if (custom && element['_on' + custom] == null) element['_on' + custom] = 0
                entry.target.attachEvent('on' + entry.eventType, entry.handler)
              } else {
                entry = registry.get(element, custom || type, rootListener, true)[0]
                if (entry) {
                  entry.target.detachEvent('on' + entry.eventType, entry.handler)
                  registry.del(entry)
                }
              }
            }
  
      , once = function (rm, element, type, fn, originalFn) {
          // wrap the handler in a handler that does a remove as well
          return function () {
            fn.apply(this, arguments)
            rm(element, type, originalFn)
          }
        }
  
      , removeListener = function (element, orgType, handler, namespaces) {
          var type     = orgType && orgType.replace(nameRegex, '')
            , handlers = registry.get(element, type, null, false)
            , removed  = {}
            , i, l
  
          for (i = 0, l = handlers.length; i < l; i++) {
            if ((!handler || handlers[i].original === handler) && handlers[i].inNamespaces(namespaces)) {
              // TODO: this is problematic, we have a registry.get() and registry.del() that
              // both do registry searches so we waste cycles doing this. Needs to be rolled into
              // a single registry.forAll(fn) that removes while finding, but the catch is that
              // we'll be splicing the arrays that we're iterating over. Needs extra tests to
              // make sure we don't screw it up. @rvagg
              registry.del(handlers[i])
              if (!removed[handlers[i].eventType] && handlers[i][eventSupport])
                removed[handlers[i].eventType] = { t: handlers[i].eventType, c: handlers[i].type }
            }
          }
          // check each type/element for removed listeners and remove the rootListener where it's no longer needed
          for (i in removed) {
            if (!registry.has(element, removed[i].t, null, false)) {
              // last listener of this type, remove the rootListener
              listener(element, removed[i].t, false, removed[i].c)
            }
          }
        }
  
        // set up a delegate helper using the given selector, wrap the handler function
      , delegate = function (selector, fn) {
          //TODO: findTarget (therefore $) is called twice, once for match and once for
          // setting e.currentTarget, fix this so it's only needed once
          var findTarget = function (target, root) {
                var i, array = isString(selector) ? selectorEngine(selector, root) : selector
                for (; target && target !== root; target = target.parentNode) {
                  for (i = array.length; i--;) {
                    if (array[i] === target) return target
                  }
                }
              }
            , handler = function (e) {
                var match = findTarget(e.target, this)
                if (match) fn.apply(match, arguments)
              }
  
          // __beanDel isn't pleasant but it's a private function, not exposed outside of Bean
          handler.__beanDel = {
              ft       : findTarget // attach it here for customEvents to use too
            , selector : selector
          }
          return handler
        }
  
      , fireListener = W3C_MODEL ? function (isNative, type, element) {
          // modern browsers, do a proper dispatchEvent()
          var evt = doc.createEvent(isNative ? 'HTMLEvents' : 'UIEvents')
          evt[isNative ? 'initEvent' : 'initUIEvent'](type, true, true, win, 1)
          element.dispatchEvent(evt)
        } : function (isNative, type, element) {
          // old browser use onpropertychange, just increment a custom property to trigger the event
          element = targetElement(element, isNative)
          isNative ? element.fireEvent('on' + type, doc.createEventObject()) : element['_on' + type]++
        }
  
        /**
          * Public API: off(), on(), add(), (remove()), one(), fire(), clone()
          */
  
        /**
          * off(element[, eventType(s)[, handler ]])
          */
      , off = function (element, typeSpec, fn) {
          var isTypeStr = isString(typeSpec)
            , k, type, namespaces, i
  
          if (isTypeStr && typeSpec.indexOf(' ') > 0) {
            // off(el, 't1 t2 t3', fn) or off(el, 't1 t2 t3')
            typeSpec = str2arr(typeSpec)
            for (i = typeSpec.length; i--;)
              off(element, typeSpec[i], fn)
            return element
          }
  
          type = isTypeStr && typeSpec.replace(nameRegex, '')
          if (type && customEvents[type]) type = customEvents[type].base
  
          if (!typeSpec || isTypeStr) {
            // off(el) or off(el, t1.ns) or off(el, .ns) or off(el, .ns1.ns2.ns3)
            if (namespaces = isTypeStr && typeSpec.replace(namespaceRegex, '')) namespaces = str2arr(namespaces, '.')
            removeListener(element, type, fn, namespaces)
          } else if (isFunction(typeSpec)) {
            // off(el, fn)
            removeListener(element, null, typeSpec)
          } else {
            // off(el, { t1: fn1, t2, fn2 })
            for (k in typeSpec) {
              if (typeSpec.hasOwnProperty(k)) off(element, k, typeSpec[k])
            }
          }
  
          return element
        }
  
        /**
          * on(element, eventType(s)[, selector], handler[, args ])
          */
      , on = function(element, events, selector, fn) {
          var originalFn, type, types, i, args, entry, first
  
          //TODO: the undefined check means you can't pass an 'args' argument, fix this perhaps?
          if (selector === undefined && typeof events == 'object') {
            //TODO: this can't handle delegated events
            for (type in events) {
              if (events.hasOwnProperty(type)) {
                on.call(this, element, type, events[type])
              }
            }
            return
          }
  
          if (!isFunction(selector)) {
            // delegated event
            originalFn = fn
            args       = slice.call(arguments, 4)
            fn         = delegate(selector, originalFn, selectorEngine)
          } else {
            args       = slice.call(arguments, 3)
            fn         = originalFn = selector
          }
  
          types = str2arr(events)
  
          // special case for one(), wrap in a self-removing handler
          if (this === ONE) {
            fn = once(off, element, events, fn, originalFn)
          }
  
          for (i = types.length; i--;) {
            // add new handler to the registry and check if it's the first for this element/type
            first = registry.put(entry = new RegEntry(
                element
              , types[i].replace(nameRegex, '') // event type
              , fn
              , originalFn
              , str2arr(types[i].replace(namespaceRegex, ''), '.') // namespaces
              , args
              , false // not root
            ))
            if (entry[eventSupport] && first) {
              // first event of this type on this element, add root listener
              listener(element, entry.eventType, true, entry.customType)
            }
          }
  
          return element
        }
  
        /**
          * add(element[, selector], eventType(s), handler[, args ])
          *
          * Deprecated: kept (for now) for backward-compatibility
          */
      , add = function (element, events, fn, delfn) {
          return on.apply(
              null
            , !isString(fn)
                ? slice.call(arguments)
                : [ element, fn, events, delfn ].concat(arguments.length > 3 ? slice.call(arguments, 5) : [])
          )
        }
  
        /**
          * one(element, eventType(s)[, selector], handler[, args ])
          */
      , one = function () {
          return on.apply(ONE, arguments)
        }
  
        /**
          * fire(element, eventType(s)[, args ])
          *
          * The optional 'args' argument must be an array, if no 'args' argument is provided
          * then we can use the browser's DOM event system, otherwise we trigger handlers manually
          */
      , fire = function (element, type, args) {
          var types = str2arr(type)
            , i, j, l, names, handlers
  
          for (i = types.length; i--;) {
            type = types[i].replace(nameRegex, '')
            if (names = types[i].replace(namespaceRegex, '')) names = str2arr(names, '.')
            if (!names && !args && element[eventSupport]) {
              fireListener(nativeEvents[type], type, element)
            } else {
              // non-native event, either because of a namespace, arguments or a non DOM element
              // iterate over all listeners and manually 'fire'
              handlers = registry.get(element, type, null, false)
              args = [false].concat(args)
              for (j = 0, l = handlers.length; j < l; j++) {
                if (handlers[j].inNamespaces(names)) {
                  handlers[j].handler.apply(element, args)
                }
              }
            }
          }
          return element
        }
  
        /**
          * clone(dstElement, srcElement[, eventType ])
          *
          * TODO: perhaps for consistency we should allow the same flexibility in type specifiers?
          */
      , clone = function (element, from, type) {
          var handlers = registry.get(from, type, null, false)
            , l = handlers.length
            , i = 0
            , args, beanDel
  
          for (; i < l; i++) {
            if (handlers[i].original) {
              args = [ element, handlers[i].type ]
              if (beanDel = handlers[i].handler.__beanDel) args.push(beanDel.selector)
              args.push(handlers[i].original)
              on.apply(null, args)
            }
          }
          return element
        }
  
      , bean = {
            on                : on
          , add               : add
          , one               : one
          , off               : off
          , remove            : off
          , clone             : clone
          , fire              : fire
          , setSelectorEngine : setSelectorEngine
          , noConflict        : function () {
              context[name] = old
              return this
            }
        }
  
    // for IE, clean up on unload to avoid leaks
    if (win.attachEvent) {
      var cleanup = function () {
        var i, entries = registry.entries()
        for (i in entries) {
          if (entries[i].type && entries[i].type !== 'unload') off(entries[i].element, entries[i].type)
        }
        win.detachEvent('onunload', cleanup)
        win.CollectGarbage && win.CollectGarbage()
      }
      win.attachEvent('onunload', cleanup)
    }
  
    // initialize selector engine to internal default (qSA or throw Error)
    setSelectorEngine()
  
    return bean
  }));
  

  provide("bean", module.exports);

  !function ($) {
    var b = require('bean')
  
      , integrate = function (method, type, method2) {
          var _args = type ? [type] : []
          return function () {
            for (var i = 0, l = this.length; i < l; i++) {
              if (!arguments.length && method == 'on' && type) method = 'fire'
              b[method].apply(this, [this[i]].concat(_args, Array.prototype.slice.call(arguments, 0)))
            }
            return this
          }
        }
  
      , add   = integrate('add')
      , on    = integrate('on')
      , one   = integrate('one')
      , off   = integrate('off')
      , fire  = integrate('fire')
      , clone = integrate('clone')
  
      , hover = function (enter, leave, i) { // i for internal
          for (i = this.length; i--;) {
            b.on.call(this, this[i], 'mouseenter', enter)
            b.on.call(this, this[i], 'mouseleave', leave)
          }
          return this
        }
  
      , methods = {
            on             : on
          , addListener    : on
          , bind           : on
          , listen         : on
          , delegate       : add // jQuery compat, same arg order as add()
  
          , one            : one
  
          , off            : off
          , unbind         : off
          , unlisten       : off
          , removeListener : off
          , undelegate     : off
  
          , emit           : fire
          , trigger        : fire
  
          , cloneEvents    : clone
  
          , hover          : hover
        }
  
      , shortcuts =
           ('blur change click dblclick error focus focusin focusout keydown keypress '
          + 'keyup load mousedown mouseenter mouseleave mouseout mouseover mouseup '
          + 'mousemove resize scroll select submit unload').split(' ')
  
    for (var i = shortcuts.length; i--;) {
      methods[shortcuts[i]] = integrate('on', shortcuts[i])
    }
  
    b.setSelectorEngine($)
  
    $.ender(methods, true)
  }(ender);

}());



(function () {

  var module = { exports: {} }, exports = module.exports;

  /*!
    * Qwery - A Blazing Fast query selector engine
    * https://github.com/ded/qwery
    * copyright Dustin Diaz & Jacob Thornton 2011
    * MIT License
    */
  
  !function () {
    var q, pseudos, i, l, p, r, nodes, m, nthPattern = /\s*((?:\+|\-)?(\d*))n\s*((?:\+|\-)\s*\d+)?\s*/
    if (typeof module != 'undefined' && typeof 'require' != 'undefined')
      q = require('qwery')
    else if (typeof qwery != 'undefined')
      q = qwery
    else
      return
  
    pseudos = q.pseudos
  
    function children(node, ofType) {
      var r = []
      nodes = node.childNodes
  
      for (i = 0, l = nodes.length; i < l; i++) {
        if (nodes[i].nodeType == 1 && (!ofType || nodes[i].nodeName == ofType)) r.push(nodes[i])
      }
      return r
    }
  
    function checkNthExpr(el, nodes, a, b) {
      if (!a) return (nodes[b-1] == el)
      for (i = b, l = nodes.length; ((a > 0) ? (i <= l) : (i >= 1)); i += a) if (el == nodes[i-1]) return true
      return false
    }
  
    function checkNth(el, nodes, val) {
      if (isFinite(val)) return nodes[val - 1] == el
      else if (val == 'odd') return checkNthExpr(el, nodes, 2, 1)
      else if (val == 'even') return checkNthExpr(el, nodes, 2, 0)
      else if (m = nthPattern.exec(val))
        return checkNthExpr(el, nodes,
                            (m[2] ? parseInt(m[1], 10) : parseInt(m[1] + '1', 10)),  // Check case where coefficient is omitted
                            (m[3] ? parseInt(m[3].replace(/\s*/, ''), 10) : 0)) // Check case where constant is omitted
  
      return false
    }
  
    function text(el, s) {
      if (el.nodeType === 3 || el.nodeType === 4) return el.nodeValue;
      if (el.nodeType !== 1 && el.nodeType !== 9) return '';
      for (s = '', el = el.firstChild; el; el = el.nextSibling) {
        if (el.nodeType !== 8) s += el.textContent || el.innerText || text(el)
      }
      return s
    }
  
    // *was* going to be in CSS3, didn't quite make it
    pseudos.contains = function(el, val) { return text(el).indexOf(val) != -1 }
  
    pseudos.not = function(el, val) { return !q.is(el, val) }
  
    pseudos['nth-child'] = function (el, val) {
      if (!val || !(p = el.parentNode)) return false
      return checkNth(el, children(p), val)
    }
  
    pseudos['nth-last-child'] = function (el, val) {
      if (!val || !(p = el.parentNode)) return false
      return checkNth(el, children(p).reverse(), val)
    }
  
    pseudos['nth-of-type'] = function (el, val) {
      if (!val || !(p = el.parentNode)) return false
      return checkNth(el, children(p, el.nodeName), val)
    }
  
    pseudos['nth-last-of-type'] = function (el, val) {
      if (!val || !(p = el.parentNode)) return false
      return checkNth(el, children(p, el.nodeName).reverse(), val)
    }
  
    pseudos['first-child'] = function (el) { return pseudos['nth-child'](el, 1) }
    pseudos['last-child'] = function (el) { return pseudos['nth-last-child'](el, 1) }
    pseudos['first-of-type'] = function (el) { return pseudos['nth-of-type'](el, 1) }
    pseudos['last-of-type'] = function (el) { return pseudos['nth-last-of-type'](el, 1) }
  
    pseudos['only-child'] = function (el) {
      return (p = el.parentNode) && (nodes = children(p)) && (nodes.length == 1) && (el == nodes[0])
    };
  
    pseudos['only-of-type'] = function (el) {
      return (p = el.parentNode) && (nodes = children(p, el.nodeName)) && (nodes.length == 1) && (el == nodes[0])
    }
  
    pseudos.target = function (el) {
      return (el.getAttribute('id') == location.hash.substr(1))
    }
  
    pseudos.checked = function (el) { return el.checked }
  
    pseudos.enabled = function (el) { return !el.disabled }
  
    pseudos.disabled = function (el) { return el.disabled }
  
    pseudos.empty = function (el) { return !el.childNodes.length }
  
  }();
  

  provide("qwery-pseudos", module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*global provide:true,ender:true*/
  // make a fake `ender` that can do some things slightly different
  !(function ($) {
    var faker = function (selector) {
          return selector === null || selector === '#' ? $([]) : $.apply(this, arguments)
        }
      , hasNewBean = !!require('bean').on
      , _$map = $.fn.map
      , _$on = $.fn.on
      , _$trigger = $.fn.trigger
      , _$data = $.fn.data
      , p
  
    for (p in $) {
      if (Object.prototype.hasOwnProperty.call($, p))
        faker[p] = $[p]
    }
    if (!faker.support) faker.support = {}
  
    // $.camelCase
    faker.camelCase = function (s) {
      return s.replace(/-([a-z]|[0-9])/ig, function (s, c) { return (c + '').toUpperCase() })
    }
    // $.extend(dst, src1, src2...)
    // simple shallow copy
    faker.extend = function () {
      var options, name, src, copy
        , target = arguments[0], i = 1, length = arguments.length
  
      for (; i < length; i++) {
        if ((options = arguments[i]) !== null) {
          for (name in options) {
            src = target[name]
            copy = options[name]
            if (target !== copy)
              target[name] = copy
          }
        }
      }
      return target
    }
    // $.map
    faker.map = function (a, fn, scope) {
      var r = [], tr, i, l
      for (i = 0, l = a.length; i < l; i++) {
        i in a && (tr = fn.call(scope, a[i], i, a)) != null && r.push(tr)
      }
      return r
    }
    // $.proxy
    faker.proxy = function (fn, ctx) {
      return function () { return fn.apply(ctx, arguments) }
    }
    // simplified version of jQuery's $.grep
    faker.grep = function (elems, callback) {
      var i = 0, l = elems.length, ret = []
      for (; i < l; i++) {
        if (!!callback(elems[i], i))
          ret.push(elems[i])
      }
      return ret;
    }
    // no index arg needed just yet
    faker.inArray = Array.prototype.indexOf
      ? function (el, arr) {
          return Array.prototype.indexOf.call(arr, el)
        }
      : function (el, arr) {
          for (var i = 0; i < arr.length; i++)
            if (arr[i] === el) return i
          return -1
        }
  
    // this is just nasty... Bootstrap uses $.Event(foo) so it can track state, we can't do that
    // with Bean but we need to pass Bean a string for trigger()
    faker.Event = function (s) {
      return s
    }
  
    // fix $().map to handle argument-less functions
    // also the explicit rejection of null values
    $.fn.map = function (fn) {
      if (!fn.length) { // no args
        return $(_$map.call(this, function (e) { return fn.call(e) }, function (e) { return e != null }))
      }
      return $(_$map.apply(this, arguments))
    }
    // fix $().on to handle jQuery style arguments
    $.fn.on = function () {
      // 'data' argument, can't use it, perhaps pass it as last arg?
      if (arguments.length == 3 && typeof arguments[2] == 'function' && typeof arguments[1] != 'string')
        return $.fn.bind.call(this, arguments[0], arguments[2])
      // this argument switch only needs to happen for old Bean
      else if (!hasNewBean && arguments.length == 3 && typeof arguments[2] == 'function' && typeof arguments[1] == 'string')
        return $.fn.bind.call(this, arguments[1], arguments[0], arguments[2])
      return _$on.apply(this, arguments)
    }
    // don't handle $().trigger({}) (object parameters)
    $.fn.trigger = function () {
      if (typeof arguments[0] == 'string')
        return _$trigger.apply(this, arguments)
      if (typeof arguments[0] == 'object' && typeof arguments[0].type == 'string')
        return _$trigger.call(this, arguments[0].type)
      return this
    }
    // fix $().data() to handle a JSON array for typeahead's "source"
    $.fn.data = function () {
      var d = _$data.apply(this, arguments)
      if (!arguments.length && typeof d.source == 'string' && /^\[/.test(d.source)) {
        if (typeof JSON != 'undefined' && JSON.parse) {
          d.source = JSON.parse(d.source)
        } else {
          d.source = d.source.replace(/(^\s*[\s*")|("\s*]\s*$)/g, '').split(/"\s*,\s*"/)
        }
      }
      return d
    }
    // implement sort which is awkward because Array.prototype.sort won't sort an Ender object
    $.fn.sort = function (fn) {
      var ar = []
      for (var i = 0; i < this.length; i++) ar[i] = this[i]
      ar.sort(fn)
      return $(ar)
    }
    // for carousel.to()
    if (!$.fn.index) {
      //TODO: support collections of elements for dropdown.js, move implementation to Traversty
      $.fn.index = function (el) {
        if (el && (!!el.nodeType || (!!(el = el[0]) && !!el.nodeType))) {
          for (var i = 0, l = this.length; i < l; i++) {
            if (this[i] === el) return i
          }
        }
        return -1
      }
    }
  
    // lifted from jQuery, modified slightly
    var rroot = /^(?:body|html)$/i
    $.fn.position = function () {
      if (!this.length)
        return null
  
      var elem = this[0],
      // Get *real* offsetParent
      offsetParent = this.offsetParent(),
  
      // Get correct offsets
      offset       = this.offset(),
      parentOffset = rroot.test(offsetParent[0].nodeName) ? { top: 0, left: 0 } : offsetParent.offset()
  
      // Subtract element margins
      // note: when an element has margin: auto the offsetLeft and marginLeft
      // are the same in Safari causing offset.left to incorrectly be 0
      offset.top  -= parseFloat($(elem).css("marginTop")) || 0
      offset.left -= parseFloat($(elem).css("marginLeft")) || 0
  
      // Add offsetParent borders
      parentOffset.top  += parseFloat($(offsetParent[0]).css("borderTopWidth")) || 0
      parentOffset.left += parseFloat($(offsetParent[0]).css("borderLeftWidth")) || 0
  
      // Subtract the two offsets
      return {
          top:  offset.top  - parentOffset.top
        , left: offset.left - parentOffset.left
      }
    }
    $.fn.offsetParent = function () {
      return $(this.map(function () {
        var offsetParent = this.offsetParent || document.body
        while (offsetParent && (!rroot.test(offsetParent.nodeName) && $(offsetParent).css("position") === "static")) {
          offsetParent = offsetParent.offsetParent
        }
        return offsetParent
      }))
    }
  
    // if (typeof module !== 'undefined') module.exports = faker
    if (typeof provide !== 'undefined') provide('ender-bootstrap-base-faker', faker)
    // else, where are we??
  
  
  }(ender))
  

  provide("ender-bootstrap-base", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /* ===================================================
   * bootstrap-transition.js v2.2.1
   * http://twitter.github.com/bootstrap/javascript.html#transitions
   * ===================================================
   * Copyright 2012 Twitter, Inc.
   *
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *
   * http://www.apache.org/licenses/LICENSE-2.0
   *
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
   * ========================================================== */
  
  
  !function ($) {
  
    "use strict"; // jshint ;_;
  
  
    /* CSS TRANSITION SUPPORT (http://www.modernizr.com/)
     * ======================================================= */
  
    $.domReady(function () {
  
      $.support.transition = (function () {
  
        var transitionEnd = (function () {
  
          var el = document.createElement('bootstrap')
            , transEndEventNames = {
                 'WebkitTransition' : 'webkitTransitionEnd'
              ,  'MozTransition'    : 'transitionend'
              ,  'OTransition'      : 'oTransitionEnd otransitionend'
              ,  'transition'       : 'transitionend'
              }
            , name
  
          for (name in transEndEventNames){
            if (el.style[name] !== undefined) {
              return transEndEventNames[name]
            }
          }
  
        }())
  
        return transitionEnd && {
          end: transitionEnd
        }
  
      })()
  
    })
  }(require('ender-bootstrap-base-faker'))

  provide("ender-bootstrap-transition", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /* =============================================================
   * bootstrap-typeahead.js v2.2.1
   * http://twitter.github.com/bootstrap/javascript.html#typeahead
   * =============================================================
   * Copyright 2012 Twitter, Inc.
   *
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *
   * http://www.apache.org/licenses/LICENSE-2.0
   *
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
   * ============================================================ */
  
  
  !function($){
  
    "use strict"; // jshint ;_;
  
  
   /* TYPEAHEAD PUBLIC CLASS DEFINITION
    * ================================= */
  
    var Typeahead = function (element, options) {
      this.$element = $(element)
      this.options = $.extend({}, $.fn.typeahead.defaults, options)
      this.matcher = this.options.matcher || this.matcher
      this.sorter = this.options.sorter || this.sorter
      this.highlighter = this.options.highlighter || this.highlighter
      this.updater = this.options.updater || this.updater
      this.$menu = $(this.options.menu).appendTo('body')
      this.source = this.options.source
      this.shown = false
      this.listen()
    }
  
    Typeahead.prototype = {
  
      constructor: Typeahead
  
    , select: function () {
        var val = this.$menu.find('.active').attr('data-value')
        this.$element
          .val(this.updater(val))
          .change()
        return this.hide()
      }
  
    , updater: function (item) {
        return item
      }
  
    , show: function () {
        var pos = $.extend({}, this.$element.offset(), {
          height: this.$element[0].offsetHeight
        })
  
        this.$menu.css({
          top: pos.top + pos.height
        , left: pos.left
        })
  
        this.$menu.show('block')
        this.shown = true
        return this
      }
  
    , hide: function () {
        this.$menu.hide()
        this.shown = false
        return this
      }
  
    , lookup: function (event) {
        var items
  
        this.query = this.$element.val()
  
        if (!this.query || this.query.length < this.options.minLength) {
          return this.shown ? this.hide() : this
        }
  
        items = (typeof this.source == "function") ? this.source(this.query, $.proxy(this.process, this)) : this.source
  
        return items ? this.process(items) : this
      }
  
    , process: function (items) {
        var that = this
  
        items = $.grep(items, function (item) {
          return that.matcher(item)
        })
  
        items = this.sorter(items)
  
        if (!items.length) {
          return this.shown ? this.hide() : this
        }
  
        return this.render(items.slice(0, this.options.items)).show()
      }
  
    , matcher: function (item) {
        return ~item.toLowerCase().indexOf(this.query.toLowerCase())
      }
  
    , sorter: function (items) {
        var beginswith = []
          , caseSensitive = []
          , caseInsensitive = []
          , item
  
        while (item = items.shift()) {
          if (!item.toLowerCase().indexOf(this.query.toLowerCase())) beginswith.push(item)
          else if (~item.indexOf(this.query)) caseSensitive.push(item)
          else caseInsensitive.push(item)
        }
  
        return beginswith.concat(caseSensitive, caseInsensitive)
      }
  
    , highlighter: function (item) {
        var query = this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
        return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
          return '<strong>' + match + '</strong>'
        })
      }
  
    , render: function (items) {
        var that = this
  
        items = $(items).map(function (item, i) {
          i = $(that.options.item).attr('data-value', item)
          i.find('a').html(that.highlighter(item))
          return i[0]
        })
  
        $(items).first().addClass('active')
        this.$menu.empty().append(items)
        return this
      }
  
    , next: function (event) {
        var active = this.$menu.find('.active').removeClass('active')
          , next = active.next()
  
        if (!next.length) {
          next = $(this.$menu.find('li')[0])
        }
  
        next.addClass('active')
      }
  
    , prev: function (event) {
        var active = this.$menu.find('.active').removeClass('active')
          , prev = active.prev()
  
        if (!prev.length) {
          prev = this.$menu.find('li').last()
        }
  
        prev.addClass('active')
      }
  
    , listen: function () {
        this.$element
          .on('blur',     $.proxy(this.blur, this))
          .on('keypress', $.proxy(this.keypress, this))
          .on('keyup',    $.proxy(this.keyup, this))
  
        if (this.eventSupported('keydown')) {
          this.$element.on('keydown', $.proxy(this.keydown, this))
        }
  
        this.$menu
          .on('click', $.proxy(this.click, this))
          .on('mouseenter', 'li', $.proxy(this.mouseenter, this))
      }
  
    , eventSupported: function(eventName) {
        var isSupported = eventName in this.$element
        if (!isSupported) {
          this.$element.setAttribute(eventName, 'return;')
          isSupported = typeof this.$element[eventName] === 'function'
        }
        return isSupported
      }
  
    , move: function (e) {
        if (!this.shown) return
  
        switch(e.keyCode) {
          case 9: // tab
          case 13: // enter
          case 27: // escape
            e.preventDefault()
            break
  
          case 38: // up arrow
            e.preventDefault()
            this.prev()
            break
  
          case 40: // down arrow
            e.preventDefault()
            this.next()
            break
        }
  
        e.stopPropagation()
      }
  
    , keydown: function (e) {
        this.suppressKeyPressRepeat = !~$.inArray(e.keyCode, [40,38,9,13,27])
        this.move(e)
      }
  
    , keypress: function (e) {
        if (this.suppressKeyPressRepeat) return
        this.move(e)
      }
  
    , keyup: function (e) {
        switch(e.keyCode) {
          case 40: // down arrow
          case 38: // up arrow
          case 16: // shift
          case 17: // ctrl
          case 18: // alt
            break
  
          case 9: // tab
          case 13: // enter
            if (!this.shown) return
            this.select()
            break
  
          case 27: // escape
            if (!this.shown) return
            this.hide()
            break
  
          default:
            this.lookup()
        }
  
        e.stopPropagation()
        e.preventDefault()
    }
  
    , blur: function (e) {
        var that = this
        setTimeout(function () { that.hide() }, 150)
      }
  
    , click: function (e) {
        e.stopPropagation()
        e.preventDefault()
        this.select()
      }
  
    , mouseenter: function (e) {
        this.$menu.find('.active').removeClass('active')
        $((e.currentTarget || e.target)).addClass('active')
      }
  
    }
  
  
    /* TYPEAHEAD PLUGIN DEFINITION
     * =========================== */
  
    $.fn.typeahead = function (option) {
      return this.each(function () {
        var $this = $(this)
          , data = $this.data('typeahead')
          , options = typeof option == 'object' && option
        if (!data) $this.data('typeahead', (data = new Typeahead(this, options)))
        if (typeof option == 'string') data[option]()
      })
    }
  
    $.fn.typeahead.defaults = {
      source: []
    , items: 8
    , menu: '<ul class="typeahead dropdown-menu"></ul>'
    , item: '<li><a href="#"></a></li>'
    , minLength: 1
    }
  
    $.fn.typeahead.Constructor = Typeahead
  
  
   /*   TYPEAHEAD DATA-API
    * ================== */
  
    $('[data-provide="typeahead"]').on('focus.typeahead.data-api', function (e) {
      var $this = $(this)
      if ($this.data('typeahead')) return
      e.preventDefault()
      $this.typeahead($this.data())
    })
  }(require('ender-bootstrap-base-faker'))

  provide("ender-bootstrap-typeahead", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /* =========================================================
   * bootstrap-modal.js v2.2.1
   * http://twitter.github.com/bootstrap/javascript.html#modals
   * =========================================================
   * Copyright 2012 Twitter, Inc.
   *
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *
   * http://www.apache.org/licenses/LICENSE-2.0
   *
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
   * ========================================================= */
  
  
  !function ($) {
  
    "use strict"; // jshint ;_;
  
  
   /* MODAL CLASS DEFINITION
    * ====================== */
  
    var Modal = function (element, options) {
      this.options = options
      this.$element = $(element)
        .delegate('[data-dismiss="modal"]', 'click.dismiss.modal', $.proxy(this.hide, this))
      this.options.remote && this.$element.find('.modal-body').load(this.options.remote)
    }
  
    Modal.prototype = {
  
        constructor: Modal
  
      , toggle: function () {
          return this[!this.isShown ? 'show' : 'hide']()
        }
  
      , show: function () {
          var that = this
            , e = $.Event('show')
  
          this.$element.trigger(e)
  
          if (this.isShown || false) return
  
          this.isShown = true
  
          this.escape()
  
          this.backdrop(function () {
            var transition = $.support.transition && that.$element.hasClass('fade')
  
            if (!that.$element.parent().length) {
              that.$element.appendTo(document.body) //don't move modals dom position
            }
  
            that.$element.show('block')
  
            if (transition) {
              that.$element[0].offsetWidth // force reflow
            }
  
            that.$element
              .addClass('in')
              .attr('aria-hidden', false)
  
            that.enforceFocus()
  
            transition ?
              that.$element.one($.support.transition.end, function () { that.$element.focus().trigger('shown') }) :
              that.$element.focus().trigger('shown')
  
          })
        }
  
      , hide: function (e) {
          e && e.preventDefault()
  
          var that = this
  
          e = $.Event('hide')
  
          this.$element.trigger(e)
  
          if (!this.isShown || false) return
  
          this.isShown = false
  
          this.escape()
  
          $(document).off('focusin.modal')
  
          this.$element
            .removeClass('in')
            .attr('aria-hidden', true)
  
          $.support.transition && this.$element.hasClass('fade') ?
            this.hideWithTransition() :
            this.hideModal()
        }
  
      , enforceFocus: function () {
          var that = this
          $(document).on('focusin.modal', function (e) {
            if (that.$element[0] !== e.target && !that.$element.has(e.target).length) {
              that.$element.focus()
            }
          })
        }
  
      , escape: function () {
          var that = this
          if (this.isShown && this.options.keyboard) {
            this.$element.on('keyup.dismiss.modal', function ( e ) {
              e.which == 27 && that.hide()
            })
          } else if (!this.isShown) {
            this.$element.off('keyup.dismiss.modal')
          }
        }
  
      , hideWithTransition: function () {
          var that = this
            , timeout = setTimeout(function () {
                that.$element.off($.support.transition.end)
                that.hideModal()
              }, 500)
  
          this.$element.one($.support.transition.end, function () {
            clearTimeout(timeout)
            that.hideModal()
          })
        }
  
      , hideModal: function (that) {
          this.$element
            .hide()
            .trigger('hidden')
  
          this.backdrop()
        }
  
      , removeBackdrop: function () {
          this.$backdrop.remove()
          this.$backdrop = null
        }
  
      , backdrop: function (callback) {
          var that = this
            , animate = this.$element.hasClass('fade') ? 'fade' : ''
  
          if (this.isShown && this.options.backdrop) {
            var doAnimate = $.support.transition && animate
  
            this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
              .appendTo(document.body)
  
            this.$backdrop.click(
              this.options.backdrop == 'static' ?
                $.proxy(this.$element[0].focus, this.$element[0])
              : $.proxy(this.hide, this)
            )
  
            if (doAnimate) this.$backdrop[0].offsetWidth // force reflow
  
            this.$backdrop.addClass('in')
  
            doAnimate ?
              this.$backdrop.one($.support.transition.end, callback) :
              callback()
  
          } else if (!this.isShown && this.$backdrop) {
            this.$backdrop.removeClass('in')
  
            $.support.transition && this.$element.hasClass('fade')?
              this.$backdrop.one($.support.transition.end, $.proxy(this.removeBackdrop, this)) :
              this.removeBackdrop()
  
          } else if (callback) {
            callback()
          }
        }
    }
  
  
   /* MODAL PLUGIN DEFINITION
    * ======================= */
  
    $.fn.modal = function (option) {
      return this.each(function () {
        var $this = $(this)
          , data = $this.data('modal')
          , options = $.extend({}, $.fn.modal.defaults, $this.data(), typeof option == 'object' && option)
        if (!data) $this.data('modal', (data = new Modal(this, options)))
        if (typeof option == 'string') data[option]()
        else if (options.show) data.show()
      })
    }
  
    $.fn.modal.defaults = {
        backdrop: true
      , keyboard: true
      , show: true
    }
  
    $.fn.modal.Constructor = Modal
  
  
   /* MODAL DATA-API
    * ============== */
  
    $(document).on('click.modal.data-api', '[data-toggle="modal"]', function (e) {
      var $this = $(this)
        , href = $this.attr('href')
        , $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) //strip for ie7
        , option = $target.data('modal') ? 'toggle' : $.extend({ remote:!/#/.test(href) && href }, $target.data(), $this.data())
  
      e.preventDefault()
  
      $target
        .modal(option)
        .one('hide', function () {
          $this.focus()
        })
    })
  }(require('ender-bootstrap-base-faker'))

  provide("ender-bootstrap-modal", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  // For questions about the dutch hyphenation patterns
  // ask Remco Bloemen (remco dot bloemen at gmail dot com)
  module.exports = {
      'id': 'nl',
      'leftmin': 2,
      'rightmin': 2,
      'patterns': {
          2 : "1b1çè1ê13ëî31ï3ñ1q1ü1z",
          3 : "_a4_b4_c4_d4_e4_é2_f4_g4_h2_i4_j4_k4_l4_m4_n4_o4_p4_r4_s4_t4_u4_w4_y2_z44a_4aea2ë2aha2qa1ta4üä3hä3r4b_3ba4bbb4o4bvby34bz4c_1ca2cbc4d1ce1céc3g3cic3j1coc3w1cy4d_1da2db1de3dè1di1do2ds2dt1du2dv2dw1dy2dz4e_4eae1de4ee2ie3oé3aé1dé1gé3hé3jé3né3pé3ré1t4ècè2lè2sè5tê2pê5t4ë_ë2bë3cë3dëe2ë3jë1l5ënë3pë2së1t4f_1fa4fbf1c4fd1fe1fé3fè3fê1fif1jf1n1fo3föfr44ft1fu4fv2fz4g_1ga4gd1ge1gé3gè1gigl41go4gs4gt1gu4gv1gy2gz4h_4hb2hdh4eh3hh3j2hlh1n2hr4hs2ht4i_i1a4ic4iei1è4ifi1hi3ii2j4ik4is4iti5w4izît42ï_ï5aï1cï1dï3oï1tï5z4j_j3bj1cj1gj3hj3j2jkj3r2jsj3vj1w4k_1ka1ke1kik1j1ko2ks4kt1ku2kû2kvky32kz4l_4ld1li2lm4lp4lsl1w1ly4lz4m_1ma4mb1me3mé3mè3mê1mi1mo2ms2mt1mu2mv1my2mz4n_1na2nb4nd1ne3né2ng1nin3n2ns2nt2nvnx31ny2nz4o_4oao4e2oë4oio3ï2oko1ö2oso2v4owo4xö3lö1pö4rös44p_1pé3pêpr42ps2pv5qequ44r_r1cr1gr3hr3lr1mr1p4rs4rtr1wr3x4rz4s_1sc3se3sé3sès1h1sisj2s1ms4qs2t1sus4y4t_1te1tétè33titr44ts4tv4tzu1a4ucu1du3èu1hu2i2uk4up4uzü4bü1n1v22v_v4bv4e3viv3j3vlv3tvy32w_2wbw1cw1gw1hw1j2wnw1p2ws2wtwu2w1wx1cx4ex1fx1hx3lx1mx1px3rx1tx3wxy3y1ay1cy1ey3èy1fy1gy1hy1iy1oy1py1rys3y1t4z_4zb4zc4zdz4e4zf4zgz2i4zm2zs2zvz4w5zy",
          4 : "_af3_es3_eu3_ik3_in1_om1_os5_st4_ts4_ui2_xe3a4a44aad4aag2abr4ac_4ace4ack2acl2acr4acu4ad_2add2adh4adk2adl2adp2adsad3w2adyae3ra3eua4ër4afa4afe4afiaf3l4afoaf1r2afy4ag_a2gr2agta1hiah3la3hoah5ra3hua3hyai1ea1ijai5ka2inaio4aiu4aïn42a1jaka2a2kr2aksak1wa1la4alda1lea3lèa1lo2alpa1lu4am_am4i4amm2anca1no4ans2a1oao4gao2la4oma3os4ap_a1paa1peap1j2apl4apr2apt2apu4ar_a1raa1rea1riark2a1roar2sa3ru4arwa1ry4asaa2sc2asea4sjas3kas3las3mas3n4asoas1pa2st2atg2atm2atnat3waua41augaup2aur44aus4auz4avia2vo4avy2a1way2a4b1cb5de3b4ebe1abee4be5gbet24b3f2b1g4b3h3b2i5bilbir34b1j4b1k3b4l2b1m4b1nbo2kbo4sbo3v4b1p3br4b5scb3sibs5s4bt4b3tab1trbts53b4u2b3w3ca_ca3bcae3cau3ca3v4c1ccca3c5do3cedcee43celceo4ce2s4ch_2chc1ché4chn2chp4cht4chw1chy5cij5cilci3o5circ3ky1c4l5clu2c1nco3dco4i5com2cooco5vc3p4c3soc3sp2c1tct5c1c2uça4o3da_3dae3dag3daida3ïd4am2d5cd5do3de_de3g2dei5dekde4ode2s2dex2d1f2d3gdg4l2d1hd5hedi2a3dig5dildi2o5div2d1j4d3l2d1md5ne3do_do3a3doi2dop5dou2dov2d3p1dr43dra2dredse2d2shds4ld2smds3nds5sdst4d1syd1tad1thd2tjd1tod1trd1tu5duedu4ndu2od1wed1wid3wre3aae1abe3ace1ade1afe1age3aie1ale3ane5ape3aqe1are1ase1ate1ave3boe5br3ecde3ce4eck4ect3eczed1seea4ee5bee2fee3iee2kee2l4eemee2nee2ree2tee5vee5zeën3e5ër4efie1fle1fr4e1ge2gle1h4e5haei5a4eidei1ee3ijei3o4eize1j2e3jee3koeku4e1lae1lee3lé2eli2ema4enf3enqe1nueny4eo3de5oee5one5ooe4ote1pae1pee1piep1je1ple1poe1prep2se1ree3rie3röe1rues4ee3sles2me1sne1soe1spe3sue3sye1tae3tee1the1toe1tre1tu1euceu3ee1ume3uu2euw4e1we5wae5weew2he5wi4ex_4exi1expeys4e5zae3zoezz4édi3èta5ëen3ën4eëns2ënt2ë1raë1reë1rië1roë3siës3tët4sëve54ëzu3fab2fadfa3gfa3mfa3vf3dafe2af4er4f1ff5fef5fiffs2f3fuf3g2fge34f5h3fib5fie5figfi3ofi4rfi4sf3kef2l24f1m3fob5foc5fokfoo4f4orfo3t4f1sf3scfse2f2shf2sifs2mfs2pfs4tf3syf1taft3hf1tof1trf1tu3fus2fuufva23fy13ga_3ganga4s3gat2g5b2g1cg3deg3drg3du3ge_ge3age3c4gex4g1fg5geg5gig5gl2g1hght4gi2f2gijgi2m5gir3gis4g1j4g1k4g1m2g1ng3nagn4e4gnu3go_3gob2goc2gofg4og4gohgo2kg2oogos12g5p1gr43grag2scgse4gs1jgs3lgs3ngs3pgs5qgs5wg5syg1tag3teg1tog3trg1tu5gu_3gueg5vo4g1wg5wa4gypha3gha3v2heahe2fhe4i2h3f4h5ghi5dh3la2h2mh3mah3meh2nahno32ho_ho3aho2fho3v2h1phpi4h4rehri4h3sah3sph3stht3w4h1w3hypi3aai4abi4aci3aei3aii2ami3ania3oi3ati3cei2drid3wieë2i2ekieo4i4epie2uie3vie3zi3ési1éti4ëgif3lif3rig4ai3geigs4i5ieii2ni5is4ij_ij5a4ijd4ijeij3i4ijnij5ui3kei3kli4krik1wi1lai3léil5fi3liil2mi1loi1lu4im_4imfi3mu1infi1noi1nu4i1oio5aio5bio3fi3oli3oni5ooio4si3oxi2ozi1pai1pei1piip1ji1pli1poi1pri4psi3rai3réi1rii1roir2si1rui1sai4sci4shi4skis3lis3pis5qi5syi1tait2ii4tji1toi3tuit3wiu4miu3ri3veiwi2i3zeïn3uï3riï3ro4ïs_ïs3aï4scïs3lï3soïs3tja3b2jaf1jag1jar3jawjaz4jda2j3drjd3wjea4jel4j1en5jepje2t5jeu2jewj3exjf3ljf3rjf2sjft2j3gejif3j3igj3kaj2krjk3wj1laj1lejl5fj1loj3luj3mijm3sjnt44joij3omj3opj4oujoy3j3paj1pejp1jj1pojps4js1ajs3nj2suj3syjt1hj3trjt1sj1tu1j4uju3d4jumjus32kafk3ahka3i3kamkao3ka4s2k3b2k1ck3ca2k5dk4eb2kefke2sk2et2k1f2k3gk3ho2kiëki3ok3jo2k1mk3makni2k4ockoo42k3p1kr43krak4siks1jkso4ks5sks4tk1syk1tak3tekt1hkt3jk1tok1trk3tuku5kku2rkut3k3ve2lac2laf2lall4anla3q2laula3v5lawl4az2lb42l1cl3drl3duld1wle2alee4le5ole2sl4folft42l1g4l1h3lid3lièl4ig2lixl3kel3kil3kolk2sl3kwl3ky2l1ll5lal3lol3mel3mil3mo2l3nlo3alog42lorlo3vl1pal1pel3pilp3jl1pll1pol1pr4l3rl3sils4jl1sll1spl1stl2sul3sy4l1tl3trl3tuly5im4agmaï4ma3qma3vm5blmb4r2m1c2m1dm5dam3dom3drm3dwme1cme4im2el3menm4es2mex2m1fmf4lm5fo2m5g2m1h3midm2is2m1j2m3l2m1m2m1nm5na5mo_mo3amog24mok3mom2mop5mosmo3vm3ox2m1pm3plmp2r2m1rms2cms2jm1slm1snmso4ms4tm3sym1tam1thm1tom3trm1tumue42muk5mut3muu5muzmve42m1wmy3emze43na_2nac5naen3aëna1h3nai3naï3nam2napnas22n1cn3cen5con3dend1wn3dy3ne_nee43nemne2n3nesne2u2nex2n3fng4ln3grng2s4n3h3nilni3o3nisn1j4n3je4n1knk2jn4kw2n3lnn4i3no_1noc3noï1nom1nos1notno3v3nox3noz2n1pn3ps2n3rn5ren5rin3sin1slns3mn1snn1sons5qns5sns4tn3syn3tan3ten3tèn3ton1trn3tu3nu_3nuc3nue4nuf2nui3numnu2n1nus4nuu2n1wn3xenxo4o3aao2ado3afo1ago3aho3aio1aloa2mo3auo3avo3ax2o3b4ob_3obj4obr4ocaod3woe3e2oep4oerof3l4ofoof1r4oftog1lo3gy2o1h3ohmoi1eoi3joi5k2o1joku4ok1wo1la4oldo1leo3lo4ols4oma4omeom2i4omm4omo3omz2onco4o24oo_oo3c4oofoo4goo4l4oonoo4soo4t2opa4opfo1piop3l1opno1poop1ro4ps2opto1rao1reo1ri4orpor1uo3ryos3fo3sio4sjo4skos3los4no3suo5syo3teo1tho4tjo1to2otro1tuot5w4ou_ou5aou1cou1e4ous2ovi4ovl4ovrovu3o1wao1weow3ho1wiow2no3woow3roys4öpe1ös5tö5su3pabpa3e1pag3pakpa4m1pap5paz2pb42p1c1ped3pegp4el3pes2p1f2p1g4pho2pidpi5op3jip1jo2p1kp2l22p1m2p1np3na3po_3poë1pol1pom1pospo3v4p3pp5pap5pep4psp2ra4psep3sip1slp1sp3psy4p1tp3tep5tipt3jp3tr1p2u3pubpu3e3pun2p1w1py12p5z5quor2aa2racra3ora3q2raur3cer3co2r1drd5lr3dor3du3re_r4ef1reg4reqre2ur3fer1flr1frr4igr5j4r3knrk2rrk1sr3mo2r1nr3narns4r3nuro1ar4oc3roë1roïro3v3royr3pa2r5rr1slrs4mr1snr1sor1spr3sur3syr1tart5cr3tor1trr1turt3wru1e4rufru2gru2kr2umru4r4rut4ruur4whr3yu5sa_s1aa1sab3sah3sai3saj2saks2alsa2p1sat1sax4s3bs5bas5bes5bo2sca4sce5scè2sci4scl2sco2scr2scu2scy4s1ds5des4dhs3dos5drs3dw5se_se2as4ebs4ees2els4ems4ens4esse4t4sez2s1f4sfi4sfu4s5g4shesh3l4shm5si_5sicsi5è3sirsi4s3sit3siu3siz4sj_s1jesjt4s5jusk4is3ko4skus2l4s2n43so_so1c1soe5soi3soï3soks2ols2om3soosos4s4ot2sov4sp_sp4asp4o4spt4s5rs5ses5sis5sls5sus5sy4st_2stb2std2stf4stg4sth2stk4stl2stm2stn2stp1stu2stv2stz5su_5sua5suc5sud3sug2sui5suk3sul5sum5supsu4s4s5v4s1ws5wo3sy_4syc3syn4s5z2tac4tadt2alta3ota3q3tas3tau3tax4t3b4t3ct4ch3tea3tectee2t4ef3teht4elt4emt4ent2er3test4ev2tex4t3ft5ge4th_4thm3thr2thu5ti_5tia5tibt4ilt2is1tj22tjo2tju4t3l4t3m4t3n5to_toa23toc3todt4oeto2f3toit2olto3vts2jt1slts5qts5st1sy4t3tt5te3tua3tub4tukt4umtu1ot3ve4t1w1ty13typtys4t3zat3zit5zwu3acu3anua3puat42u2bu5biu3bouda2ud1wu3ecu3efu3eiu1elu1eru1euu3ezu1fluf2su5gaug1lu2goui3e4uig4uik4uim4uisui3v4u3juk1wu1lau1leu1lou3lu2umeun2cun3gun4ou3olu3onu3oou1oru3osu1pau1peu3phu3piu1plu1pou1pru1ra4urfu1ri4urku1rou3ru4urvu4scu4sju4smuso2u1taut5cut2hu2tju1toutt4u1tuut5wu4u44uutuw1au1weu1wiuw1ouw1ruw3uuxa3u3yaü3riüs3lva2nva1pva3z4v3c3ve_5veb3vek5vel5vemve2rvi3ovje45vo_3voe3vog3voi3voovos33vot3vouvu2lw2adw4ag4wam3wapw4ar3way2w1dwe2a3wed3wegwe2mwe2n2wex2w1fw4ijwi4k3wil2w1k2w1l4w1mw3new3niw3now3obw2oewo4lw3scw1slw3spw1tow1trwva2xaf4xa3gxan3xi3gxi5ox4opx3sox3spy3aty3coy1d4y5dryes3y3ésygu2y4iny5isy3lay3ley3loy3nay3noyn1ty3ony3osyo3typo3yp3sy5riys4iy3syyto3yu5ay3uiy1w4ze5k2z3h3zifzi4t4z3k4z3lzoi4zo1pzo2tzo3v4z3p4z3r4z5tzus32z3z",
          5 : "_aan5_aat5_ab5l_adi5_af5l_af5s_al3f_alk4_ar5d_as5h_as5l_as3t_as3u_at4a_bos1_coo5_cus5_da2k_dan2_de2k_di4a_di3o_do3v_du4w_ede2_ed3w_ee4n_eet3_ei3l_ei5t_en5s_ep4a_er2f_ert4_es5c_et4h_eus5_ge3s_gid4_go4m_ij4s_ink2_is5c_jor5_ka3d_ka5g_ke4s_ko3v_kun2_lof5_lu3e_lu4s_ma5d_ma5ï_me2s_mo4s_na3d_na3n_ne2p_ne4s_no5v_ol3f_on3a_on3d_on1e_on5g_on3i_on5k_on1o_opi5_op5l_op3r_op5s_org4_ove4_pu2t_re5o_ro4l_ro5v_sap3_sa5v_sci3_see3_set3_se5v_sno2_te4a_te4f_tek2_te4s_ti2n_to4p_to5v_tsa3_ty2r_ui5s_uit1_uke5_ur4a_zes5_zit5aad1aaad1oaad1raaf5aaag1aaag3eaag3oaag5raags4aai3laak1aaak1oaak5raal1eaal1iaal5kaal5maal1uaam1aaam3oaan1aaan5gaan5i3aanj3aannaan3oaant43aanvaap1aaap3iaap3raar3aaar1i4aarnaar3uaas3eaas3i4aastaat3aaat5eaat3haat3iaat1oaat5raba4la2bonab3rua3cala3car4ach_a3chaach3la1choa3chr4achsa1chuac3kl2a3co4actaa5da_ad3acada2dada4ladas5a5de_ad3eia5desa3det4ad4i4ado_a3dooad5sead3soad3uiaege4ae5k4a3e2pae2s3af3aaaf4asaf4atafd4iafd2rafee4a5fo_a2foeaf3opaf3s4afs2caf5se3afsl3afspaft4aaf5traf3uiag3afag3arag3dia5ge_ag3exa4gil4ag1lag3ofag4raag3ruag3slags2pag1stagu5a4a1ha4a5heah5t2ai5a2ai4drail3mai2loai3ovai3s4ai5scai5snai1soai1stai5tjai3traïns5ak3afak3agake2tak3idak5is1akkoa2k3nak5neak4nia3kofak3onak5ruak4soak1stak5toak3wia3lala5le_2alegalf3lalfu4al2gla3liealk3sal5leal5mealo2nal3ouals5jal2slals5mal4sna2luiama4f4amag5ambta2meu3ampèam2plam4sma3nad4anda2andja4nema3nen4aner4ang_an2granij4ani5t4aniv4ank_ano3sano5van1stan3th2antiant3w4a1nua5nufan3uian3uran3uua2op2aor5taos3paos5ta4paka4pas4a1piap3leap3liap3loa1pluapon5ap3ooapo3p1appaap3raa3preap3ruap2saap4siap3snap3tjar2daardo4ar4duard3wa3rega3remar4ena3revar3ghar2glarm3ua3roka3rotarpi4ars2ear3siars3lars5mar4soar4spar4su4artear2th4artoar3uias3adas4agas3akas1apas5cea4secas5haasis1as5jaas3jias5kaas5kias4luas5mias4neas4nias3obaso2laso4ras3pla4s5qas5sa4assm3assuas3teas3tèas1to4astras4tuat1acata3s2atekate2nat4euat3hua2t3jat4jea2tooat5ruatsi4ats5mats3nat2stau3chau3coau5deaud4jau3naaun3tau5reau3soau3t44aut_1autoauw3aave3cavee4a5vooa5voraxis44azifämme3ba4dabad3sba3gl5b2akbale4ba3lobals4ba4meba3saba4stba2trbbe2n4b1d4bdi5abe3asbe2aube3chbeet1beie4bei3sbe5kibe1kwbe3lebel5fbe3libel3kbel4obelt4be5otbe1raber4ibe3rube3rybe1s4be4shbe3sobe5spbes5sbe3twbid3sbi2dubi4enbij5dbij3fbij1pbik4abi3lobi5obbi3okbi5ombi5owbi4stbi1trb5ledbles3b5lidbli2kblu2sbody3boes4bolk4bo5nabond23bonebo3nobo3p2bos3abo5sibo5sobos5pbot3jbo4tobot3rbo2tubove4bri4lbro2nbru2l4b1s4b2s5absi3dbs5jeb2s5mbul4kbu4lub5urbbu5ribus5cbus3obut4abut3jbu2tobut4sca1ch5cadaca3doca3drca3g2ca3loca3nacant4ca2ofca1prca3racar4uca5secas3tca3tacces53ceelcel3dce5licel5k2cenece3no5centce3racer2nce5roce5scce3taceto43chaï5chao3chas1chau1chef5cheq5ches5chirci5abci3amcie3kci1euci5lecil3m4cindci5omci3t2ci5tac2k3ack3efck3idc2k3lck4lec2k3nc4k3rck5seck3sock5stcla2ncle3uco3adcoin5co3k4co3la5condcon1g5cons3copa4copico5ricor2o5corrcors4co3ruco5scco5seco5spco3thco3tr5coun2cout1c4r23crascre5d2crip3criscro5fcro5kcroo3cro5vcrus5c3stect3adc2t1hc2t3jc3tolct4orct3slct3spcu5d4cu3encu3éscui5scui2tcu3k4cu3racus3o3daagd4aald3aap5daat4daboda4ce4dadr2d1afda3geda2guda3ke2dakk4dalad3alcda3le4dalfda3li2dalmdam4a2d1ap5dapud3arb3dare3dari3darodar3s3das35dasa3d4atda3tadat5j5daue3dauwdbei54d3d4dde2nddi3addo3pde2alde1chdee4ldee4rd3eied3eigd3eild1eisd3eiwde3kedek3wdel3kdel2s2demh5demid2en_de3nude5oldeo3vder2edes3mdes3ndes3pde3stde3t4de5twdeve44dexpd4gafdge2tdi5aedi4ak5dichdi4dodie2fdie2tdi1eudi2gadi3jo2dinfdi4oldio1s3di4sdi5sedi5sidis5pdis1tdi3thdit3jdit3r2d3k2d5le_dli4n2d3n2dni3s2dobj3d4oe5doe_doe5d4doef5doek5doen5doetd4ole5domid3omr5domud3omv4domz5don_d4ona5donedo5nido3nudo5ny5donzdo3pad3opbd3opd5dopj3dopod3opsd3opzd3orkdo3spdot3j3dovl3dovo5dra_d4rac5draf4drap4drasd1redd2ree4dretd3ric3dris5drop2drou2drozd4saadsa4bd3salds2chd4sefd4seid5send4setds3hods2imds5isd4s3jds4jod1slads5mods4ned3snuds1o4ds3obds3omd4sonds2oods3opd4spad1spid1stad3steds3thd1stods5tyd2su4ds3uu2ducadu3endu3et5duid5duif5duikd3uil2duit5duivdun5idu4ol3durf3durv5du1sdut3jdvee3d3wacd3wasd3watd3wekd3wetd3wez4d1wod3wor4d3yody4spea3boea4caeac5tea3daea3doea3lae3alie4alsea5mie4an_eang3ean4sea3prear2ce2ascea3soe4at_eat3se1chee1chie3chuec5le4ecorec3taed4age3damede3aedem4ede5oed4ere5die4edired3ove3d2red5seed2sled4soed5sped3sue4d2wee5caee5deee5doeed3wee3faee3fieef3leef3reeg3lee3kaeek1eee3kiee3kleek3neek3weel3aee3leee3lieema4een3aeen5gee3nieen5kee5o2ee2paee3pleepo4eer1aee3reeer5kee2s3ee3sjee3snee5teeet5hee3tjee3toee3tref3adefa4zef3doef3eie5feref3lie3fluef3ome3fooef3opef3sfegas4e3g4iegip4e4go_e2goseg3s4eg5soegut4ehit4ei3do4eienei3f4ei3gl4eigneik4lei3knei5kreiks44eil_4eileeil5mein5kei2noei2saeis4p4eit2eive4e5kamek4eeek1eie3kemekes3e3kete5kice4kile5kisek4nie5kofe5koreks4eek3toek3urek1uuel1acel1ale3lane3lapel1aue3lazel4drel4due3le_e4lele3lere3lesel1flel3gue5liee5lig3elixelk3sel4kw4e1loe3lokel3ole3looe5loue5lozelp4oel4ps4e1luemes3e5mokem3opem3saem5scem4smem1stem3suemut4en1acen5afe2nale2nane3naten1avenci4en3daen3dre3neee3neuen5gaen3gleng4rengs4e3niee3niveno3sen3ouen3su3entèent4ren3twe2nunen3uren3uueodo3eoes3e5offeo3freo5nieo3paeo3peeo3ple5opseor5de5orge5orieo3roeo3s4eo5steo5tee3paaep3ace4pafepa4ke3pale3pape3pare5pe_e5per3epide3potepou4ep4rae3prieps3leps5neps3pep2tjep4tr4equae3ra_e1raae3rader3afe1raierd4o4erecer3efere2oerg2le5rife5rige5rike5rioe5riser2kner5moer5nue1ro_e3rober3ocer3ofero2ge3roker3omer1oner1ove3rozer3sjer5teer3tre3rube2ruie2rune3ruser5uu3erwte4safe3same5sanes3ape5sece5seles5exes2fees5hee4shie3side3siees1ine4sires5kres4laes4lee3soles4ooeso4pe3sples4sme3styesu4re3ta_et3acet3ade3take2tape5tate4taue2tave5teae5tek4etele5teset3haet5hue5tiee4tiqe5tise4tjae5toce3toee3toleto4pet4slets3nets4uetu4ret3weet2wi1eua4eu5dreu4lie3um_e2umdeu2naeun3t1eu1oeu2poeur5keus4peu4steu3tjeu1treu4wa4everewo3vex3af4exco3exeg3exemex3inex5ope3y4oey3ste3zeeédee4égee5ê3perënce3ën5scën5thën5twëro3sëven4f4aatfa2bof3accface4f1ach2f1affal3sf3angfant2f4armfa2tofda4gf5danfd1arfde4kfd3offd4rafd5sefd3sifd3sofd3spf4d2wfd3wofede3f3een5fees3felife3no3fes32f3exff3shff3sifge5tfi5acfi4alfi3amfia4sfi1chf3ijs3f2ilfi3lo4find3finif3inj4fink2finrfi5sef5isofjes54f1k4f1laff4lamf3leif4lesfle2t4flevf4lexf3lez2flie2flijf4likf4lipf4litf3lokfoe5d2f3of2fomsfo5nafo1no4fontfooi5f3oom5foon2fo4pfo5ri5formfo1ru4f1ov4f5p45fracf3radf2ras5frauf1recf3rek5freqf4rikf4rodfrus3f2sa4fs3adfs3anfs3arf5schf4scrf4seifs4fefs5hef3siefs3imfs1infs3mafs4mifs3mofs3mufs3obfs3omfs4oof3stef3stif3stof3strf3stuft1acfta4pft4smfts3nft4softs3pftu4r2fuitfu4ma3f2unfur4ofval34f1w4fzet5g4aat2g1acg4af_g3afdga3fr4gal_ga3lagal3s4gambgan5d5ganega3pl3gar_ga3reg1arm3garsgas5cgas3igas3ogas3pgat5jgat3s4gautga5veg1avog5dacg5daggd3atgd3img2ding5drugd3sagd5spgea3qge5au2gebbge3d4ge5drge5dw3gee4ge3f4ge5g44geig5geitge3kege5kige3krgek4ugel5fgel5kge5ma4gembge5mogems3g4en_3genigen5kge1no5genwge5omge5osge5otge5p4ge1rager4ige3sage3scge5sege3sige5slge3snge3soge5swge3tage2thget4oge3trge5tuge3ui5g4ev5g4ezgédi44g3g4g4g5hg2hetgh5teg2humgi1eugif5rgi3gag3ijs4gijzgi3na5ginggin3o2ginrgi4ocgi2odgi2org5lab3glaig5lat3gle_g3leng3lesg5lev3gliëg2lifg2limg2lob3glofg5log3glomg3lopg5loz3g2lygne5ggne5m3go2ag1och4goefgoe1r5gom_go2mag3oml4gomzgo5no3goot2g1opgo3pag4oprg4oragor2sgo3tr2g3ovgpes35gra_g5rakgra2m5gravgre4sg4reug3rev5griagrof5g3rok2grougro5vg3rupgs1a2gsa4ggs3eigs3engs3etgs3evgs5heg3siegs5isgs4lags1legs4logs4lug4snag5solgs3opgs4pigs5psgs5scgst2ag1stegs3thg3stugs5tygtes4gu4eu2guitgu4nigu2s3h3afdha5ge5hals5halz2hamp4han_hap2shat5jhat3she2ar3hechhe3co4hee_hee4kheis4he2klhek3whe3lehe3lihelo4he5mohe5ne4he5ohe2prhe1rahert4he2ruhe5sehe2sphi3kwhil3mhin5dh3ins2hir2his5phi3trh4lagh3leph3loch4merhoa3nho3chhoe4shoe3thof5dhof3eho3g2ho2kaho5mohon3ghoni4ho1no4hoom2hootho3paho1pehop3r5horlho3roho3ruho3sahot3jho3tr2ho4whow3ohra4bh5reahro2kht3ach3talh3te_ht5emh3tenh4tevht3exh2t5hh2t1jht1o4ht5oph4t1rhtse4ht2siht4slht1u2hu4ba3huizhut3jhy4lahypo1i5abii3adyi5ae_i3agri5ak_ia3klia3kri3al_i3alii5am_i3amiian4oia5pai5apiia3scia5seia3soiave4i5blei1chai1chei1chii1choi3chrick5licos4i3damide3aides4idi3ai3dokid3ruid2s1ids5iids5jids5lid4smid3uuidu3wie1a2ie3deied3wi1ee4ie3fiie2flie4frie3geie4klie2knie2kuiel4eiel5kie3mai3ennierk4ie3ruie3sfie2siie4slies3mie3tji3etyieu3ki1euri1eusi1euzi4ëvaif3aaif3adif4raif3uiig3aaig5aci5galige2sig3ijigi3oig5noig4opig3skig3slig3spig3unija4dij3efij3eiij3elij1erij3o4i3jou4ijso4ijsp4ijstij5teij4tr4ijvo4ijzoi4kamik3efiket3i2kijik3lai2k4ni3komi2kooiko2pik3reik3riik3roik5seik5siiks3nik3spik1sti3la_il4aail3acil3adil3afi3lakil3alil2dailds44i3leile3lile4tile3uilk4l1illuilme2il4moi3looil1orils5jil2thi2magim5auimee4im3exim3opim5paim1stin1aci2nau1induinek4ineo2i5neuin2goin4gr4ini_i3nie4inkjin2kn3innoi3noci3nodin1onin3ov1inri4ins_in5sein3slin3soin1spin5tein3thioas5i3o1ci3odeioes3io3g2i5ol_i5ongi2op4io3paio3pri3optio3rai3oriio3rui3os_ios3cio5shio5siio5soio5spi3osyi3otiiot3jio3tri2o3vip3afi3papip3luipo4gi2priip3ruipse4ip4siira3ki1r2eires4irk4siro3piro5vir4scir3spirt3ri2saais3apise2dis5hoisi2di2sijis3jais3kais3keis5lei4s5mi4s3nis5no5isoli4sooiso3si2sotis2piis5plis5sais5soi2s3tis1tais4this1toi3stri3styisu2mit3acita5di3teni3terites4ite4tit1hoit1huit1ruit1sp4i3u2ium3eium3oiwie2iwit3ize3tïe4n3ï2n3aïns5mïn3sp1jaarja3knja3mija3plja1pojare41jas3jas5pjba4ljd3anjdes4jdi3aj2do4j3domjd5onjd3opjd1stj2d3u3jebaje3ch2j1eeje3laje3rojers4je4s33jesajes5ljes5mjeso23jesr3jevrj2f1ajf5lejfs3ajf4scjfs5fjfs3ljfs5mjfs3njfs3pjf3stjf5tijf5twj2g3ljg3snjg2stjin3gj4kaaj4karj4kauj4kavj2kijj2k4lj4klejk5lij2knaj4krajk3rejk3roj3laaj2loejm3opj4naajn5acjn3akjn2amj2nefjne4njn3gljn3k4jn4sijn2spjn1stjn3trjoet3jol4e1j4onjone2jo3pejo3rajo3ru1jourjp3ijj1pinj3pioj1plajp3lij4prejp3rij4sefj2s1ijs5injs4irjs4lejs3lijs4mej5soejs3olj3spejs3plj4spoj1stajs3thjt1acj1tagj3takj3tanj3te_j3toejt3rajt3rijuve5jve2njve3tkabe2ka3bo2k1acka3dok3advk3afdk4affka3flka4gaka3le5kalfkal2k4kalt5kalv4kambkan5d4kang5kap_ka3pekap3lka1poka3prkap3sk3arckart4kas5ckati4kat5jk3atlkato4ka5trkat3s2kavokdi3a2k3ecke4dikee4r4keffk4ei_k4eiek2eilkei5tkel5f2kemm2kempken5kke3toket3w3k2euke4vl4k1ex2k1h42ki2d4kiedk3ijs4kijvki3loki3na4kinb5kingki5sekit4s2k3jak3jew2k3ju4k5k41k2l45klack4las5klemk3lesk5lic4klidk3ligk4limk5lob4klod3klok5klos4kluc1k2n44knamk4nap5kneck5nemknip13knol2knumko4bl5koekkoes3koge43k4okko5ko2kolm5koloko4ly4komgkom5p4komzk4onikoot3ko3pa4kopbko1pe4kopg5kopjko2pl2kops4kopz2kordko3rukos4jkoso44koss4k1ov4k3oxkpi3skra4bk3refk2regk4rit2krolk4ron2krou3k4ruk5rubkru4lk3salks3anks3apks1arks3asks2e2k5secks3edks3epks3etkse3vk5silks1ink5sisk5sitk1slaks3leks3lik4smoks3naks3noks3nuks3omk1spek3spik3spuk1stak1steks3thk3stik3stokt3ackte2ck4texk5tijkt3imkt3ink5titkt3omkto4pkt4orku5bekui2f2kuitku5me5kunsku3raku3rekur3s3ku2sk2wadk1wag4kwat2kwegk1wei5kwelk2wiek4wik2kwil2kwin4k1wo2laanla4cal4aci2ladj4ladmlad5sla2du4ladv3lae3la2fala3fllafo2la2golag3rlags4la4kila3kr3laldlal4o3land4lannlan2sla3pilap3l2larmlase4la2sila3telat5j5laufl2auwlava3la4vo4lazildak4l2dauldi3ald1ovld1reld3sald3slld5spld3uule3atleeg3lee5lleem33leen4leep2leeu2leffleg3lle4goleg5s4leig4leks5leldle2le5lelil3elp3lenele2no4lep_le4sale3scles3mle4sples3tlet4ileus45leuz4lexc4lexpl2faclfa3sl2faulfe4nlf3lilf3lul5foelf1opl5foul1fral3frulf4sllf4solf5talf5twlf3uul4gapl3glal3gogl3goolg3s4lgse5li3agli3am5lid_5lidmli3eu3liftli3go5lijn4lijp4lijtli5krli4kwlim4ali3mi2limp4linfli5ni3linn2linrl3inv4linzli3obli5omli3otli2pali3pi2lisol5iswlit3r4l1j2lk3afl5koel5korl5koul5kral2krelk4selk4solk3willa3dlle3kll4ellleo4l3l4illo5fl5lonll3shl3maalm3aflma5ïl3maklm3aul4medlme2sl5moglm3orlm5sclm3shlm3sulni4s2lobj4loeg4loesl3oeu5loevlog5llo3go5logrlo4krlo2ku2lo2llo3lal3omll3omtl3omv4lomz3lon_4lond5longlon3o2lont3looklo3pa4lopbl3opdlo1pe2lopn4loptlo3relo3rilo3rulost4lo2talot3jlot3rlou3s2love3lo5zl3paalp3aml3parl3paslpe2nl2pexlp3ofl3pomlp3onl3posl3potlrus5l4saal1samls3anl3sapls3asl2satls4cul4sefl5senl4sinls5jal3slal3slols3nal3snel3snol3socls3ofl3solls3opls1ovl2spals4pel3spil2sprl3stal3stels4til3stol3stuls5tyl3surls3uslt4aalt1acl4taml3thulto4llt3sllt3splu4b1lub5elub5llu1en3lui_5luia5luid2luitluk2slu3na3lunclu3talut3jlu3wily3stmaas3m3actm3aflma3frma3glma5goma3grma5kama5kema3kwma5noma5pama3prma1so5massma5tamat5jma3trmdi3amdis5mdo3pme5demee5g5meesm5eg_m5egdm5eggm5egtmei2n5melome4mim4en_me3namen5k4menqme5nume1ra5merkmes3mme3some4sp3metime5trmeve4mfa3tmf3limger44mid_5middmi3kn5milimi3lom3imp2minf5ming4minhmi5numis5fmi4stmi1tr2m3k25moda5mode2moef5moeimoe2s5mogemo3gl5mole2moli4moltmo5no5mons3mooimo3pam1opem4oppmop4smo3ramo3romo5scmot3j5mouwm5panm5penm4plum5ponm4ps2mp5scmp3shmp5sum3samms3apms3coms3cum3sjem2slem3s2mms3mam3solms3orm3s2pm3stam1stem1stim1stomst5smtes4mu5da2muitmul3pmu2m3mu3nomu3samut3jmuts2mvee3mzet53naal2naap5naatn3abd5nabena2can2aci3naconad4e3nadi4n1afn2akena3krn3albn3alm2naly4nambn4amin3ampn3ank3nantnap3s3naro4narsna1spn4at_nat5jna3tonats45nau_5naus2na3v3navinbe5tn3chencht2nch3un5da_n4dapn2darn4dasn4davndi3andi3on5do_n5docn4donnd3ovnd1rend4smnd3spnd3sune5acne3am3neckne2clne3don3edune5dw4neednee5k3neemne3g24n1ein2eigne2la4nelf5nenbne4ni5nenpne5ocne5okne5omneo5pne5osne5otne3pene1ra2nergner3une3ry4neumng3afn2garn3gavn5genng3ijng2lin3goeng3ofn3goïng5opn3gotng4senhek5n4i2d3nieunij3fnij3kni5krnik4s3nim_5nimfn3impn3inb2ninfn3inj2ninrni5ornip3lni4slnis5nni1trnits4njes4nje3tnk3afn3kefn3kennk3idn3knenk4rank5senk5sink3slnk3snnk1spnk1stnk3wi2n3m4n5n2enno5v1no3d2noefnoes3n5ogi1nogrno3klno2li1nolon2oman3omln1omsn3omv2nomz3nonc4nont3noodno1pen1opg2nops2nordno3re1norm4norr3nors3norzno3sfno3snno3sp3nota5notinot3jnot3r3nou_3novano4ven3sagn1saln1samns3ann1sapns4cin4scon4sefnse4gn2sinn1sjon4slen4smun2snan5snen2sofn3soln2sonns4orns1ovn1spens4pins1pon1sprn1stan3sten1stin1stons5tyn5tabnt1adnt4asn5tecn5temnt3han4thon5tignt4jont4ognt4oln5tonnt4oon4topn5trynts3ant1snnt1spnt1stntu4nnu3ennu3etnu2lo5numm3nuncn3uni2nu4rnu5ronu3tr5nuutnuw5a4n3yi4n3yoo1a2nobal4o3chao1cheo3chio3choo3chrocke44o3cooco3aoc3t4od5acoda3godes4odi3ao5druod5scod2slods4toe5anoe3asoe4droed3woe5er1oefeoe2fioe2floeg1loeii4oei3noe2kuoek1woel5foelo4oen3ooe4ploe4psoe1raoer4eoero24oes_oe3sioe2tjoet3wof3arof3ato4favofd3wo4flio4floof3omo3fooof3opo3forof3oxof5seof4slof2spof4tuof3uiog5acoga4log5deog3dioge4d2ogemo3ger2og5h1ogigog5neog3opog3spog3uioi3doo3ingoi3o4oi3s4oi5scois2poist2o3ka_o3kaaok3abok3ago3kalok3efo2k4lo4kliok5luo2k3nokos5o2k3rok4raok1saok3snok5teok3urok3uuok2wiol3aco3lalo3le_ole5gol1eio3leno5leroleu2ol2faolf3lol3frol2glo3liao3lico5lido3liko3lino3litol5keol2krolk4solk2vo5locolo3kol4omo4lopol5seol5siol1sjol3sool3sp4o1luolu4rom2aaom1acom1afo3manome5tomo5lomo3s5omroom3slom3uion1acon4agon4anon3apon2dro5ne_o3nebon3eiong2rongs4on5ido5nigon3knon5kwono3lon1onon3scons4eons2fon1st4ont_3ontvon1uion3ur4oo4dood1aood1oood1roo3fioog1aoog3eoo5gioog1roogs4ook3aoo3keook5loole2ool5fool5gool3kool1uoo3meoom3ioon5aoon1ooo4p1oopa2oop3roor3aoor5ioor5koor5moor1o4oortoos3aoo5seoos5noot1aoot3hoot5ooot3rop3adop3amo3pan3opdro3pe_op3eeop3eio1pelo5pico5pis4op1jopoe3op1ofo5pogo5poio5polo4pruop5seop5siops4mop3snop3soop3spop3su4opt_op5trop3uior1afora4gor4door3drord3w4orecoree4or2glo5ria3oriëork2aor3klor5knor3kwor3nior3oeo3rolor1onor1ooor3oro3rosor5ovor5scor5seor3soor3spor2too4saco5saso3sauosca4o4scios3cuo5sedos4elo5seros4feo4shao3shios2hoos5koo4s3mos5noo3s2oos3paos1pio4spro2s3tos4taos4thos4toos1tuo3sty4o1taot1acot3afo3tagotas4o5tato5tegot3eio5teno5terote4tot3huotli2o5tomoto3sot3ruot2slot3snot3spot3uio3tulou2doou3k4ou3saous5coust4ou2taout3hout1jout1rouw3a2o3vao5ve_o5ves4o3voo5wenozet54paan5paasp3acc2pachp4aci5pacu3pad_pa4da4padvpag2apa3ghpa4ki3palepal3fpa3lipa3na4pank5papipap3lpa3popa3pr4par_p3arbpard43park3parl4parmpa5ro4parrpa5rupa5sapas5cpa5sepa5sopa5te1pathp3atl2paut5pauzpa4vl2p3d2pe4al4pecipe3depe3dop4ee43pee_3peeë4peen5pees4peispek5spe3lepe3napen5k5pennpent4pep3opep5sp4er_pe1ra3perc1periper1o3persp2ertpe3sa3pet_pe5ta3petipetu53peuk5peutpge5s2p1h44p3hap4hispi3ampi5anpi4at5pieppi3gl3pij_pij3k3pijn5pijp2pijz2pind4pinr2pinspis5npi3thpit3jpit3r2p1japjes5p3la_p3lapp4lecp3lepp4lex4plijp4lompoda53poei3poezp2ofa3pogipo5grpol4s5ponypoo3d3poolpoot34popd2popepop5hpo3rop4ortpo3rupo1sapo3sfpo5tepot1jpot3r3poul3pra_p5rad4pram3praop4ratp4rax1prem3pres3pret4pric1prij3prik5priv1proj3promp4roq3pros4proypru2tp3sabp3sakps3arp4sinp5sisps3leps2meps5mip4sofp3solpso4rps2plp1s4tp3steps5thp3stups5ty5psycp3syspt3adp2t1hpu3ch4pun_3put_put1jput3rpvan4que4s2raan3raarra4cara3ce5raclrad4a3radbra3di4radm4radv2rafdr4affra5gira5gorag4s3raisrak5rr3altra3mir2amp4rana4ranj4rap_ra3pora4skra4slra1sora2spr4atirat5j3rausr1aut5ravrrces3r3chirda2mr3danrd3eirdi3ardi5or5docrdo3vrd2rurd3sard3sord1sprds4trd3surd2wird5wo1reac4reakre3co3recr3reda3redd3redure5dwree4kree2pree4s4refb2reff3reflre3fu4reg_4regd4regg3regire3gl4regt4reie4reil5reizre4kore4kure1kwrel5k3rem_2rempre2nar4end5renfr4enn4renqre4ofre5parer4s2rerwre3sare5sere4slres3mres3tret4i2retn3revo2r3exrf3alr3fasrf2s2rf3smrf3sprf3uurg3eir3glorg1s4rg2smrg5sori4agri2akri5anrias4ri4avri4bl4riceri3cori3diri1euri3flri3frri3glr4ijlrij5o4rijvrik5nril5mri3ma4rindri5ner4ingr3inl4rins4rintri5onri3scrit3j3rittr5ka_rk3afr2kahrke4nrke4sr2klor4kner2kobrk4rirk5sirks4prkt3hrk4tirkt3orkt1rr1kwar1kwirmes3rmi2sr5moer4moprm1strmun4rnes3r2ninr5notrn3smrn3sprn1strn3thrn5tjrn5tornu5rro5acro1ch3roe_4roef4roeg3roem4roevr4ofiro3flro3kl3rokmr4ol_2roliro5maro3mo4romzr2on_r2oner2onir2onkr2onnr2onsro3nu4ronv3roof2roog4roonro3pa4ropbro1pe4ropnr4oporo4puror5dro3roro3saro5sero3sfro3shro3spro5terot3jro3trr1oudro4ve4roxir5peerpi3sr2p3jrp4lorp4ror3psarp4sirp2slrre5orreu2r3salr3sanrs3aprs3arrs3asrs2crrs4etr4sj4r5sjtr3smers5mur5solr2sorrs1ovr3sper4spur1s4tr3ster3stir3stor3strr3styr5ta_rt1adrt3amr2tarr4taur2tavrtes4r4thart1hert3hir1thort3hurt3hyrt4ijr5tokr3trart3rirts5mrt1sprt2wi5rubrru4grruk3iru2lirul5sru2mi3run_r2undru5rar2u4srus3erut3jru3warves41saagsa3bo2s1acsa2ca3sacrs1adv2s1af3safe3safosa3frs5agg3sagnsa3go3sakss1akt5sal_3sald5salhs3all4salms3aln5sammsam5ps4ancs4ant3sap_sa3pasa4prsa3ras1arb3sardsa2res1armsaro4s4ars4sas_3sasasa3sc5satis3aud1saurs1aut3sauz4sch_sch4as2chi3scoosdi5asdis5se3akse3alsear4se3ause3cr5sect4secz5seeisee4t4seevs1effse3geseg2rs4ein2seis5seizse1kwse3le4selfsel5k5selmselo45selp5selts5emm5sen_5senhsen5ks4ergser4ise3roser2sse3ruse5scse3sf5sessse5tase5tise3tjset3rset3wse3um4s1exse2ze4sfeds5fei4s5frsfu5msgue4s4ha_sha4gs5hies3hoes3hoos2hot3shows5hul5s4iasi5acsi3amsi5ansici4si3co3sie_3sieësiep4sies4si1f45s2igs3ijv5sile3simu5sinas3inb2sinfsing4s3inhs4inn4sinr2sint5siros3irrsis3isi5tosi3trsi5tu5sjab4sj3ds5jeb3sjee1sjers3jes3sjew5sjofsj3s22s1k2s5kads5ken3skiës2k3js5kres3k4w4slabs4lac3slap4slaws3leds4leps5less3li_4slid2slies3lifs5ligsli2ms4lip3slofs3lols3los3slot4slun4s3ly3smad3smals5mapsmie2s4mijs5min5smoks3mons5nams4nar3snau3snees5negs3nies5nim4snods3nog2snoosno5v3snufs4nui2snumso4bls3oce2soef3soepsoes33soft2so2gs1ogeso3gl3sogy5sol_so3laso3le5sols5somms3omv2somz3sonas3onb2song3sonns4onss3onw2s1opso3prs2orbs3ordsor4oso3sfs3oudsou2lsou3ts1ove3so5z2spad2spaks4pans3paus4peks5pep4sper4spess3pezs3pids3piss3plos3plus2poe4spoë4spog4spols2poos3pop4spou4s3pss2p4u4spub4spuns4pur5spuwsro5v4s3s4s4scosseo4s4spa3stad2stafs4tags4taks4tap2stas4staus4tav4staz2st5c4stea4stecs4thast1hi2stia2stibs4tim2stiv2stob2stocs4tols4tops4tov1s4tr3strust3scst5sest3sfst3skst3slst3sost5spst5st4stub4stuc5stuk4stus2st3w2s4ty1styls5typsuba4sub5esu5bl5suik5survsus3esuur5sve4r1sys5t4aalt3aap3tabe3tablta2cata4de5tadot3adr2taf_4tafft4afr4tafztag3r5taka5takgta3kl5takn5takp5taks4talbta3li4talt4tamb4tamp5tan_t4ape5tapita3pl5tapota3ra5tari3tarw5tasa5tasj5tasota3sy4tatatat5j3tatr4tautt3a2ztba2lt5chat5chet5chit5chu4t3d2tdo3v4tecot3edutee4g4teekteem13teertee4tt5eff3tefl4teigt4ein5teit4tekk3teko3tekste3kwtel3ftel5ktels42temb4temmtene23tenh3tent5tenu3terj3termtes3mte3sote3tate5tr5tevl3tevr3tex_4texp4t3g2tger42t1hat3haat4hadt3hakt5hamt3hart3hav5theat3heb5theo3thes3thett4hint1hoet2hogt3hokt1hoot1hul4thumt4hurti5abti5aeti3ap5tica5tice5tici5ticuti3d45tie_5tiep5tiesti1euti3feti3frti2ga4tigm5tijdtije45tijntij5pti3koti5kr4tils5timm5timo4tinft3inht3inq4tinrti3nu4tinwti5omti5sati3slti3so5titeti3th5tiviti4vo2t1jat5jaat5jeet5jekt3jent5jet4tjeut1jou4t3k2t5le_5tlebt5lestli4ntmos5tne4rtnes4to3acto3arto5bl1tochto3da3toejtoe5k5toen3toer3toev5toeztof5dto4fr3togn5togrtok3sto3lato5let3olf2toli5tolot3oly4tom_t3omlto3mo5ton_4tondto3no5tonstoo4m5toont4op_to3pi2topmto4pot4oppto4pu5tor_to3rat3ordt4oritor3ktoro45torr3tors3tos4to3sato1sl5totato3tr3tourto3w44t3p4tpi3s3tra_3trag5trau4trea2trec3tref4trelt4reutrig22trij3trogt4roï5trojt4ros3troutro5v5truf4trugt3rukt4rumtsa4gts1amt3sapts3astse4dt2sijts3ja3tsjits2mets4nots3nuts3obtso2lts3omts1onts1ovt3spit3stat3stets3tht1stots5tyt4su4ts3urts3usts3uut5t4atte2nttop2t5t4rt5tumtt3uu3tuch3tu3e5tueutu3és3tuig5tuin4tuip2tuit5tune5tunn5turbtu3ritut3jtu3wat2winua5neu5ar_uar5tua3saub3acub5emub5oru1cheuc4kiucle3u5da_ud5amud3eiudi4oudoe2ud3ooud3ovu4d1ruds4mud1stue2cou1ee4u4eneue3stu5eulu1f4rug2doug4drug3ijug3oru2g1rugs4pui5acuid4suid3uui2fauif1luif5rui2fuuig1lui4guui2koui2kuui2lauil5muin5gui2nouis5cui4sluis5pui4st1uitguit1j3uitl3uitw3uitzuk3asu2k3lu2k3nu2k3ou3kocuko2pu4k3rul3aculam4ula4pul3flul5foul3frul5keu3losul2paul4piul2poul3saul3soum3afum3ar3umdaumee4umes4um3omum3opum3soum3stun3acund4sune4t1univuno3gun2tjuo3ruuota3upe3ku3polup3omup3opup4trur1acuras3urd4ou1r2eu4remure4nu3resur2faur3giur3oruro5sur5prur2slur2snur4spur3taur3uiu1r4y4usaaus3adus1apu5sieu4s5lu2s5nus3oïus3osu2s3pus5pius5puus4tau4stiustu4ut1acut3afu3tanu4tekut3emut3exut3houto5futo5suts2mut1snut3sput2stut5suuur5iuur3kuut3auut3ru3waguw3aruw3ecuwe5duwes4u3woeuzie2ût3s4va3deva3g4va2kiva4klva2koval5mva3lovalu5vand4va3nova3reva5seva3suva3tevat5jvee4lvees4ve3level3kvem4ave4na5vendven5k2venrver1aver3over5pver1uve3ryve2s3ve2tj5ve5zvi3euvijf5vik4sving4vi5omvi1sovis5pvi4stvi1trvjet1v3larv3lovvoge4vo2levo2livol5pvoo5dvo3ravot3jvous53v4r2vrei5vues4vul5pwaad3w2aarwa4b3wa2bawa5blwa3drwa2law5arc5wardwa2si1watewat5jwa3trw4doo2we2cwede4weg3lwei3swe3liwe2lowel3swem3awe3mewena4we3niwen3ower2f4wergwer4swe2s33wet_we2thwie4t3wijdwij4sw4ing2winrwin2swit3jwit3rwn3acwoes3wol3aws3a2w3somws2plw4sprw1s4twtes3xan5txen4dxe3roxie4txo3noxo3s4xpre2x2takxtie2ya3s4ycho3ydi3aydro3yl3alylo3lym5payn3ery3p4hypot4yp5siy3r4ey1s4ay3s4cy5s4eyse5ty3s4fy3s4hy3s4oy3s4pys5plys4tays5tryt3huy2tofytop4y3u2rza3f2zak3rzan2dza3poza3s4zee3kze5gezen5kze3rozer2sze4s3zes5ezes5lze4tizeve2zik2wzin4szi3o5zipi3zit3ezit3jzodi5zo3f2zo5iezo3lazome4zo2nazot3hzo3trzz3inzz3orz4z5w",
          6 : "_ac5re_al3ee_al5ko_al5ma_al3om_al4st_ana3s_an3d2_an3en_an3gl_an5th_ar5tr_ave5n_be3la_be5ra_be5ri_co3ro_daar5_da4gi_dag5r_debe4_dek5l_dek5s_de5od_de3ro_die4p_doet3_eest3_ei5sc_ei3sp_el4s5_en5th_ere5s_erf3l_er3in_erts3_es5pe_es5tr_eten4_fel4s_gaat5_gea5v_ge5le_ge5ne_ge5no_ge3ra_ge5sk_ge5ta_ge5tj_ge5to_goot3_ho4lo_ide5o_ijs5l_ijs3p_ijs3t_in5d4_in3g4_in5gr_in5kr_in5kw_in3s4_in5sl_in5st_in5ta_koot5_ko5pe_kop5l_le4b5_leg3o_le5r4_le4s3_le5th_lin5d_loot3_lo4s1_me5la_me5ni_me4st_moot3_naat5_na3s4_nee5s_nep3a_ne5te_noot5_nos5t_oe4r5_oe4s5_oeve4_omme3_ono5v_on2t3_ont5s_op5ee_peri5_po4st_puit4_ran4d_ren4o_ro4st_se5re_side3_sneu3_so2k3_song5_ste4m_te3le_te3no_te3ra_ter5s_tin3a_tin3e_to4lo_ve4r3_ves5p_vet3j_vie4r_vol5s_we4l3_win4s_zooi5aag3saaag5soaag3spaak3e2aak3spaal5a2aal1o2aal3slaal5soaand4raan1e2aan5k4aan3sp3aantaaap3o2aar3e4aar1o2aas5trabat4sab5eunab3ijzabot4jace3st2a1che4a1chiac5resada2r3ade5rea5detaadi3aladi4ocadi4odad3reia3d4riad3rolad1s4tad5staae4s5ta2f3acaf5d4wafon4daf5orgag1a2dag3a2magee5tager4sag3indagi5ota4g3orag4o3vag5rapag4sleag5sluag3speag3spiag3staag5stra2g3uiag3u4ra2g3uuahe5riai4s5laïs3o4a4k3ara4k3edak3emiak3ink4a2k3lak3o2pak5speak5t4wa2k3u4al3adra3l4aga5lapral3artal3effa2l3elale5roale4tjal4fenal5fonal3intalk5eial5kleal4kuial4maca4l3olal3sanal3scrals5lial3thaalt4stal3uital3u4ralu2s5a4m3acam3adram3artame5tjam3oliam4pleam4s3oam4spra2m3uian3algan4a3nan3arcanda4dan4dexan4domanen3ian3estane3usan4gananga5pang5leaniet33animaan5ionan4kaaanka4nan2k3jan4kluank3ofan2k3ran3ochan3orkano3t4a4n3ouan3sanans3cran4segan4sidan2so4ans5orans3pian4tacante4nant5slanze5sap3as_ap3assap3ijzap3o4v4appena4premap2s3lara3s4ard3acard3akar2d1rar4draard3re5a2reaare4noare3sparij3sar3insark3acar3k4lar4mapa2r3obar3ogearo4koar3oogar5schar3scrar5seear4slaar3sniar5spoars3taar4strart4aaar4tanar4tapar3tarar4teiar5tij4ar4tjar5tofar2t3rar4troart5ruart4slarwe3sa4s3egaser5aase5tjaseve44as3taa4sta_as5tagas4tasas4tata3steka3stemas5tenas3tobast3opat3adeat3af_at3anka5tellate3noati5niatjes5at3oogatos5fato3stat3racat3reiat3ribat4roeat2s3lat4sloat4sneats3pra2t3uiaure3uau4s5pau3stoauto3p2auts3avast4aver3aave3re1a4vonbad3arba4d3rba3g4hbak4spba3lanba4larbal3dwbal3evba3liëbal4klbal3sfba5n2aban4klban4krbank3wba3trobben3abe5dwebe5dwibe5dwobei5tjbe3k4lbe3larbel5drbe4lexbel3scbel3spbe3nepbe5n4ober4glber4grbe1r4obero5vbes5acbe4sjebe3t4hbe5tonbe5twibe3undbeur4sbie4libij3g4bij5k4bij1s2bil3s2bin4drbin4stbi3osobit4se2b5lapble2t3blijs44b5loiblok5lboe4knboe4koboe3stbo3f4lbok3anbokje5bok4stbo2m3oboot3jbo5scobos5tobot4spbot4stbou5tabouw5sbrie4tb2s5lab4stijbuit4jbune5tcal4l3came5rca4praca5prica3s2pcas5trcate4nca3t4hcau4stceles5ce4l3oce3s2ace3s2hce3stacesu5rce4t3jcet3ogcet3oo5chauf5chef_5chefs5chemiche5riche3ruche3usc4k3edcke5rec5k4etc2k3o42co1no2co1p2cor4drco4relct3actctee5tcte2n3c4t3ofc2t1onct3rapc4t3recuit5ecula5p5cur3sdaar5e2d3accda5denda4g3rda4kerda4k1rd3alarda2l3uda5macdames3dam4pl2da2nadan3asdank3ldan4sidan4smdan4stda2r3adar4modar5stda3stu4d5atl4d5atmda2t3r4d1autddags4dden5addera4ddere4dder3od5dles5dedirde4ditdee4g3deel3i4d3een4d3eff4d5eg_4d5egg2d5egydek3ludel4aadel5dadel5dr4delemde4levdels3idel4soden4acden3eiden3evde4nocden3sh5denvlde5ofodeo4lide3rabde3rakde3ramde3rande3rapde3rasde4repde4retde5rijder3k4der3ondero4rder5thder5twde2r3ude3rupde3savde3spede4spldes5smde4stide3us_deu4tj4d1exadge3ladgeto4dge4trdheer43d4hi_di4anodia3s4di4atrdi3esrdie3stdiet3rdig5aadiges5dijk3r2d3ijz2d3impdi5n2a2d3ind4d3inj2d5inr2d3ins4d3int2d3inv2d3inw2d3inzdi4onedi4onidio5sc2d3irrdis5agdis4krdis5trdlot4s4d3obsd5oefe4d5oev2do2lid4olindolk5s5dol5s3d4om_dom4sn5d4onndo3pee4d1opl4d5orgdo4riëdors5mdo3stadpren4d3raamd3raapd5race5drachd3rad_d3rada5d4ragd4ramad3rame4d3raz4d1recd5reco4drendd4ress3d2rev5drevedries45d2rifdri5gad3rijdd3rijkd3rijmd3rijs5d4rin4d3ritd3roer5d2rog4d3rokd3romad3rond3droog4droosdrug4sd3ruimd3ruit4d3rusd2s1a2ds4ated5schids3ecod4s3edd4s5eeds3eisds3elfdse4lid4s3esd2s1i2d4s5iddsig5ads4ingds5jonds5lasds5licds5limd3slinds4makd3smijds5neud5spand5specd4s3pld5spoed5spokd5spord4stabds3takds4tand5stavds4te_d5steed4stekds4terd4stevd3s4tid4stitds3ure4duit_d3uitd5duite4duitgd3uitvdu5wendvaat5dve5nadvies53d2weidy2s4te5ademead3s2ead5shea5s4eease5tec4taae3d4ane4d4ased3ei_ede5leedi3aledi3ame3d4ooed3opved3roded3roled3uite5dwanee5cheee2d3aeed4aceed5aseed3rueed3sieef3acee4gapee5kaaeek3akee5keteek3reee3krieek3roeek5stee3ladeel4eeee5lijeel5k4ee3lobeel3ogee3lu4eel3ureel3uueena4reen3e2een5ieeep3aneep3rueer3aaee4radeera4lee3ramee3ranee4reeee5reiee4r3iee5riceer3ogee3rotee5schees5etees5loee3s4pees5plee3stueet5aaee3talee3taneetna4eet3ogeet3ooeeto4ree4troeet3spefde5lefie4tef3inse3fis5ef3looef3rije5froneg3as_ega5skeg3ebbe4ge4ceg3eigege4raege4roeg3ijzeg3orgeg3oude5grafeg5slee4g3uueheis5eid4sceien5seie5re4eild4eil5drei4levei2l3oein4doein5grein5slei5shaei3sloei4tooeit4s3eits5ceits5nek3aanekaat4ek3af_ek3al_ek3altek3ange5ker_e5kersekes4tekla4mek3leve5klime4k3obek3oliek3opzek5os_ek5osse5kranek3rozek5setek4strek5t4eek3uitek3winel3aanel4adeel3adjel3admel3adrel3advel1a4fel5anae5lap_e4lappel3arbel3arcel3armel3artel3asie4lautel5aziel4decel3eeuel5effe5leidel3eig3e2lemel3empe5l4eneler4sel3erveles4tele4trel3excelfi4delf3s4eli5kwel3impe3lingel5inzel4keee5loepel3oesel3omsel5ondel5onte5loode5loosel3opsel5optel5opvel3o2rel5orgelot4jel4s3kel5tweel3uiteluks5e4manaema3scema5toemees5emer4sem3oliem3orge4mo4vem4sliem3uiten3aape3naare2n1aken3al_en3alsen3amben4ameen1a2pe5narien3arsenas3p3ency_en5daaen3d4oenede4en3eedenee5ten5eg_en5eggen3elaen3elfen3emae2ne2pen3epoe5nere5energe4nerven3etaen3eteen5grieng5seeng3sme5nijde2n3imen3k2aen3offe2n1onenoot5e3nor_en3orde2n1oven5sceen4seiensek53ensemens4feen4sinen1s2pen4tacen5teeen5teien1t2hen5tomen3treent4slents3me4n1uie4o3k4eop4laeo3p2rep3aakep5akeep3aspep5eenep3ijsep3ijzep3insepit4sep3lede5ploeep3luseprot4ep4sereps3taeps5toeps3trep4takept3raep5troep3uite5raader3aane5raate4r1ace5rac_e5racee5racoe5rad_er3adoe3raffer3amaer3anae5raneer3arce3raree3rarie1rat4er3azier3d2aer3d4ier3d2rer3d4wer5eater3eene5reeper3eeter5effer5eg_er3egder5egger5egter3eieer3eiger3eilere3kler3elker3empe3rendere4nee3renme3rentere4oger3epier3e2qer3erie3res_er3eske3ressere4ster3etne4r5exeri5aberig5aer3ijler3ijser3ijver5inder3inser3inter3m4ier3oefe5roeper3oeve1ro2le5rol_er3olie5rolle3ron_e3roneer3onver3ooger3oore5roose4r3operop3ae2r3orer3oxier4pluer3screr3t2her5t4ier5t4oert5seerts5ler3t4uer3t4we3rug5er3uite3runse4r3ur3ervares3arre3sa3se3scope3s2cres5eenes5enees5je_es5jese3s4joes5jone4s3kae5sla_e5slages3lakes5lates5leges4muie3s4nees3orees5pases4pele3stake3stapes4tares4teaes3tenes3teres5teses4tete3steues4tice4stiee3stotest4sces4turet3aanet3afz3e2tage5tak_et4anae5tande4tappet3edie5tel_e5telset5embet5emmete3roet3hore5toevet3opeet3opleto3sfet3rece3troee5trone5trooetros4e4t3ruet5sluet3speets3pret3spuet4steet5stiet5suueudi5oeugd3reu3g2reu4lereu4radeu4receu3reneu4reseu4rijeuro5veur4sueu5scheu3spaeu5streu5wineval4sevari5eve4loeve3raewen4s2ex3aa4e3zenezers5ëro1g2ëts3tef3aanb2f3a2p3fa5sefbe5dwfdek3lfde4s3fdes5efdes5lfde5smfdes5tf2d3inf3d4rufec4trfe4delfel5drfe4lomfel3spfe3rabfe3ranfe3romfe3ronfe4t3jfetu5rfge5r4fi3apafi3apo2f1ijzfik4st4f3laaflet3j3f4lorflu4t3foe5tafon5tefop5s43fo5re2f3oudfraam5frie4sfrie4t4f3rolf4rolof3romaf4s3ecf4s5eef5slaaf5slacf5slagfs3lapf2s1o4fs4prefst3asfs5tecf5stiff4stonfta4klft3artf5tondf4tontft2s3lfum3ac4g3adm4g3afs4g3afw2g3a4hga5lerg4a3mi4g3arb2g3artgar5tjga3sliga5slogas3trgd3artgd5ateg5der_gd3ervg4d3idgea3drgea5nage4ari4g3eb_gedi3age4ditgeet3a2g3effgege4s2g3eikgeit3jge3k4age5k4lgek4stge3k4wge3lauge3l4egel5sigel3slgel3sp4g3emf2g3empge3m4uge3nakgen4az3ge3nege4noggeo5pegera4pge5regge3remge1r2oger4ofge5rolger4spge3r4u3ge1s44ge3skge5spoge3strget4aage5tamge5t4ige3t4jge5trage5troge5truge5tsjge5t4wgge3lagie5ragier4s4g3inb4g3infg5infeg5infr2g3inhgip4st1gla4sglas3e3g4lazg5leerglee5t2g5lep4g5ler3gle4tglet3jg5liceg5lichg5lijs3g4lio4g3long3loon3g2losgo4d3agod4s3gods5tgo3f2r2g3ong2g1ont2g3oor4go4rego5re_5g4orig4ram_gram3ag3rampgra4s32g3rec2g3red5gredig5redug3reekg3reelg4reepg3reis4g3rek2g3remgren4sg5rijdg5rijkg5rijmg5ring5g4risgrit5s2g3rivg3rookg3room2g3rugg3ruimgs3altgs3ecog4s3edgs5eengs5enegs3ervg2s1i2gs5lamgs5lasg3slepg4sleugs5liegs4lings5loggs5lokgs5long4s5mag3snijg4s1o4g5som_gs5onsg3specg3spieg3spilgs5pirgs5polgs5tacg5stadg5statg5stedg5steeg3steigs3tekg5stelg3steng3sterg5stofg5stopg5storg4strug2s1u4gsver3g2t3apgte3rogte3stgut4sthal2f1han4drhan3gahang5lhang5shan3sohan4sthap4sehar4tahart3jha2t3r4have_hee3g4heek3aheek5lheep4shei5tjhe2k3ahek4sthel3smhen4krhe3n4ohe4pijhe2p3lher4aahe4r3ihe3roshero5v3hersthe2s5theu5lehie4f3hie4r3hier5uhie4trhiet5shij4slhik4s5him4plhim4prhi2p5lhit4sthoes5lhon3drhond4shool3ehoort4ho2p3ohor5dehor4sthot4sthrok3ohroot3h4t1a2ht3alah5tansh4t3echt4ecoh2t3eeh2t3efh2t3eihter3aht5eveh5tevoht5oefht5rooht4sapht4serht5slaht3smeht4s3oht3spehts3plht3sprht4stihur4t5huur5si2a3f4i2a3g2i3ake4ia4kemi4a3lai2a1p4ia3staia3t2hi5atriiboot4i4dee_idi5abi2di5oid4makid3ranid4s3aid4serids5maid4s3oids3taid4stiids5trid3u4rie4droie3fleie3fonie4gasiek3liie5kluiek4spie3kwaie5lanie5lapiel5doiel3sciem3ovien4drien3ijien5spie5peniepou5iep3s4iep5stiep5trie4puiie3ramie3rapier3asie4ratie3r2oie4rofier4slier4uiie5sleies3liie2s3nie2so4ie3staie3stoie4taaie5talie5tenie3to4ie4tooie4topie4toriet3uriet3uuie3twiieu3spif4taaif4tarif4treiftu5ri4g5avig3eski4gindi3g4omig3stoik3aarike4rai4k3loi4k3lui4k5naik5o2gik3opeik3ordik3s4lik3snoik4spaik5staik5waril5aanil4acti5landil4d3ril3eenilet5rilie5gilie5til3inkilk3s2illa3sil4minilo4geil3ondi5loonil3oorilo4reilo4veil3s2hil4stii4magoim3eeni4m3emim3enci2m3ofim3orgind4aaind3scin3ediin3eedinet4sin2ga4ing3aaing3aging3al3inganing5loing4stini5onini5sl3inkomin4kriin4o2gino5pein5schin3smiin5spoin5swiintes51int4rinuut3i5oleni5olusion4s3ions5ci3o5sei3o5sfi5osi_io5s4ti5o5sui2p1aci4perwip4sleire3stir5stei4s3adis3a2gi2s1ari2s3asi5schai5schris5coli5scooi4s3eiis3ellis5engise3stiset3jis4feeis4feri2s3imis5lagis5lasis5nedis5nijis4ooris3ottis5pasi3stakist3apis4tatis5triit3eenite3stit3hieit5oefit3oogi3t2oui4to4vit3redit3sjeit3sliit3sopits4teit4tooium3a4iven5sive3reï3n4urï5schejan4stj2d3aaj4d3arj2d3eejden4sjde3spjde5stj4d3rej4d1rij4d3roj4d3rujd5seijd3spojec4taje2na2je3n4ojer3sp5jesalje5sch3jesknjes5pajes4prjes5tr5jesvo3jeswa3jeswijet3erjeto4vjet5stj2f3eij4f3ijjf3inkj2f3o4j3f4raj3f4rojfs5pajf4stajf4stijg4s5eji5t2jjk3arbj3klaajk5lakjk5lapjk5lasj5kledjk5lesj3klonjk5lopjk5lucj2k3ofj2k3onj2ko4pjk3opbjk3opejk3oplj3kopsjk3raaj5kranj4k5rujk3slojks3pljk4staj2k3uijl5anaj2l3efj2l3eljl3inkj2m3afj5m4arj2n1a4j3na5gjna5mej3n4anjn5d2rj4n3imj2n1o4jn2s3ljn3slujns5orjns3pljo5lijjou5rej4p3acjp3armj2p3emj2p3orjp3rokj5selij4s5emjs3leejs5liejs5meljs5metj4s1o4js3pacjs3parjs3pooj5sporj4starj2s3tej3steejs4tijj4stoojs3touj3taaljt3aarjt3optj5tredj5treejt3reij5trekj5trokjt3rotjver4sjvie5sk3aanbk3aanl5kaart4k3adm3k4aft2k3albka3l4ikalk3akamen4kam4pakam4plkam4prka5naakan4slkan4st4kappak4a3rokar3tr4k3asika3strka4tan2k1aut2k3eenkeer4skei3s4ke4lapkel5dakel5drke5lel4kelemke4lomkel3sp5k4emake4nauke5nenke2n1o4k3e4qke3ramker3klker4knker4kuker4kwker4noker3o4ke3rosker4sm4kerva4kerwtke3s4pke3stake3sto5ketelke2t3jke2t3r2k3e2zkie4spkie4tjkieze4kijk5l4k1ijzkilo5v4kindukin3en2k3inhkinie4k3inko4k1inr2k1ins2k3int4k3invki2p3lki3s4pkker4skke3stk3ladikla2p15klas_5klassk3lastk3lat_k3latt3k4lav3k4led5kledi5kleed4k5leg4k5lenk3ler_4klerak3lers2k3lij4klijskli4me3k4lin5klok_k5lokak3lokek3lood5kloofk3lope2k5loz4kluih3k4nar5knie_4k5nivk3note2k5oct4k1oefkoe3tj5ko5grkol2e2kolen3ko2m3ak3omslkonge4k3ontb2k1oogkoot4j4k3opd3ko5pikor5do2k1org2k3orkkor4takor4tr4k3os_kot4stk4plamkpren4k5raad4k5radk5rand2k1rea2k3reck4ree4k5reepkreet32k3rel2k1rick3rijkk3rijpkrij4tk5ritmkron3t5kroonkrop3akro4tok3ro5v5kruiskrul5aks3almks5ei_k4servks3labk4slank5songk2s3pak4sparks3pook5sporks3potks3pruks5teck3stenkste4rks5tonk5stook4stopk5stotks3trik3stuekt3aank3taarktaat5kt3artkt3ecokt5ordkt5orgkt5orikt3o4vkt3resktro3s3k4u2n4k5uni2k3wac5k2wal5k2wam3k4wark5warek3weer4k1wer5kwetsk3wijzk3wind4laandl3aanhlaa5rel3abon5lach_la4cha5lachela2d5ala4detla2d3o4la2dr4l3afsla2g3alag5sala2k3a4la2nalan3aclan4dalanel5lang5llank3wla4norlans3llan4stlap3aclap3o4la5prela2p3ular3da4larm_lar5stlas3a4las3to5lastt4lats4lat3sllau4stla4zijlber4tlboot4lce4l5ldaat5l2d3acld3alfl4da4rld3arcld3arild3artld3ecoldeks5ld5oefld3olil2d3oml2d3onld3oogl4do4pld3opild3ordld3ramld3ratl5dreeld3rijld3roeld3rolld3romld3ruild3smald5steld3uitle4aneleba4lleege4leeg5i4leekhleep3olees5elees5llega5sleg3ecle5go_3leidi4leierlei5tjleit5sle4ko4len5kwlen3oplen3sflen3sm3le1rale5reiler5g4le3r4ole4ronler4slles5etle3s4hle3speles4tale3strle4s3ule4t4hle3thale5tinle4tople2t3rlet4stle2t3uleu3koleum3aleur4ol3f4aglf3enel2fe2zl3f4lolf3o4llf5ordlf5orglfs5eilfs3lelf2s3ml5gaarl3g4oelid3s4lie4grlie3kalie4splie4tolijk3a4lijmv3lij2s4l3ijzlik5spli3kwilin4da4l3inhl3inna2l3insli2p3lli5seeli1t2hlit4salit4sllit4stlk3armlk3artl4k3eil4k3emlken5elken4sl4k3eplking4lk3laalk3lagl5klasl5klimlk3ontlkooi5lk3opblk3replk3reslk3rijl2k3rolk3sonlks3oolk3stelks5trl4k3uulla3g4lla5trll3eiglle5thlmaat5lm3arclm3artlma3s2lm3edil4m3eplme5telm3olilmro4zlo4booloed3rloe4grlo4faalof5d2lof4s4log2s3lon4grloo5pi3looshloot3e2l3oph2l3opllop4lalo3p2r4l3opv4l3opw3l4or_4l1org3l4orslo3spelos5tolo5s2ulo4tetlo2t3hlo4toflp3aanlp3a4gl5pinglp3insl3p4lal4plamlp3opelp3ramls1a2dls3a2gls4corls3ecol3s2hil4s3imls3injls3inkls3intl3s2kil2s4lels5ledls5leels5legls5lenl2s3lils4linls4medls4meel3smidls3norls3o4rls3pacl3spanls3parls3plil3spool3sporls3pral4stafl4stakl4stekl4stevl5tamel5t4anlt4hanl4t3hil2t3holt3oliltra3slt3rugluids35luie_luks3tlu3stalut4stlven5slvera4lzooi52m3adv2m3afs4m3afwma3l4ama5lacmal5st5m4an_man3acm3analman5daman5domand4s5m4ann5man2sman4seman4somans3pman4thmant4rm4a5ri5m4arkmar3shmar5tima4stema3str5materma4tommat4stmbo5st5media5mediumee3lomee5remee5rime3g2amega5smei5tjmel5drmel4komel4krmen4asme5norment3wme3p2j2m3e2qme4rapme3raume4ravmer3eimer4klmer4knmer4kwmer5ocme3roome3rotmer4simer4slmers5mme2ru4me3s4hme4s4lmes5lime5slomes3pame5spemesto4me3stume3t4hmf5liemie5klmie3st4m3ijs4m3ijzmimie4mi5nar2m5inr2m3ins4m3inwmi2s3imi3t4amit4stmoed4smoes3pmo4lie4m3omvmond3rmon4somon5tamo3r4emor4spmor4stmo4s5lmo3stamo3t2hmot3olmot4st2m3oudmou4wimp3achm4p3afmp3armm4p3ecmp3insmp3lamm5planmp3legmp3leimp3levmp3liempon4gmp3opemp3recmp3redm5presm5p4sems3anams3lenms3liems3neems5tecm5stelm5stenms5tocmte5remunt3jmus5tamvari5mver3e5n4aam4n1aann4aar_5naars5naast4n3actna5d4a3nade_3nades4n3admna5dra2n1advn2a3g4na3k4l3n4ale5nalen4n3alf4nalysn3a2na5nant_5nantenap3acna3p4rnap5st2n1arb5nares2n3arg2n1armnar4st4n1art3na3sana1s4lna3stana3stu3n4ati4n3atlnat3sp5naven3nazifna4zijnbe5stnces4tnch5trnd3aannd5aasnd3abond3actnd3adrnd3alfnd3almnd3artnd3assnda3stn4d1ein5den_ndera4n4dijsn3d2jin4d5ofnd3olind3omdn5donand5ondn5donsnd3ontnd3oognd3opend3oppnd5rapnd3ratnd4rekn4dresnd3rotnd3rugnd4secnd5setnd3s4ind3sjond4spond3uitnd5uren4d3uunebe4sne4ditneel5dneel3o4n1eennee5rinee5seneet3aneet5oneet3rneet5s4n1effne4gel5neienn5eier5neigd5nei5tne4k3r4nelem4n3emb5n4eme4n3emm4n3emp3n4en_nen5done5nignen5k4nen1o4nep3agnepi3snera4dn3erfene3rosner4slner4spner4stness5aness5tne3stanes3tene4ternet3onnet4sine3umsng3anan4ga4pnga5slnge4adng3embn4gigsn4gindng3inkng5ladng5lamng5lanng5ledng5leung5linng5lopn2g1onng5oorng3oreng3orgng3racng3radng3rain4grasng4redn4g4ring5rieng3rijn5gronng3ruings5lung3uitni3erinie4trnig3ra2n3ijzniks3p2n3in_2n1indning3r2n3inh2n1ins2n1int2n3invni4on_ni4oneni4sauni4selni3sfeni2s3ini3sotnis5toni3t2hnje5spnje5stnk3aann5k4amnkar5snk3effnk3empnken4enk3ladnk3lodnk3lucnk3lusn2k3nan4ko4gnk3ogenkoot5n4krimnk3rolnk3s4mnk4s5onk3waank3weznnee5tnne3nennepo4nne5tennet4jnnoot5noen5snoet5sn5offin3o2geno3k2w4n3om_2n3omw3n2on_3n4onb4n5ondn4o5ni4n5oof4n1oog3noot3noot4j3no3pa4n3opbno4poono4por2n3opz2n1orgnpi4s5npoor4ns3a4dns3alpn3sancn5schonsee5tns5egens3eisns5empns3idin5singns3injns3inkns3intn5sla_n5slagn5slepns4letn5sleun5slibns3lien5slimn5slipns5macn3s4men3smijn3smolns3nodn4snoon4snotn2s3obns3ongns3onzns4oppn2s3ouns3padn5speen5spelns3pern4spetns3poln4spotn3stalns5tecns5tesns3then3stign4stonn3storns3uiln5taalnt3achnt4actnt3agan5t4atn4t3einte4lon5te2nn5tholnt3inwnt5oognt3oplnt3opmnt3optnt3recnt3reint3relntre4snt5rijnt4rount3rusnt5slunt4snont4sprnt5ste4n3uilnu2m3anu4s3onut4stnu2w3inve5nanzet5sobalt31o4bliob5oorocaat55o2ceaoco3s4ode4moode5reod5seiod3s4iod3sliod4s3ood3spood4sprod5staod4steoe2d3aoeda4doede4noed3reoed3rioed3rooe2d3uoe4f1aoef3laoef5leoef3looe2f3roege3loeg5ijoe4gouoei5s4oei5tjoei3troe4kaaoeke4toe2k3loe4k3roe4lapoe4laroel5droe3lemoe5loeoelo5poel3spoem3o4oen3aloe5n4eoen5groen4snoe5plooe4p3roe3praoeps3eoe2p3uoe4raaoe3roeoer3ogoer5omoer4sloer4spoe3sfeoe4slioes4taoes4thoe3stooe4taaoe2t3hoe5t4ioe5toeofd3eiof2d3oof2d3ro3f2raof5slaofs3leof3speofs3plof3spoofs3profs3troft3uroft3uuog3al_oge4roog3staog3stoog4strois5tjok3ankok5letok3o2lok3op_o2k3ouok3s4lok1st4o3l4abol3a2pol3armol3d4ool3d2wol3eksol3emmole3umol3exaolf5slol2g1ool4greol2g3uo5lingol3intol3kafollie4ol3op_ol3oppolo4veol4praol4s5hol3s4lol3s4nol3uito4m3efom3elaomen4some5spo4n3amona3thon5derond3reond3roond5sjon3d4uon4duron3erfon3ervone3st4onet_on1e3vong3ap4ongenong5leong5seong3spong3ston3k2ionnes4o4n3ofo2n1ovon5seion3s4mon2s3nons5opon3soron1s2pons4peon3splon5stron4taa3ont1hon4tidont5sp1ont3wood1e4oo5de_ood3slood3spoog3shoog3slook3s4ook5stoo4k5wool5a2ool3edoo5ligool3ijool1o4oom5a4oom1o4oon5duoon5k4oon5taoop5eeoop3o4oop4spoor1e4oor3g4oor3smoor4thoot3esoot4slo4p3aco4p3afo4p3akop3andop3at_op3att3opbreop5eeto3pen_o5per_o4peraop3e4vop3i2dopie5top3ijzop3in_o5pinaop5losop3ondo5poniop3ontop3ordop3o4vop3ricop5s2c3ops4lop3smaop3staor3achor3actor3admor3anao5rateor4daaord3orore5adore4noo5rig_or3insor5k4eor4masor3ontor1o2por3sagor3slior3smior4sonor5spuor4tofort5sporzet54o3s2co5s4clos5li4os3peros4piros4s5mosta3cos5tanos5taro3staso3statos5te_os4temos5touost3reost3rio3stroot3aarot3aktot3appot3artot3e2do5tee_o5teesote4stot3etao2t1hoot3offot3olvot3ontot3opmoto5poot3opro5t4oro1t4root5s4iot3slaots3liot3smoot4stuou4d1aou4desoue2t3ou4renou2t3oout5spouw5do2o5veeovi5so4p3afdpa4genp4a5gipa2k3a4p4akepa4k5l2p3albpalle4pal4mo5panee5panelpan5sppan4tr3pa3rapar3dapar4kapar5ta3partipart3j3partnparu5r1pa4s3pas4thpas5topas5tr3pa3trp3e2co3pectupee5li1p4eilpek5eepe2k3lpe2k3npel5drpe3l4ipel5sipel3sopel3sp2p3emmpe4nakpe4nappe4naupe4nazp3encypenge5pen3sapen5slpen3smpen5sppen5to2p3epiper4atpe5reqperi3spe3ronpe5rosper4sm3p4hecpie4tjpi2g5apij5kepij4lipi4k3lpilo5gpi5nam3pinda3p4ing5ping_pin4ga4p3injpink3rpink5spin4tapis5tapit4sp2p3ladpla3dip4lant1p4las3p4lat5p4layp3leid3p4len2p3ligp3lonep5loodp3loonp3luie3pneumpoe2s3poes5tpo4kolpo5l4opolo3p2p3oml3pondspon4smpon4stpon5tapoo5de4poog_4poor_po4p3a2p3org2p3orkpor4topo4taappe5nippie5kppij5pp5raad3praktp5rand4p3rapp3remm3prentp3resopret3r4priet3princ5prins3p4rio3p4riu5p4rob3p2roc1p2rodp3roed3proef3proev5p4rof5p2rogpro3lap3roodprooi5pro5papro5sc3proto3pro5vps3a2gps3assps3erkp4s3etp4s3naps3neups3optps3plep3statps5tesps3torpt3albpt3ricpuil3opul4stpunt3jpu2t3oput4stpvari54raand5raar_5raarsra5den5radia3radio4r3adr3rad3sra3fra3ragezra3g2nraket3ra3k4l4r3alfra4manr5ameuran4drran4grra4nimran4klrank3wran4saran4str3antw4rappa2r3arbr4a5re4rarit2r1arm4r3arr2r1artra5seiras3pora4tomra4trara5trirat3sprat4stra3t4ura4zijrbe4tird3alkrd5amar2d3arrde5o4r4d3olrd3ontrd3oosrdo3perd3rasrd3resrd3s4crd5stard5stere4adere3amb4re5atrec5ta2r1eenr5eenhreeps54reersr3eerwree5shrege4s4reindrei5tjre4kapre2k3lre2k5nrek3sprel4direld3rre4mai3r4en_re4naare3nalre5ne_re4nel2r1enire4nocren4ogre3nov5r4enpren4slr4entor3entwre4op43repetre4piere3qua4r1erf2r1ergre3r2o2r3ert4r5ervres5lere2s1pre4temre3t4hre4tikre5tinreus4t3revisr4f3aarf3actrf3levr2f3lirf3lusr4f3opr4f3rer5frear4g3abrg3ambrg4eisr5gen_rge4rar5glasr4g3lurg4o3vr5grijrg3ritr3g4roridde4ri4dolri4doorie5drrie5klrie3kwrie4larie4rorie4tariet3ori4gaar5ijldr5ijltrij3plrij3prrij3spri4k5lri3k4orim4pr4r3inb4r5infring5l4r3inhri4nitr3inko4rinkt4r1inrr3inst4r1invri3o5sri4samri3sotris5torit3ovrit4strk3adrrk3angr2k3eirken4srk5ieprk3ijvrk3inbrkjes5rk3lagrk3loork3lusrk3olmrk3omgrkoot5rk3opgrk3ordrk5os_rk5ossr5krisr5kronrk3s4frk3uitrk3waark5watrk3weerk3winrlofs5rmaf4rr2m3ebr2m5egrm3inhrm3opmrmos5frm3s4arm3uitr5n4amr4n3aprn3arsrnee5trne5ter2n5idr2n1onrn3oorr5nootrn3opsrn3overn3staro3d4oroens4roep3lroet4jr5offiroges5rok3spro2l3arol3g2rol3ovron4daron4kar2o1no4ron2tront3jront3r2r1oorro4paaro4panro5peero4pinrop3shr4opter4o5siro3t2hro5tonrp3aanrp3advrp3ankrp3eisrp5lodrp3ricrp3slirp5sperrie4trron5krrot4jrs3a2drs3a2grs3almrs3ambrs3anarsa4ter5schirseve3r2s3ezrs4ferrs4halr3s2hirs3hotrs3inirs3intr5sjacr5sjour4slanr5slecr5slepr5sleur5slibrs4liers3lobrs5makr3smijrs5misr5smitr2s3nars3neur2s3nors3ongr3spaars3padr5specr5speer5spekr5spitr5spoer5spogr5sponr5spoors3potr5spulrs3putrs5tasr5statr5stesr4stitr4stonrs3usar4t1acrt3af_rt3affr5tansrt3artr5tecort3eigrt3eilrt5embr5ten_rte3norte3ror3therrt3holrtij3krt3offr5tofort3om_rt3ondr4t3opr5torirt4raprt3recr2t4rurt5rukrt5rusrt5seirt2s3lrt3slerts5lirt4slurts5nort4soorude3rr5uitr4ru3kerul3aarul3apru3linrunet3ru3nivru5re_ru5resrus5trrut4strvaat5rve3sprw2t3j5s2aaisaai4s3s2aal3s4aats4a3gi3sa3lasal5ma3s2ame4sa2nasa3nats2a3ne2s3apesa5prosar3ol4s1art3s4ast3sa3te2s3atl2s1att3s2aus5scena3s4ch25schak5schap4schau5sche_s5chec4schef5schen4scheq5scher5schev5schew4schir5schol5schoo5schotsch5ta3scope5scopi3scout4scris4s3ech4s5eed4s1eens5eenhsee5ts4s3ei_4s3eig5sein_5seineseis4tsei5tjsek4st5s4el_sel3adse4lakse4lassel3el5s4elssel3spse2l3usem3oose5nanse4net5sengrse4n3o4s5enqsen5tw5s4er_se1r4aser5ause4reese5ren5sergls5ergo5sergrse5rij4s3ernse5ropsers3pser3stsert5w2s5esk4s3eteset5st4s5etuse4vens5hal_3shampsheid45s4hir3s4hops3hotesie5frsie5klsie5slsie3sosie3stsie5tasie5tosi5go54s1ijz4s3inc4s1ind3sing_s3ingasin3gl4si2nisin5kr4s3inm2s1ins4s5inv4s3inzsis3e4sis5eesis5trsito5vsi4tru3s4ja_2s3je_3s2jeisje4ri3s4jez4sj5k44s3jons4kele3s2kes3s2ki_3skiedskie3sski5scsk3ste3s4la_5slaap4s3lad3s4lag5slagmsla4me3slang5slapesla3pl4s3las2s3lat3s4laz5sleeps4leet4s3leg2s5leis5lengs3leni3slent4s5ler3s4leus5leugs5leus5sleut2s5lev4s3lics5lieds3lief5slijps4li4kslim5as5lini4slinn4s3litslo4b52s3loc3s4loe4s3logs3loods5loons5loosslo4tr4s3lou4s5loz4s5luc1s4lui4sluid5sluit5sluiz2s5lus3smak_2s5mans4mart4s5mat4s5mec3smeed4s5mei4smelo4s5men5smid_smies55s4nap3s4nav3s4nedsnee5t5s4nel2s5nes4s5netsneus4s5neuz1s4nij3s4nip4s5niv3s4noes4nor_s3norm3s4o3d2s1off3so3gaso3lissolo5v3s4om_2s3oms5s4on_so5nar2s1ond3so3no4s3onv4s5oog4s3ooks3oord5soort3s4op_4s5opeso3phis2o5posop4re4s5orkso3rorsor4st3s2ort5spaaks3paal5spaan5spaat5spake3spann4s5pap5spar_s4pari5sparr2spas55spatt5s4pea3s4peespeet34s3pei5spell4s3pens5pen_spe4nas4per_s5peris4perm1s4pie4spijn4spijps5ping5s2pio2s1p4l4s5plas4plets2pli45splin3splits3poes2s3poms4pon_s4ponns4pori4s3pos5spots5sprays5pred5spreis4prek4sprem4spres5spreu4sprik4sprob4sproc4sprof4sprogs4proo4spros5s4puissa1s2s4s5cussei3ss5spaass5pas5staafs4taatst3abo5stads5staf_sta4fo5staki4stakkst3akt5stam_5stamm3stampstan4s4stapo4starist3aut4stavo4s5tax5steaks5tech5steco3s4ted4stedu3steek3steens5teerstee5t5stein5stekk3stell5stem_5stemd5stemm4stemo4stent4stenu4sterms5teru4ste4sst3hedst3heks5thems3thers4t1hos4t1hus4t3hy4sticus4t3ids5tiev4stijdst3ijs3stilsst3impsti5ni4stins4stitest3ivo4s4t1j4stoef3stoel4stoen4stoer4stoes4stoez3s4tof5s4toksto5li4stoma4stomzs4tong3s4too4stora4stordsto5ri4s5tos4stra_s5trag4strais5tref4streg5strel3strepst3rifs5trisst4rom4stroz4st1s42st5t25s4tud4stuin2s4tunst3uni5su4b14s1uit5suit_s5uitl4s1u2nsvaat5svari5sve5risy4n3e3taak_t3aankt3aanw4t3aas3t4acit3adertad4s3t3adve2t3afd5ta3fet3afhata3fro4t1afs2t3afwta4gaa5tagee5tak3rta3laa5tale_5taligtalm3ata4makt3amba5tament3amputa3nagta3nat4t3arb4t1armta2ro4tar5spt3artita3s2pta3stata3str4tatio4t3atl2t1avote3akt5tea4mte4dit4tee4n4t3eeutei4lot5eindtei5tj2t3eiw4tekerte4laptel5da4telec5telef5telegte5lel5televte4loetelo4r4telsetel3sotel5sute4mortem3ov5tempote3nakte4nauten3edten3eltene4tten5k44t5enqten3snten3sptensu45tentaten5tot3entwte4radter3agte3ralte4ranter3apter3as5terecte4reite4relte4rem4terfdter3fr4terk_4terkt5term_5termiter5octe3rodte3rofte3rog5terontero4rte3rostes3tatest3u4t3euvteve4r4t3exetgaat5tge3lat4haant4hans5thee_4t3hei4t3hel3t2hen1t2her4t1hout3houd5thous4t3hovtie5d45tiefstie3knti3enctie5tatie5totie5twtig5aati4gu4tig3urtij5katij4klt3ijs_tij3sttij3t2tij5trtij5tw4t1ijztina4dtin3as4t1indti4nit4t3injt3inko4t3inl4t3ins4t3invti3s4jti4sonti3s4pti3stati1t2rtkars3toe5d4toe5letoe5pl5toeri5toerntoe5sttoe3tj3toetstof5artof3thto4kan5tolaa5tolet5tolicto4lietolk5stolp3r5tomaa3t2one5toneeto5ner3t4ong5tong_3t4oni5t4onnton3sktoom3etop3asto3pento3petto5posto5pust3opvato5rec4t1orgt5orga3toriato4riëto3romto3r2uto1s2ptos5te2t3oudtpe4t35tracé3t4rai5train5trakat3rake3trakt3trans5trap_4t3raz3t4re_5tred_4treda4tredu4t5reg4treizt3reset3resutre2t3t3rib_5tribu5trico5t4riltri5nit3risit3rit_5trodyt3roedt3roes5trofy4trol_5trola5trolo5tromm5tron_5tronat5rond3trone5tronn5trono5tronst3rood5troont4roostro5pi5trotu5truc_5trui_5truiet3ruimts3a2dts5eent4s3eits3intt3sjents4laat3slacts3lamt2s3let5slibt5sloet3s4luts4moets3neuts5norts5notts3olits4oppts1o4rts3padt3spant5spect3spoet3spoots3pott4sprots4prut4start4stast5stedt5steet5stemt5steut1s4tit3stijt5stilts5tints5t4jt3stritte5rit5tlettt3oogtuit4jtu4k3itul5pi3tu4s3tvaat5ube4liuc4t3auc4tinud3essu4de4zud3ezeudi3omud3onduds5louds5maud3smeud3smiud4staud4stiuer3ilu4f3anug4da2ug4derug5sceug4secugs5paug1s4tui2d3aui2d1ouid3spuien4tui2g3oui4g3rui2k3aui4k3luil5aaui4loouil3ovui2m3aui3magui4n1auin5oguin3oruin5toui2p3lui2p3rui2s3aui5sluuit5aauit5alui5tarui2t1o1uit5ruit3sluit3snuit5spu2k3alu3klasuk3s2muk3spauk3spluk4stiul4d3aul5dopul4d3uule5spul3in_u5lingul3innul3k2aul2k3lul3o2pulp3acul2p3lul4p3rul2s3pume3stu2m3uiunch3run4draun4k3run5o2punst3aunst3oun4tagun4t3uupe4rour3aanur3adaur3advur3echur3eenuree5sure5luur3embur3essure3stur3etauri4glur3ijzur3indur3intur4kieur3k4lur5opburs5liur4s5murs5paurs5thur4stiur4trous3a2mu5s2cruse5tjus5tagust3alu2s3teust3oous5trous5truust3urust3uuut3aanutaar5uta3s4ut3eksut5emmut3ooguto3peutop4luto5poutop4rut3saaut3s2cut4spaut4spout3struur3a4uur3e4uur1o2uvel4suve5nauw5artuw3eenu2w3eiu2w3ijuw5ijzu4windu3wingu4winsuw3inzuwo4geuze3t4va2l3ava4loeval5sivan3acvang3avan4grva4nocva3s4ovast3rva2t3hveel5evee3p4ve3g4hvei3s4vei5tjve2n3oven4slven4spve4radvera4gver5dove3recver3edve3regve3reiver5k4ve3romvero5vver5twves5tive2to4vet3ogvet3oove3torve2t3rvid5stvie4s3vies5nvie4tjvings3vis5otvis5trvlot5svol3ijvond5uvooi5tvoorn4vorm3avrie4svrijs4vuur5swaar5ewa3lanwan4grwa2n1owan3s4war4stwart3jwar4towa4s5lwa4s5pwas5trwd3oomwe2d3iwe4d3rwee4kiwee3lowe4g1awe2g3owe4g5rwei5tjwe4k3rwe4le24welemwen3adwe3ne4we4nemwen5tower4kawer4knwer4prwe3spowe2t3jwet4stwe2t3uwie4lawij4kawijs3lwijs3pwind3awi4t3hw2s3lew5spraw4stijxi3staxi3stoxi4t3ixpres5ya4s5pyba2l3yksge4y4l3etym2f5lyvari5zaar5tzags4tza2k3azan3dizan4drzang3szeel5dzeer5szee3s4zeg4slzei3spzel5drze3lemzel2f1zel4soze4ninzen3o4zen4ogze3nonze4r3aze5schze5steze2t3aze2t3hze2t3jze2t3rzeven3ziek3lziek3wziel4szie5slzi2g5azij5klzij3pozij5s4zings3zins3tzit3u4zoet3jzon3sfzon5tazor4glzor4grzui4dr",
          7 : "_aarts5_alko5v_as5tra_de5sta_edel5a_eesto4_gang5s_ge3l4a_gelo5v_ge3n4a_gena5z_ge5r4e_ge5r4o_her5in_hits5t_houd5s_ka4taa_kerk5l_kerk5r_kerk5u_le4g3r_len4s3_meel5d_merk5l_met5ee_ne4t3j_onde4r_on4tee_on4ter_ooi5tj_pee5tj_piet5j_pui5tj_rand5a_re4men_reno5v_rie4t3_rij5sp_roe5tj_ro4t3h_ski3s4_tan4da_ten5ac_toe5pr_tri3s4_tuit5j_uit4je_vaat5j_wee4ko_wee4t3_west5raad5sapaal5f4oaalfo5laal5speaal5steaam4staaam4ste5aandeeaans4poaarts5labak4s5aboot4jach5tecachuut5ad3e4te4a5gen_a4g3insajaars5a4l3achale5stea4l3o4val3s4agal4s3ooal4stemal5stenals5toualtaar5al4t3roament4jame4rana2m3o4vams5te_and5ankan5d4riand5rooands5loan4d3ulange5stang5snaangs4tea4n5islan4k3asa4n3ooran4servans5piran5struap5etenapo5staa5p4risap4ste_araat5ja4r3appar4d3omar4d3opar4d3ovarie4tja2r1o2pars5talar4t3akart5ankart5oogart5steast5remas5tro_ater5adater5slat5jesbat5jeshat5jesmat5jespat4s3a2at4s3ecat4s3idat4staaat4ste_at5stenat5stijats5tolat4t3u4ave4n3iaven5spave3r4ubaar5tjba4k3o4ban4k3aban4k3obe5l4asbe4l3ecbe3lo5vbemen4sbere5s4bes5te_be5stiebet5renbie4t3jbin4t3jbit4s3pblad5ijble5spebloot5jbo4d3ecboe4g3aboet5stbo2m3a4bond4s5bon4t3jbor4staborst5obraad5sbran4dabra5strbrei5s4bron3o4buts5tebuur4tj2ce3n4acen4t3jcer4t3rce3s4ti5chromocier4s53con5t4da2g3a4da4g3edda4g3ondag4s3td3a4matd2a5me4danoot5dan4s3pdans5ta4d3antw4d3a2pedarm5onddag5spddel5evdder5aldder5eedder5epd4e5dendel5eekdel4s3edem5ondden5ateden3e4p4d3engtden3o4rden5strde4r3adder3a4gder5ededer5egdde4r3eide4r3emde5re4nde4r3im4d3erosder4s3ader5steder5sto4d3ertsde5speldes5takde5stalde4s3tede5sticdes5topdget5ondget5ovdie4r3odi2k3o43d4ing_4d3inkodintel5di5ofon2d3i2rodo4m3o45do3n4odover5s4d3rand4d3reek4d3roei2d3ro5v5d4ru4kd4s5lieds5patid5s4peld4s3petd5staatd4s3tald3s4tatd4sterrds5tramea4k3o4eau3s4techt5ecechts5oede5nacede5rogedoe5tjeek5allee4k3loeel4as_eel5d4ueelo4geee4p3reeer5oomeer5stree4s5emees5potees5teneeto4geeet5rokeet5steefiet5jege4netegen5ofeger5onegiste4e2g3u4reil5antei4n3abei3n4aceind5ooein4d3rei3s4laei3s4taeits5tre4k3a4gekes5trek5etereklam5aek5looseks5erveks5trael5aande4l3as_e4l3aspe4l3assel5eierel3ei5sele5r4ae4l3etae4l3etue4l3indel3o4veel4s5emel5smedemie4tje4n3aase5n4acce4n3ange2n3a2sena4tele4n3atte2n3a2zend5amaen5dreke2n3e2cene4tenen4g5lee4ningae4n3inke4n3oche4n3olie4n3oore2n1o2pens5einen5slaken4s3onens5poten5stanen5stenen4stinente5reen4tervent5rolent4s3p5enveloe5o3t4he4p3appep5ingre4p3lodepoot4j3e4pos_ep5rodeep4s5eeeps5taaeps5taleps5troe4raak_er5aanpe4raap_era4gene4r3alle5randae5ra3pler3a4trer5eerser5einde4r3emmeren5eger5enthe5rentoeren5twere4t3je4r3etse4rijs_e4r3ijze4r3inier5inkte4r3oederoe5tjero5pener5slager5spaners4poter5steme4s3a2ge4s5enges5oor_e4spriee3s4tale4s3te_es4teelest5ei_e4stekae3s4temes5temoe4sten_es5tenbes5tra_es5traces5trake5stralest5rapes5treie4t5elfetens5ueten5tjete5r4aeter5sme4t5i4de4t3ince4t3orke4t5resets5lapet4s3ooets5teket5stenet5su5retui5tjeur4staeuw4strevel5opewest5rfan4t3jfant4s5feest5rfe4l3eefe4l3opfe4r3etfil4m3afilm5onflen4stfond5enfonds5lfon5engfor4t3jf4raak_friet5jf4s3ethf5stellfste4m3f4sterrf4st3ocfter5shgaar5tjga4l3apgan4s5tga5sla_gas5trag4d3elfgeest5rgei4l5a4ge4lem5ge3l4ogel5stege4n3edge4nend4g3engtge4n4ofgen5sfegen5stugeorke5ger5aalger5apeger5as_ge5ren_ger5iniger4sli4g3ertsge5sperge5stanges5te_gges5tiggings5gids5te5gigere5gigstegi4onetgmaat5jgne4t3jgnie4tjgo4n3azgraat5jgroet5jgroot5jgs5alarg3s4ke_gs5laagg5slinggs5pandg3s4pelg3s4petg5spin_g5spinng3s4pongs5taalg5s4tang4st3apg5s4te_g5ster_gs5terrg5stersg5s4ticg3s4tiggs5tijggst3o4vg4s3trags5tradgs5trakgst5ramgs5trapg5stratgst5resgs5troegs5trong5struchaar5slhaar5sphaar5tjhaf4t3uhal4stohand5slhan4s3lharte5lhar5trehart5slhee4l3oheeps5chee5stoheids5phe4l3eehel4m3ahel4p3ahe4r3adhe3r4auhie5renhie4t5ohin4t3jhoboot4hoe4kerhoor5trhop4strhor4t3jho4t3reh4ten5tht5entwhte4r5oh4t3eskh4t3intht5slotht5smijhul4deria4s5po5i4cepaichee4tic4t3opict4s5cider4spider4stids5takids5tekid4stemie4d3aciek3e4viek5ondiek4s5niel5d4riel5ei_i5enne_ien3s4mien4staien4striepiet5iep5oogiep5reliepro4sie5r4adier3a4lier5el_ier5elsie5ren_ie5ringier5sluie4s3plies5te_ie5steliet5antie4t3ogieto5reie4t3ovie5troeieu5r4eiezel5aij5e4n3iks5te_i4l3ervil3e4veilevin4i4l3e2zim4s3ooin4deneind5stein3e4deini5staino3s4tin5stenin4t3apioneel4i5othekipe4t3jips5te_ip5steni5scopei4s3ervi4s5tasis5terdis5tereist5ongi5stro_ite5reiitie5stit4ste_ïe5nen_jaar5tjjagers5jan4s3ljbe4l3ijde4n3ejdens5pj4d3ervj3d4wanjepiet5j2f1en5j3f4latjf4s3erjfs5takjf5stanjf4steljf4s5tojger5sljg3s4tejk5aardj4k3o4ljks5takjk5stanj3k4wasj4n3erkj4n3ervj4n3inkjns5lacjn4ste_jraads5j4s3elaj4s3e4rj3s4tekj3s4telj5stondjst5ranj5strokjvers5pjze4r5okade4t5k3a4genkalf4s5kame4rekan4t3j4k3artikast3o4kast5raka5strokas3u4rkat5aalka4t5ioka4t3ogkee4p5lke4l3opke4n3anken4ei_kens5pokepie5tker4kleker4k3rker4n3aker5speker4sprker4staker4sti4k3ertskes5ten4k3e2tukeviet5khoud5skie4s4tkie5stekings5lkits5tekke5neik5leer_5k4le4ukoe4ketkoers5pkom4strkon4t3jkon4t3rkooi5tjko5pen_3k4o4s3kraads54k3redekrijt5jkroet5jksges5tks5pandk5staank5staatk4st3edks5tentkster5ak4sterrks5trekkst5uitk5trollkven4t35k4waalkwen4st5k2wes1kwes5trlaar5tjlach5telacht4sla4gentlam4p3jlam4p5llam4po4lam4s3pland5aalan4d3rla4n3ec5lange_lang5splan4k3alan4k3llan4t3jla4t3hela4t3rolbert5jl4d3e4zl5dradeld5ranglees5polek5strlen3a4kler5spo4l3erts4l3essales5taale3t4releu5steleven4sl4f3endlge4n5alie4g3alie3s4tlij4m3alijst5alim4p3jlin4k3alin4t3jli5o5s4l4k3ankl4k3levlks5taalks5telller5onlle3s4mllevie5lm3a4caloe4d5aloen4stlo4k3arlo2k3o2lom4p3jlom4p3llon4gaalon4g3olon4t3jlo4s5trlot3a4llraads5l4s3e2dl4s3e2plsge4stl3s4kells4maakls5tak_l5straals5trakl5stratl2t3o4vlts5te_2l3u2nimaat5stma5esto5ma3k4rmans5eeman4s3tmans5tamariet5mar4s5tma3s4pomboot4jmee3k4rmee5lasmee5strme4l4asmel5as_mel3s4mmeng5ramen4t3jme4r4amme5rong4m3erosmers5tame5spotme5stelmest5ovme5ta5nm3e4venmi3s4lami5stramis5tromoers5tmoes4temogen4smol4m3amp5artsm4p3ervmp5oliem4s5tonmte5stamuts5te4n3aardnaar5tjnacee5tna3f4luname5stnan4t3j3na3p4lnarie5tnd5adelndags5pn4d3anan4d3a4zn4d3edinde5laan4d3emmnder5alnder5ognde4tenndie4tjnd5ijs_n4d3inknd3s4cund4spran2d3u4rnege4re5n4end_nen5t4a3n2e5rene4r3idners5tenes4teineu5stengaat5jn2g1a2dn4g3eennge4rapnge4rasn4gigern4g3insng5rassngs5lopng4s5neng5strinie5klenie4s3pni4g3eeni5o5s4ni4s3evni5stelnk5aardn4k3arbn4k3aspnker5kunning5rnooi5tjno4p3asnot5a4pn3s4caln3s4laans5laagns5lap_ns5lappns5lot_n4s3paanst3a4gn4st3eins5teksn5sten_ns5tentn5ster_n4stijvnst5oefn5streens5troens5trogn4t3artnte5radnte4rofn4t5olint5ribbn5troosnts5pren4t3uitn5twijfn5t4wis3n4u5rioa4tievo4b5o4rods5lamod5slanod5smakods5te_od5stekod5stenoe2d3o2oe4f5o4oek5erkoe4k3opoe4l3eioe4m3acoep5indoer5aaloer5ei_oer5eieoer4staoe4s3o4oe4t3o4oe4t3raoet4s3pof4d1a4ofs5traoge5laaogel5eioger5onoge4s3tog4stonogs5troo4k3aaso4k3a4zok3o4peok5sprioks5te_ok5stenok4s5trokter4sola3s4molg5rapol4g3riolo3s4tol5sterome5renomer5klo4m3intom4p5eiom4ste_on4d3acon5d4asond5eteon4d3idond5ijsond5om_ond5sloo2n1e2cong5aanong5aapon4k3apon4k3loonne5ston5sten3ont1s4oon3in5oord5aaoor5dopoor5steoor5sto3o4peniop3e4teop3o4reo2p3u2no4r3algor4d3asor4denvord5ondord3o4vor4drado4r3inkor4m3acor4m3eior4n3acorno3s4or4p3acorp4s5cor5s4paor4t3akort5eenor4t3ooor4tredort5steos5jer_os5taalos5taarost3a4gos5toliost3o4vos5tra_os5traaos5trumote4lanoter5spotje5spot4s3paot4ste_ots5tekot5stenou5ren_ou5rennou2r3o2out5steouw5ins3o4vergover5sppaar5dupaar5tjpacht5spalm5acpa4pe4tpar4k5lpei4l3ape3l4aape4l3akpe4l3eepe3l4orpen5d4rpera3s4pe4r5egper5stiper4str5pe5terpe4t3rapets5tepiek5lapie4r3opie4s3ppij4p3apin5griplaat5j4p3lamp4p3langpla4t3rplee5tjpleu5ropmans5tpo2k3i2poo5lenpoor4tj5portefpo4t3aspotes5tppe4l3opraat5j4preekupre4t3j4p3riekproet5jpro4s5tpro3t4aprut3o4p4s3i2dps5tentps5tronp4t3o4v4r3aardraar5tjraf5ond4r3a2lara4l3eer3a4limran4g3oran4t3jrap5roerast5rir4d3actrden5drr4dervarde5s4trd5olierd5roosrede4s3ree3n4erege5ne5rekenire4k3rerel4d3ore4l3eire4lu4rre5mo5vren5aarre5nadere4n3anren3a4r5rendeere5nen_ren5enkren3e4pre5ner_ren5erfren5erv5r4enklren4oplre4t3ooreur5esreu5ster2f3a4gr4f3engr4f3lagr4f3u4rr4g3eenrgel5drrge5rapr4g3insrg4s5prri5abelriel5aarie4lei5rigste4r5ijl_rij4strrin4k3l4r3innarjaars5r4k3artrker4slr4k3ervr4k3inkr4k3latrk5leidr5k4ranrle4g3rrlink4srlui5t4r4m3artrme4t3jrmet5str4n3enerne4t3jroe4g3rroen5smroe4reirole5stron3a4d5r4onalron4d3oron4d3rron4d5uron4stero3p4larop5rakros4s5tro5stelros5trarot4ste3rou5t4ro5verir4p3o4vr4p3recrp4s5torre4l3urren5s4rri5er_rs4asser4s3eisr3s4hocr3s4katr5slingrs5loepr4s3loor5sluisr5smaakrs5maalr4s3parrs4parers4pener4s3petr5spraar4s3te_r5ster_r5sterkrs5termr5stersrste5str4stevars5tomarst5orars5traprs5treir5strenrs5trogrst5rozr4t3aanrt5aandrt5aanvrte4leirte5star2t5e2vr4t3inir4t3inkrt5jescr4t3rasr3t4rekr4t3resr4t3rolrt4s3prrts5tenrt3ui4tru4l3ij4r5u2nirval4strvloot5rwen4strzet5st3s4a3losan4t3jsart5se3s4cola2s5e2go4se4lemse5ling4s3elitse4m3ac5se3r4ese5t4rasheids5s3in5gr4s3inkosk5ruim5s4laan4s5laars5lamp_s5lampe4s5land3s4la5v3s4lee_4s5leerslee5tjslen4stsle4t3j3s4lier4s3lijf4s5lijs4s5lui_5sluis_sluis4t4s5maat5smeden5s4meet4s5mes3s5muile5smuiltsneu5st4s3oor_4s3oorl3s4opra2s1or3g4spectu5s4perrspie5tjspi5sto5s4pore4sprakt5spriet4s5prij4s5prod5s4pron5staan_4staang4staanwstaat5j2s4t1ac4s3tali3s4tands4t3arcstasie45statio4steenh5stekar5steldhste4leest5elemste5ranster5og4s4t3exs4t3e2zst5heer5stiefe3s4tijg5s4tijl4s5tint5stoel_5stoelest3o4ge4st3oogstoot5jst3o5pest5optosto4rat4st4rad3stra4f5straf_4st3rec4s3treist5rijp4s3troes5troep5strook5stroom4stroos4s5trou4strui_5struikstui5tjst5uitkstu4nie5suits_t5aandotaan4sttaar5sptaat4st5ta5g4l5takkenta5lact5talentt5allia4t3a2natan4d3rtan4k5rtar5taatdor5st4t3echttee4k3lt5eenhetee5rin4t3eier5tekene5tekenste4k3omte4k3witel5ant5telecot5electtel5eentel5ei_tel5eietel5eit5te5lextel5oogte4l3opte4l3uu5temperten4achten3a4gte5narete5noreten5scrtens5uuter3a4btera5catera4dete4r5after5eikte5ren_4t4erf_4t4erftter3k4wte5ronstero5pe5terrei5terreu5terrorter4sprte3s4apte5steltes5tentest5optest5rit5e4van5the3ra4t3heretie4kontien5sttie5s4l5tieven4t3incutin4g3iting4sati3o4p5tmen4sttna4m3o5toelicto5ende5toe1s45toets_5toetsetomaat5tom4p3j4t3om5s5to5nentop5artto4r3ag5torenstor4m3atou4r3etove5nato4vens4toverg4t3raad5trafo_4tragez5transat5redes4t3reistrie5ta5t4rio45t4rititront5j4t5routtrui5t4ts5eindt4s5enet4s3engt4s3ergts5erget4s3e2vt4s3inkt5slag_t4s3pett4s3pilt5s4port4staakts5tantts5tekot5stellt5stelst5ster_t4sterrt5sterst5stijgts5toepts5tongt4storets5tradts5treits5troette5loe3t4wijfucht5sluds5takuge4l5ougs5traui4g5aaui4l3emui4l3ogui4p3o4ui4t3a4ukkers5uk4o3pluld5erkuls5te_uls5telunds5taund5steun4ste_un4st5runst5uiunt5eenun4t5o4unt3s4muper5stu4p3leiu2r3a4ru4r3a2zurelu5rurken5surk4s5tur4servur4s3evur3s4feurs5laaur5spelur5sporurs5tikur5troeus4t3eius5tra_us5tre_u4t3eesuter5anuts5enguts5takut4ste_ut5stenuur5steuur5stiuwe4nenvaar4tavaart5rval4s5pvel4d3ove5nareven4s3evens5lover5aasve4randver5eisve5ren_ve5rendver3e4tver5ijdver5ijlver5ijsve5ringver5spever5staver5stovet4roevet5ste4vicepavie4r3avil4t3jvi4s3anvlei3s4vlie4s5voe4t3avoe4t3rvoet5spvol4g3avol4gravon4detvoor5navrij5k4wan4d5rwang5slwars5tewee4k3rwee3s4twee5stewe4gervwe5nen_wen5enkwen4k3awer4k5lwer4k3ower4k3rwerk5ruwer4k3wwer4p3awer4p3lwer5stewes4t5owijs5tawin4d3rwinst5rwi2t3o4woest5awolf4s5woon5sfwor4g3ewren4stwtje5spxamen5tyber4t3zand5a4zee3r4ozeero5vzen4d3azer4s5ezie4k3ozi4n3a4zin4k3lzins5tazin5strzooi5tjzor4g3azui4d3i",
          8 : "_aftu5re_den4k5r_eer5ste_ets5te__gerst5a_leid5st_lui5t4j_mij4n5i_neu4t5j_nie4t5j_oot5jes_poort5j_ring5s4_seks5te_taart5j_ten4t5j_ter4p5a_ven4t5j_wen4s5taar4d5asaar5spelaar4t5onan4d3e4dan4s5te_apij4t5jar4s5tekart5jesvart5o4gear4t3o4vataart5jaten4t5rat4s5takats5top_ats5trekbaar5stebbe4l5agbbe4l5eebe4l5intber4g5afber4g5etbes5ten_bis5trooblij5stebon4t5o4bor4st5rda4g3e4tdbou4w5i4d3e4lek4d3e4lit4d3e4maiden4k5ofde4r5as_de4r5assder4s5omder5stra4d3e4tapdeu4r3o44d5ingelds5trekkdtaart5je4d5ernsedors5teeer5stonegel5ei_ege4l5oveger5engeits5te_eit5stenekaart5je4l5inktel4k3u4remens5te5endertie4n3en5te4n3i4voenst5ijven4stu4repits5tee4p5o4geepoort5jerd5uit_er5editie5rendeler5enen_erkeers5errie5tjerui5t4je5smuil_esp5riemes5tatiees5tekamestere5oeters5laeurs5taaeurs5te_eur4s5trevaar5tjeve5n4aafdors5tegast5rolgel4d3a4gen5stongenstu5rger5aap_ge4r3a4lger5slangers5lijge5spendges5ten_gge4r5ongou4d5eegrie4t5jgst5aangguts5te_haams5tahaars5tehar4t3o4hee5tjesheks5te_hek5stenherm5engher4p5aaherts5tehets5te_hits5te_hit5stenhors5te_hor5stenhots5te_hts5taalht4s5takht4s5tekhts5torehuts5te_idde4r5aid5s4meeie4g5insien4st5oienst5uries5tereiets5te_itper5stjks5taakjks5taalj5s4tengkeers5toke4l5intke4r5enkker5ste_ke5straakets5te_4k5indelkors5te_kor4t3o4ko5sjerekots5te_laat5stalan4d5oolang5stalecht5stle4n3a4dle4n3e4m4l3en5thle4r3a4kle4r3e4vle5s4tellets5te_levink5jlicht5stlits5te_lit5stenl4o1r2o3l4s5pootluts5te_ma4l5entmats5te_meest5almee5stovmen4s5uume4r5aakme4r3a4kmer5antemets5te_mits5te_mit5stenmkaart5jmors5te_mots5te_5muilde_naars5tr5n4a5denn5antennnars5te_nar5stennder5aalnde4r5annder5in_nds5taalnegen5en4n3e4migne4n5enkne5s4tekngs5tak_ngs5takengs5treknkaart5jnne4p5olnpoort5jnraads5ln5s4liepnst5aangnst5aansn4s5tekonst5radens5trekknst5roosn4t5aardntaar5tjnte5nachode4m5aroe4r3a4loers5takoers5te_og4st5eionke5lapooms5te_o5rigereor4t5ijlo5steroïover5stepaling5spa4n3a4dpats5te_pe4l3e4tpkaart5jplooi5tjpols5te_pons5te_por4t5rapper5stepren4t5jprie4t5jpring5s4puter5inputs5te_r5angst_rats5te_4re4ditiree5r4adreer5steremie5tjr5endertr5enveerre4t3o4grets5te_rie4k5aprij5ster4r5inganr5ingenirits5te_rit5stenrkaart5jrk4t5e4vrme4r3a4rmors5terons5te_root5sterots5te_rpoort5jrsek5stersorkes5r4s5taakrst5aangr4st5redrte5nachrt4s5ecoruts5te_3s4co5reseks5tense4l3a4g4s3e4lekse4n3a4g4s5impers5ingeniskaart5j5s4loot3slui5ste3so5l4o32s1on4t3sraads5lstaart5jst5e4ros5sterrenstraat5jst5roos_taats5tatament5jte4l3o4g5tenten_teraads5te4r5aakte4r5enkte4r5envte4r5in_ter5ste_ter5stonthoof5ditmens5te5toe3l4atoemaat5to4r5olitors5te_t4s5tankt5s4tes_tte5l4optten4t5jtuurs5lauid5spreuid5ste_uin4s5louits5te_urs5takevals5tekve4l3a4gvens5lanvens5tekven4s3u4vors5te_vor5stenvrij5stewaar5stewer4k3u4wezen4s5winst5aawoor4d5rzoet5ste",
          9 : "_acht5end_handels5_ker5sten_laat5ste_mor5sten_pers5te__pits5te__raads5le_spoor5tj_wals5te_asting5spboots5te_brie5tje_ebots5te_ekwet5steemor5stenepers5te_espit5steewens5te_flens5te_fpers5te_fpits5te_gfijn5stehaats5te_heers5takhielsges5hts5trekki5otorensjspoort5jkaart5jeskaats5te_ka4t5a4leketting5skinds5te_kkers5tenklots5te_koets5te_kwens5te_lands5te_loens5te_nbots5te_n4d5e4recngels5te_n5opleidinpers5te_ntene5tenomen5ste_poets5te_r4d5e4lasrke5streerke5strerrlaat5sterlinks5ter5treden_rvals5te_rvers5te_rwens5te_slens5te_5smuildenteeds5te_toets5te_udi5ologevens5taakvens5takewrens5te_zwets5te_"
      }
  };
  

  provide("hyphenation.nl", module.exports);

  !function (doc, $) {
      var pattern = require('hyphenation.nl');
  
      $.registerHyphenationLanguage(pattern);
  
  }(document, ender);
  

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*!
    * Bonzo: DOM Utility (c) Dustin Diaz 2012
    * https://github.com/ded/bonzo
    * License MIT
    */
  (function (name, context, definition) {
    if (typeof module != 'undefined' && module.exports) module.exports = definition()
    else if (typeof context['define'] == 'function' && context['define']['amd']) define(name, definition)
    else context[name] = definition()
  })('bonzo', this, function() {
    var win = window
      , doc = win.document
      , html = doc.documentElement
      , parentNode = 'parentNode'
      , query = null // used for setting a selector engine host
      , specialAttributes = /^(checked|value|selected|disabled)$/i
      , specialTags = /^(select|fieldset|table|tbody|tfoot|td|tr|colgroup)$/i // tags that we have trouble inserting *into*
      , table = ['<table>', '</table>', 1]
      , td = ['<table><tbody><tr>', '</tr></tbody></table>', 3]
      , option = ['<select>', '</select>', 1]
      , noscope = ['_', '', 0, 1]
      , tagMap = { // tags that we have trouble *inserting*
            thead: table, tbody: table, tfoot: table, colgroup: table, caption: table
          , tr: ['<table><tbody>', '</tbody></table>', 2]
          , th: td , td: td
          , col: ['<table><colgroup>', '</colgroup></table>', 2]
          , fieldset: ['<form>', '</form>', 1]
          , legend: ['<form><fieldset>', '</fieldset></form>', 2]
          , option: option, optgroup: option
          , script: noscope, style: noscope, link: noscope, param: noscope, base: noscope
        }
      , stateAttributes = /^(checked|selected|disabled)$/
      , ie = /msie/i.test(navigator.userAgent)
      , hasClass, addClass, removeClass
      , uidMap = {}
      , uuids = 0
      , digit = /^-?[\d\.]+$/
      , dattr = /^data-(.+)$/
      , px = 'px'
      , setAttribute = 'setAttribute'
      , getAttribute = 'getAttribute'
      , byTag = 'getElementsByTagName'
      , features = function() {
          var e = doc.createElement('p')
          e.innerHTML = '<a href="#x">x</a><table style="float:left;"></table>'
          return {
            hrefExtended: e[byTag]('a')[0][getAttribute]('href') != '#x' // IE < 8
          , autoTbody: e[byTag]('tbody').length !== 0 // IE < 8
          , computedStyle: doc.defaultView && doc.defaultView.getComputedStyle
          , cssFloat: e[byTag]('table')[0].style.styleFloat ? 'styleFloat' : 'cssFloat'
          , transform: function () {
              var props = ['transform', 'webkitTransform', 'MozTransform', 'OTransform', 'msTransform'], i
              for (i = 0; i < props.length; i++) {
                if (props[i] in e.style) return props[i]
              }
            }()
          , classList: 'classList' in e
          , opasity: function () {
              return typeof doc.createElement('a').style.opacity !== 'undefined'
            }()
          }
        }()
      , trimReplace = /(^\s*|\s*$)/g
      , whitespaceRegex = /\s+/
      , toString = String.prototype.toString
      , unitless = { lineHeight: 1, zoom: 1, zIndex: 1, opacity: 1, boxFlex: 1, WebkitBoxFlex: 1, MozBoxFlex: 1 }
      , trim = String.prototype.trim ?
          function (s) {
            return s.trim()
          } :
          function (s) {
            return s.replace(trimReplace, '')
          }
  
  
    function isNode(node) {
      return node && node.nodeName && (node.nodeType == 1 || node.nodeType == 11)
    }
  
  
    function normalize(node, host, clone) {
      var i, l, ret
      if (typeof node == 'string') return bonzo.create(node)
      if (isNode(node)) node = [ node ]
      if (clone) {
        ret = [] // don't change original array
        for (i = 0, l = node.length; i < l; i++) ret[i] = cloneNode(host, node[i])
        return ret
      }
      return node
    }
  
  
    /**
     * @param {string} c a class name to test
     * @return {boolean}
     */
    function classReg(c) {
      return new RegExp("(^|\\s+)" + c + "(\\s+|$)")
    }
  
  
    /**
     * @param {Bonzo|Array} ar
     * @param {function(Object, number, (Bonzo|Array))} fn
     * @param {Object=} opt_scope
     * @param {boolean=} opt_rev
     * @return {Bonzo|Array}
     */
    function each(ar, fn, opt_scope, opt_rev) {
      var ind, i = 0, l = ar.length
      for (; i < l; i++) {
        ind = opt_rev ? ar.length - i - 1 : i
        fn.call(opt_scope || ar[ind], ar[ind], ind, ar)
      }
      return ar
    }
  
  
    /**
     * @param {Bonzo|Array} ar
     * @param {function(Object, number, (Bonzo|Array))} fn
     * @param {Object=} opt_scope
     * @return {Bonzo|Array}
     */
    function deepEach(ar, fn, opt_scope) {
      for (var i = 0, l = ar.length; i < l; i++) {
        if (isNode(ar[i])) {
          deepEach(ar[i].childNodes, fn, opt_scope)
          fn.call(opt_scope || ar[i], ar[i], i, ar)
        }
      }
      return ar
    }
  
  
    /**
     * @param {string} s
     * @return {string}
     */
    function camelize(s) {
      return s.replace(/-(.)/g, function (m, m1) {
        return m1.toUpperCase()
      })
    }
  
  
    /**
     * @param {string} s
     * @return {string}
     */
    function decamelize(s) {
      return s ? s.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase() : s
    }
  
  
    /**
     * @param {Element} el
     * @return {*}
     */
    function data(el) {
      el[getAttribute]('data-node-uid') || el[setAttribute]('data-node-uid', ++uuids)
      var uid = el[getAttribute]('data-node-uid')
      return uidMap[uid] || (uidMap[uid] = {})
    }
  
  
    /**
     * removes the data associated with an element
     * @param {Element} el
     */
    function clearData(el) {
      var uid = el[getAttribute]('data-node-uid')
      if (uid) delete uidMap[uid]
    }
  
  
    function dataValue(d) {
      var f
      try {
        return (d === null || d === undefined) ? undefined :
          d === 'true' ? true :
            d === 'false' ? false :
              d === 'null' ? null :
                (f = parseFloat(d)) == d ? f : d;
      } catch(e) {}
      return undefined
    }
  
  
    /**
     * @param {Bonzo|Array} ar
     * @param {function(Object, number, (Bonzo|Array))} fn
     * @param {Object=} opt_scope
     * @return {boolean} whether `some`thing was found
     */
    function some(ar, fn, opt_scope) {
      for (var i = 0, j = ar.length; i < j; ++i) if (fn.call(opt_scope || null, ar[i], i, ar)) return true
      return false
    }
  
  
    /**
     * this could be a giant enum of CSS properties
     * but in favor of file size sans-closure deadcode optimizations
     * we're just asking for any ol string
     * then it gets transformed into the appropriate style property for JS access
     * @param {string} p
     * @return {string}
     */
    function styleProperty(p) {
        (p == 'transform' && (p = features.transform)) ||
          (/^transform-?[Oo]rigin$/.test(p) && (p = features.transform + 'Origin')) ||
          (p == 'float' && (p = features.cssFloat))
        return p ? camelize(p) : null
    }
  
    var getStyle = features.computedStyle ?
      function (el, property) {
        var value = null
          , computed = doc.defaultView.getComputedStyle(el, '')
        computed && (value = computed[property])
        return el.style[property] || value
      } :
  
      (ie && html.currentStyle) ?
  
      /**
       * @param {Element} el
       * @param {string} property
       * @return {string|number}
       */
      function (el, property) {
        if (property == 'opacity' && !features.opasity) {
          var val = 100
          try {
            val = el['filters']['DXImageTransform.Microsoft.Alpha'].opacity
          } catch (e1) {
            try {
              val = el['filters']('alpha').opacity
            } catch (e2) {}
          }
          return val / 100
        }
        var value = el.currentStyle ? el.currentStyle[property] : null
        return el.style[property] || value
      } :
  
      function (el, property) {
        return el.style[property]
      }
  
    // this insert method is intense
    function insert(target, host, fn, rev) {
      var i = 0, self = host || this, r = []
        // target nodes could be a css selector if it's a string and a selector engine is present
        // otherwise, just use target
        , nodes = query && typeof target == 'string' && target.charAt(0) != '<' ? query(target) : target
      // normalize each node in case it's still a string and we need to create nodes on the fly
      each(normalize(nodes), function (t, j) {
        each(self, function (el) {
          fn(t, r[i++] = j > 0 ? cloneNode(self, el) : el)
        }, null, rev)
      }, this, rev)
      self.length = i
      each(r, function (e) {
        self[--i] = e
      }, null, !rev)
      return self
    }
  
  
    /**
     * sets an element to an explicit x/y position on the page
     * @param {Element} el
     * @param {?number} x
     * @param {?number} y
     */
    function xy(el, x, y) {
      var $el = bonzo(el)
        , style = $el.css('position')
        , offset = $el.offset()
        , rel = 'relative'
        , isRel = style == rel
        , delta = [parseInt($el.css('left'), 10), parseInt($el.css('top'), 10)]
  
      if (style == 'static') {
        $el.css('position', rel)
        style = rel
      }
  
      isNaN(delta[0]) && (delta[0] = isRel ? 0 : el.offsetLeft)
      isNaN(delta[1]) && (delta[1] = isRel ? 0 : el.offsetTop)
  
      x != null && (el.style.left = x - offset.left + delta[0] + px)
      y != null && (el.style.top = y - offset.top + delta[1] + px)
  
    }
  
    // classList support for class management
    // altho to be fair, the api sucks because it won't accept multiple classes at once
    if (features.classList) {
      hasClass = function (el, c) {
        return el.classList.contains(c)
      }
      addClass = function (el, c) {
        el.classList.add(c)
      }
      removeClass = function (el, c) {
        el.classList.remove(c)
      }
    }
    else {
      hasClass = function (el, c) {
        return classReg(c).test(el.className)
      }
      addClass = function (el, c) {
        el.className = trim(el.className + ' ' + c)
      }
      removeClass = function (el, c) {
        el.className = trim(el.className.replace(classReg(c), ' '))
      }
    }
  
  
    /**
     * this allows method calling for setting values
     *
     * @example
     * bonzo(elements).css('color', function (el) {
     *   return el.getAttribute('data-original-color')
     * })
     *
     * @param {Element} el
     * @param {function (Element)|string}
     * @return {string}
     */
    function setter(el, v) {
      return typeof v == 'function' ? v(el) : v
    }
  
    /**
     * @constructor
     * @param {Array.<Element>|Element|Node|string} elements
     */
    function Bonzo(elements) {
      this.length = 0
      if (elements) {
        elements = typeof elements !== 'string' &&
          !elements.nodeType &&
          typeof elements.length !== 'undefined' ?
            elements :
            [elements]
        this.length = elements.length
        for (var i = 0; i < elements.length; i++) this[i] = elements[i]
      }
    }
  
    Bonzo.prototype = {
  
        /**
         * @param {number} index
         * @return {Element|Node}
         */
        get: function (index) {
          return this[index] || null
        }
  
        // itetators
        /**
         * @param {function(Element|Node)} fn
         * @param {Object=} opt_scope
         * @return {Bonzo}
         */
      , each: function (fn, opt_scope) {
          return each(this, fn, opt_scope)
        }
  
        /**
         * @param {Function} fn
         * @param {Object=} opt_scope
         * @return {Bonzo}
         */
      , deepEach: function (fn, opt_scope) {
          return deepEach(this, fn, opt_scope)
        }
  
  
        /**
         * @param {Function} fn
         * @param {Function=} opt_reject
         * @return {Array}
         */
      , map: function (fn, opt_reject) {
          var m = [], n, i
          for (i = 0; i < this.length; i++) {
            n = fn.call(this, this[i], i)
            opt_reject ? (opt_reject(n) && m.push(n)) : m.push(n)
          }
          return m
        }
  
      // text and html inserters!
  
      /**
       * @param {string} h the HTML to insert
       * @param {boolean=} opt_text whether to set or get text content
       * @return {Bonzo|string}
       */
      , html: function (h, opt_text) {
          var method = opt_text
                ? html.textContent === undefined ? 'innerText' : 'textContent'
                : 'innerHTML'
            , that = this
            , append = function (el, i) {
                each(normalize(h, that, i), function (node) {
                  el.appendChild(node)
                })
              }
            , updateElement = function (el, i) {
                try {
                  if (opt_text || (typeof h == 'string' && !specialTags.test(el.tagName))) {
                    return el[method] = h
                  }
                } catch (e) {}
                append(el, i)
              }
          return typeof h != 'undefined'
            ? this.empty().each(updateElement)
            : this[0] ? this[0][method] : ''
        }
  
        /**
         * @param {string=} opt_text the text to set, otherwise this is a getter
         * @return {Bonzo|string}
         */
      , text: function (opt_text) {
          return this.html(opt_text, true)
        }
  
        // more related insertion methods
  
        /**
         * @param {Bonzo|string|Element|Array} node
         * @return {Bonzo}
         */
      , append: function (node) {
          var that = this
          return this.each(function (el, i) {
            each(normalize(node, that, i), function (i) {
              el.appendChild(i)
            })
          })
        }
  
  
        /**
         * @param {Bonzo|string|Element|Array} node
         * @return {Bonzo}
         */
      , prepend: function (node) {
          var that = this
          return this.each(function (el, i) {
            var first = el.firstChild
            each(normalize(node, that, i), function (i) {
              el.insertBefore(i, first)
            })
          })
        }
  
  
        /**
         * @param {Bonzo|string|Element|Array} target the location for which you'll insert your new content
         * @param {Object=} opt_host an optional host scope (primarily used when integrated with Ender)
         * @return {Bonzo}
         */
      , appendTo: function (target, opt_host) {
          return insert.call(this, target, opt_host, function (t, el) {
            t.appendChild(el)
          })
        }
  
  
        /**
         * @param {Bonzo|string|Element|Array} target the location for which you'll insert your new content
         * @param {Object=} opt_host an optional host scope (primarily used when integrated with Ender)
         * @return {Bonzo}
         */
      , prependTo: function (target, opt_host) {
          return insert.call(this, target, opt_host, function (t, el) {
            t.insertBefore(el, t.firstChild)
          }, 1)
        }
  
  
        /**
         * @param {Bonzo|string|Element|Array} node
         * @return {Bonzo}
         */
      , before: function (node) {
          var that = this
          return this.each(function (el, i) {
            each(normalize(node, that, i), function (i) {
              el[parentNode].insertBefore(i, el)
            })
          })
        }
  
  
        /**
         * @param {Bonzo|string|Element|Array} node
         * @return {Bonzo}
         */
      , after: function (node) {
          var that = this
          return this.each(function (el, i) {
            each(normalize(node, that, i), function (i) {
              el[parentNode].insertBefore(i, el.nextSibling)
            }, null, 1)
          })
        }
  
  
        /**
         * @param {Bonzo|string|Element|Array} target the location for which you'll insert your new content
         * @param {Object=} opt_host an optional host scope (primarily used when integrated with Ender)
         * @return {Bonzo}
         */
      , insertBefore: function (target, opt_host) {
          return insert.call(this, target, opt_host, function (t, el) {
            t[parentNode].insertBefore(el, t)
          })
        }
  
  
        /**
         * @param {Bonzo|string|Element|Array} target the location for which you'll insert your new content
         * @param {Object=} opt_host an optional host scope (primarily used when integrated with Ender)
         * @return {Bonzo}
         */
      , insertAfter: function (target, opt_host) {
          return insert.call(this, target, opt_host, function (t, el) {
            var sibling = t.nextSibling
            sibling ?
              t[parentNode].insertBefore(el, sibling) :
              t[parentNode].appendChild(el)
          }, 1)
        }
  
  
        /**
         * @param {Bonzo|string|Element|Array} node
         * @return {Bonzo}
         */
      , replaceWith: function (node) {
          bonzo(normalize(node)).insertAfter(this)
          return this.remove()
        }
  
        // class management
  
        /**
         * @param {string} c
         * @return {Bonzo}
         */
      , addClass: function (c) {
          c = toString.call(c).split(whitespaceRegex)
          return this.each(function (el) {
            // we `each` here so you can do $el.addClass('foo bar')
            each(c, function (c) {
              if (c && !hasClass(el, setter(el, c)))
                addClass(el, setter(el, c))
            })
          })
        }
  
  
        /**
         * @param {string} c
         * @return {Bonzo}
         */
      , removeClass: function (c) {
          c = toString.call(c).split(whitespaceRegex)
          return this.each(function (el) {
            each(c, function (c) {
              if (c && hasClass(el, setter(el, c)))
                removeClass(el, setter(el, c))
            })
          })
        }
  
  
        /**
         * @param {string} c
         * @return {boolean}
         */
      , hasClass: function (c) {
          c = toString.call(c).split(whitespaceRegex)
          return some(this, function (el) {
            return some(c, function (c) {
              return c && hasClass(el, c)
            })
          })
        }
  
  
        /**
         * @param {string} c classname to toggle
         * @param {boolean=} opt_condition whether to add or remove the class straight away
         * @return {Bonzo}
         */
      , toggleClass: function (c, opt_condition) {
          c = toString.call(c).split(whitespaceRegex)
          return this.each(function (el) {
            each(c, function (c) {
              if (c) {
                typeof opt_condition !== 'undefined' ?
                  opt_condition ? addClass(el, c) : removeClass(el, c) :
                  hasClass(el, c) ? removeClass(el, c) : addClass(el, c)
              }
            })
          })
        }
  
        // display togglers
  
        /**
         * @param {string=} opt_type useful to set back to anything other than an empty string
         * @return {Bonzo}
         */
      , show: function (opt_type) {
          opt_type = typeof opt_type == 'string' ? opt_type : ''
          return this.each(function (el) {
            el.style.display = opt_type
          })
        }
  
  
        /**
         * @return {Bonzo}
         */
      , hide: function () {
          return this.each(function (el) {
            el.style.display = 'none'
          })
        }
  
  
        /**
         * @param {Function=} opt_callback
         * @param {string=} opt_type
         * @return {Bonzo}
         */
      , toggle: function (opt_callback, opt_type) {
          opt_type = typeof opt_type == 'string' ? opt_type : '';
          typeof opt_callback != 'function' && (opt_callback = null)
          return this.each(function (el) {
            el.style.display = (el.offsetWidth || el.offsetHeight) ? 'none' : opt_type;
            opt_callback && opt_callback.call(el)
          })
        }
  
  
        // DOM Walkers & getters
  
        /**
         * @return {Element|Node}
         */
      , first: function () {
          return bonzo(this.length ? this[0] : [])
        }
  
  
        /**
         * @return {Element|Node}
         */
      , last: function () {
          return bonzo(this.length ? this[this.length - 1] : [])
        }
  
  
        /**
         * @return {Element|Node}
         */
      , next: function () {
          return this.related('nextSibling')
        }
  
  
        /**
         * @return {Element|Node}
         */
      , previous: function () {
          return this.related('previousSibling')
        }
  
  
        /**
         * @return {Element|Node}
         */
      , parent: function() {
          return this.related(parentNode)
        }
  
  
        /**
         * @private
         * @param {string} method the directional DOM method
         * @return {Element|Node}
         */
      , related: function (method) {
          return this.map(
            function (el) {
              el = el[method]
              while (el && el.nodeType !== 1) {
                el = el[method]
              }
              return el || 0
            },
            function (el) {
              return el
            }
          )
        }
  
  
        /**
         * @return {Bonzo}
         */
      , focus: function () {
          this.length && this[0].focus()
          return this
        }
  
  
        /**
         * @return {Bonzo}
         */
      , blur: function () {
          this.length && this[0].blur()
          return this
        }
  
        // style getter setter & related methods
  
        /**
         * @param {Object|string} o
         * @param {string=} opt_v
         * @return {Bonzo|string}
         */
      , css: function (o, opt_v) {
          var p, iter = o
          // is this a request for just getting a style?
          if (opt_v === undefined && typeof o == 'string') {
            // repurpose 'v'
            opt_v = this[0]
            if (!opt_v) return null
            if (opt_v === doc || opt_v === win) {
              p = (opt_v === doc) ? bonzo.doc() : bonzo.viewport()
              return o == 'width' ? p.width : o == 'height' ? p.height : ''
            }
            return (o = styleProperty(o)) ? getStyle(opt_v, o) : null
          }
  
          if (typeof o == 'string') {
            iter = {}
            iter[o] = opt_v
          }
  
          if (ie && iter.opacity) {
            // oh this 'ol gamut
            iter.filter = 'alpha(opacity=' + (iter.opacity * 100) + ')'
            // give it layout
            iter.zoom = o.zoom || 1;
            delete iter.opacity;
          }
  
          function fn(el, p, v) {
            for (var k in iter) {
              if (iter.hasOwnProperty(k)) {
                v = iter[k];
                // change "5" to "5px" - unless you're line-height, which is allowed
                (p = styleProperty(k)) && digit.test(v) && !(p in unitless) && (v += px)
                try { el.style[p] = setter(el, v) } catch(e) {}
              }
            }
          }
          return this.each(fn)
        }
  
  
        /**
         * @param {number=} opt_x
         * @param {number=} opt_y
         * @return {Bonzo|number}
         */
      , offset: function (opt_x, opt_y) {
          if (opt_x && typeof opt_x == 'object' && (typeof opt_x.top == 'number' || typeof opt_x.left == 'number')) {
            return this.each(function (el) {
              xy(el, opt_x.left, opt_x.top)
            })
          } else if (typeof opt_x == 'number' || typeof opt_y == 'number') {
            return this.each(function (el) {
              xy(el, opt_x, opt_y)
            })
          }
          if (!this[0]) return {
              top: 0
            , left: 0
            , height: 0
            , width: 0
          }
          var el = this[0]
            , de = el.ownerDocument.documentElement
            , bcr = el.getBoundingClientRect()
            , scroll = getWindowScroll()
            , width = el.offsetWidth
            , height = el.offsetHeight
            , top = bcr.top + scroll.y - Math.max(0, de && de.clientTop, doc.body.clientTop)
            , left = bcr.left + scroll.x - Math.max(0, de && de.clientLeft, doc.body.clientLeft)
  
          return {
              top: top
            , left: left
            , height: height
            , width: width
          }
        }
  
  
        /**
         * @return {number}
         */
      , dim: function () {
          if (!this.length) return { height: 0, width: 0 }
          var el = this[0]
            , de = el.nodeType == 9 && el.documentElement // document
            , orig = !de && !!el.style && !el.offsetWidth && !el.offsetHeight ?
               // el isn't visible, can't be measured properly, so fix that
               function (t) {
                 var s = {
                     position: el.style.position || ''
                   , visibility: el.style.visibility || ''
                   , display: el.style.display || ''
                 }
                 t.first().css({
                     position: 'absolute'
                   , visibility: 'hidden'
                   , display: 'block'
                 })
                 return s
              }(this) : null
            , width = de
                ? Math.max(el.body.scrollWidth, el.body.offsetWidth, de.scrollWidth, de.offsetWidth, de.clientWidth)
                : el.offsetWidth
            , height = de
                ? Math.max(el.body.scrollHeight, el.body.offsetHeight, de.scrollWidth, de.offsetWidth, de.clientHeight)
                : el.offsetHeight
  
          orig && this.first().css(orig)
          return {
              height: height
            , width: width
          }
        }
  
        // attributes are hard. go shopping
  
        /**
         * @param {string} k an attribute to get or set
         * @param {string=} opt_v the value to set
         * @return {Bonzo|string}
         */
      , attr: function (k, opt_v) {
          var el = this[0]
          if (typeof k != 'string' && !(k instanceof String)) {
            for (var n in k) {
              k.hasOwnProperty(n) && this.attr(n, k[n])
            }
            return this
          }
          return typeof opt_v == 'undefined' ?
            !el ? null : specialAttributes.test(k) ?
              stateAttributes.test(k) && typeof el[k] == 'string' ?
                true : el[k] : (k == 'href' || k =='src') && features.hrefExtended ?
                  el[getAttribute](k, 2) : el[getAttribute](k) :
            this.each(function (el) {
              specialAttributes.test(k) ? (el[k] = setter(el, opt_v)) : el[setAttribute](k, setter(el, opt_v))
            })
        }
  
  
        /**
         * @param {string} k
         * @return {Bonzo}
         */
      , removeAttr: function (k) {
          return this.each(function (el) {
            stateAttributes.test(k) ? (el[k] = false) : el.removeAttribute(k)
          })
        }
  
  
        /**
         * @param {string=} opt_s
         * @return {Bonzo|string}
         */
      , val: function (s) {
          return (typeof s == 'string') ?
            this.attr('value', s) :
            this.length ? this[0].value : null
        }
  
        // use with care and knowledge. this data() method uses data attributes on the DOM nodes
        // to do this differently costs a lot more code. c'est la vie
        /**
         * @param {string|Object=} opt_k the key for which to get or set data
         * @param {Object=} opt_v
         * @return {Bonzo|Object}
         */
      , data: function (opt_k, opt_v) {
          var el = this[0], o, m
          if (typeof opt_v === 'undefined') {
            if (!el) return null
            o = data(el)
            if (typeof opt_k === 'undefined') {
              each(el.attributes, function (a) {
                (m = ('' + a.name).match(dattr)) && (o[camelize(m[1])] = dataValue(a.value))
              })
              return o
            } else {
              if (typeof o[opt_k] === 'undefined')
                o[opt_k] = dataValue(this.attr('data-' + decamelize(opt_k)))
              return o[opt_k]
            }
          } else {
            return this.each(function (el) { data(el)[opt_k] = opt_v })
          }
        }
  
        // DOM detachment & related
  
        /**
         * @return {Bonzo}
         */
      , remove: function () {
          this.deepEach(clearData)
          return this.detach()
        }
  
  
        /**
         * @return {Bonzo}
         */
      , empty: function () {
          return this.each(function (el) {
            deepEach(el.childNodes, clearData)
  
            while (el.firstChild) {
              el.removeChild(el.firstChild)
            }
          })
        }
  
  
        /**
         * @return {Bonzo}
         */
      , detach: function () {
          return this.each(function (el) {
            el[parentNode] && el[parentNode].removeChild(el)
          })
        }
  
        // who uses a mouse anyway? oh right.
  
        /**
         * @param {number} y
         */
      , scrollTop: function (y) {
          return scroll.call(this, null, y, 'y')
        }
  
  
        /**
         * @param {number} x
         */
      , scrollLeft: function (x) {
          return scroll.call(this, x, null, 'x')
        }
  
    }
  
  
    function cloneNode(host, el) {
      var c = el.cloneNode(true)
        , cloneElems
        , elElems
  
      // check for existence of an event cloner
      // preferably https://github.com/fat/bean
      // otherwise Bonzo won't do this for you
      if (host.$ && typeof host.cloneEvents == 'function') {
        host.$(c).cloneEvents(el)
  
        // clone events from every child node
        cloneElems = host.$(c).find('*')
        elElems = host.$(el).find('*')
  
        for (var i = 0; i < elElems.length; i++)
          host.$(cloneElems[i]).cloneEvents(elElems[i])
      }
      return c
    }
  
    function scroll(x, y, type) {
      var el = this[0]
      if (!el) return this
      if (x == null && y == null) {
        return (isBody(el) ? getWindowScroll() : { x: el.scrollLeft, y: el.scrollTop })[type]
      }
      if (isBody(el)) {
        win.scrollTo(x, y)
      } else {
        x != null && (el.scrollLeft = x)
        y != null && (el.scrollTop = y)
      }
      return this
    }
  
    function isBody(element) {
      return element === win || (/^(?:body|html)$/i).test(element.tagName)
    }
  
    function getWindowScroll() {
      return { x: win.pageXOffset || html.scrollLeft, y: win.pageYOffset || html.scrollTop }
    }
  
    /**
     * @param {Array.<Element>|Element|Node|string} els
     * @return {Bonzo}
     */
    function bonzo(els) {
      return new Bonzo(els)
    }
  
    bonzo.setQueryEngine = function (q) {
      query = q;
      delete bonzo.setQueryEngine
    }
  
    bonzo.aug = function (o, target) {
      // for those standalone bonzo users. this love is for you.
      for (var k in o) {
        o.hasOwnProperty(k) && ((target || Bonzo.prototype)[k] = o[k])
      }
    }
  
    bonzo.create = function (node) {
      // hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh
      return typeof node == 'string' && node !== '' ?
        function () {
          var tag = /^\s*<([^\s>]+)/.exec(node)
            , el = doc.createElement('div')
            , els = []
            , p = tag ? tagMap[tag[1].toLowerCase()] : null
            , dep = p ? p[2] + 1 : 1
            , ns = p && p[3]
            , pn = parentNode
            , tb = features.autoTbody && p && p[0] == '<table>' && !(/<tbody/i).test(node)
  
          el.innerHTML = p ? (p[0] + node + p[1]) : node
          while (dep--) el = el.firstChild
          // for IE NoScope, we may insert cruft at the begining just to get it to work
          if (ns && el && el.nodeType !== 1) el = el.nextSibling
          do {
            // tbody special case for IE<8, creates tbody on any empty table
            // we don't want it if we're just after a <thead>, <caption>, etc.
            if ((!tag || el.nodeType == 1) && (!tb || el.tagName.toLowerCase() != 'tbody')) {
              els.push(el)
            }
          } while (el = el.nextSibling)
          // IE < 9 gives us a parentNode which messes up insert() check for cloning
          // `dep` > 1 can also cause problems with the insert() check (must do this last)
          each(els, function(el) { el[pn] && el[pn].removeChild(el) })
          return els
        }() : isNode(node) ? [node.cloneNode(true)] : []
    }
  
    bonzo.doc = function () {
      var vp = bonzo.viewport()
      return {
          width: Math.max(doc.body.scrollWidth, html.scrollWidth, vp.width)
        , height: Math.max(doc.body.scrollHeight, html.scrollHeight, vp.height)
      }
    }
  
    bonzo.firstChild = function (el) {
      for (var c = el.childNodes, i = 0, j = (c && c.length) || 0, e; i < j; i++) {
        if (c[i].nodeType === 1) e = c[j = i]
      }
      return e
    }
  
    bonzo.viewport = function () {
      return {
          width: ie ? html.clientWidth : self.innerWidth
        , height: ie ? html.clientHeight : self.innerHeight
      }
    }
  
    bonzo.isAncestor = 'compareDocumentPosition' in html ?
      function (container, element) {
        return (container.compareDocumentPosition(element) & 16) == 16
      } : 'contains' in html ?
      function (container, element) {
        return container !== element && container.contains(element);
      } :
      function (container, element) {
        while (element = element[parentNode]) {
          if (element === container) {
            return true
          }
        }
        return false
      }
  
    return bonzo
  }); // the only line we care about using a semi-colon. placed here for concatenation tools
  

  provide("bonzo", module.exports);

  (function ($) {
  
    var b = require('bonzo')
    b.setQueryEngine($)
    $.ender(b)
    $.ender(b(), true)
    $.ender({
      create: function (node) {
        return $(b.create(node))
      }
    })
  
    $.id = function (id) {
      return $([document.getElementById(id)])
    }
  
    function indexOf(ar, val) {
      for (var i = 0; i < ar.length; i++) if (ar[i] === val) return i
      return -1
    }
  
    function uniq(ar) {
      var r = [], i = 0, j = 0, k, item, inIt
      for (; item = ar[i]; ++i) {
        inIt = false
        for (k = 0; k < r.length; ++k) {
          if (r[k] === item) {
            inIt = true; break
          }
        }
        if (!inIt) r[j++] = item
      }
      return r
    }
  
    $.ender({
      parents: function (selector, closest) {
        if (!this.length) return this
        if (!selector) selector = '*'
        var collection = $(selector), j, k, p, r = []
        for (j = 0, k = this.length; j < k; j++) {
          p = this[j]
          while (p = p.parentNode) {
            if (~indexOf(collection, p)) {
              r.push(p)
              if (closest) break;
            }
          }
        }
        return $(uniq(r))
      }
  
    , parent: function() {
        return $(uniq(b(this).parent()))
      }
  
    , closest: function (selector) {
        return this.parents(selector, true)
      }
  
    , first: function () {
        return $(this.length ? this[0] : this)
      }
  
    , last: function () {
        return $(this.length ? this[this.length - 1] : [])
      }
  
    , next: function () {
        return $(b(this).next())
      }
  
    , previous: function () {
        return $(b(this).previous())
      }
  
    , appendTo: function (t) {
        return b(this.selector).appendTo(t, this)
      }
  
    , prependTo: function (t) {
        return b(this.selector).prependTo(t, this)
      }
  
    , insertAfter: function (t) {
        return b(this.selector).insertAfter(t, this)
      }
  
    , insertBefore: function (t) {
        return b(this.selector).insertBefore(t, this)
      }
  
    , siblings: function () {
        var i, l, p, r = []
        for (i = 0, l = this.length; i < l; i++) {
          p = this[i]
          while (p = p.previousSibling) p.nodeType == 1 && r.push(p)
          p = this[i]
          while (p = p.nextSibling) p.nodeType == 1 && r.push(p)
        }
        return $(r)
      }
  
    , children: function () {
        var i, l, el, r = []
        for (i = 0, l = this.length; i < l; i++) {
          if (!(el = b.firstChild(this[i]))) continue;
          r.push(el)
          while (el = el.nextSibling) el.nodeType == 1 && r.push(el)
        }
        return $(uniq(r))
      }
  
    , height: function (v) {
        return dimension.call(this, 'height', v)
      }
  
    , width: function (v) {
        return dimension.call(this, 'width', v)
      }
    }, true)
  
    /**
     * @param {string} type either width or height
     * @param {number=} opt_v becomes a setter instead of a getter
     * @return {number}
     */
    function dimension(type, opt_v) {
      return typeof opt_v == 'undefined'
        ? b(this).dim()[type]
        : this.css(type, opt_v)
    }
  }(ender));

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*!
    * Morpheus - A Brilliant Animator
    * https://github.com/ded/morpheus - (c) Dustin Diaz 2011
    * License MIT
    */
  !function (name, definition) {
    if (typeof define == 'function') define(definition)
    else if (typeof module != 'undefined') module.exports = definition()
    else this[name] = definition()
  }('morpheus', function () {
  
    var doc = document
      , win = window
      , perf = win.performance
      , perfNow = perf && (perf.now || perf.webkitNow || perf.msNow || perf.mozNow)
      , now = perfNow ? function () { return perfNow.call(perf) } : function () { return +new Date() }
      , html = doc.documentElement
      , thousand = 1000
      , rgbOhex = /^rgb\(|#/
      , relVal = /^([+\-])=([\d\.]+)/
      , numUnit = /^(?:[\+\-]=)?\d+(?:\.\d+)?(%|in|cm|mm|em|ex|pt|pc|px)$/
      , rotate = /rotate\(((?:[+\-]=)?([\-\d\.]+))deg\)/
      , scale = /scale\(((?:[+\-]=)?([\d\.]+))\)/
      , skew = /skew\(((?:[+\-]=)?([\-\d\.]+))deg, ?((?:[+\-]=)?([\-\d\.]+))deg\)/
      , translate = /translate\(((?:[+\-]=)?([\-\d\.]+))px, ?((?:[+\-]=)?([\-\d\.]+))px\)/
        // these elements do not require 'px'
      , unitless = { lineHeight: 1, zoom: 1, zIndex: 1, opacity: 1, transform: 1}
  
    // which property name does this browser use for transform
    var transform = function () {
      var styles = doc.createElement('a').style
        , props = ['webkitTransform', 'MozTransform', 'OTransform', 'msTransform', 'Transform']
        , i
      for (i = 0; i < props.length; i++) {
        if (props[i] in styles) return props[i]
      }
    }()
  
    // does this browser support the opacity property?
    var opasity = function () {
      return typeof doc.createElement('a').style.opacity !== 'undefined'
    }()
  
    // initial style is determined by the elements themselves
    var getStyle = doc.defaultView && doc.defaultView.getComputedStyle ?
      function (el, property) {
        property = property == 'transform' ? transform : property
        var value = null
          , computed = doc.defaultView.getComputedStyle(el, '')
        computed && (value = computed[camelize(property)])
        return el.style[property] || value
      } : html.currentStyle ?
  
      function (el, property) {
        property = camelize(property)
  
        if (property == 'opacity') {
          var val = 100
          try {
            val = el.filters['DXImageTransform.Microsoft.Alpha'].opacity
          } catch (e1) {
            try {
              val = el.filters('alpha').opacity
            } catch (e2) {}
          }
          return val / 100
        }
        var value = el.currentStyle ? el.currentStyle[property] : null
        return el.style[property] || value
      } :
      function (el, property) {
        return el.style[camelize(property)]
      }
  
    var frame = function () {
      // native animation frames
      // http://webstuff.nfshost.com/anim-timing/Overview.html
      // http://dev.chromium.org/developers/design-documents/requestanimationframe-implementation
      return win.requestAnimationFrame  ||
        win.webkitRequestAnimationFrame ||
        win.mozRequestAnimationFrame    ||
        win.msRequestAnimationFrame     ||
        win.oRequestAnimationFrame      ||
        function (callback) {
          win.setTimeout(function () {
            callback(+new Date())
          }, 17) // when I was 17..
        }
    }()
  
    var children = []
  
    function has(array, elem, i) {
      if (Array.prototype.indexOf) return array.indexOf(elem)
      for (i = 0; i < array.length; ++i) {
        if (array[i] === elem) return i
      }
    }
  
    function render(timestamp) {
      var i, count = children.length
      // if we're using a high res timer, make sure timestamp is not the old epoch-based value.
      // http://updates.html5rocks.com/2012/05/requestAnimationFrame-API-now-with-sub-millisecond-precision
      if (perfNow && timestamp > 1e12) timestamp = now()
      for (i = count; i--;) {
        children[i](timestamp)
      }
      children.length && frame(render)
    }
  
    function live(f) {
      if (children.push(f) === 1) frame(render)
    }
  
    function die(f) {
      var rest, index = has(children, f)
      if (index >= 0) {
        rest = children.slice(index + 1)
        children.length = index
        children = children.concat(rest)
      }
    }
  
    function parseTransform(style, base) {
      var values = {}, m
      if (m = style.match(rotate)) values.rotate = by(m[1], base ? base.rotate : null)
      if (m = style.match(scale)) values.scale = by(m[1], base ? base.scale : null)
      if (m = style.match(skew)) {values.skewx = by(m[1], base ? base.skewx : null); values.skewy = by(m[3], base ? base.skewy : null)}
      if (m = style.match(translate)) {values.translatex = by(m[1], base ? base.translatex : null); values.translatey = by(m[3], base ? base.translatey : null)}
      return values
    }
  
    function formatTransform(v) {
      var s = ''
      if ('rotate' in v) s += 'rotate(' + v.rotate + 'deg) '
      if ('scale' in v) s += 'scale(' + v.scale + ') '
      if ('translatex' in v) s += 'translate(' + v.translatex + 'px,' + v.translatey + 'px) '
      if ('skewx' in v) s += 'skew(' + v.skewx + 'deg,' + v.skewy + 'deg)'
      return s
    }
  
    function rgb(r, g, b) {
      return '#' + (1 << 24 | r << 16 | g << 8 | b).toString(16).slice(1)
    }
  
    // convert rgb and short hex to long hex
    function toHex(c) {
      var m = c.match(/rgba?\((\d+),\s*(\d+),\s*(\d+)/)
      return (m ? rgb(m[1], m[2], m[3]) : c)
        .replace(/#(\w)(\w)(\w)$/, '#$1$1$2$2$3$3') // short skirt to long jacket
    }
  
    // change font-size => fontSize etc.
    function camelize(s) {
      return s.replace(/-(.)/g, function (m, m1) {
        return m1.toUpperCase()
      })
    }
  
    // aren't we having it?
    function fun(f) {
      return typeof f == 'function'
    }
  
    function nativeTween(t) {
      // default to a pleasant-to-the-eye easeOut (like native animations)
      return Math.sin(t * Math.PI / 2)
    }
  
    /**
      * Core tween method that requests each frame
      * @param duration: time in milliseconds. defaults to 1000
      * @param fn: tween frame callback function receiving 'position'
      * @param done {optional}: complete callback function
      * @param ease {optional}: easing method. defaults to easeOut
      * @param from {optional}: integer to start from
      * @param to {optional}: integer to end at
      * @returns method to stop the animation
      */
    function tween(duration, fn, done, ease, from, to) {
      ease = fun(ease) ? ease : morpheus.easings[ease] || nativeTween
      var time = duration || thousand
        , self = this
        , diff = to - from
        , start = now()
        , stop = 0
        , end = 0
  
      function run(t) {
        var delta = t - start
        if (delta > time || stop) {
          to = isFinite(to) ? to : 1
          stop ? end && fn(to) : fn(to)
          die(run)
          return done && done.apply(self)
        }
        // if you don't specify a 'to' you can use tween as a generic delta tweener
        // cool, eh?
        isFinite(to) ?
          fn((diff * ease(delta / time)) + from) :
          fn(ease(delta / time))
      }
  
      live(run)
  
      return {
        stop: function (jump) {
          stop = 1
          end = jump // jump to end of animation?
          if (!jump) done = null // remove callback if not jumping to end
        }
      }
    }
  
    /**
      * generic bezier method for animating x|y coordinates
      * minimum of 2 points required (start and end).
      * first point start, last point end
      * additional control points are optional (but why else would you use this anyway ;)
      * @param points: array containing control points
         [[0, 0], [100, 200], [200, 100]]
      * @param pos: current be(tween) position represented as float  0 - 1
      * @return [x, y]
      */
    function bezier(points, pos) {
      var n = points.length, r = [], i, j
      for (i = 0; i < n; ++i) {
        r[i] = [points[i][0], points[i][1]]
      }
      for (j = 1; j < n; ++j) {
        for (i = 0; i < n - j; ++i) {
          r[i][0] = (1 - pos) * r[i][0] + pos * r[parseInt(i + 1, 10)][0]
          r[i][1] = (1 - pos) * r[i][1] + pos * r[parseInt(i + 1, 10)][1]
        }
      }
      return [r[0][0], r[0][1]]
    }
  
    // this gets you the next hex in line according to a 'position'
    function nextColor(pos, start, finish) {
      var r = [], i, e, from, to
      for (i = 0; i < 6; i++) {
        from = Math.min(15, parseInt(start.charAt(i),  16))
        to   = Math.min(15, parseInt(finish.charAt(i), 16))
        e = Math.floor((to - from) * pos + from)
        e = e > 15 ? 15 : e < 0 ? 0 : e
        r[i] = e.toString(16)
      }
      return '#' + r.join('')
    }
  
    // this retreives the frame value within a sequence
    function getTweenVal(pos, units, begin, end, k, i, v) {
      if (k == 'transform') {
        v = {}
        for (var t in begin[i][k]) {
          v[t] = (t in end[i][k]) ? Math.round(((end[i][k][t] - begin[i][k][t]) * pos + begin[i][k][t]) * thousand) / thousand : begin[i][k][t]
        }
        return v
      } else if (typeof begin[i][k] == 'string') {
        return nextColor(pos, begin[i][k], end[i][k])
      } else {
        // round so we don't get crazy long floats
        v = Math.round(((end[i][k] - begin[i][k]) * pos + begin[i][k]) * thousand) / thousand
        // some css properties don't require a unit (like zIndex, lineHeight, opacity)
        if (!(k in unitless)) v += units[i][k] || 'px'
        return v
      }
    }
  
    // support for relative movement via '+=n' or '-=n'
    function by(val, start, m, r, i) {
      return (m = relVal.exec(val)) ?
        (i = parseFloat(m[2])) && (start + (m[1] == '+' ? 1 : -1) * i) :
        parseFloat(val)
    }
  
    /**
      * morpheus:
      * @param element(s): HTMLElement(s)
      * @param options: mixed bag between CSS Style properties & animation options
      *  - {n} CSS properties|values
      *     - value can be strings, integers,
      *     - or callback function that receives element to be animated. method must return value to be tweened
      *     - relative animations start with += or -= followed by integer
      *  - duration: time in ms - defaults to 1000(ms)
      *  - easing: a transition method - defaults to an 'easeOut' algorithm
      *  - complete: a callback method for when all elements have finished
      *  - bezier: array of arrays containing x|y coordinates that define the bezier points. defaults to none
      *     - this may also be a function that receives element to be animated. it must return a value
      */
    function morpheus(elements, options) {
      var els = elements ? (els = isFinite(elements.length) ? elements : [elements]) : [], i
        , complete = options.complete
        , duration = options.duration
        , ease = options.easing
        , points = options.bezier
        , begin = []
        , end = []
        , units = []
        , bez = []
        , originalLeft
        , originalTop
  
      if (points) {
        // remember the original values for top|left
        originalLeft = options.left;
        originalTop = options.top;
        delete options.right;
        delete options.bottom;
        delete options.left;
        delete options.top;
      }
  
      for (i = els.length; i--;) {
  
        // record beginning and end states to calculate positions
        begin[i] = {}
        end[i] = {}
        units[i] = {}
  
        // are we 'moving'?
        if (points) {
  
          var left = getStyle(els[i], 'left')
            , top = getStyle(els[i], 'top')
            , xy = [by(fun(originalLeft) ? originalLeft(els[i]) : originalLeft || 0, parseFloat(left)),
                    by(fun(originalTop) ? originalTop(els[i]) : originalTop || 0, parseFloat(top))]
  
          bez[i] = fun(points) ? points(els[i], xy) : points
          bez[i].push(xy)
          bez[i].unshift([
            parseInt(left, 10),
            parseInt(top, 10)
          ])
        }
  
        for (var k in options) {
          switch (k) {
          case 'complete':
          case 'duration':
          case 'easing':
          case 'bezier':
            continue;
            break
          }
          var v = getStyle(els[i], k), unit
            , tmp = fun(options[k]) ? options[k](els[i]) : options[k]
          if (typeof tmp == 'string' &&
              rgbOhex.test(tmp) &&
              !rgbOhex.test(v)) {
            delete options[k]; // remove key :(
            continue; // cannot animate colors like 'orange' or 'transparent'
                      // only #xxx, #xxxxxx, rgb(n,n,n)
          }
  
          begin[i][k] = k == 'transform' ? parseTransform(v) :
            typeof tmp == 'string' && rgbOhex.test(tmp) ?
              toHex(v).slice(1) :
              parseFloat(v)
          end[i][k] = k == 'transform' ? parseTransform(tmp, begin[i][k]) :
            typeof tmp == 'string' && tmp.charAt(0) == '#' ?
              toHex(tmp).slice(1) :
              by(tmp, parseFloat(v));
          // record original unit
          (typeof tmp == 'string') && (unit = tmp.match(numUnit)) && (units[i][k] = unit[1])
        }
      }
      // ONE TWEEN TO RULE THEM ALL
      return tween.apply(els, [duration, function (pos, v, xy) {
        // normally not a fan of optimizing for() loops, but we want something
        // fast for animating
        for (i = els.length; i--;) {
          if (points) {
            xy = bezier(bez[i], pos)
            els[i].style.left = xy[0] + 'px'
            els[i].style.top = xy[1] + 'px'
          }
          for (var k in options) {
            v = getTweenVal(pos, units, begin, end, k, i)
            k == 'transform' ?
              els[i].style[transform] = formatTransform(v) :
              k == 'opacity' && !opasity ?
                (els[i].style.filter = 'alpha(opacity=' + (v * 100) + ')') :
                (els[i].style[camelize(k)] = v)
          }
        }
      }, complete, ease])
    }
  
    // expose useful methods
    morpheus.tween = tween
    morpheus.getStyle = getStyle
    morpheus.bezier = bezier
    morpheus.transform = transform
    morpheus.parseTransform = parseTransform
    morpheus.formatTransform = formatTransform
    morpheus.easings = {}
  
    return morpheus
  
  });
  

  provide("morpheus", module.exports);

  var morpheus = require('morpheus')
  !function ($) {
    $.ender({
      animate: function (options) {
        return morpheus(this, options)
      }
    , fadeIn: function (d, fn) {
        return morpheus(this, {
            duration: d
          , opacity: 1
          , complete: fn
        })
      }
    , fadeOut: function (d, fn) {
        return morpheus(this, {
            duration: d
          , opacity: 0
          , complete: fn
        })
      }
    }, true)
    $.ender({
      tween: morpheus.tween
    })
  }(ender)

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*!
    * Ender-Overlay: Highly Customizable Overlay for Ender
    * copyright Andras Nemeseri @nemeseri 2012 | License MIT
    * https://github.com/nemeseri/ender-overlay
    */
  !function ($) {
      var is,
          transition;
  
      // from valentine
      is = {
          fun: function (f) {
              return typeof f === 'function';
          },
          arr: function (ar) {
              return ar instanceof Array;
          },
          obj: function (o) {
              return o instanceof Object && !is.fun(o) && !is.arr(o);
          }
      };
  
      /*
          Based on Bootstrap
          Mozilla and Webkit support only
      */
      transition = (function () {
          var st = document.createElement("div").style,
              transitionEnd = "TransitionEnd",
              transitionProp = "Transition",
              support = st.transition !== undefined ||
                  st.WebkitTransition !== undefined ||
                  st.MozTransition !== undefined;
  
          return support && {
              prop: (function () {
                  if (st.WebkitTransition !== undefined) {
                      transitionProp = "WebkitTransition";
                  } else if (st.MozTransition !== undefined) {
                      transitionProp = "MozTransition";
                  }
                  return transitionProp;
              }()),
              end: (function () {
                  if (st.WebkitTransition !== undefined) {
                      transitionEnd = "webkitTransitionEnd";
                  } else if (st.MozTransition !== undefined) {
                      transitionEnd = "transitionend";
                  }
                  return transitionEnd;
              }())
          };
      }());
  
      function extend() {
          // based on jQuery deep merge
          var options, name, src, copy, clone,
              target = arguments[0], i = 1, length = arguments.length;
  
          for (; i < length; i += 1) {
              if ((options = arguments[i]) !== null) {
                  // Extend the base object
                  for (name in options) {
                      src = target[name];
                      copy = options[name];
                      if (target === copy) {
                          continue;
                      }
                      if (copy && (is.obj(copy))) {
                          clone = src && is.obj(src) ? src : {};
                          target[name] = extend(clone, copy);
                      } else if (copy !== undefined) {
                          target[name] = copy;
                      }
                  }
              }
          }
          return target;
      }
  
      function clone(obj) {
          if (null === obj || "object" !== typeof obj) {
              return obj;
          }
          var copy = obj.constructor(),
              attr;
          for (attr in obj) {
              if (obj.hasOwnProperty(attr)) {
                  copy[attr] = obj[attr];
              }
          }
          return copy;
      }
  
      // from jquery
      function proxy(fn, context) {
          var slice = Array.prototype.slice,
              args = slice.call(arguments, 2);
          return function () {
              return fn.apply(context, args.concat(slice.call(arguments)));
          };
      }
  
      function animate(options) {
          var el = options.el,
              complete = options.complete ? options.complete : function () {},
              animation,
              dummy;
  
          // no animation obj OR animation is not available,
          // fallback to css and call the callback
          if (! options.animation ||
              ! (el.animate || (options.css3transition && transition))) {
              el.css(options.fallbackCss);
              complete();
              return;
          }
  
          // we will animate, apply start CSS
          if (options.animStartCss) {
              if (options.animStartCss.opacity === 0) {
                  options.animStartCss.opacity = 0.01; // ie quirk
              }
              el.css(options.animStartCss);
          }
          animation = options.animation;
  
          // css3 setted, if available apply the css
          if (options.css3transition && transition) {
              dummy = el[0].offsetWidth; // force reflow; source: bootstrap
              el[0].style[transition.prop] = "all " + animation.duration + "ms ease-out";
  
              // takaritas
              delete animation.duration;
  
              el.css(animation);
              //el.unbind(transition.end);
              el.bind(transition.end, function () {
                  // delete transition properties and events
                  el.unbind(transition.end);
                  el[0].style[transition.prop] = "none";
                  complete();
              });
          } else if (window.ender) {
              // use morpheus
              el.animate(extend(animation, {"complete": complete}));
          } else {
              // use animate from jquery
              el.animate(animation, animation.duration, "swing", complete);
          }
      }
  
      /*
          OverlayMask Constructor
      */
      function OverlayMask(settings) {
          this.init(settings);
      }
  
      OverlayMask.prototype = {
          init: function (options) {
              this.options = {
                  id: "ender-overlay-mask",
                  zIndex: 9998,
                  opacity: 0.6,
                  color: "#777"
              };
  
              extend(this.options, options || {});
  
              var $mask = $("#" + this.options.id),
                  opt = this.options;
  
              if (! $mask.length) {
                  $mask = $("<div></div>")
                      .attr("id", this.options.id)
                      .css({
                          display: "none",
                          position: "absolute",
                          top: 0,
                          left: 0
                      })
                      .appendTo("body");
              }
  
              this.$mask = $mask;
          },
  
          show: function () {
              // apply instance mask options
              var opt = this.options,
                  docSize = this.getDocSize(),
                  animObj = false;
  
              this.$mask.css({
                  zIndex: opt.zIndex,
                  backgroundColor: opt.color,
                  width: docSize.width,
                  height: docSize.height
              });
  
              if (opt.durationIn) {
                  animObj = {
                      opacity: opt.opacity,
                      duration: opt.durationIn
                  };
              }
  
              animate({
                  el: this.$mask,
                  animStartCss: {
                      opacity: 0.01, // ie quirk
                      display: "block"
                  },
                  animation: animObj,
                  fallbackCss: {display: "block", opacity: opt.opacity},
                  css3transition: opt.css3transition
              });
  
          },
  
          hide: function () {
              var opt = this.options,
                  self = this,
                  animObj = false;
  
              if (opt.durationOut) {
                  animObj = {
                      opacity: 0,
                      duration: opt.durationOut
                  };
              }
  
              animate({
                  el: this.$mask,
                  animation: animObj,
                  complete: function () {
                      self.$mask.css({display: "none"});
                  },
                  fallbackCss: {display: "none"},
                  css3transition: opt.css3transition
              });
          },
  
          getDocSize: function () {
              if (window.ender) { // ender
                  return {
                      width: $("body").width(),
                      height: $.doc().height
                  };
              } else { // jquery
                  return {
                      width: $(document).width(),
                      height: $(document).height()
                  };
              }
          },
  
          getMask: function () {
              return this.$mask;
          }
      };
  
      /*
          Overlay Constructor
      */
      function Overlay(el, settings) {
          this.init(el, settings);
  
          // only return the API
          // instead of this
          return this.getApi();
      }
  
      Overlay.prototype = {
          init: function ($el, options) {
              this.options = {
                  top: 80,
                  position: "absolute",
                  cssClass: "ender-overlay",
                  close: ".close",
                  trigger: null,
                  zIndex: 9999,
                  showMask: true,
                  closeOnEsc: true,
                  closeOnMaskClick: true,
                  autoOpen: false,
                  allowMultipleDisplay: false,
  
                  // morpheus required for JS fallback
                  css3transition: false, // experimental
  
                  // start values before animation
                  startAnimationCss: {
                      opacity: 0.01 // ie quirk
                  },
  
                  // morpheus animation options
                  animationIn: {
                      opacity: 1,
                      duration: 250
                  },
  
                  animationOut: {
                      opacity: 0,
                      duration: 250
                  },
  
                  mask: {},
  
                  onBeforeOpen: function () {},
                  onBeforeClose: function () {},
                  onOpen: function () {},
                  onClose: function () {}
              };
  
              this.setOptions(options);
              this.$overlay = $el.css({
                  display: "none"
              });
  
              if (this.options.showMask) {
                  this.mask = new OverlayMask(this.options.mask);
              }
  
              // prevent multiple event binding
              if (! this.$overlay.attr("data-overlayloaded")) {
                  this.attachEvents();
                  this.$overlay.attr("data-overlayloaded", 1);
              }
  
              if (this.options.autoOpen) {
                  this.open();
              }
          },
  
          attachEvents: function () {
              var self = this,
                  opt = this.options;
  
              // Bind open method to trigger's click event
              if (opt.trigger && $(opt.trigger).length) {
                  $(opt.trigger).click(function (e) {
                      e.preventDefault();
                      self.open();
                  });
              }
  
              this.$overlay
                  .delegate(opt.close, 'click', function (e) {
                      e.preventDefault();
                      self.close();
                  });
  
              // attach event listeners
              $(document).bind("ender-overlay.close", function () {
                  self.close();
              });
  
              $(document).bind("ender-overlay.closeOverlay", function () {
                  self.close(true);
              });
  
              if (opt.closeOnEsc) {
                  $(document).keyup(function (e) {
                      self.onKeyUp(e);
                  });
              }
  
              if (this.mask && opt.closeOnMaskClick) {
                  this.mask.getMask().click(function () {
                      self.close();
                  });
              }
          },
  
          setupOverlay: function () {
              var opt = this.options,
                  topPos = opt.top,
                  scrollTop = $(window).scrollTop(),
                  overlayWidth = this.$overlay.width();
  
              // setup overlay
              this.$overlay
                  .addClass(opt.cssClass)
                  .appendTo("body");
  
              if (opt.position === "absolute") {
                  topPos += scrollTop;
              }
  
              // width is not defined explicitly
              // so we try to find out
              if (overlayWidth === 0) {
                  this.$overlay.css({
                      display: "block",
                      position: "absolute",
                      left: -9999
                  });
                  overlayWidth = this.$overlay.width();
              }
  
              this.$overlay.css({
                  display: "none",
                  position: opt.position,
                  top: topPos,
                  left: "50%",
                  zIndex: opt.zIndex,
                  marginLeft: overlayWidth / 2 * -1
              });
          },
  
          open: function (dontOpenMask) {
              var opt = this.options,
                  self = this,
                  animationIn = opt.animationIn ? clone(opt.animationIn) : false,
                  api = this.getApi();
  
              if (this.$overlay.css("display") === "block" ||
                  opt.onBeforeOpen(api) === false) {
                  return;
              }
  
              this.setupOverlay();
  
              if (! opt.allowMultipleDisplay) {
                  $(document).trigger("ender-overlay.closeOverlay");
              }
  
              animate({
                  el: this.$overlay,
                  animStartCss: extend({display: "block"}, opt.startAnimationCss),
                  animation: animationIn,
                  complete: function () {
                      if (animationIn && animationIn.opacity === 1) {
                          self.$overlay.css({ "filter": "" }); // ie quirk
                      }
                      self.options.onOpen(api);
                  },
                  fallbackCss: {display: "block", opacity: 1},
                  css3transition: opt.css3transition
              });
  
              if (this.mask &&
                  typeof dontOpenMask === "undefined") {
                  this.mask.show();
              }
          },
  
          close: function (dontHideMask) {
              var opt = this.options,
                  self = this,
                  animationOut = opt.animationOut ? clone(opt.animationOut) : false,
                  api = this.getApi();
  
              if (opt.onBeforeClose(api) === false ||
                  this.$overlay.css("display") !== "block") {
                  return;
              }
  
              animate({
                  el: this.$overlay,
                  animation: animationOut,
                  complete: function () {
                      self.$overlay.css({display: "none"});
                      self.options.onClose(api);
                  },
                  fallbackCss: {display: "none", opacity: 0},
                  css3transition: opt.css3transition
              });
  
              if (this.mask &&
                  typeof dontHideMask === "undefined") {
                  this.mask.hide();
              }
          },
  
          onKeyUp: function (e) {
              if (e.keyCode === 27 &&
                  this.$overlay.css("display") !== "none") {
                  this.close();
              }
          },
  
          getOverlay: function () {
              return this.$overlay;
          },
  
          getOptions: function () {
              return this.options;
          },
  
          setOptions: function (options) {
              extend(this.options, options || {});
              var opt = this.options;
  
              if (opt.animationIn === "none") {
                  opt.animationIn = false;
              }
  
              if (opt.animationOut === "none") {
                  opt.animationOut = false;
              }
  
              if (opt.showMask) {
                  // If there is no explicit duration set for OverlayMask
                  // set it from overlay animation
                  if (! opt.mask.durationIn && opt.animationIn && opt.animationIn.duration) {
                      opt.mask.durationIn = opt.animationIn.duration;
                  }
  
                  if (! opt.mask.durationOut && opt.animationOut && opt.animationOut.duration) {
                      opt.mask.durationOut = opt.animationOut.duration;
                  }
  
                  // no animation
                  if (! opt.mask.durationIn && ! opt.animationIn) {
                      opt.mask.durationIn = 0;
                  }
  
                  if (! opt.mask.durationOut && ! opt.animationOut) {
                      opt.mask.durationOut = 0;
                  }
  
                  if (typeof opt.mask.css3transition !== "boolean") {
                      opt.mask.css3transition = opt.css3transition;
                  }
              }
  
          },
  
          getApi: function () {
              return {
                  open: proxy(this.open, this),
                  close: proxy(this.close, this),
                  getOverlay: proxy(this.getOverlay, this),
                  getOptions: proxy(this.getOptions, this),
                  setOptions: proxy(this.setOptions, this)
              };
          }
      };
  
      $.fn.overlay = function (options) {
          var el = $(this).first();
          return new Overlay(el, options);
      };
  
  }(window.ender || window.jQuery);

  provide("ender-overlay", module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /** vim: et:ts=4:sw=4:sts=4
   * @license amdefine 0.0.4 Copyright (c) 2011, The Dojo Foundation All Rights Reserved.
   * Available via the MIT or new BSD license.
   * see: http://github.com/jrburke/amdefine for details
   */
  
  /*jslint node: true */
  /*global module, process */
  'use strict';
  
  var path = require('path');
  
  /**
   * Creates a define for node.
   * @param {Object} module the "module" object that is defined by Node for the
   * current module.
   * @param {Function} [require]. Node's require function for the current module.
   * It only needs to be passed in Node versions before 0.5, when module.require
   * did not exist.
   * @returns {Function} a define function that is usable for the current node
   * module.
   */
  function amdefine(module, require) {
      var defineCache = {},
          loaderCache = {},
          alreadyCalled = false,
          makeRequire, stringRequire;
  
      /**
       * Trims the . and .. from an array of path segments.
       * It will keep a leading path segment if a .. will become
       * the first path segment, to help with module name lookups,
       * which act like paths, but can be remapped. But the end result,
       * all paths that use this function should look normalized.
       * NOTE: this method MODIFIES the input array.
       * @param {Array} ary the array of path segments.
       */
      function trimDots(ary) {
          var i, part;
          for (i = 0; ary[i]; i+= 1) {
              part = ary[i];
              if (part === '.') {
                  ary.splice(i, 1);
                  i -= 1;
              } else if (part === '..') {
                  if (i === 1 && (ary[2] === '..' || ary[0] === '..')) {
                      //End of the line. Keep at least one non-dot
                      //path segment at the front so it can be mapped
                      //correctly to disk. Otherwise, there is likely
                      //no path mapping for a path starting with '..'.
                      //This can still fail, but catches the most reasonable
                      //uses of ..
                      break;
                  } else if (i > 0) {
                      ary.splice(i - 1, 2);
                      i -= 2;
                  }
              }
          }
      }
  
      function normalize(name, baseName) {
          var baseParts;
  
          //Adjust any relative paths.
          if (name && name.charAt(0) === '.') {
              //If have a base name, try to normalize against it,
              //otherwise, assume it is a top-level require that will
              //be relative to baseUrl in the end.
              if (baseName) {
                  baseParts = baseName.split('/');
                  baseParts = baseParts.slice(0, baseParts.length - 1);
                  baseParts = baseParts.concat(name.split('/'));
                  trimDots(baseParts);
                  name = baseParts.join('/');
              }
          }
  
          return name;
      }
  
      /**
       * Create the normalize() function passed to a loader plugin's
       * normalize method.
       */
      function makeNormalize(relName) {
          return function (name) {
              return normalize(name, relName);
          };
      }
  
      function makeLoad(id) {
          function load(value) {
              loaderCache[id] = value;
          }
  
          load.fromText = function (id, text) {
              //This one is difficult because the text can/probably uses
              //define, and any relative paths and requires should be relative
              //to that id was it would be found on disk. But this would require
              //bootstrapping a module/require fairly deeply from node core.
              //Not sure how best to go about that yet.
              throw new Error('amdefine does not implement load.fromText');
          };
  
          return load;
      }
  
      makeRequire = function (systemRequire, exports, module, relId) {
          function amdRequire(deps, callback) {
              if (typeof deps === 'string') {
                  //Synchronous, single module require('')
                  return stringRequire(systemRequire, exports, module, deps, relId);
              } else {
                  //Array of dependencies with a callback.
  
                  //Convert the dependencies to modules.
                  deps = deps.map(function (depName) {
                      return stringRequire(systemRequire, exports, module, depName, relId);
                  });
  
                  //Wait for next tick to call back the require call.
                  process.nextTick(function () {
                      callback.apply(null, deps);
                  });
              }
          }
  
          amdRequire.toUrl = function (filePath) {
              if (filePath.indexOf('.') === 0) {
                  return normalize(filePath, path.dirname(module.filename));
              } else {
                  return filePath;
              }
          };
  
          return amdRequire;
      };
  
      //Favor explicit value, passed in if the module wants to support Node 0.4.
      require = require || function req() {
          return module.require.apply(module, arguments);
      };
  
      function runFactory(id, deps, factory) {
          var r, e, m, result;
  
          if (id) {
              e = loaderCache[id] = {};
              m = {
                  id: id,
                  uri: __filename,
                  exports: e
              };
              r = makeRequire(undefined, e, m, id);
          } else {
              //Only support one define call per file
              if (alreadyCalled) {
                  throw new Error('amdefine with no module ID cannot be called more than once per file.');
              }
              alreadyCalled = true;
  
              //Use the real variables from node
              //Use module.exports for exports, since
              //the exports in here is amdefine exports.
              e = module.exports;
              m = module;
              r = makeRequire(require, e, m, module.id);
          }
  
          //If there are dependencies, they are strings, so need
          //to convert them to dependency values.
          if (deps) {
              deps = deps.map(function (depName) {
                  return r(depName);
              });
          }
  
          //Call the factory with the right dependencies.
          if (typeof factory === 'function') {
              result = factory.apply(module.exports, deps);
          } else {
              result = factory;
          }
  
          if (result !== undefined) {
              m.exports = result;
              if (id) {
                  loaderCache[id] = m.exports;
              }
          }
      }
  
      stringRequire = function (systemRequire, exports, module, id, relId) {
          //Split the ID by a ! so that
          var index = id.indexOf('!'),
              originalId = id,
              prefix, plugin;
  
          if (index === -1) {
              id = normalize(id, relId);
  
              //Straight module lookup. If it is one of the special dependencies,
              //deal with it, otherwise, delegate to node.
              if (id === 'require') {
                  return makeRequire(systemRequire, exports, module, relId);
              } else if (id === 'exports') {
                  return exports;
              } else if (id === 'module') {
                  return module;
              } else if (loaderCache.hasOwnProperty(id)) {
                  return loaderCache[id];
              } else if (defineCache[id]) {
                  runFactory.apply(null, defineCache[id]);
                  return loaderCache[id];
              } else {
                  if(systemRequire) {
                      return systemRequire(originalId);
                  } else {
                      throw new Error('No module with ID: ' + id);
                  }
              }
          } else {
              //There is a plugin in play.
              prefix = id.substring(0, index);
              id = id.substring(index + 1, id.length);
  
              plugin = stringRequire(systemRequire, exports, module, prefix, relId);
  
              if (plugin.normalize) {
                  id = plugin.normalize(id, makeNormalize(relId));
              } else {
                  //Normalize the ID normally.
                  id = normalize(id, relId);
              }
  
              if (loaderCache[id]) {
                  return loaderCache[id];
              } else {
                  plugin.load(id, makeRequire(systemRequire, exports, module, relId), makeLoad(id), {});
  
                  return loaderCache[id];
              }
          }
      };
  
      //Create a define function specific to the module asking for amdefine.
      function define(id, deps, factory) {
          if (Array.isArray(id)) {
              factory = deps;
              deps = id;
              id = undefined;
          } else if (typeof id !== 'string') {
              factory = id;
              id = deps = undefined;
          }
  
          if (deps && !Array.isArray(deps)) {
              factory = deps;
              deps = undefined;
          }
  
          if (!deps) {
              deps = ['require', 'exports', 'module'];
          }
  
          //Set up properties for this module. If an ID, then use
          //internal cache. If no ID, then use the external variables
          //for this node module.
          if (id) {
              //Put the module in deep freeze until there is a
              //require call for it.
              defineCache[id] = [id, deps, factory];
          } else {
              runFactory(id, deps, factory);
          }
      }
  
      //define.require, which has access to all the values in the
      //cache. Useful for AMD modules that all have IDs in the file,
      //but need to finally export a value to node based on one of those
      //IDs.
      define.require = function (id) {
          if (loaderCache[id]) {
              return loaderCache[id];
          }
  
          if (defineCache[id]) {
              runFactory.apply(null, defineCache[id]);
              return loaderCache[id];
          }
      };
  
      define.amd = {};
  
      return define;
  }
  
  module.exports = amdefine;
  

  provide("amdefine", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*
   * Copyright 2009-2011 Mozilla Foundation and contributors
   * Licensed under the New BSD license. See LICENSE.txt or:
   * http://opensource.org/licenses/BSD-3-Clause
   */
  exports.SourceMapGenerator = require('./source-map/source-map-generator').SourceMapGenerator;
  exports.SourceMapConsumer = require('./source-map/source-map-consumer').SourceMapConsumer;
  exports.SourceNode = require('./source-map/source-node').SourceNode;
  

  provide("source-map", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  var wordwrap = module.exports = function (start, stop, params) {
      if (typeof start === 'object') {
          params = start;
          start = params.start;
          stop = params.stop;
      }
      
      if (typeof stop === 'object') {
          params = stop;
          start = start || params.start;
          stop = undefined;
      }
      
      if (!stop) {
          stop = start;
          start = 0;
      }
      
      if (!params) params = {};
      var mode = params.mode || 'soft';
      var re = mode === 'hard' ? /\b/ : /(\S+\s+)/;
      
      return function (text) {
          var chunks = text.toString()
              .split(re)
              .reduce(function (acc, x) {
                  if (mode === 'hard') {
                      for (var i = 0; i < x.length; i += stop - start) {
                          acc.push(x.slice(i, i + stop - start));
                      }
                  }
                  else acc.push(x)
                  return acc;
              }, [])
          ;
          
          return chunks.reduce(function (lines, rawChunk) {
              if (rawChunk === '') return lines;
              
              var chunk = rawChunk.replace(/\t/g, '    ');
              
              var i = lines.length - 1;
              if (lines[i].length + chunk.length > stop) {
                  lines[i] = lines[i].replace(/\s+$/, '');
                  
                  chunk.split(/\n/).forEach(function (c) {
                      lines.push(
                          new Array(start + 1).join(' ')
                          + c.replace(/^\s+/, '')
                      );
                  });
              }
              else if (chunk.match(/\n/)) {
                  var xs = chunk.split(/\n/);
                  lines[i] += xs.shift();
                  xs.forEach(function (c) {
                      lines.push(
                          new Array(start + 1).join(' ')
                          + c.replace(/^\s+/, '')
                      );
                  });
              }
              else {
                  lines[i] += chunk;
              }
              
              return lines;
          }, [ new Array(start + 1).join(' ') ]).join('\n');
      };
  };
  
  wordwrap.soft = wordwrap;
  
  wordwrap.hard = function (start, stop) {
      return wordwrap(start, stop, { mode : 'hard' });
  };
  

  provide("wordwrap", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  var path = require('path');
  var wordwrap = require('wordwrap');
  
  /*  Hack an instance of Argv with process.argv into Argv
      so people can do
          require('optimist')(['--beeble=1','-z','zizzle']).argv
      to parse a list of args and
          require('optimist').argv
      to get a parsed version of process.argv.
  */
  
  var inst = Argv(process.argv.slice(2));
  Object.keys(inst).forEach(function (key) {
      Argv[key] = typeof inst[key] == 'function'
          ? inst[key].bind(inst)
          : inst[key];
  });
  
  var exports = module.exports = Argv;
  function Argv (args, cwd) {
      var self = {};
      if (!cwd) cwd = process.cwd();
      
      self.$0 = process.argv
          .slice(0,2)
          .map(function (x) {
              var b = rebase(cwd, x);
              return x.match(/^\//) && b.length < x.length
                  ? b : x
          })
          .join(' ')
      ;
      
      if (process.argv[1] == process.env._) {
          self.$0 = process.env._.replace(
              path.dirname(process.execPath) + '/', ''
          );
      }
      
      var flags = { bools : {}, strings : {} };
      
      self.boolean = function (bools) {
          if (!Array.isArray(bools)) {
              bools = [].slice.call(arguments);
          }
          
          bools.forEach(function (name) {
              flags.bools[name] = true;
          });
          
          return self;
      };
      
      self.string = function (strings) {
          if (!Array.isArray(strings)) {
              strings = [].slice.call(arguments);
          }
          
          strings.forEach(function (name) {
              flags.strings[name] = true;
          });
          
          return self;
      };
      
      var aliases = {};
      self.alias = function (x, y) {
          if (typeof x === 'object') {
              Object.keys(x).forEach(function (key) {
                  self.alias(key, x[key]);
              });
          }
          else if (Array.isArray(y)) {
              y.forEach(function (yy) {
                  self.alias(x, yy);
              });
          }
          else {
              var zs = (aliases[x] || []).concat(aliases[y] || []).concat(x, y);
              aliases[x] = zs.filter(function (z) { return z != x });
              aliases[y] = zs.filter(function (z) { return z != y });
          }
          
          return self;
      };
      
      var demanded = {};
      self.demand = function (keys) {
          if (typeof keys == 'number') {
              if (!demanded._) demanded._ = 0;
              demanded._ += keys;
          }
          else if (Array.isArray(keys)) {
              keys.forEach(function (key) {
                  self.demand(key);
              });
          }
          else {
              demanded[keys] = true;
          }
          
          return self;
      };
      
      var usage;
      self.usage = function (msg, opts) {
          if (!opts && typeof msg === 'object') {
              opts = msg;
              msg = null;
          }
          
          usage = msg;
          
          if (opts) self.options(opts);
          
          return self;
      };
      
      function fail (msg) {
          self.showHelp();
          if (msg) console.error(msg);
          process.exit(1);
      }
      
      var checks = [];
      self.check = function (f) {
          checks.push(f);
          return self;
      };
      
      var defaults = {};
      self.default = function (key, value) {
          if (typeof key === 'object') {
              Object.keys(key).forEach(function (k) {
                  self.default(k, key[k]);
              });
          }
          else {
              defaults[key] = value;
          }
          
          return self;
      };
      
      var descriptions = {};
      self.describe = function (key, desc) {
          if (typeof key === 'object') {
              Object.keys(key).forEach(function (k) {
                  self.describe(k, key[k]);
              });
          }
          else {
              descriptions[key] = desc;
          }
          return self;
      };
      
      self.parse = function (args) {
          return Argv(args).argv;
      };
      
      self.option = self.options = function (key, opt) {
          if (typeof key === 'object') {
              Object.keys(key).forEach(function (k) {
                  self.options(k, key[k]);
              });
          }
          else {
              if (opt.alias) self.alias(key, opt.alias);
              if (opt.demand) self.demand(key);
              if (typeof opt.default !== 'undefined') {
                  self.default(key, opt.default);
              }
              
              if (opt.boolean || opt.type === 'boolean') {
                  self.boolean(key);
              }
              if (opt.string || opt.type === 'string') {
                  self.string(key);
              }
              
              var desc = opt.describe || opt.description || opt.desc;
              if (desc) {
                  self.describe(key, desc);
              }
          }
          
          return self;
      };
      
      var wrap = null;
      self.wrap = function (cols) {
          wrap = cols;
          return self;
      };
      
      self.showHelp = function (fn) {
          if (!fn) fn = console.error;
          fn(self.help());
      };
      
      self.help = function () {
          var keys = Object.keys(
              Object.keys(descriptions)
              .concat(Object.keys(demanded))
              .concat(Object.keys(defaults))
              .reduce(function (acc, key) {
                  if (key !== '_') acc[key] = true;
                  return acc;
              }, {})
          );
          
          var help = keys.length ? [ 'Options:' ] : [];
          
          if (usage) {
              help.unshift(usage.replace(/\$0/g, self.$0), '');
          }
          
          var switches = keys.reduce(function (acc, key) {
              acc[key] = [ key ].concat(aliases[key] || [])
                  .map(function (sw) {
                      return (sw.length > 1 ? '--' : '-') + sw
                  })
                  .join(', ')
              ;
              return acc;
          }, {});
          
          var switchlen = longest(Object.keys(switches).map(function (s) {
              return switches[s] || '';
          }));
          
          var desclen = longest(Object.keys(descriptions).map(function (d) { 
              return descriptions[d] || '';
          }));
          
          keys.forEach(function (key) {
              var kswitch = switches[key];
              var desc = descriptions[key] || '';
              
              if (wrap) {
                  desc = wordwrap(switchlen + 4, wrap)(desc)
                      .slice(switchlen + 4)
                  ;
              }
              
              var spadding = new Array(
                  Math.max(switchlen - kswitch.length + 3, 0)
              ).join(' ');
              
              var dpadding = new Array(
                  Math.max(desclen - desc.length + 1, 0)
              ).join(' ');
              
              var type = null;
              
              if (flags.bools[key]) type = '[boolean]';
              if (flags.strings[key]) type = '[string]';
              
              if (!wrap && dpadding.length > 0) {
                  desc += dpadding;
              }
              
              var prelude = '  ' + kswitch + spadding;
              var extra = [
                  type,
                  demanded[key]
                      ? '[required]'
                      : null
                  ,
                  defaults[key] !== undefined
                      ? '[default: ' + JSON.stringify(defaults[key]) + ']'
                      : null
                  ,
              ].filter(Boolean).join('  ');
              
              var body = [ desc, extra ].filter(Boolean).join('  ');
              
              if (wrap) {
                  var dlines = desc.split('\n');
                  var dlen = dlines.slice(-1)[0].length
                      + (dlines.length === 1 ? prelude.length : 0)
                  
                  body = desc + (dlen + extra.length > wrap - 2
                      ? '\n'
                          + new Array(wrap - extra.length + 1).join(' ')
                          + extra
                      : new Array(wrap - extra.length - dlen + 1).join(' ')
                          + extra
                  );
              }
              
              help.push(prelude + body);
          });
          
          help.push('');
          return help.join('\n');
      };
      
      Object.defineProperty(self, 'argv', {
          get : parseArgs,
          enumerable : true,
      });
      
      function parseArgs () {
          var argv = { _ : [], $0 : self.$0 };
          Object.keys(flags.bools).forEach(function (key) {
              setArg(key, defaults[key] || false);
          });
          
          function setArg (key, val) {
              var num = Number(val);
              var value = typeof val !== 'string' || isNaN(num) ? val : num;
              if (flags.strings[key]) value = val;
              
              setKey(argv, key.split('.'), value);
              
              (aliases[key] || []).forEach(function (x) {
                  argv[x] = argv[key];
              });
          }
          
          for (var i = 0; i < args.length; i++) {
              var arg = args[i];
              
              if (arg === '--') {
                  argv._.push.apply(argv._, args.slice(i + 1));
                  break;
              }
              else if (arg.match(/^--.+=/)) {
                  var m = arg.match(/^--([^=]+)=(.*)/);
                  setArg(m[1], m[2]);
              }
              else if (arg.match(/^--no-.+/)) {
                  var key = arg.match(/^--no-(.+)/)[1];
                  setArg(key, false);
              }
              else if (arg.match(/^--.+/)) {
                  var key = arg.match(/^--(.+)/)[1];
                  var next = args[i + 1];
                  if (next !== undefined && !next.match(/^-/)
                  && !flags.bools[key]
                  && (aliases[key] ? !flags.bools[aliases[key]] : true)) {
                      setArg(key, next);
                      i++;
                  }
                  else if (/^(true|false)$/.test(next)) {
                      setArg(key, next === 'true');
                      i++;
                  }
                  else {
                      setArg(key, true);
                  }
              }
              else if (arg.match(/^-[^-]+/)) {
                  var letters = arg.slice(1,-1).split('');
                  
                  var broken = false;
                  for (var j = 0; j < letters.length; j++) {
                      if (letters[j+1] && letters[j+1].match(/\W/)) {
                          setArg(letters[j], arg.slice(j+2));
                          broken = true;
                          break;
                      }
                      else {
                          setArg(letters[j], true);
                      }
                  }
                  
                  if (!broken) {
                      var key = arg.slice(-1)[0];
                      
                      if (args[i+1] && !args[i+1].match(/^-/)
                      && !flags.bools[key]
                      && (aliases[key] ? !flags.bools[aliases[key]] : true)) {
                          setArg(key, args[i+1]);
                          i++;
                      }
                      else if (args[i+1] && /true|false/.test(args[i+1])) {
                          setArg(key, args[i+1] === 'true');
                          i++;
                      }
                      else {
                          setArg(key, true);
                      }
                  }
              }
              else {
                  var n = Number(arg);
                  argv._.push(flags.strings['_'] || isNaN(n) ? arg : n);
              }
          }
          
          Object.keys(defaults).forEach(function (key) {
              if (!(key in argv)) {
                  argv[key] = defaults[key];
                  if (key in aliases) {
                      argv[aliases[key]] = defaults[key];
                  }
              }
          });
          
          if (demanded._ && argv._.length < demanded._) {
              fail('Not enough non-option arguments: got '
                  + argv._.length + ', need at least ' + demanded._
              );
          }
          
          var missing = [];
          Object.keys(demanded).forEach(function (key) {
              if (!argv[key]) missing.push(key);
          });
          
          if (missing.length) {
              fail('Missing required arguments: ' + missing.join(', '));
          }
          
          checks.forEach(function (f) {
              try {
                  if (f(argv) === false) {
                      fail('Argument check failed: ' + f.toString());
                  }
              }
              catch (err) {
                  fail(err)
              }
          });
          
          return argv;
      }
      
      function longest (xs) {
          return Math.max.apply(
              null,
              xs.map(function (x) { return x.length })
          );
      }
      
      return self;
  };
  
  // rebase an absolute path to a relative one with respect to a base directory
  // exported for tests
  exports.rebase = rebase;
  function rebase (base, dir) {
      var ds = path.normalize(dir).split('/').slice(1);
      var bs = path.normalize(base).split('/').slice(1);
      
      for (var i = 0; ds[i] && ds[i] == bs[i]; i++);
      ds.splice(0, i); bs.splice(0, i);
      
      var p = path.normalize(
          bs.map(function () { return '..' }).concat(ds).join('/')
      ).replace(/\/$/,'').replace(/^$/, '.');
      return p.match(/^[.\/]/) ? p : './' + p;
  };
  
  function setKey (obj, keys, value) {
      var o = obj;
      keys.slice(0,-1).forEach(function (key) {
          if (o[key] === undefined) o[key] = {};
          o = o[key];
      });
      
      var key = keys[keys.length - 1];
      if (o[key] === undefined || typeof o[key] === 'boolean') {
          o[key] = value;
      }
      else if (Array.isArray(o[key])) {
          o[key].push(value);
      }
      else {
          o[key] = [ o[key], value ];
      }
  }
  

  provide("optimist", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  var path = require("path");
  var fs = require("fs");
  var vm = require("vm");
  var sys = require("util");
  
  var UglifyJS = vm.createContext({
      sys           : sys,
      console       : console,
      MOZ_SourceMap : require("source-map")
  });
  
  function load_global(file) {
      file = path.resolve(path.dirname(module.filename), file);
      try {
          var code = fs.readFileSync(file, "utf8");
          return vm.runInContext(code, UglifyJS, file);
      } catch(ex) {
          // XXX: in case of a syntax error, the message is kinda
          // useless. (no location information).
          sys.debug("ERROR in file: " + file + " / " + ex);
          process.exit(1);
      }
  };
  
  var FILES = exports.FILES = [
      "../lib/utils.js",
      "../lib/ast.js",
      "../lib/parse.js",
      "../lib/transform.js",
      "../lib/scope.js",
      "../lib/output.js",
      "../lib/compress.js",
      "../lib/sourcemap.js",
      "../lib/mozilla-ast.js"
  ].map(function(file){
      return path.join(path.dirname(fs.realpathSync(__filename)), file);
  });
  
  FILES.forEach(load_global);
  
  UglifyJS.AST_Node.warn_function = function(txt) {
      sys.error("WARN: " + txt);
  };
  
  // XXX: perhaps we shouldn't export everything but heck, I'm lazy.
  for (var i in UglifyJS) {
      if (UglifyJS.hasOwnProperty(i)) {
          exports[i] = UglifyJS[i];
      }
  }
  
  exports.minify = function(files, options) {
      options = UglifyJS.defaults(options, {
          outSourceMap : null,
          sourceRoot   : null,
          inSourceMap  : null,
          fromString   : false,
          warnings     : false,
      });
      if (typeof files == "string")
          files = [ files ];
  
      // 1. parse
      var toplevel = null;
      files.forEach(function(file){
          var code = options.fromString
              ? file
              : fs.readFileSync(file, "utf8");
          toplevel = UglifyJS.parse(code, {
              filename: options.fromString ? "?" : file,
              toplevel: toplevel
          });
      });
  
      // 2. compress
      toplevel.figure_out_scope();
      var sq = UglifyJS.Compressor({
          warnings: options.warnings,
      });
      toplevel = toplevel.transform(sq);
  
      // 3. mangle
      toplevel.figure_out_scope();
      toplevel.compute_char_frequency();
      toplevel.mangle_names();
  
      // 4. output
      var map = null;
      var inMap = null;
      if (options.inSourceMap) {
          inMap = fs.readFileSync(options.inSourceMap, "utf8");
      }
      if (options.outSourceMap) map = UglifyJS.SourceMap({
          file: options.outSourceMap,
          orig: inMap,
          root: options.sourceRoot
      });
      var stream = UglifyJS.OutputStream({ source_map: map });
      toplevel.print(stream);
      return {
          code : stream + "",
          map  : map + ""
      };
  };
  
  // exports.describe_ast = function() {
  //     function doitem(ctor) {
  //         var sub = {};
  //         ctor.SUBCLASSES.forEach(function(ctor){
  //             sub[ctor.TYPE] = doitem(ctor);
  //         });
  //         var ret = {};
  //         if (ctor.SELF_PROPS.length > 0) ret.props = ctor.SELF_PROPS;
  //         if (ctor.SUBCLASSES.length > 0) ret.sub = sub;
  //         return ret;
  //     }
  //     return doitem(UglifyJS.AST_Node).sub;
  // }
  
  exports.describe_ast = function() {
      var out = UglifyJS.OutputStream({ beautify: true });
      function doitem(ctor) {
          out.print("AST_" + ctor.TYPE);
          var props = ctor.SELF_PROPS.filter(function(prop){
              return !/^\$/.test(prop);
          });
          if (props.length > 0) {
              out.space();
              out.with_parens(function(){
                  props.forEach(function(prop, i){
                      if (i) out.space();
                      out.print(prop);
                  });
              });
          }
          if (ctor.documentation) {
              out.space();
              out.print_string(ctor.documentation);
          }
          if (ctor.SUBCLASSES.length > 0) {
              out.space();
              out.with_block(function(){
                  ctor.SUBCLASSES.forEach(function(ctor, i){
                      out.indent();
                      doitem(ctor);
                      out.newline();
                  });
              });
          }
      };
      doitem(UglifyJS.AST_Node);
      return out + "";
  };
  

  provide("uglify-js", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*!
   * ENDER - The open module JavaScript framework
   *
   * Copyright (c) 2011-2012 @ded, @fat, @rvagg and other contributors
   *
   * Permission is hereby granted, free of charge, to any person obtaining a copy
   * of this software and associated documentation files (the "Software"), to deal
   * in the Software without restriction, including without limitation the rights
   * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   * copies of the Software, and to permit persons to whom the Software is furnished
   * to do so, subject to the following conditions:
   *
   * The above copyright notice and this permission notice shall be included in all
   * copies or substantial portions of the Software.
   *
   * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   * SOFTWARE.
   */
  
  var minifiers = {
          uglify  : require('./uglify')
        , closure : require('./closure')
      }
  
    , minify = function (minifier, source, options, callback) {
        if (minifiers[minifier]) {
          minifiers[minifier].minify(source, options, callback)
        } else {
          callback('No such minifier "' + minifier + '"')
        }
      }
  
  module.exports = {
      minify        : minify
    , minifiers     : Object.keys(minifiers)
    , closureLevels : minifiers.closure.levels
    , closureJar    : minifiers.closure.jarPath
  }

  provide("ender-minify", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*
  colors.js
  
  Copyright (c) 2010
  
  Marak Squires
  Alexis Sellier (cloudhead)
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  
  */
  
  var isHeadless = false;
  
  if (typeof module !== 'undefined') {
    isHeadless = true;
  }
  
  if (!isHeadless) {
    var exports = {};
    var module = {};
    var colors = exports;
    exports.mode = "browser";
  } else {
    exports.mode = "console";
  }
  
  //
  // Prototypes the string object to have additional method calls that add terminal colors
  //
  var addProperty = function (color, func) {
    var allowOverride = ['bold'];
    exports[color] = function(str) {
      return func.apply(str);
    };
    String.prototype.__defineGetter__(color, func);
  }
  
  //
  // Iterate through all default styles and colors
  //
  
  var x = ['bold', 'underline', 'italic', 'inverse', 'grey', 'black', 'yellow', 'red', 'green', 'blue', 'white', 'cyan', 'magenta'];
  x.forEach(function (style) {
  
    // __defineGetter__ at the least works in more browsers
    // http://robertnyman.com/javascript/javascript-getters-setters.html
    // Object.defineProperty only works in Chrome
    addProperty(style, function () {
      return stylize(this, style);
    });
  });
  
  function sequencer(map) {
    return function () {
      if (!isHeadless) {
        return this.replace(/( )/, '$1');
      }
      var exploded = this.split("");
      var i = 0;
      exploded = exploded.map(map);
      return exploded.join("");
    }
  }
  
  var rainbowMap = (function () {
    var rainbowColors = ['red','yellow','green','blue','magenta']; //RoY G BiV
    return function (letter, i, exploded) {
      if (letter == " ") {
        return letter;
      } else {
        return stylize(letter, rainbowColors[i++ % rainbowColors.length]);
      }
    }
  })();
  
  exports.addSequencer = function (name, map) {
    addProperty(name, sequencer(map));
  }
  
  exports.addSequencer('rainbow', rainbowMap);
  exports.addSequencer('zebra', function (letter, i, exploded) {
    return i % 2 === 0 ? letter : letter.inverse;
  });
  
  exports.setTheme = function (theme) {
    Object.keys(theme).forEach(function(prop){
      addProperty(prop, function(){
        return exports[theme[prop]](this);
      });
    });
  }
  
  function stylize(str, style) {
  
    if (exports.mode == 'console') {
      var styles = {
        //styles
        'bold'      : ['\033[1m',  '\033[22m'],
        'italic'    : ['\033[3m',  '\033[23m'],
        'underline' : ['\033[4m',  '\033[24m'],
        'inverse'   : ['\033[7m',  '\033[27m'],
        //grayscale
        'white'     : ['\033[37m', '\033[39m'],
        'grey'      : ['\033[90m', '\033[39m'],
        'black'     : ['\033[30m', '\033[39m'],
        //colors
        'blue'      : ['\033[34m', '\033[39m'],
        'cyan'      : ['\033[36m', '\033[39m'],
        'green'     : ['\033[32m', '\033[39m'],
        'magenta'   : ['\033[35m', '\033[39m'],
        'red'       : ['\033[31m', '\033[39m'],
        'yellow'    : ['\033[33m', '\033[39m']
      };
    } else if (exports.mode == 'browser') {
      var styles = {
        //styles
        'bold'      : ['<b>',  '</b>'],
        'italic'    : ['<i>',  '</i>'],
        'underline' : ['<u>',  '</u>'],
        'inverse'   : ['<span style="background-color:black;color:white;">',  '</span>'],
        //grayscale
        'white'     : ['<span style="color:white;">',   '</span>'],
        'grey'      : ['<span style="color:grey;">',    '</span>'],
        'black'     : ['<span style="color:black;">',   '</span>'],
        //colors
        'blue'      : ['<span style="color:blue;">',    '</span>'],
        'cyan'      : ['<span style="color:cyan;">',    '</span>'],
        'green'     : ['<span style="color:green;">',   '</span>'],
        'magenta'   : ['<span style="color:magenta;">', '</span>'],
        'red'       : ['<span style="color:red;">',     '</span>'],
        'yellow'    : ['<span style="color:yellow;">',  '</span>']
      };
    } else if (exports.mode == 'none') {
      return str;
    } else {
      console.log('unsupported mode, try "browser", "console" or "none"');
    }
    return styles[style][0] + str + styles[style][1];
  };
  
  // don't summon zalgo
  addProperty('zalgo', function () {
    return zalgo(this);
  });
  
  // please no
  function zalgo(text, options) {
    var soul = {
      "up" : [
        '̍','̎','̄','̅',
        '̿','̑','̆','̐',
        '͒','͗','͑','̇',
        '̈','̊','͂','̓',
        '̈','͊','͋','͌',
        '̃','̂','̌','͐',
        '̀','́','̋','̏',
        '̒','̓','̔','̽',
        '̉','ͣ','ͤ','ͥ',
        'ͦ','ͧ','ͨ','ͩ',
        'ͪ','ͫ','ͬ','ͭ',
        'ͮ','ͯ','̾','͛',
        '͆','̚'
        ],
      "down" : [
        '̖','̗','̘','̙',
        '̜','̝','̞','̟',
        '̠','̤','̥','̦',
        '̩','̪','̫','̬',
        '̭','̮','̯','̰',
        '̱','̲','̳','̹',
        '̺','̻','̼','ͅ',
        '͇','͈','͉','͍',
        '͎','͓','͔','͕',
        '͖','͙','͚','̣'
        ],
      "mid" : [
        '̕','̛','̀','́',
        '͘','̡','̢','̧',
        '̨','̴','̵','̶',
        '͜','͝','͞',
        '͟','͠','͢','̸',
        '̷','͡',' ҉'
        ]
    },
    all = [].concat(soul.up, soul.down, soul.mid),
    zalgo = {};
  
    function randomNumber(range) {
      r = Math.floor(Math.random()*range);
      return r;
    };
  
    function is_char(character) {
      var bool = false;
      all.filter(function(i){
       bool = (i == character);
      });
      return bool;
    }
  
    function heComes(text, options){
        result = '';
        options = options || {};
        options["up"] = options["up"] || true;
        options["mid"] = options["mid"] || true;
        options["down"] = options["down"] || true;
        options["size"] = options["size"] || "maxi";
        var counts;
        text = text.split('');
         for(var l in text){
           if(is_char(l)) { continue; }
           result = result + text[l];
  
          counts = {"up" : 0, "down" : 0, "mid" : 0};
  
          switch(options.size) {
            case 'mini':
              counts.up = randomNumber(8);
              counts.min= randomNumber(2);
              counts.down = randomNumber(8);
            break;
            case 'maxi':
              counts.up = randomNumber(16) + 3;
              counts.min = randomNumber(4) + 1;
              counts.down = randomNumber(64) + 3;
            break;
            default:
              counts.up = randomNumber(8) + 1;
              counts.mid = randomNumber(6) / 2;
              counts.down= randomNumber(8) + 1;
            break;
          }
  
          var arr = ["up", "mid", "down"];
          for(var d in arr){
            var index = arr[d];
            for (var i = 0 ; i <= counts[index]; i++)
            {
              if(options[index]) {
                  result = result + soul[index][randomNumber(soul[index].length)];
                }
              }
            }
          }
        return result;
    };
    return heComes(text);
  }
  
  addProperty('stripColors', function() {
    return ("" + this).replace(/\u001b\[\d+m/g,'');
  });
  

  provide("colors", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*!
   * mustache.js - Logic-less {{mustache}} templates with JavaScript
   * http://github.com/janl/mustache.js
   */
  
  /*global define: false*/
  
  var Mustache;
  
  (function (exports) {
    if (typeof module !== "undefined" && module.exports) {
      module.exports = exports; // CommonJS
    } else if (typeof define === "function") {
      define(exports); // AMD
    } else {
      Mustache = exports; // <script>
    }
  }((function () {
  
    var exports = {};
  
    exports.name = "mustache.js";
    exports.version = "0.7.1";
    exports.tags = ["{{", "}}"];
  
    exports.Scanner = Scanner;
    exports.Context = Context;
    exports.Writer = Writer;
  
    var whiteRe = /\s*/;
    var spaceRe = /\s+/;
    var nonSpaceRe = /\S/;
    var eqRe = /\s*=/;
    var curlyRe = /\s*\}/;
    var tagRe = /#|\^|\/|>|\{|&|=|!/;
  
    // Workaround for https://issues.apache.org/jira/browse/COUCHDB-577
    // See https://github.com/janl/mustache.js/issues/189
    function testRe(re, string) {
      return RegExp.prototype.test.call(re, string);
    }
  
    function isWhitespace(string) {
      return !testRe(nonSpaceRe, string);
    }
  
    var isArray = Array.isArray || function (obj) {
      return Object.prototype.toString.call(obj) === "[object Array]";
    };
  
    function escapeRe(string) {
      return string.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
    }
  
    var entityMap = {
      "&": "&amp;",
      "<": "&lt;",
      ">": "&gt;",
      '"': '&quot;',
      "'": '&#39;',
      "/": '&#x2F;'
    };
  
    function escapeHtml(string) {
      return String(string).replace(/[&<>"'\/]/g, function (s) {
        return entityMap[s];
      });
    }
  
    // Export the escaping function so that the user may override it.
    // See https://github.com/janl/mustache.js/issues/244
    exports.escape = escapeHtml;
  
    function Scanner(string) {
      this.string = string;
      this.tail = string;
      this.pos = 0;
    }
  
    /**
     * Returns `true` if the tail is empty (end of string).
     */
    Scanner.prototype.eos = function () {
      return this.tail === "";
    };
  
    /**
     * Tries to match the given regular expression at the current position.
     * Returns the matched text if it can match, the empty string otherwise.
     */
    Scanner.prototype.scan = function (re) {
      var match = this.tail.match(re);
  
      if (match && match.index === 0) {
        this.tail = this.tail.substring(match[0].length);
        this.pos += match[0].length;
        return match[0];
      }
  
      return "";
    };
  
    /**
     * Skips all text until the given regular expression can be matched. Returns
     * the skipped string, which is the entire tail if no match can be made.
     */
    Scanner.prototype.scanUntil = function (re) {
      var match, pos = this.tail.search(re);
  
      switch (pos) {
      case -1:
        match = this.tail;
        this.pos += this.tail.length;
        this.tail = "";
        break;
      case 0:
        match = "";
        break;
      default:
        match = this.tail.substring(0, pos);
        this.tail = this.tail.substring(pos);
        this.pos += pos;
      }
  
      return match;
    };
  
    function Context(view, parent) {
      this.view = view;
      this.parent = parent;
      this.clearCache();
    }
  
    Context.make = function (view) {
      return (view instanceof Context) ? view : new Context(view);
    };
  
    Context.prototype.clearCache = function () {
      this._cache = {};
    };
  
    Context.prototype.push = function (view) {
      return new Context(view, this);
    };
  
    Context.prototype.lookup = function (name) {
      var value = this._cache[name];
  
      if (!value) {
        if (name === ".") {
          value = this.view;
        } else {
          var context = this;
  
          while (context) {
            if (name.indexOf(".") > 0) {
              var names = name.split("."), i = 0;
  
              value = context.view;
  
              while (value && i < names.length) {
                value = value[names[i++]];
              }
            } else {
              value = context.view[name];
            }
  
            if (value != null) {
              break;
            }
  
            context = context.parent;
          }
        }
  
        this._cache[name] = value;
      }
  
      if (typeof value === "function") {
        value = value.call(this.view);
      }
  
      return value;
    };
  
    function Writer() {
      this.clearCache();
    }
  
    Writer.prototype.clearCache = function () {
      this._cache = {};
      this._partialCache = {};
    };
  
    Writer.prototype.compile = function (template, tags) {
      var fn = this._cache[template];
  
      if (!fn) {
        var tokens = exports.parse(template, tags);
        fn = this._cache[template] = this.compileTokens(tokens, template);
      }
  
      return fn;
    };
  
    Writer.prototype.compilePartial = function (name, template, tags) {
      var fn = this.compile(template, tags);
      this._partialCache[name] = fn;
      return fn;
    };
  
    Writer.prototype.compileTokens = function (tokens, template) {
      var fn = compileTokens(tokens);
      var self = this;
  
      return function (view, partials) {
        if (partials) {
          if (typeof partials === "function") {
            self._loadPartial = partials;
          } else {
            for (var name in partials) {
              self.compilePartial(name, partials[name]);
            }
          }
        }
  
        return fn(self, Context.make(view), template);
      };
    };
  
    Writer.prototype.render = function (template, view, partials) {
      return this.compile(template)(view, partials);
    };
  
    Writer.prototype._section = function (name, context, text, callback) {
      var value = context.lookup(name);
  
      switch (typeof value) {
      case "object":
        if (isArray(value)) {
          var buffer = "";
  
          for (var i = 0, len = value.length; i < len; ++i) {
            buffer += callback(this, context.push(value[i]));
          }
  
          return buffer;
        }
  
        return value ? callback(this, context.push(value)) : "";
      case "function":
        var self = this;
        var scopedRender = function (template) {
          return self.render(template, context);
        };
  
        var result = value.call(context.view, text, scopedRender);
        return result != null ? result : "";
      default:
        if (value) {
          return callback(this, context);
        }
      }
  
      return "";
    };
  
    Writer.prototype._inverted = function (name, context, callback) {
      var value = context.lookup(name);
  
      // Use JavaScript's definition of falsy. Include empty arrays.
      // See https://github.com/janl/mustache.js/issues/186
      if (!value || (isArray(value) && value.length === 0)) {
        return callback(this, context);
      }
  
      return "";
    };
  
    Writer.prototype._partial = function (name, context) {
      if (!(name in this._partialCache) && this._loadPartial) {
        this.compilePartial(name, this._loadPartial(name));
      }
  
      var fn = this._partialCache[name];
  
      return fn ? fn(context) : "";
    };
  
    Writer.prototype._name = function (name, context) {
      var value = context.lookup(name);
  
      if (typeof value === "function") {
        value = value.call(context.view);
      }
  
      return (value == null) ? "" : String(value);
    };
  
    Writer.prototype._escaped = function (name, context) {
      return exports.escape(this._name(name, context));
    };
  
    /**
     * Calculates the bounds of the section represented by the given `token` in
     * the original template by drilling down into nested sections to find the
     * last token that is part of that section. Returns an array of [start, end].
     */
    function sectionBounds(token) {
      var start = token[3];
      var end = start;
  
      var tokens;
      while ((tokens = token[4]) && tokens.length) {
        token = tokens[tokens.length - 1];
        end = token[3];
      }
  
      return [start, end];
    }
  
    /**
     * Low-level function that compiles the given `tokens` into a function
     * that accepts three arguments: a Writer, a Context, and the template.
     */
    function compileTokens(tokens) {
      var subRenders = {};
  
      function subRender(i, tokens, template) {
        if (!subRenders[i]) {
          var fn = compileTokens(tokens);
          subRenders[i] = function (writer, context) {
            return fn(writer, context, template);
          };
        }
  
        return subRenders[i];
      }
  
      return function (writer, context, template) {
        var buffer = "";
        var token, sectionText;
  
        for (var i = 0, len = tokens.length; i < len; ++i) {
          token = tokens[i];
  
          switch (token[0]) {
          case "#":
            sectionText = template.slice.apply(template, sectionBounds(token));
            buffer += writer._section(token[1], context, sectionText, subRender(i, token[4], template));
            break;
          case "^":
            buffer += writer._inverted(token[1], context, subRender(i, token[4], template));
            break;
          case ">":
            buffer += writer._partial(token[1], context);
            break;
          case "&":
            buffer += writer._name(token[1], context);
            break;
          case "name":
            buffer += writer._escaped(token[1], context);
            break;
          case "text":
            buffer += token[1];
            break;
          }
        }
  
        return buffer;
      };
    }
  
    /**
     * Forms the given array of `tokens` into a nested tree structure where
     * tokens that represent a section have a fifth item: an array that contains
     * all tokens in that section.
     */
    function nestTokens(tokens) {
      var tree = [];
      var collector = tree;
      var sections = [];
      var token, section;
  
      for (var i = 0; i < tokens.length; ++i) {
        token = tokens[i];
  
        switch (token[0]) {
        case "#":
        case "^":
          token[4] = [];
          sections.push(token);
          collector.push(token);
          collector = token[4];
          break;
        case "/":
          if (sections.length === 0) {
            throw new Error("Unopened section: " + token[1]);
          }
  
          section = sections.pop();
  
          if (section[1] !== token[1]) {
            throw new Error("Unclosed section: " + section[1]);
          }
  
          if (sections.length > 0) {
            collector = sections[sections.length - 1][4];
          } else {
            collector = tree;
          }
          break;
        default:
          collector.push(token);
        }
      }
  
      // Make sure there were no open sections when we're done.
      section = sections.pop();
  
      if (section) {
        throw new Error("Unclosed section: " + section[1]);
      }
  
      return tree;
    }
  
    /**
     * Combines the values of consecutive text tokens in the given `tokens` array
     * to a single token.
     */
    function squashTokens(tokens) {
      var token, lastToken, squashedTokens = [];
  
      for (var i = 0; i < tokens.length; ++i) {
        token = tokens[i];
  
        if (lastToken && lastToken[0] === "text" && token[0] === "text") {
          lastToken[1] += token[1];
          lastToken[3] = token[3];
        } else {
          lastToken = token;
          squashedTokens.push(token);
        }
      }
  
      return squashedTokens;
    }
  
    function escapeTags(tags) {
      if (tags.length !== 2) {
        throw new Error("Invalid tags: " + tags.join(" "));
      }
  
      return [
        new RegExp(escapeRe(tags[0]) + "\\s*"),
        new RegExp("\\s*" + escapeRe(tags[1]))
      ];
    }
  
    /**
     * Breaks up the given `template` string into a tree of token objects. If
     * `tags` is given here it must be an array with two string values: the
     * opening and closing tags used in the template (e.g. ["<%", "%>"]). Of
     * course, the default is to use mustaches (i.e. Mustache.tags).
     */
    exports.parse = function (template, tags) {
      template = template || '';
      tags = tags || exports.tags;
  
      var tagRes = escapeTags(tags);
      var scanner = new Scanner(template);
  
      var tokens = [],      // Buffer to hold the tokens
          spaces = [],      // Indices of whitespace tokens on the current line
          hasTag = false,   // Is there a {{tag}} on the current line?
          nonSpace = false; // Is there a non-space char on the current line?
  
      // Strips all whitespace tokens array for the current line
      // if there was a {{#tag}} on it and otherwise only space.
      function stripSpace() {
        if (hasTag && !nonSpace) {
          while (spaces.length) {
            tokens.splice(spaces.pop(), 1);
          }
        } else {
          spaces = [];
        }
  
        hasTag = false;
        nonSpace = false;
      }
  
      var start, type, value, chr;
  
      while (!scanner.eos()) {
        start = scanner.pos;
        value = scanner.scanUntil(tagRes[0]);
  
        if (value) {
          for (var i = 0, len = value.length; i < len; ++i) {
            chr = value.charAt(i);
  
            if (isWhitespace(chr)) {
              spaces.push(tokens.length);
            } else {
              nonSpace = true;
            }
  
            tokens.push(["text", chr, start, start + 1]);
            start += 1;
  
            if (chr === "\n") {
              stripSpace(); // Check for whitespace on the current line.
            }
          }
        }
  
        start = scanner.pos;
  
        // Match the opening tag.
        if (!scanner.scan(tagRes[0])) {
          break;
        }
  
        hasTag = true;
        type = scanner.scan(tagRe) || "name";
  
        // Skip any whitespace between tag and value.
        scanner.scan(whiteRe);
  
        // Extract the tag value.
        if (type === "=") {
          value = scanner.scanUntil(eqRe);
          scanner.scan(eqRe);
          scanner.scanUntil(tagRes[1]);
        } else if (type === "{") {
          var closeRe = new RegExp("\\s*" + escapeRe("}" + tags[1]));
          value = scanner.scanUntil(closeRe);
          scanner.scan(curlyRe);
          scanner.scanUntil(tagRes[1]);
          type = "&";
        } else {
          value = scanner.scanUntil(tagRes[1]);
        }
  
        // Match the closing tag.
        if (!scanner.scan(tagRes[1])) {
          throw new Error("Unclosed tag at " + scanner.pos);
        }
  
        tokens.push([type, value, start, scanner.pos]);
  
        if (type === "name" || type === "{" || type === "&") {
          nonSpace = true;
        }
  
        // Set the tags for the next time around.
        if (type === "=") {
          tags = value.split(spaceRe);
          tagRes = escapeTags(tags);
        }
      }
  
      tokens = squashTokens(tokens);
  
      return nestTokens(tokens);
    };
  
    // The high-level clearCache, compile, compilePartial, and render functions
    // use this default writer.
    var _writer = new Writer();
  
    /**
     * Clears all cached templates and partials in the default writer.
     */
    exports.clearCache = function () {
      return _writer.clearCache();
    };
  
    /**
     * Compiles the given `template` to a reusable function using the default
     * writer.
     */
    exports.compile = function (template, tags) {
      return _writer.compile(template, tags);
    };
  
    /**
     * Compiles the partial with the given `name` and `template` to a reusable
     * function using the default writer.
     */
    exports.compilePartial = function (name, template, tags) {
      return _writer.compilePartial(name, template, tags);
    };
  
    /**
     * Compiles the given array of tokens (the output of a parse) to a reusable
     * function using the default writer.
     */
    exports.compileTokens = function (tokens, template) {
      return _writer.compileTokens(tokens, template);
    };
  
    /**
     * Renders the `template` with the given `view` and `partials` using the
     * default writer.
     */
    exports.render = function (template, view, partials) {
      return _writer.render(template, view, partials);
    };
  
    // This is here for backwards compatibility with 0.4.x.
    exports.to_html = function (template, view, partials, send) {
      var result = exports.render(template, view, partials);
  
      if (typeof send === "function") {
        send(result);
      } else {
        return result;
      }
    };
  
    return exports;
  
  }())));
  

  provide("mustache", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  module.exports = process.env.EXPRESS_COV
    ? require('./lib-cov/express')
    : require('./lib/express');

  provide("express", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  module.exports = require('./lib/stache');
  

  provide("stache", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  module.exports = require('./lib/commander');

  provide("commander", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  /**
   * Module dependencies.
   */
  
  var crypto = require('crypto');
  
  /**
   * Sign the given `val` with `secret`.
   *
   * @param {String} val
   * @param {String} secret
   * @return {String}
   * @api private
   */
  
  exports.sign = function(val, secret){
    if ('string' != typeof val) throw new TypeError('cookie required');
    if ('string' != typeof secret) throw new TypeError('secret required');
    return val + '.' + crypto
      .createHmac('sha256', secret)
      .update(val)
      .digest('base64')
      .replace(/\=+$/, '');
  };
  
  /**
   * Unsign and decode the given `val` with `secret`,
   * returning `false` if the signature is invalid.
   *
   * @param {String} val
   * @param {String} secret
   * @return {String|Boolean}
   * @api private
   */
  
  exports.unsign = function(val, secret){
    if ('string' != typeof val) throw new TypeError('cookie required');
    if ('string' != typeof secret) throw new TypeError('secret required');
    var str = val.slice(0, val.lastIndexOf('.'));
    return exports.sign(str, secret) == val ? str : false;
  };

  provide("cookie-signature", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  module.exports = [
      'get'
    , 'post'
    , 'put'
    , 'head'
    , 'delete'
    , 'options'
    , 'trace'
    , 'copy'
    , 'lock'
    , 'mkcol'
    , 'move'
    , 'propfind'
    , 'proppatch'
    , 'unlock'
    , 'report'
    , 'mkactivity'
    , 'checkout'
    , 'merge'
    , 'm-search'
    , 'notify'
    , 'subscribe'
    , 'unsubscribe'
    , 'patch'
  ];

  provide("methods", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  /**
   * Expose `fresh()`.
   */
  
  module.exports = fresh;
  
  /**
   * Check freshness of `req` and `res` headers.
   *
   * When the cache is "fresh" __true__ is returned,
   * otherwise __false__ is returned to indicate that
   * the cache is now stale.
   *
   * @param {Object} req
   * @param {Object} res
   * @return {Boolean}
   * @api public
   */
  
  function fresh(req, res) {
    // defaults
    var etagMatches = true;
    var notModified = true;
  
    // fields
    var modifiedSince = req['if-modified-since'];
    var noneMatch = req['if-none-match'];
    var lastModified = res['last-modified'];
    var etag = res['etag'];
  
    // unconditional request
    if (!modifiedSince && !noneMatch) return false;
  
    // parse if-none-match
    if (noneMatch) noneMatch = noneMatch.split(/ *, */);
  
    // if-none-match
    if (noneMatch) etagMatches = ~noneMatch.indexOf(etag) || '*' == noneMatch[0];
  
    // if-modified-since
    if (modifiedSince) {
      modifiedSince = new Date(modifiedSince);
      lastModified = new Date(lastModified);
      notModified = lastModified <= modifiedSince;
    }
  
    return !! (etagMatches && notModified);
  }

  provide("fresh", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  /**
   * Parse "Range" header `str` relative to the given file `size`.
   *
   * @param {Number} size
   * @param {String} str
   * @return {Array}
   * @api public
   */
  
  module.exports = function(size, str){
    var valid = true;
    var i = str.indexOf('=');
  
    if (-1 == i) return -2;
  
    var arr = str.slice(i + 1).split(',').map(function(range){
      var range = range.split('-')
        , start = parseInt(range[0], 10)
        , end = parseInt(range[1], 10);
  
      // -nnn
      if (isNaN(start)) {
        start = size - end;
        end = size - 1;
      // nnn-
      } else if (isNaN(end)) {
        end = size - 1;
      }
  
      // limit last-byte-pos to current length
      if (end > size - 1) end = size - 1;
  
      // invalid
      if (isNaN(start)
        || isNaN(end)
        || start > end
        || start < 0) valid = false;
  
      return {
        start: start,
        end: end
      };
    });
  
    arr.type = str.slice(0, i);
  
    return valid ? arr : -1;
  };

  provide("range-parser", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  var Buffer = require('buffer').Buffer;
  
  var CRC_TABLE = [
    0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419,
    0x706af48f, 0xe963a535, 0x9e6495a3, 0x0edb8832, 0x79dcb8a4,
    0xe0d5e91e, 0x97d2d988, 0x09b64c2b, 0x7eb17cbd, 0xe7b82d07,
    0x90bf1d91, 0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de,
    0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7, 0x136c9856,
    0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 0x14015c4f, 0x63066cd9,
    0xfa0f3d63, 0x8d080df5, 0x3b6e20c8, 0x4c69105e, 0xd56041e4,
    0xa2677172, 0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,
    0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940, 0x32d86ce3,
    0x45df5c75, 0xdcd60dcf, 0xabd13d59, 0x26d930ac, 0x51de003a,
    0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423, 0xcfba9599,
    0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
    0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d, 0x76dc4190,
    0x01db7106, 0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f,
    0x9fbfe4a5, 0xe8b8d433, 0x7807c9a2, 0x0f00f934, 0x9609a88e,
    0xe10e9818, 0x7f6a0dbb, 0x086d3d2d, 0x91646c97, 0xe6635c01,
    0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e, 0x6c0695ed,
    0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
    0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3,
    0xfbd44c65, 0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2,
    0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a,
    0x346ed9fc, 0xad678846, 0xda60b8d0, 0x44042d73, 0x33031de5,
    0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa, 0xbe0b1010,
    0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
    0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17,
    0x2eb40d81, 0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6,
    0x03b6e20c, 0x74b1d29a, 0xead54739, 0x9dd277af, 0x04db2615,
    0x73dc1683, 0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8,
    0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1, 0xf00f9344,
    0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb,
    0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a,
    0x67dd4acc, 0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5,
    0xd6d6a3e8, 0xa1d1937e, 0x38d8c2c4, 0x4fdff252, 0xd1bb67f1,
    0xa6bc5767, 0x3fb506dd, 0x48b2364b, 0xd80d2bda, 0xaf0a1b4c,
    0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55, 0x316e8eef,
    0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
    0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe,
    0xb2bd0b28, 0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31,
    0x2cd99e8b, 0x5bdeae1d, 0x9b64c2b0, 0xec63f226, 0x756aa39c,
    0x026d930a, 0x9c0906a9, 0xeb0e363f, 0x72076785, 0x05005713,
    0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38, 0x92d28e9b,
    0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242,
    0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1,
    0x18b74777, 0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c,
    0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45, 0xa00ae278,
    0xd70dd2ee, 0x4e048354, 0x3903b3c2, 0xa7672661, 0xd06016f7,
    0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc, 0x40df0b66,
    0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
    0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605,
    0xcdd70693, 0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8,
    0x5d681b02, 0x2a6f2b94, 0xb40bbe37, 0xc30c8ea1, 0x5a05df1b,
    0x2d02ef8d
  ];
  
  function bufferizeInt(num) {
    var tmp = Buffer(4);
    tmp.writeInt32BE(num, 0);
    return tmp;
  }
  
  function _crc32(buf) {
    if (!Buffer.isBuffer(buf))
      buf = Buffer(buf);
    var crc = 0xffffffff;
    for (var n = 0; n < buf.length; n++) {
      crc = CRC_TABLE[(crc ^ buf[n]) & 0xff] ^ (crc >>> 8);
    }
    return (crc ^ 0xffffffff);
  }
  
  function crc32() {
    return bufferizeInt(_crc32.apply(null, arguments));
  }
  crc32.signed = function () {
    return _crc32.apply(null, arguments);
  };
  crc32.unsigned = function () {
    return crc32.apply(null, arguments).readUInt32BE(0);
  };
  
  module.exports = crc32;
  

  provide("buffer-crc32", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  var path = require('path');
  var fs = require('fs');
  
  module.exports = mkdirP.mkdirp = mkdirP.mkdirP = mkdirP;
  
  function mkdirP (p, mode, f, made) {
      if (typeof mode === 'function' || mode === undefined) {
          f = mode;
          mode = 0777 & (~process.umask());
      }
      if (!made) made = null;
  
      var cb = f || function () {};
      if (typeof mode === 'string') mode = parseInt(mode, 8);
      p = path.resolve(p);
  
      fs.mkdir(p, mode, function (er) {
          if (!er) {
              made = made || p;
              return cb(null, made);
          }
          switch (er.code) {
              case 'ENOENT':
                  mkdirP(path.dirname(p), mode, function (er, made) {
                      if (er) cb(er, made);
                      else mkdirP(p, mode, cb, made);
                  });
                  break;
  
              case 'EISDIR':
              case 'EPERM':
                  // Operation not permitted or already is a dir.
                  // This is the error you get when trying to mkdir('c:/')
                  // on windows, or mkdir('/') on unix.  Make sure it's a
                  // dir by falling through to the EEXIST case.
              case 'EROFS':
                  // a read-only file system.
                  // However, the dir could already exist, in which case
                  // the EROFS error will be obscuring a EEXIST!
                  // Fallthrough to that case.
              case 'EEXIST':
                  fs.stat(p, function (er2, stat) {
                      // if the stat fails, then that's super weird.
                      // let the original error be the failure reason.
                      if (er2 || !stat.isDirectory()) cb(er, made)
                      else cb(null, made);
                  });
                  break;
  
              default:
                  cb(er, made);
                  break;
          }
      });
  }
  
  mkdirP.sync = function sync (p, mode, made) {
      if (mode === undefined) {
          mode = 0777 & (~process.umask());
      }
      if (!made) made = null;
  
      if (typeof mode === 'string') mode = parseInt(mode, 8);
      p = path.resolve(p);
  
      try {
          fs.mkdirSync(p, mode);
          made = made || p;
      }
      catch (err0) {
          switch (err0.code) {
              case 'ENOENT' :
                  made = sync(path.dirname(p), mode, made);
                  sync(p, mode, made);
                  break;
  
              case 'EEXIST' :
                  var stat;
                  try {
                      stat = fs.statSync(p);
                  }
                  catch (err1) {
                      throw err0;
                  }
                  if (!stat.isDirectory()) throw err0;
                  break;
              default :
                  throw err0
                  break;
          }
      }
  
      return made;
  };
  

  provide("mkdirp", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  module.exports = require('./lib/debug');

  provide("debug", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  /// Serialize the a name value pair into a cookie string suitable for
  /// http headers. An optional options object specified cookie parameters
  ///
  /// serialize('foo', 'bar', { httpOnly: true })
  ///   => "foo=bar; httpOnly"
  ///
  /// @param {String} name
  /// @param {String} val
  /// @param {Object} options
  /// @return {String}
  var serialize = function(name, val, opt){
      var pairs = [name + '=' + encode(val)];
      opt = opt || {};
  
      if (opt.maxAge) pairs.push('Max-Age=' + opt.maxAge);
      if (opt.domain) pairs.push('Domain=' + opt.domain);
      if (opt.path) pairs.push('Path=' + opt.path);
      if (opt.expires) pairs.push('Expires=' + opt.expires.toUTCString());
      if (opt.httpOnly) pairs.push('HttpOnly');
      if (opt.secure) pairs.push('Secure');
  
      return pairs.join('; ');
  };
  
  /// Parse the given cookie header string into an object
  /// The object has the various cookies as keys(names) => values
  /// @param {String} str
  /// @return {Object}
  var parse = function(str) {
      var obj = {}
      var pairs = str.split(/[;,] */);
  
      pairs.forEach(function(pair) {
          var eq_idx = pair.indexOf('=')
          var key = pair.substr(0, eq_idx).trim()
          var val = pair.substr(++eq_idx, pair.length).trim();
  
          // quoted values
          if ('"' == val[0]) {
              val = val.slice(1, -1);
          }
  
          // only assign once
          if (undefined == obj[key]) {
              try {
                  obj[key] = decode(val);
              } catch (e) {
                  obj[key] = val;
              }
          }
      });
  
      return obj;
  };
  
  var encode = encodeURIComponent;
  var decode = decodeURIComponent;
  
  module.exports.serialize = serialize;
  module.exports.parse = parse;
  

  provide("cookie", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  module.exports = require('./lib/querystring');

  provide("qs", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  module.exports = function(obj){
    var onData
      , onEnd
      , events = [];
  
    // buffer data
    obj.on('data', onData = function(data, encoding){
      events.push(['data', data, encoding]);
    });
  
    // buffer end
    obj.on('end', onEnd = function(data, encoding){
      events.push(['end', data, encoding]);
    });
  
    return {
      end: function(){
        obj.removeListener('data', onData);
        obj.removeListener('end', onEnd);
      },
      resume: function(){
        this.end();
        for (var i = 0, len = events.length; i < len; ++i) {
          obj.emit.apply(obj, events[i]);
        }
      }
    };
  };

  provide("pause", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  var IncomingForm = require('./incoming_form').IncomingForm;
  IncomingForm.IncomingForm = IncomingForm;
  module.exports = IncomingForm;
  

  provide("formidable", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  (function()
  {
      // CRC-8 in table form
      // 
      // Copyright (c) 1989 AnDan Software. You may use this program, or
      // code or tables extracted from it, as long as this notice is not
      // removed or changed.
      var CRC8_TAB = new Array(
          // C/C++ language:
          // 
          // unsigned char CRC8_TAB[] = {...};
          0x00,0x1B,0x36,0x2D,0x6C,0x77,0x5A,0x41,0xD8,0xC3,0xEE,0xF5,0xB4,0xAF,0x82,0x99,0xD3,0xC8,0xE5,
          0xFE,0xBF,0xA4,0x89,0x92,0x0B,0x10,0x3D,0x26,0x67,0x7C,0x51,0x4A,0xC5,0xDE,0xF3,0xE8,0xA9,0xB2,
          0x9F,0x84,0x1D,0x06,0x2B,0x30,0x71,0x6A,0x47,0x5C,0x16,0x0D,0x20,0x3B,0x7A,0x61,0x4C,0x57,0xCE,
          0xD5,0xF8,0xE3,0xA2,0xB9,0x94,0x8F,0xE9,0xF2,0xDF,0xC4,0x85,0x9E,0xB3,0xA8,0x31,0x2A,0x07,0x1C,
          0x5D,0x46,0x6B,0x70,0x3A,0x21,0x0C,0x17,0x56,0x4D,0x60,0x7B,0xE2,0xF9,0xD4,0xCF,0x8E,0x95,0xB8,
          0xA3,0x2C,0x37,0x1A,0x01,0x40,0x5B,0x76,0x6D,0xF4,0xEF,0xC2,0xD9,0x98,0x83,0xAE,0xB5,0xFF,0xE4,
          0xC9,0xD2,0x93,0x88,0xA5,0xBE,0x27,0x3C,0x11,0x0A,0x4B,0x50,0x7D,0x66,0xB1,0xAA,0x87,0x9C,0xDD,
          0xC6,0xEB,0xF0,0x69,0x72,0x5F,0x44,0x05,0x1E,0x33,0x28,0x62,0x79,0x54,0x4F,0x0E,0x15,0x38,0x23,
          0xBA,0xA1,0x8C,0x97,0xD6,0xCD,0xE0,0xFB,0x74,0x6F,0x42,0x59,0x18,0x03,0x2E,0x35,0xAC,0xB7,0x9A,
          0x81,0xC0,0xDB,0xF6,0xED,0xA7,0xBC,0x91,0x8A,0xCB,0xD0,0xFD,0xE6,0x7F,0x64,0x49,0x52,0x13,0x08,
          0x25,0x3E,0x58,0x43,0x6E,0x75,0x34,0x2F,0x02,0x19,0x80,0x9B,0xB6,0xAD,0xEC,0xF7,0xDA,0xC1,0x8B,
          0x90,0xBD,0xA6,0xE7,0xFC,0xD1,0xCA,0x53,0x48,0x65,0x7E,0x3F,0x24,0x09,0x12,0x9D,0x86,0xAB,0xB0,
          0xF1,0xEA,0xC7,0xDC,0x45,0x5E,0x73,0x68,0x29,0x32,0x1F,0x04,0x4E,0x55,0x78,0x63,0x22,0x39,0x14,
          0x0F,0x96,0x8D,0xA0,0xBB,0xFA,0xE1,0xCC,0xD7
      );
  
      function crc8Add(crc,c)
      // 'crc' should be initialized to 0x00.
      {
          return CRC8_TAB[(crc^c)&0xFF];
      };
      // C/C++ language:
      // 
      // inline unsigned char crc8Add(unsigned char crc, unsigned char c)
      // {
      //     return CRC8_TAB[crc^c];
      // }
  
      // CRC-16 (as it is in SEA's ARC) in table form
      // 
      // The logic for this method of calculating the CRC 16 bit polynomial
      // is taken from an article by David Schwaderer in the April 1985
      // issue of PC Tech Journal.
      var CRC_ARC_TAB = new Array(
          // C/C++ language:
          // 
          // unsigned short CRC_ARC_TAB[] = {...};
          0x0000,0xC0C1,0xC181,0x0140,0xC301,0x03C0,0x0280,0xC241,0xC601,0x06C0,0x0780,0xC741,0x0500,
          0xC5C1,0xC481,0x0440,0xCC01,0x0CC0,0x0D80,0xCD41,0x0F00,0xCFC1,0xCE81,0x0E40,0x0A00,0xCAC1,
          0xCB81,0x0B40,0xC901,0x09C0,0x0880,0xC841,0xD801,0x18C0,0x1980,0xD941,0x1B00,0xDBC1,0xDA81,
          0x1A40,0x1E00,0xDEC1,0xDF81,0x1F40,0xDD01,0x1DC0,0x1C80,0xDC41,0x1400,0xD4C1,0xD581,0x1540,
          0xD701,0x17C0,0x1680,0xD641,0xD201,0x12C0,0x1380,0xD341,0x1100,0xD1C1,0xD081,0x1040,0xF001,
          0x30C0,0x3180,0xF141,0x3300,0xF3C1,0xF281,0x3240,0x3600,0xF6C1,0xF781,0x3740,0xF501,0x35C0,
          0x3480,0xF441,0x3C00,0xFCC1,0xFD81,0x3D40,0xFF01,0x3FC0,0x3E80,0xFE41,0xFA01,0x3AC0,0x3B80,
          0xFB41,0x3900,0xF9C1,0xF881,0x3840,0x2800,0xE8C1,0xE981,0x2940,0xEB01,0x2BC0,0x2A80,0xEA41,
          0xEE01,0x2EC0,0x2F80,0xEF41,0x2D00,0xEDC1,0xEC81,0x2C40,0xE401,0x24C0,0x2580,0xE541,0x2700,
          0xE7C1,0xE681,0x2640,0x2200,0xE2C1,0xE381,0x2340,0xE101,0x21C0,0x2080,0xE041,0xA001,0x60C0,
          0x6180,0xA141,0x6300,0xA3C1,0xA281,0x6240,0x6600,0xA6C1,0xA781,0x6740,0xA501,0x65C0,0x6480,
          0xA441,0x6C00,0xACC1,0xAD81,0x6D40,0xAF01,0x6FC0,0x6E80,0xAE41,0xAA01,0x6AC0,0x6B80,0xAB41,
          0x6900,0xA9C1,0xA881,0x6840,0x7800,0xB8C1,0xB981,0x7940,0xBB01,0x7BC0,0x7A80,0xBA41,0xBE01,
          0x7EC0,0x7F80,0xBF41,0x7D00,0xBDC1,0xBC81,0x7C40,0xB401,0x74C0,0x7580,0xB541,0x7700,0xB7C1,
          0xB681,0x7640,0x7200,0xB2C1,0xB381,0x7340,0xB101,0x71C0,0x7080,0xB041,0x5000,0x90C1,0x9181,
          0x5140,0x9301,0x53C0,0x5280,0x9241,0x9601,0x56C0,0x5780,0x9741,0x5500,0x95C1,0x9481,0x5440,
          0x9C01,0x5CC0,0x5D80,0x9D41,0x5F00,0x9FC1,0x9E81,0x5E40,0x5A00,0x9AC1,0x9B81,0x5B40,0x9901,
          0x59C0,0x5880,0x9841,0x8801,0x48C0,0x4980,0x8941,0x4B00,0x8BC1,0x8A81,0x4A40,0x4E00,0x8EC1,
          0x8F81,0x4F40,0x8D01,0x4DC0,0x4C80,0x8C41,0x4400,0x84C1,0x8581,0x4540,0x8701,0x47C0,0x4680,
          0x8641,0x8201,0x42C0,0x4380,0x8341,0x4100,0x81C1,0x8081,0x4040
      );
  
      function crcArcAdd(crc,c)
      // 'crc' should be initialized to 0x0000.
      {
          return CRC_ARC_TAB[(crc^c)&0xFF]^((crc>>8)&0xFF);
      };
      // C/C++ language:
      // 
      // inline unsigned short crcArcAdd(unsigned short crc, unsigned char c)
      // {
      //     return CRC_ARC_TAB[(unsigned char)crc^c]^(unsigned short)(crc>>8);
      // }
  
      // CRC-16 (as it is in ZMODEM) in table form
      // 
      // Copyright (c) 1989 AnDan Software. You may use this program, or
      // code or tables extracted from it, as long as this notice is not
      // removed or changed.
      var CRC16_TAB = new Array(
          // C/C++ language:
          // 
          // unsigned short CRC16_TAB[] = {...};
          0x0000,0x1021,0x2042,0x3063,0x4084,0x50A5,0x60C6,0x70E7,0x8108,0x9129,0xA14A,0xB16B,0xC18C,
          0xD1AD,0xE1CE,0xF1EF,0x1231,0x0210,0x3273,0x2252,0x52B5,0x4294,0x72F7,0x62D6,0x9339,0x8318,
          0xB37B,0xA35A,0xD3BD,0xC39C,0xF3FF,0xE3DE,0x2462,0x3443,0x0420,0x1401,0x64E6,0x74C7,0x44A4,
          0x5485,0xA56A,0xB54B,0x8528,0x9509,0xE5EE,0xF5CF,0xC5AC,0xD58D,0x3653,0x2672,0x1611,0x0630,
          0x76D7,0x66F6,0x5695,0x46B4,0xB75B,0xA77A,0x9719,0x8738,0xF7DF,0xE7FE,0xD79D,0xC7BC,0x48C4,
          0x58E5,0x6886,0x78A7,0x0840,0x1861,0x2802,0x3823,0xC9CC,0xD9ED,0xE98E,0xF9AF,0x8948,0x9969,
          0xA90A,0xB92B,0x5AF5,0x4AD4,0x7AB7,0x6A96,0x1A71,0x0A50,0x3A33,0x2A12,0xDBFD,0xCBDC,0xFBBF,
          0xEB9E,0x9B79,0x8B58,0xBB3B,0xAB1A,0x6CA6,0x7C87,0x4CE4,0x5CC5,0x2C22,0x3C03,0x0C60,0x1C41,
          0xEDAE,0xFD8F,0xCDEC,0xDDCD,0xAD2A,0xBD0B,0x8D68,0x9D49,0x7E97,0x6EB6,0x5ED5,0x4EF4,0x3E13,
          0x2E32,0x1E51,0x0E70,0xFF9F,0xEFBE,0xDFDD,0xCFFC,0xBF1B,0xAF3A,0x9F59,0x8F78,0x9188,0x81A9,
          0xB1CA,0xA1EB,0xD10C,0xC12D,0xF14E,0xE16F,0x1080,0x00A1,0x30C2,0x20E3,0x5004,0x4025,0x7046,
          0x6067,0x83B9,0x9398,0xA3FB,0xB3DA,0xC33D,0xD31C,0xE37F,0xF35E,0x02B1,0x1290,0x22F3,0x32D2,
          0x4235,0x5214,0x6277,0x7256,0xB5EA,0xA5CB,0x95A8,0x8589,0xF56E,0xE54F,0xD52C,0xC50D,0x34E2,
          0x24C3,0x14A0,0x0481,0x7466,0x6447,0x5424,0x4405,0xA7DB,0xB7FA,0x8799,0x97B8,0xE75F,0xF77E,
          0xC71D,0xD73C,0x26D3,0x36F2,0x0691,0x16B0,0x6657,0x7676,0x4615,0x5634,0xD94C,0xC96D,0xF90E,
          0xE92F,0x99C8,0x89E9,0xB98A,0xA9AB,0x5844,0x4865,0x7806,0x6827,0x18C0,0x08E1,0x3882,0x28A3,
          0xCB7D,0xDB5C,0xEB3F,0xFB1E,0x8BF9,0x9BD8,0xABBB,0xBB9A,0x4A75,0x5A54,0x6A37,0x7A16,0x0AF1,
          0x1AD0,0x2AB3,0x3A92,0xFD2E,0xED0F,0xDD6C,0xCD4D,0xBDAA,0xAD8B,0x9DE8,0x8DC9,0x7C26,0x6C07,
          0x5C64,0x4C45,0x3CA2,0x2C83,0x1CE0,0x0CC1,0xEF1F,0xFF3E,0xCF5D,0xDF7C,0xAF9B,0xBFBA,0x8FD9,
          0x9FF8,0x6E17,0x7E36,0x4E55,0x5E74,0x2E93,0x3EB2,0x0ED1,0x1EF0
      );
  
      function crc16Add(crc,c)
      // 'crc' should be initialized to 0x0000.
      {
          return CRC16_TAB[((crc>>8)^c)&0xFF]^((crc<<8)&0xFFFF);
      };
      // C/C++ language:
      // 
      // inline unsigned short crc16Add(unsigned short crc, unsigned char c)
      // {
      //     return CRC16_TAB[(unsigned char)(crc>>8)^c]^(unsigned short)(crc<<8);
      // }
  
      // FCS-16 (as it is in PPP) in table form
      // 
      // Described in RFC-1662 by William Allen Simpson, see RFC-1662 for references.
      // 
      // Modified by Anders Danielsson, March 10, 2006.
      var FCS_16_TAB = new Array(
          // C/C++ language:
          // 
          // unsigned short FCS_16_TAB[256] = {...};
          0x0000,0x1189,0x2312,0x329B,0x4624,0x57AD,0x6536,0x74BF,0x8C48,0x9DC1,0xAF5A,0xBED3,0xCA6C,
          0xDBE5,0xE97E,0xF8F7,0x1081,0x0108,0x3393,0x221A,0x56A5,0x472C,0x75B7,0x643E,0x9CC9,0x8D40,
          0xBFDB,0xAE52,0xDAED,0xCB64,0xF9FF,0xE876,0x2102,0x308B,0x0210,0x1399,0x6726,0x76AF,0x4434,
          0x55BD,0xAD4A,0xBCC3,0x8E58,0x9FD1,0xEB6E,0xFAE7,0xC87C,0xD9F5,0x3183,0x200A,0x1291,0x0318,
          0x77A7,0x662E,0x54B5,0x453C,0xBDCB,0xAC42,0x9ED9,0x8F50,0xFBEF,0xEA66,0xD8FD,0xC974,0x4204,
          0x538D,0x6116,0x709F,0x0420,0x15A9,0x2732,0x36BB,0xCE4C,0xDFC5,0xED5E,0xFCD7,0x8868,0x99E1,
          0xAB7A,0xBAF3,0x5285,0x430C,0x7197,0x601E,0x14A1,0x0528,0x37B3,0x263A,0xDECD,0xCF44,0xFDDF,
          0xEC56,0x98E9,0x8960,0xBBFB,0xAA72,0x6306,0x728F,0x4014,0x519D,0x2522,0x34AB,0x0630,0x17B9,
          0xEF4E,0xFEC7,0xCC5C,0xDDD5,0xA96A,0xB8E3,0x8A78,0x9BF1,0x7387,0x620E,0x5095,0x411C,0x35A3,
          0x242A,0x16B1,0x0738,0xFFCF,0xEE46,0xDCDD,0xCD54,0xB9EB,0xA862,0x9AF9,0x8B70,0x8408,0x9581,
          0xA71A,0xB693,0xC22C,0xD3A5,0xE13E,0xF0B7,0x0840,0x19C9,0x2B52,0x3ADB,0x4E64,0x5FED,0x6D76,
          0x7CFF,0x9489,0x8500,0xB79B,0xA612,0xD2AD,0xC324,0xF1BF,0xE036,0x18C1,0x0948,0x3BD3,0x2A5A,
          0x5EE5,0x4F6C,0x7DF7,0x6C7E,0xA50A,0xB483,0x8618,0x9791,0xE32E,0xF2A7,0xC03C,0xD1B5,0x2942,
          0x38CB,0x0A50,0x1BD9,0x6F66,0x7EEF,0x4C74,0x5DFD,0xB58B,0xA402,0x9699,0x8710,0xF3AF,0xE226,
          0xD0BD,0xC134,0x39C3,0x284A,0x1AD1,0x0B58,0x7FE7,0x6E6E,0x5CF5,0x4D7C,0xC60C,0xD785,0xE51E,
          0xF497,0x8028,0x91A1,0xA33A,0xB2B3,0x4A44,0x5BCD,0x6956,0x78DF,0x0C60,0x1DE9,0x2F72,0x3EFB,
          0xD68D,0xC704,0xF59F,0xE416,0x90A9,0x8120,0xB3BB,0xA232,0x5AC5,0x4B4C,0x79D7,0x685E,0x1CE1,
          0x0D68,0x3FF3,0x2E7A,0xE70E,0xF687,0xC41C,0xD595,0xA12A,0xB0A3,0x8238,0x93B1,0x6B46,0x7ACF,
          0x4854,0x59DD,0x2D62,0x3CEB,0x0E70,0x1FF9,0xF78F,0xE606,0xD49D,0xC514,0xB1AB,0xA022,0x92B9,
          0x8330,0x7BC7,0x6A4E,0x58D5,0x495C,0x3DE3,0x2C6A,0x1EF1,0x0F78
      );
  
      function fcs16Add(fcs,c)
      // 'fcs' should be initialized to 0xFFFF and after the computation it should be
      // complemented (inverted).
      // 
      // If the FCS-16 is calculated over the data and over the complemented FCS-16, the
      // result will always be 0xF0B8 (without the complementation).
      {
          return FCS_16_TAB[(fcs^c)&0xFF]^((fcs>>8)&0xFF);
      };
  
      // C/C++ language:
      // 
      // inline unsigned short fcs16Add(unsigned short fcs, unsigned char c)
      // {
      //     return FCS_16_TAB[(unsigned char)fcs^c]^(unsigned short)(fcs>>8);
      // }
  
      //
      // CRC-32 (as it is in ZMODEM) in table form
      // 
      // Copyright (C) 1986 Gary S. Brown. You may use this program, or
      // code or tables extracted from it, as desired without restriction.
      // 
      // Modified by Anders Danielsson, February 5, 1989 and March 10, 2006.
      // 
      // This is also known as FCS-32 (as it is in PPP), described in
      // RFC-1662 by William Allen Simpson, see RFC-1662 for references.
      // 
      var CRC32_TAB = new Array( /* CRC polynomial 0xEDB88320 */
          // C/C++ language:
          // 
          // unsigned long CRC32_TAB[] = {...};
          0x00000000,0x77073096,0xEE0E612C,0x990951BA,0x076DC419,0x706AF48F,0xE963A535,0x9E6495A3,
          0x0EDB8832,0x79DCB8A4,0xE0D5E91E,0x97D2D988,0x09B64C2B,0x7EB17CBD,0xE7B82D07,0x90BF1D91,
          0x1DB71064,0x6AB020F2,0xF3B97148,0x84BE41DE,0x1ADAD47D,0x6DDDE4EB,0xF4D4B551,0x83D385C7,
          0x136C9856,0x646BA8C0,0xFD62F97A,0x8A65C9EC,0x14015C4F,0x63066CD9,0xFA0F3D63,0x8D080DF5,
          0x3B6E20C8,0x4C69105E,0xD56041E4,0xA2677172,0x3C03E4D1,0x4B04D447,0xD20D85FD,0xA50AB56B,
          0x35B5A8FA,0x42B2986C,0xDBBBC9D6,0xACBCF940,0x32D86CE3,0x45DF5C75,0xDCD60DCF,0xABD13D59,
          0x26D930AC,0x51DE003A,0xC8D75180,0xBFD06116,0x21B4F4B5,0x56B3C423,0xCFBA9599,0xB8BDA50F,
          0x2802B89E,0x5F058808,0xC60CD9B2,0xB10BE924,0x2F6F7C87,0x58684C11,0xC1611DAB,0xB6662D3D,
          0x76DC4190,0x01DB7106,0x98D220BC,0xEFD5102A,0x71B18589,0x06B6B51F,0x9FBFE4A5,0xE8B8D433,
          0x7807C9A2,0x0F00F934,0x9609A88E,0xE10E9818,0x7F6A0DBB,0x086D3D2D,0x91646C97,0xE6635C01,
          0x6B6B51F4,0x1C6C6162,0x856530D8,0xF262004E,0x6C0695ED,0x1B01A57B,0x8208F4C1,0xF50FC457,
          0x65B0D9C6,0x12B7E950,0x8BBEB8EA,0xFCB9887C,0x62DD1DDF,0x15DA2D49,0x8CD37CF3,0xFBD44C65,
          0x4DB26158,0x3AB551CE,0xA3BC0074,0xD4BB30E2,0x4ADFA541,0x3DD895D7,0xA4D1C46D,0xD3D6F4FB,
          0x4369E96A,0x346ED9FC,0xAD678846,0xDA60B8D0,0x44042D73,0x33031DE5,0xAA0A4C5F,0xDD0D7CC9,
          0x5005713C,0x270241AA,0xBE0B1010,0xC90C2086,0x5768B525,0x206F85B3,0xB966D409,0xCE61E49F,
          0x5EDEF90E,0x29D9C998,0xB0D09822,0xC7D7A8B4,0x59B33D17,0x2EB40D81,0xB7BD5C3B,0xC0BA6CAD,
          0xEDB88320,0x9ABFB3B6,0x03B6E20C,0x74B1D29A,0xEAD54739,0x9DD277AF,0x04DB2615,0x73DC1683,
          0xE3630B12,0x94643B84,0x0D6D6A3E,0x7A6A5AA8,0xE40ECF0B,0x9309FF9D,0x0A00AE27,0x7D079EB1,
          0xF00F9344,0x8708A3D2,0x1E01F268,0x6906C2FE,0xF762575D,0x806567CB,0x196C3671,0x6E6B06E7,
          0xFED41B76,0x89D32BE0,0x10DA7A5A,0x67DD4ACC,0xF9B9DF6F,0x8EBEEFF9,0x17B7BE43,0x60B08ED5,
          0xD6D6A3E8,0xA1D1937E,0x38D8C2C4,0x4FDFF252,0xD1BB67F1,0xA6BC5767,0x3FB506DD,0x48B2364B,
          0xD80D2BDA,0xAF0A1B4C,0x36034AF6,0x41047A60,0xDF60EFC3,0xA867DF55,0x316E8EEF,0x4669BE79,
          0xCB61B38C,0xBC66831A,0x256FD2A0,0x5268E236,0xCC0C7795,0xBB0B4703,0x220216B9,0x5505262F,
          0xC5BA3BBE,0xB2BD0B28,0x2BB45A92,0x5CB36A04,0xC2D7FFA7,0xB5D0CF31,0x2CD99E8B,0x5BDEAE1D,
          0x9B64C2B0,0xEC63F226,0x756AA39C,0x026D930A,0x9C0906A9,0xEB0E363F,0x72076785,0x05005713,
          0x95BF4A82,0xE2B87A14,0x7BB12BAE,0x0CB61B38,0x92D28E9B,0xE5D5BE0D,0x7CDCEFB7,0x0BDBDF21,
          0x86D3D2D4,0xF1D4E242,0x68DDB3F8,0x1FDA836E,0x81BE16CD,0xF6B9265B,0x6FB077E1,0x18B74777,
          0x88085AE6,0xFF0F6A70,0x66063BCA,0x11010B5C,0x8F659EFF,0xF862AE69,0x616BFFD3,0x166CCF45,
          0xA00AE278,0xD70DD2EE,0x4E048354,0x3903B3C2,0xA7672661,0xD06016F7,0x4969474D,0x3E6E77DB,
          0xAED16A4A,0xD9D65ADC,0x40DF0B66,0x37D83BF0,0xA9BCAE53,0xDEBB9EC5,0x47B2CF7F,0x30B5FFE9,
          0xBDBDF21C,0xCABAC28A,0x53B39330,0x24B4A3A6,0xBAD03605,0xCDD70693,0x54DE5729,0x23D967BF,
          0xB3667A2E,0xC4614AB8,0x5D681B02,0x2A6F2B94,0xB40BBE37,0xC30C8EA1,0x5A05DF1B,0x2D02EF8D
      );
  
      function crc32Add(crc,c)
      // 'crc' should be initialized to 0xFFFFFFFF and after the computation it should be
      // complemented (inverted).
      // 
      // CRC-32 is also known as FCS-32.
      // 
      // If the FCS-32 is calculated over the data and over the complemented FCS-32, the
      // result will always be 0xDEBB20E3 (without the complementation).
      {
          return CRC32_TAB[(crc^c)&0xFF]^((crc>>8)&0xFFFFFF);
      };
      //
      // C/C++ language:
      // 
      // inline unsigned long crc32Add(unsigned long crc, unsigned char c)
      // {
      //     return CRC32_TAB[(unsigned char)crc^c]^(crc>>8);
      // }
      //
  
      function crc8(str)
      {
          var n,
              len = str.length,
              crc = 0
              ;
              
          for(var i = 0; i < len; i++)
              crc = crc8Add(crc, str.charCodeAt(i));
          
          return crc;
      };
  
      function crc8Buffer(buf)
      {
          var crc = 0;
  
          for (var i = 0, len = buf.length; i < len; ++i)
          {
              crc = crc8Add(crc, buf[i]);
          }
  
          return crc;
      }
  
      function crcArc(str)
      {
          var i,
              len = str.length,
              crc = 0
              ;
          
          for(i = 0; i < len; i++)
              crc = crcArcAdd(crc, str.charCodeAt(i));
              
          return crc;
      };
  
      function crc16(str)
      {
          var i,
              len = str.length,
              crc = 0
              ;
              
          for(i = 0; i < len; i++)
              crc = crc16Add(crc, str.charCodeAt(i));
          
          return crc;
      };
  
      function crc16Buffer(buf)
      {
          var crc = 0;
  
          for (var i = 0, len = buf.length; i < len; ++i)
          {
              crc = crc16Add(crc, buf[i]);
          }
  
          return crc;
      }
  
      function fcs16(str)
      {
          var i,
              len = str.length,
              fcs = 0xFFFF
              ;
              
          for(i = 0; i < len; i++)
              fcs = fcs16Add(fcs,str.charCodeAt(i));
              
          return fcs^0xFFFF;
      };
  
      function crc32(str)
      {
          var i,
              len = str.length,
              crc = 0xFFFFFFFF
              ;
              
          for(i = 0; i < len; i++)
              crc = crc32Add(crc, str.charCodeAt(i));
              
          return crc^0xFFFFFFFF;
      };
  
      function crc32Buffer(buf)
      {
          var crc = 0xFFFFFFFF;
  
          for (var i = 0, len = buf.length; i < len; ++i) 
          {
              crc = crc32Add(crc, buf[i]);
          }
  
          return crc ^ 0xFFFFFFFF;
      }
  
      /**
       * Convert value as 8-bit unsigned integer to 2 digit hexadecimal number.
       */
      function hex8(val)
      {
          var n = val & 0xFF,
              str = n.toString(16).toUpperCase()
              ;
              
          while(str.length < 2)
              str = "0" + str;
              
          return str;
      };
  
      /**
       * Convert value as 16-bit unsigned integer to 4 digit hexadecimal number.
       */
      function hex16(val)
      {
          return hex8(val >> 8) + hex8(val);
      };
  
      /**
       * Convert value as 32-bit unsigned integer to 8 digit hexadecimal number.
       */
      function hex32(val)
      {
          return hex16(val >> 16) + hex16(val);
      };
  
      var target, property;
  
      if(typeof(window) == 'undefined')
      {
          target = module;
          property = 'exports';
      }
      else
      {
          target = window;
          property = 'crc';
      }
  
      target[property] = {
          'crc8'    : crc8,
          'crcArc'  : crcArc,
          'crc16'   : crc16,
          'fcs16'   : fcs16,
          'crc32'   : crc32,
          'hex8'    : hex8,
          'hex16'   : hex16,
          'hex32'   : hex32,
          'buffer'  : {
              crc8  : crc8Buffer,
              crc16 : crc16Buffer,
              crc32 : crc32Buffer
          }
      };
  })();
  

  provide("crc", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  /**
   * Parse byte `size` string.
   *
   * @param {String} size
   * @return {Number}
   * @api public
   */
  
  module.exports = function(size) {
    if ('number' == typeof size) return convert(size);
    var parts = size.match(/^(\d+(?:\.\d+)?) *(kb|mb|gb)$/)
      , n = parseFloat(parts[1])
      , type = parts[2];
  
    var map = {
        kb: 1 << 10
      , mb: 1 << 20
      , gb: 1 << 30
    };
  
    return map[type] * n;
  };
  
  /**
   * convert bytes into string.
   * 
   * @param {Number} b - bytes to convert
   * @return {String}i
   * @api public
   */
  
  function convert (b) {
    var gb = 1 << 30, mb = 1 << 20, kb = 1 << 10;
    if (b >= gb) return (Math.round(b / gb * 100) / 100) + 'gb';
    if (b >= mb) return (Math.round(b / mb * 100) / 100) + 'mb';
    if (b >= kb) return (Math.round(b / kb * 100) / 100) + 'kb';
    return b;
  }

  provide("bytes", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  module.exports = require('./lib/send');

  provide("send", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  module.exports = process.env.CONNECT_COV
    ? require('./lib-cov/connect')
    : require('./lib/connect');

  provide("connect", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  var path = require('path');
  var fs = require('fs');
  
  function Mime() {
    // Map of extension -> mime type
    this.types = Object.create(null);
  
    // Map of mime type -> extension
    this.extensions = Object.create(null);
  }
  
  /**
   * Define mimetype -> extension mappings.  Each key is a mime-type that maps
   * to an array of extensions associated with the type.  The first extension is
   * used as the default extension for the type.
   *
   * e.g. mime.define({'audio/ogg', ['oga', 'ogg', 'spx']});
   *
   * @param map (Object) type definitions
   */
  Mime.prototype.define = function (map) {
    for (var type in map) {
      var exts = map[type];
  
      for (var i = 0; i < exts.length; i++) {
        this.types[exts[i]] = type;
      }
  
      // Default extension is the first one we encounter
      if (!this.extensions[type]) {
        this.extensions[type] = exts[0];
      }
    }
  };
  
  /**
   * Load an Apache2-style ".types" file
   *
   * This may be called multiple times (it's expected).  Where files declare
   * overlapping types/extensions, the last file wins.
   *
   * @param file (String) path of file to load.
   */
  Mime.prototype.load = function(file) {
    // Read file and split into lines
    var map = {},
        content = fs.readFileSync(file, 'ascii'),
        lines = content.split(/[\r\n]+/);
  
    lines.forEach(function(line) {
      // Clean up whitespace/comments, and split into fields
      var fields = line.replace(/\s*#.*|^\s*|\s*$/g, '').split(/\s+/);
      map[fields.shift()] = fields;
    });
  
    this.define(map);
  };
  
  /**
   * Lookup a mime type based on extension
   */
  Mime.prototype.lookup = function(path, fallback) {
    var ext = path.replace(/.*[\.\/]/, '').toLowerCase();
  
    return this.types[ext] || fallback || this.default_type;
  };
  
  /**
   * Return file extension associated with a mime type
   */
  Mime.prototype.extension = function(mimeType) {
    return this.extensions[mimeType];
  };
  
  // Default instance
  var mime = new Mime();
  
  // Load local copy of
  // http://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types
  mime.load(path.join(__dirname, 'types/mime.types'));
  
  // Load additional types from node.js community
  mime.load(path.join(__dirname, 'types/node.types'));
  
  // Default type
  mime.default_type = mime.lookup('bin');
  
  //
  // Additional API specific to the default instance
  //
  
  mime.Mime = Mime;
  
  /**
   * Lookup a charset based on mime type.
   */
  mime.charsets = {
    lookup: function(mimeType, fallback) {
      // Assume text types are utf8
      return (/^text\//).test(mimeType) ? 'UTF-8' : fallback;
    }
  }
  
  module.exports = mime;
  

  provide("mime", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  // This file is just added for convenience so this repository can be
  // directly checked out into a project's deps folder
  module.exports = require('./lib/async');
  

  provide("async", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  ;(function (exports) { // nothing in here is node-specific.
  
  // See http://semver.org/
  // This implementation is a *hair* less strict in that it allows
  // v1.2.3 things, and also tags that don't begin with a char.
  
  var semver = "\\s*[v=]*\\s*([0-9]+)"                // major
             + "\\.([0-9]+)"                  // minor
             + "\\.([0-9]+)"                  // patch
             + "(-[0-9]+-?)?"                 // build
             + "([a-zA-Z-][a-zA-Z0-9-\.:]*)?" // tag
    , exprComparator = "^((<|>)?=?)\s*("+semver+")$|^$"
    , xRangePlain = "[v=]*([0-9]+|x|X|\\*)"
                  + "(?:\\.([0-9]+|x|X|\\*)"
                  + "(?:\\.([0-9]+|x|X|\\*)"
                  + "([a-zA-Z-][a-zA-Z0-9-\.:]*)?)?)?"
    , xRange = "((?:<|>)=?)?\\s*" + xRangePlain
    , exprSpermy = "(?:~>?)"+xRange
    , expressions = exports.expressions =
      { parse : new RegExp("^\\s*"+semver+"\\s*$")
      , parsePackage : new RegExp("^\\s*([^\/]+)[-@](" +semver+")\\s*$")
      , parseRange : new RegExp(
          "^\\s*(" + semver + ")\\s+-\\s+(" + semver + ")\\s*$")
      , validComparator : new RegExp("^"+exprComparator+"$")
      , parseXRange : new RegExp("^"+xRange+"$")
      , parseSpermy : new RegExp("^"+exprSpermy+"$")
      }
  
  
  Object.getOwnPropertyNames(expressions).forEach(function (i) {
    exports[i] = function (str) {
      return ("" + (str || "")).match(expressions[i])
    }
  })
  
  exports.rangeReplace = ">=$1 <=$7"
  exports.clean = clean
  exports.compare = compare
  exports.rcompare = rcompare
  exports.satisfies = satisfies
  exports.gt = gt
  exports.gte = gte
  exports.lt = lt
  exports.lte = lte
  exports.eq = eq
  exports.neq = neq
  exports.cmp = cmp
  exports.inc = inc
  
  exports.valid = valid
  exports.validPackage = validPackage
  exports.validRange = validRange
  exports.maxSatisfying = maxSatisfying
  
  exports.replaceStars = replaceStars
  exports.toComparators = toComparators
  
  function stringify (version) {
    var v = version
    return [v[1]||'', v[2]||'', v[3]||''].join(".") + (v[4]||'') + (v[5]||'')
  }
  
  function clean (version) {
    version = exports.parse(version)
    if (!version) return version
    return stringify(version)
  }
  
  function valid (version) {
    if (typeof version !== "string") return null
    return exports.parse(version) && version.trim().replace(/^[v=]+/, '')
  }
  
  function validPackage (version) {
    if (typeof version !== "string") return null
    return version.match(expressions.parsePackage) && version.trim()
  }
  
  // range can be one of:
  // "1.0.3 - 2.0.0" range, inclusive, like ">=1.0.3 <=2.0.0"
  // ">1.0.2" like 1.0.3 - 9999.9999.9999
  // ">=1.0.2" like 1.0.2 - 9999.9999.9999
  // "<2.0.0" like 0.0.0 - 1.9999.9999
  // ">1.0.2 <2.0.0" like 1.0.3 - 1.9999.9999
  var starExpression = /(<|>)?=?\s*\*/g
    , starReplace = ""
    , compTrimExpression = new RegExp("((<|>)?=?)\\s*("
                                      +semver+"|"+xRangePlain+")", "g")
    , compTrimReplace = "$1$3"
  
  function toComparators (range) {
    var ret = (range || "").trim()
      .replace(expressions.parseRange, exports.rangeReplace)
      .replace(compTrimExpression, compTrimReplace)
      .split(/\s+/)
      .join(" ")
      .split("||")
      .map(function (orchunk) {
        return orchunk
          .split(" ")
          .map(replaceXRanges)
          .map(replaceSpermies)
          .map(replaceStars)
          .join(" ").trim()
      })
      .map(function (orchunk) {
        return orchunk
          .trim()
          .split(/\s+/)
          .filter(function (c) { return c.match(expressions.validComparator) })
      })
      .filter(function (c) { return c.length })
    return ret
  }
  
  function replaceStars (stars) {
    return stars.trim().replace(starExpression, starReplace)
  }
  
  // "2.x","2.x.x" --> ">=2.0.0- <2.1.0-"
  // "2.3.x" --> ">=2.3.0- <2.4.0-"
  function replaceXRanges (ranges) {
    return ranges.split(/\s+/)
                 .map(replaceXRange)
                 .join(" ")
  }
  
  function replaceXRange (version) {
    return version.trim().replace(expressions.parseXRange,
                                  function (v, gtlt, M, m, p, t) {
      var anyX = !M || M.toLowerCase() === "x" || M === "*"
                 || !m || m.toLowerCase() === "x" || m === "*"
                 || !p || p.toLowerCase() === "x" || p === "*"
        , ret = v
  
      if (gtlt && anyX) {
        // just replace x'es with zeroes
        ;(!M || M === "*" || M.toLowerCase() === "x") && (M = 0)
        ;(!m || m === "*" || m.toLowerCase() === "x") && (m = 0)
        ;(!p || p === "*" || p.toLowerCase() === "x") && (p = 0)
        ret = gtlt + M+"."+m+"."+p+"-"
      } else if (!M || M === "*" || M.toLowerCase() === "x") {
        ret = "*" // allow any
      } else if (!m || m === "*" || m.toLowerCase() === "x") {
        // append "-" onto the version, otherwise
        // "1.x.x" matches "2.0.0beta", since the tag
        // *lowers* the version value
        ret = ">="+M+".0.0- <"+(+M+1)+".0.0-"
      } else if (!p || p === "*" || p.toLowerCase() === "x") {
        ret = ">="+M+"."+m+".0- <"+M+"."+(+m+1)+".0-"
      }
      //console.error("parseXRange", [].slice.call(arguments), ret)
      return ret
    })
  }
  
  // ~, ~> --> * (any, kinda silly)
  // ~2, ~2.x, ~2.x.x, ~>2, ~>2.x ~>2.x.x --> >=2.0.0 <3.0.0
  // ~2.0, ~2.0.x, ~>2.0, ~>2.0.x --> >=2.0.0 <2.1.0
  // ~1.2, ~1.2.x, ~>1.2, ~>1.2.x --> >=1.2.0 <1.3.0
  // ~1.2.3, ~>1.2.3 --> >=1.2.3 <1.3.0
  // ~1.2.0, ~>1.2.0 --> >=1.2.0 <1.3.0
  function replaceSpermies (version) {
    return version.trim().replace(expressions.parseSpermy,
                                  function (v, gtlt, M, m, p, t) {
      if (gtlt) throw new Error(
        "Using '"+gtlt+"' with ~ makes no sense. Don't do it.")
  
      if (!M || M.toLowerCase() === "x") {
        return ""
      }
      // ~1 == >=1.0.0- <2.0.0-
      if (!m || m.toLowerCase() === "x") {
        return ">="+M+".0.0- <"+(+M+1)+".0.0-"
      }
      // ~1.2 == >=1.2.0- <1.3.0-
      if (!p || p.toLowerCase() === "x") {
        return ">="+M+"."+m+".0- <"+M+"."+(+m+1)+".0-"
      }
      // ~1.2.3 == >=1.2.3- <1.3.0-
      t = t || "-"
      return ">="+M+"."+m+"."+p+t+" <"+M+"."+(+m+1)+".0-"
    })
  }
  
  function validRange (range) {
    range = replaceStars(range)
    var c = toComparators(range)
    return (c.length === 0)
         ? null
         : c.map(function (c) { return c.join(" ") }).join("||")
  }
  
  // returns the highest satisfying version in the list, or undefined
  function maxSatisfying (versions, range) {
    return versions
      .filter(function (v) { return satisfies(v, range) })
      .sort(compare)
      .pop()
  }
  function satisfies (version, range) {
    version = valid(version)
    if (!version) return false
    range = toComparators(range)
    for (var i = 0, l = range.length ; i < l ; i ++) {
      var ok = false
      for (var j = 0, ll = range[i].length ; j < ll ; j ++) {
        var r = range[i][j]
          , gtlt = r.charAt(0) === ">" ? gt
                 : r.charAt(0) === "<" ? lt
                 : false
          , eq = r.charAt(!!gtlt) === "="
          , sub = (!!eq) + (!!gtlt)
        if (!gtlt) eq = true
        r = r.substr(sub)
        r = (r === "") ? r : valid(r)
        ok = (r === "") || (eq && r === version) || (gtlt && gtlt(version, r))
        if (!ok) break
      }
      if (ok) return true
    }
    return false
  }
  
  // return v1 > v2 ? 1 : -1
  function compare (v1, v2) {
    var g = gt(v1, v2)
    return g === null ? 0 : g ? 1 : -1
  }
  
  function rcompare (v1, v2) {
    return compare(v2, v1)
  }
  
  function lt (v1, v2) { return gt(v2, v1) }
  function gte (v1, v2) { return !lt(v1, v2) }
  function lte (v1, v2) { return !gt(v1, v2) }
  function eq (v1, v2) { return gt(v1, v2) === null }
  function neq (v1, v2) { return gt(v1, v2) !== null }
  function cmp (v1, c, v2) {
    switch (c) {
      case ">": return gt(v1, v2)
      case "<": return lt(v1, v2)
      case ">=": return gte(v1, v2)
      case "<=": return lte(v1, v2)
      case "==": return eq(v1, v2)
      case "!=": return neq(v1, v2)
      case "===": return v1 === v2
      case "!==": return v1 !== v2
      default: throw new Error("Y U NO USE VALID COMPARATOR!? "+c)
    }
  }
  
  // return v1 > v2
  function num (v) {
    return v === undefined ? -1 : parseInt((v||"0").replace(/[^0-9]+/g, ''), 10)
  }
  function gt (v1, v2) {
    v1 = exports.parse(v1)
    v2 = exports.parse(v2)
    if (!v1 || !v2) return false
  
    for (var i = 1; i < 5; i ++) {
      v1[i] = num(v1[i])
      v2[i] = num(v2[i])
      if (v1[i] > v2[i]) return true
      else if (v1[i] !== v2[i]) return false
    }
    // no tag is > than any tag, or use lexicographical order.
    var tag1 = v1[5] || ""
      , tag2 = v2[5] || ""
  
    // kludge: null means they were equal.  falsey, and detectable.
    // embarrassingly overclever, though, I know.
    return tag1 === tag2 ? null
           : !tag1 ? true
           : !tag2 ? false
           : tag1 > tag2
  }
  
  function inc (version, release) {
    version = exports.parse(version)
    if (!version) return null
  
    var parsedIndexLookup =
      { 'major': 1
      , 'minor': 2
      , 'patch': 3
      , 'build': 4 }
    var incIndex = parsedIndexLookup[release]
    if (incIndex === undefined) return null
  
    var current = num(version[incIndex])
    version[incIndex] = current === -1 ? 1 : current + 1
  
    for (var i = incIndex + 1; i < 5; i ++) {
      if (num(version[i]) !== -1) version[i] = "0"
    }
  
    if (version[4]) version[4] = "-" + version[4]
    version[5] = ""
  
    return stringify(version)
  }
  })(typeof exports === "object" ? exports : semver = {})
  

  provide("semver", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  // this keeps a queue of opened file descriptors, and will make
  // fs operations wait until some have closed before trying to open more.
  
  var fs = require("fs")
  
  // there is such a thing as TOO graceful.
  if (fs.open === gracefulOpen) return
  
  var queue = []
    , curOpen = 0
    , constants = require("constants")
  
  
  exports = module.exports = fs
  
  
  fs.MIN_MAX_OPEN = 64
  fs.MAX_OPEN = 1024
  
  var originalOpen = fs.open
    , originalOpenSync = fs.openSync
    , originalClose = fs.close
    , originalCloseSync = fs.closeSync
  
  
  // prevent EMFILE errors
  function OpenReq (path, flags, mode, cb) {
    this.path = path
    this.flags = flags
    this.mode = mode
    this.cb = cb
  }
  
  function noop () {}
  
  fs.open = gracefulOpen
  
  function gracefulOpen (path, flags, mode, cb) {
    if (typeof mode === "function") cb = mode, mode = null
    if (typeof cb !== "function") cb = noop
  
    if (curOpen >= fs.MAX_OPEN) {
      queue.push(new OpenReq(path, flags, mode, cb))
      setTimeout(flush)
      return
    }
    open(path, flags, mode, function (er, fd) {
      if (er && er.code === "EMFILE" && curOpen > fs.MIN_MAX_OPEN) {
        // that was too many.  reduce max, get back in queue.
        // this should only happen once in a great while, and only
        // if the ulimit -n is set lower than 1024.
        fs.MAX_OPEN = curOpen - 1
        return fs.open(path, flags, mode, cb)
      }
      cb(er, fd)
    })
  }
  
  function open (path, flags, mode, cb) {
    cb = cb || noop
    curOpen ++
    originalOpen.call(fs, path, flags, mode, function (er, fd) {
      if (er) {
        onclose()
      }
  
      cb(er, fd)
    })
  }
  
  fs.openSync = function (path, flags, mode) {
    curOpen ++
    return originalOpenSync.call(fs, path, flags, mode)
  }
  
  function onclose () {
    curOpen --
    flush()
  }
  
  function flush () {
    while (curOpen < fs.MAX_OPEN) {
      var req = queue.shift()
      if (!req) break
      open(req.path, req.flags || "r", req.mode || 0777, req.cb)
    }
    if (queue.length === 0) return
  }
  
  fs.close = function (fd, cb) {
    cb = cb || noop
    originalClose.call(fs, fd, function (er) {
      onclose()
      cb(er)
    })
  }
  
  fs.closeSync = function (fd) {
    onclose()
    return originalCloseSync.call(fs, fd)
  }
  
  
  // (re-)implement some things that are known busted or missing.
  
  var constants = require("constants")
  
  // lchmod, broken prior to 0.6.2
  // back-port the fix here.
  if (constants.hasOwnProperty('O_SYMLINK') &&
      process.version.match(/^v0\.6\.[0-2]|^v0\.5\./)) {
    fs.lchmod = function (path, mode, callback) {
      callback = callback || noop
      fs.open( path
             , constants.O_WRONLY | constants.O_SYMLINK
             , mode
             , function (err, fd) {
        if (err) {
          callback(err)
          return
        }
        // prefer to return the chmod error, if one occurs,
        // but still try to close, and report closing errors if they occur.
        fs.fchmod(fd, mode, function (err) {
          fs.close(fd, function(err2) {
            callback(err || err2)
          })
        })
      })
    }
  
    fs.lchmodSync = function (path, mode) {
      var fd = fs.openSync(path, constants.O_WRONLY | constants.O_SYMLINK, mode)
  
      // prefer to return the chmod error, if one occurs,
      // but still try to close, and report closing errors if they occur.
      var err, err2
      try {
        var ret = fs.fchmodSync(fd, mode)
      } catch (er) {
        err = er
      }
      try {
        fs.closeSync(fd)
      } catch (er) {
        err2 = er
      }
      if (err || err2) throw (err || err2)
      return ret
    }
  }
  
  
  // lstat on windows, missing from early 0.5 versions
  // replacing with stat isn't quite perfect, but good enough to get by.
  if (process.platform === "win32" && !process.binding("fs").lstat) {
    fs.lstat = fs.stat
    fs.lstatSync = fs.statSync
  }
  
  
  // lutimes implementation, or no-op
  if (!fs.lutimes) {
    if (constants.hasOwnProperty("O_SYMLINK")) {
      fs.lutimes = function (path, at, mt, cb) {
        fs.open(path, constants.O_SYMLINK, function (er, fd) {
          cb = cb || noop
          if (er) return cb(er)
          fs.futimes(fd, at, mt, function (er) {
            fs.close(fd, function (er2) {
              return cb(er || er2)
            })
          })
        })
      }
  
      fs.lutimesSync = function (path, at, mt) {
        var fd = fs.openSync(path, constants.O_SYMLINK)
          , err
          , err2
          , ret
  
        try {
          var ret = fs.futimesSync(fd, at, mt)
        } catch (er) {
          err = er
        }
        try {
          fs.closeSync(fd)
        } catch (er) {
          err2 = er
        }
        if (err || err2) throw (err || err2)
        return ret
      }
  
    } else if (fs.utimensat && constants.hasOwnProperty("AT_SYMLINK_NOFOLLOW")) {
      // maybe utimensat will be bound soonish?
      fs.lutimes = function (path, at, mt, cb) {
        fs.utimensat(path, at, mt, constants.AT_SYMLINK_NOFOLLOW, cb)
      }
  
      fs.lutimesSync = function (path, at, mt) {
        return fs.utimensatSync(path, at, mt, constants.AT_SYMLINK_NOFOLLOW)
      }
  
    } else {
      fs.lutimes = function (_a, _b, _c, cb) { process.nextTick(cb) }
      fs.lutimesSync = function () {}
    }
  }
  
  
  // https://github.com/isaacs/node-graceful-fs/issues/4
  // Chown should not fail on einval or eperm if non-root.
  
  fs.chown = chownFix(fs.chown)
  fs.fchown = chownFix(fs.fchown)
  fs.lchown = chownFix(fs.lchown)
  
  fs.chownSync = chownFixSync(fs.chownSync)
  fs.fchownSync = chownFixSync(fs.fchownSync)
  fs.lchownSync = chownFixSync(fs.lchownSync)
  
  function chownFix (orig) {
    if (!orig) return orig
    return function (target, uid, gid, cb) {
      return orig.call(fs, target, uid, gid, function (er, res) {
        if (chownErOk(er)) er = null
        cb(er, res)
      })
    }
  }
  
  function chownFixSync (orig) {
    if (!orig) return orig
    return function (target, uid, gid) {
      try {
        return orig.call(fs, target, uid, gid)
      } catch (er) {
        if (!chownErOk(er)) throw er
      }
    }
  }
  
  function chownErOk (er) {
    // if there's no getuid, or if getuid() is something other than 0,
    // and the error is EINVAL or EPERM, then just ignore it.
    // This specific case is a silent failure in cp, install, tar,
    // and most other unix tools that manage permissions.
    // When running as root, or if other types of errors are encountered,
    // then it's strict.
    if (!er || (!process.getuid || process.getuid() !== 0)
        && (er.code === "EINVAL" || er.code === "EPERM")) return true
  }
  
  
  
  // on Windows, A/V software can lock the directory, causing this
  // to fail with an EACCES or EPERM if the directory contains newly
  // created files.  Try again on failure, for up to 1 second.
  if (process.platform === "win32") {
    var rename_ = fs.rename
    fs.rename = function rename (from, to, cb) {
      var start = Date.now()
      rename_(from, to, function CB (er) {
        if (er
            && (er.code === "EACCES" || er.code === "EPERM")
            && Date.now() - start < 1000) {
          return rename_(from, to, CB)
        }
        cb(er)
      })
    }
  }
  

  provide("graceful-fs", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  module.exports = inherits
  
  function inherits (c, p, proto) {
    proto = proto || {}
    var e = {}
    ;[c.prototype, proto].forEach(function (s) {
      Object.getOwnPropertyNames(s).forEach(function (k) {
        e[k] = Object.getOwnPropertyDescriptor(s, k)
      })
    })
    c.prototype = Object.create(p.prototype, e)
    c.super = p
  }
  
  //function Child () {
  //  Child.super.call(this)
  //  console.error([this
  //                ,this.constructor
  //                ,this.constructor === Child
  //                ,this.constructor.super === Parent
  //                ,Object.getPrototypeOf(this) === Child.prototype
  //                ,Object.getPrototypeOf(Object.getPrototypeOf(this))
  //                 === Parent.prototype
  //                ,this instanceof Child
  //                ,this instanceof Parent])
  //}
  //function Parent () {}
  //inherits(Child, Parent)
  //new Child
  

  provide("inherits", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  ;(function () { // closure for web browsers
  
  if (module) {
    module.exports = LRUCache
  } else {
    // just set the global for non-node platforms.
    ;(function () { return this })().LRUCache = LRUCache
  }
  
  function hOP (obj, key) {
    return Object.prototype.hasOwnProperty.call(obj, key)
  }
  
  function naiveLength () { return 1 }
  
  function LRUCache (maxLength, lengthCalculator) {
    if (!(this instanceof LRUCache)) {
      return new LRUCache(maxLength, lengthCalculator)
    }
  
    if (typeof lengthCalculator !== "function") {
      lengthCalculator = naiveLength
    }
    if (!maxLength || !(typeof maxLength === "number") || maxLength <= 0 ) {
      maxLength = Infinity
    }
  
  
    var cache = {} // hash of items by key
      , lruList = {} // list of items in order of use recency
      , lru = 0 // least recently used
      , mru = 0 // most recently used
      , length = 0 // number of items in the list
      , itemCount = 0
  
  
    // resize the cache when the maxLength changes.
    Object.defineProperty(this, "maxLength",
      { set : function (mL) {
          if (!mL || !(typeof mL === "number") || mL <= 0 ) mL = Infinity
          maxLength = mL
          // if it gets above double maxLength, trim right away.
          // otherwise, do it whenever it's convenient.
          if (length > maxLength) trim()
        }
      , get : function () { return maxLength }
      , enumerable : true
      })
  
    // resize the cache when the lengthCalculator changes.
    Object.defineProperty(this, "lengthCalculator",
      { set : function (lC) {
          if (typeof lC !== "function") {
            lengthCalculator = naiveLength
            length = itemCount
            Object.keys(cache).forEach(function (key) {
              cache[key].length = 1
            })
          } else {
            lengthCalculator = lC
            length = 0
            Object.keys(cache).forEach(function (key) {
              cache[key].length = lengthCalculator(cache[key].value)
              length += cache[key].length
            })
          }
  
          if (length > maxLength) trim()
        }
      , get : function () { return lengthCalculator }
      , enumerable : true
      })
  
    Object.defineProperty(this, "length",
      { get : function () { return length }
      , enumerable : true
      })
  
  
    Object.defineProperty(this, "itemCount",
      { get : function () { return itemCount }
      , enumerable : true
      })
  
    this.reset = function () {
      cache = {}
      lruList = {}
      lru = 0
      mru = 0
      length = 0
      itemCount = 0
    }
  
    // Provided for debugging/dev purposes only. No promises whatsoever that
    // this API stays stable.
    this.dump = function () {
      return cache
    }
  
    this.set = function (key, value) {
      if (hOP(cache, key)) {
        this.get(key)
        cache[key].value = value
        return
      }
  
      var hit = {key:key, value:value, lu:mru++, length:lengthCalculator(value)}
  
      // oversized objects fall out of cache automatically.
      if (hit.length > maxLength) return
  
      length += hit.length
      lruList[hit.lu] = cache[key] = hit
      itemCount ++
  
      if (length > maxLength) trim()
    }
  
    this.get = function (key) {
      if (!hOP(cache, key)) return
      var hit = cache[key]
      delete lruList[hit.lu]
      if (hit.lu === lru) lruWalk()
      hit.lu = mru ++
      lruList[hit.lu] = hit
      return hit.value
    }
  
    this.del = function (key) {
      if (!hOP(cache, key)) return
      var hit = cache[key]
      delete cache[key]
      delete lruList[hit.lu]
      if (hit.lu === lru) lruWalk()
      length -= hit.length
      itemCount --
    }
  
    function lruWalk () {
      // lru has been deleted, hop up to the next hit.
      lru = Object.keys(lruList)[0]
    }
  
    function trim () {
      if (length <= maxLength) return
      var prune = Object.keys(lruList)
      for (var i = 0; i < prune.length && length > maxLength; i ++) {
        length -= lruList[prune[i]].length
        delete cache[ lruList[prune[i]].key ]
        delete lruList[prune[i]]
      }
      lruWalk()
    }
  }
  
  })()
  

  provide("lru-cache", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  /**
   * References:
   *
   *   - http://en.wikipedia.org/wiki/ANSI_escape_code
   *   - http://www.termsys.demon.co.uk/vtansi.htm
   *
   */
  
  /**
   * Module dependencies.
   */
  
  var emitNewlineEvents = require('./newlines')
    , prefix = '\033[' // For all escape codes
    , suffix = 'm'     // Only for color codes
  
  /**
   * The ANSI escape sequences.
   */
  
  var codes = {
      up: 'A'
    , down: 'B'
    , forward: 'C'
    , back: 'D'
    , nextLine: 'E'
    , previousLine: 'F'
    , horizontalAbsolute: 'G'
    , eraseData: 'J'
    , eraseLine: 'K'
    , scrollUp: 'S'
    , scrollDown: 'T'
    , savePosition: 's'
    , restorePosition: 'u'
    , queryPosition: '6n'
    , hide: '?25l'
    , show: '?25h'
  }
  
  /**
   * Rendering ANSI codes.
   */
  
  var styles = {
      bold: 1
    , italic: 3
    , underline: 4
    , inverse: 7
  }
  
  /**
   * The negating ANSI code for the rendering modes.
   */
  
  var reset = {
      bold: 22
    , italic: 23
    , underline: 24
    , inverse: 27
  }
  
  /**
   * The standard, styleable ANSI colors.
   */
  
  var colors = {
      white: 37
    , black: 30
    , blue: 34
    , cyan: 36
    , green: 32
    , magenta: 35
    , red: 31
    , yellow: 33
    , grey: 90
    , brightBlack: 90
    , brightRed: 91
    , brightGreen: 92
    , brightYellow: 93
    , brightBlue: 94
    , brightMagenta: 95
    , brightCyan: 96
    , brightWhite: 97
  }
  
  
  /**
   * Creates a Cursor instance based off the given `writable stream` instance.
   */
  
  function ansi (stream, options) {
    if (stream._ansicursor) {
      return stream._ansicursor
    } else {
      return stream._ansicursor = new Cursor(stream, options)
    }
  }
  module.exports = exports = ansi
  
  /**
   * The `Cursor` class.
   */
  
  function Cursor (stream, options) {
    if (!(this instanceof Cursor)) {
      return new Cursor(stream, options)
    }
    if (typeof stream != 'object' || typeof stream.write != 'function') {
      throw new Error('a valid Stream instance must be passed in')
    }
  
    // the stream to use
    this.stream = stream
  
    // when 'enabled' is false then all the functions are no-ops except for write()
    this.enabled = options && options.enabled
    if (typeof this.enabled === 'undefined') {
      this.enabled = stream.isTTY
    }
    this.enabled = !!this.enabled
  
    // controls the foreground and background colors
    this.fg = this.foreground = new Colorer(this, 0)
    this.bg = this.background = new Colorer(this, 10)
  
    // defaults
    this.Bold = false
    this.Italic = false
    this.Underline = false
    this.Inverse = false
  
    // keep track of the number of "newlines" that get encountered
    this.newlines = 0
    emitNewlineEvents(stream)
    stream.on('newline', function () {
      this.newlines++
    }.bind(this))
  }
  exports.Cursor = Cursor
  
  /**
   * Helper function that calls `write()` on the underlying Stream.
   * Returns `this` instead of the write() return value to keep
   * the chaining going.
   */
  
  Cursor.prototype.write = function () {
    this.stream.write.apply(this.stream, arguments)
    return this
  }
  
  
  /**
   * The `Colorer` class manages both the background and foreground colors.
   */
  
  function Colorer (cursor, base) {
    this.current = null
    this.cursor = cursor
    this.base = base
  }
  exports.Colorer = Colorer
  
  /**
   * Write an ANSI color code, ensuring that the same code doesn't get rewritten.
   */
  
  Colorer.prototype._setColorCode = function setColorCode (code) {
    var c = String(code)
    if (this.current === c) return
    this.cursor.enabled && this.cursor.write(prefix + c + suffix)
    this.current = c
    return this
  }
  
  
  /**
   * Set up the positional ANSI codes.
   */
  
  Object.keys(codes).forEach(function (name) {
    var code = String(codes[name])
    Cursor.prototype[name] = function () {
      var c = code
      if (arguments.length > 0) {
        c = toArray(arguments).map(Math.round).join(';') + code
      }
      this.enabled && this.write(prefix + c)
      return this
    }
  })
  
  /**
   * Set up the functions for the rendering ANSI codes.
   */
  
  Object.keys(styles).forEach(function (style) {
    var name = style[0].toUpperCase() + style.substring(1)
      , c = styles[style]
      , r = reset[style]
  
    Cursor.prototype[style] = function () {
      if (this[name]) return
      this.enabled && this.write(prefix + c + suffix)
      this[name] = true
      return this
    }
  
    Cursor.prototype['reset' + name] = function () {
      if (!this[name]) return
      this.enabled && this.write(prefix + r + suffix)
      this[name] = false
      return this
    }
  })
  
  /**
   * Setup the functions for the standard colors.
   */
  
  Object.keys(colors).forEach(function (color) {
    var code = colors[color]
  
    Colorer.prototype[color] = function () {
      this._setColorCode(this.base + code)
      return this.cursor
    }
  
    Cursor.prototype[color] = function () {
      return this.foreground[color]()
    }
  })
  
  /**
   * Makes a beep sound!
   */
  
  Cursor.prototype.beep = function () {
    this.enabled && this.write('\007')
    return this
  }
  
  /**
   * Moves cursor to specific position
   */
  
  Cursor.prototype.goto = function (x, y) {
    x = x | 0
    y = y | 0
    this.enabled && this.write(prefix + y + ';' + x + 'H')
    return this
  }
  
  /**
   * Resets the color.
   */
  
  Colorer.prototype.reset = function () {
    this._setColorCode(this.base + 39)
    return this.cursor
  }
  
  /**
   * Resets all ANSI formatting on the stream.
   */
  
  Cursor.prototype.reset = function () {
    this.enabled && this.write(prefix + '0' + suffix)
    this.Bold = false
    this.Italic = false
    this.Underline = false
    this.Inverse = false
    this.foreground.current = null
    this.background.current = null
    return this
  }
  
  /**
   * Sets the foreground color with the given RGB values.
   * The closest match out of the 216 colors is picked.
   */
  
  Colorer.prototype.rgb = function (r, g, b) {
    var base = this.base + 38
      , code = rgb(r, g, b)
    this._setColorCode(base + ';5;' + code)
    return this.cursor
  }
  
  /**
   * Same as `cursor.fg.rgb(r, g, b)`.
   */
  
  Cursor.prototype.rgb = function (r, g, b) {
    return this.foreground.rgb(r, g, b)
  }
  
  /**
   * Accepts CSS color codes for use with ANSI escape codes.
   * For example: `#FF000` would be bright red.
   */
  
  Colorer.prototype.hex = function (color) {
    return this.rgb.apply(this, hex(color))
  }
  
  /**
   * Same as `cursor.fg.hex(color)`.
   */
  
  Cursor.prototype.hex = function (color) {
    return this.foreground.hex(color)
  }
  
  
  // UTIL FUNCTIONS //
  
  /**
   * Translates a 255 RGB value to a 0-5 ANSI RGV value,
   * then returns the single ANSI color code to use.
   */
  
  function rgb (r, g, b) {
    var red = r / 255 * 5
      , green = g / 255 * 5
      , blue = b / 255 * 5
    return rgb5(red, green, blue)
  }
  
  /**
   * Turns rgb 0-5 values into a single ANSI color code to use.
   */
  
  function rgb5 (r, g, b) {
    var red = Math.round(r)
      , green = Math.round(g)
      , blue = Math.round(b)
    return 16 + (red*36) + (green*6) + blue
  }
  
  /**
   * Accepts a hex CSS color code string (# is optional) and
   * translates it into an Array of 3 RGB 0-255 values, which
   * can then be used with rgb().
   */
  
  function hex (color) {
    var c = color[0] === '#' ? color.substring(1) : color
      , r = c.substring(0, 2)
      , g = c.substring(2, 4)
      , b = c.substring(4, 6)
    return [parseInt(r, 16), parseInt(g, 16), parseInt(b, 16)]
  }
  
  /**
   * Turns an array-like object into a real array.
   */
  
  function toArray (a) {
    var i = 0
      , l = a.length
      , rtn = []
    for (; i<l; i++) {
      rtn.push(a[i])
    }
    return rtn
  }
  

  provide("ansi", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  exports.parse = exports.decode = decode
  exports.stringify = exports.encode = encode
  
  exports.safe = safe
  exports.unsafe = unsafe
  
  function encode (obj, section) {
    var children = []
      , out = ""
  
    Object.keys(obj).forEach(function (k, _, __) {
      var val = obj[k]
      if (val && typeof val === "object") {
        children.push(k)
      } else {
        out += safe(k) + " = " + safe(val) + "\n"
      }
    })
  
    if (section && out.length) {
      out = "[" + safe(section) + "]" + "\n" + out
    }
  
    children.forEach(function (k, _, __) {
      var child = encode(obj[k], (section ? section + "." : "") + k)
      if (out.length && child.length) {
        out += "\n"
      }
      out += child
    })
  
    return out
  }
  
  function decode (str) {
    var out = {}
      , p = out
      , section = null
      , state = "START"
             // section     |key = value
      , re = /^\[([^\]]*)\]$|^([^=]+)(=(.*))?$/i
      , lines = str.split(/[\r\n]+/g)
      , section = null
  
    lines.forEach(function (line, _, __) {
      //line = line
      var rem = line.indexOf(";")
      if (rem !== -1) line = line.substr(0, rem)//.trim()
      if (!line) return
      var match = line.match(re)
      if (!match) return
      if (match[1] !== undefined) {
        section = unsafe(match[1])
        p = out[section] = out[section] || {}
        return
      }
      var key = unsafe(match[2])
        , value = match[3] ? unsafe((match[4] || "")) : true
      p[key] = value
    })
  
    // {a:{y:1},"a.b":{x:2}} --> {a:{y:1,b:{x:2}}}
    // use a filter to return the keys that have to be deleted.
    Object.keys(out).filter(function (k, _, __) {
      if (!out[k] || typeof out[k] !== "object") return false
      // see if the parent section is also an object.
      // if so, add it to that, and mark this one for deletion
      var parts = k.split(".")
        , p = out
        , l = parts.pop()
      parts.forEach(function (part, _, __) {
        if (!p[part] || typeof p[part] !== "object") p[part] = {}
        p = p[part]
      })
      if (p === out) return false
      p[l] = out[k]
      return true
    }).forEach(function (del, _, __) {
      delete out[del]
    })
  
    return out
  }
  
  function safe (val) {
    return ( typeof val !== "string"
           || val.match(/[\r\n]/)
           || val.match(/^\[/)
           || (val.length > 1
               && val.charAt(0) === "\""
               && val.slice(-1) === "\"")
           || val !== val.trim() ) ? JSON.stringify(val) : val
  }
  
  function unsafe (val) {
    val = (val || "").trim()
    if (val.charAt(0) === "\"" && val.slice(-1) === "\"") {
      try { val = JSON.parse(val) } catch (_) {}
    }
    return val
  }
  

  provide("ini", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  module.exports = chownr
  chownr.sync = chownrSync
  
  var fs = require("fs")
  , path = require("path")
  
  function chownr (p, uid, gid, cb) {
    fs.readdir(p, function (er, children) {
      // any error other than ENOTDIR means it's not readable, or
      // doesn't exist.  give up.
      if (er && er.code !== "ENOTDIR") return cb(er)
      if (er || !children.length) return fs.chown(p, uid, gid, cb)
  
      var len = children.length
      , errState = null
      children.forEach(function (child) {
        chownr(path.resolve(p, child), uid, gid, then)
      })
      function then (er) {
        if (errState) return
        if (er) return cb(errState = er)
        if (-- len === 0) return fs.chown(p, uid, gid, cb)
      }
    })
  }
  
  function chownrSync (p, uid, gid) {
    var children
    try {
      children = fs.readdirSync(p)
    } catch (er) {
      if (er && er.code === "ENOTDIR") return fs.chownSync(p, uid, gid)
      throw er
    }
    if (!children.length) return fs.chownSync(p, uid, gid)
  
    children.forEach(function (child) {
      chownrSync(path.resolve(p, child), uid, gid)
    })
    return fs.chownSync(p, uid, gid)
  }
  

  provide("chownr", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  module.exports = ProtoList
  
  function ProtoList () { this.list = [] }
  ProtoList.prototype =
    { get length () { return this.list.length }
    , get keys () {
        var k = []
        for (var i in this.list[0]) k.push(i)
        return k
      }
    , get snapshot () {
        var o = {}
        this.keys.forEach(function (k) { o[k] = this.get(k) }, this)
        return o
      }
    , push : function (obj) {
        if (typeof obj !== "object") obj = {valueOf:obj}
        if (this.list.length >= 1) {
          this.list[this.list.length - 1].__proto__ = obj
        }
        obj.__proto__ = Object.prototype
        return this.list.push(obj)
      }
    , pop : function () {
        if (this.list.length >= 2) {
          this.list[this.list.length - 2].__proto__ = Object.prototype
        }
        return this.list.pop()
      }
    , unshift : function (obj) {
        obj.__proto__ = this.list[0] || Object.prototype
        return this.list.unshift(obj)
      }
    , shift : function () {
        if (this.list.length >= 1) {
          this.list[0].__proto__ = Object.prototype
        }
        return this.list.shift()
      }
    , get : function (key) {
        return this.list[0][key]
      }
    , set : function (key, val, save) {
        if (!this.length) this.push({})
        if (save && this.list[0].hasOwnProperty(key)) this.push({})
        return this.list[0][key] = val
      }
    , forEach : function (fn, thisp) {
        for (var key in this.list[0]) fn.call(thisp, key, this.list[0][key])
      }
    , slice : function () {
        return this.list.slice.apply(this.list, arguments)
      }
    , splice : function () {
        return this.list.splice.apply(this.list, arguments)
      }
    }
  
  if (module === require.main) {
  
  var tap = require("tap")
    , test = tap.test
  
  tap.plan(1)
  
  tap.test("protoList tests", function (t) {
    var p = new ProtoList
    p.push({foo:"bar"})
    p.push({})
    p.set("foo", "baz")
    t.equal(p.get("foo"), "baz")
  
    var p = new ProtoList
    p.push({foo:"bar"})
    p.set("foo", "baz")
    t.equal(p.get("foo"), "baz")
    t.equal(p.length, 1)
    p.pop()
    t.equal(p.length, 0)
    p.set("foo", "asdf")
    t.equal(p.length, 1)
    t.equal(p.get("foo"), "asdf")
    p.push({bar:"baz"})
    t.equal(p.length, 2)
    t.equal(p.get("foo"), "asdf")
    p.shift()
    t.equal(p.length, 1)
    t.equal(p.get("foo"), undefined)
    t.end()
  })
  
  
  }
  

  provide("proto-list", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  module.exports = which
  which.sync = whichSync
  
  var path = require("path")
    , fs
    , COLON = process.platform === "win32" ? ";" : ":"
    , isExe
  
  try {
    fs = require("graceful-fs")
  } catch (ex) {
    fs = require("fs")
  }
  
  if (process.platform == "win32") {
    // On windows, there is no good way to check that a file is executable
    isExe = function isExe () { return true }
  } else {
    isExe = function isExe (mod, uid, gid) {
      //console.error(mod, uid, gid);
      //console.error("isExe?", (mod & 0111).toString(8))
      var ret = (mod & 0001)
          || (mod & 0010) && process.getgid && gid === process.getgid()
          || (mod & 0100) && process.getuid && uid === process.getuid()
      //console.error("isExe?", ret)
      return ret
    }
  }
  
  
  
  function which (cmd, cb) {
    if (isAbsolute(cmd)) return cb(null, cmd)
    var pathEnv = (process.env.PATH || "").split(COLON)
      , pathExt = [""]
    if (process.platform === "win32") {
      pathEnv.push(process.cwd())
      pathExt = (process.env.PATHEXT || ".EXE").split(COLON)
      if (cmd.indexOf(".") !== -1) pathExt.unshift("")
    }
    //console.error("pathEnv", pathEnv)
    ;(function F (i, l) {
      if (i === l) return cb(new Error("not found: "+cmd))
      var p = path.resolve(pathEnv[i], cmd)
      ;(function E (ii, ll) {
        if (ii === ll) return F(i + 1, l)
        var ext = pathExt[ii]
        //console.error(p + ext)
        fs.stat(p + ext, function (er, stat) {
          if (!er &&
              stat &&
              stat.isFile() &&
              isExe(stat.mode, stat.uid, stat.gid)) {
            //console.error("yes, exe!", p + ext)
            return cb(null, p + ext)
          }
          return E(ii + 1, ll)
        })
      })(0, pathExt.length)
    })(0, pathEnv.length)
  }
  
  function whichSync (cmd) {
    if (isAbsolute(cmd)) return cmd
    var pathEnv = (process.env.PATH || "").split(COLON)
      , pathExt = [""]
    if (process.platform === "win32") {
      pathEnv.push(process.cwd())
      pathExt = (process.env.PATHEXT || ".EXE").split(COLON)
      if (cmd.indexOf(".") !== -1) pathExt.unshift("")
    }
    for (var i = 0, l = pathEnv.length; i < l; i ++) {
      var p = path.join(pathEnv[i], cmd)
      for (var j = 0, ll = pathExt.length; j < ll; j ++) {
        var cur = p + pathExt[j]
        var stat
        try { stat = fs.statSync(cur) } catch (ex) {}
        if (stat &&
            stat.isFile() &&
            isExe(stat.mode, stat.uid, stat.gid)) return cur
      }
    }
    throw new Error("not found: "+cmd)
  }
  
  var isAbsolute = process.platform === "win32" ? absWin : absUnix
  
  function absWin (p) {
    if (absUnix(p)) return true
    // pull off the device/UNC bit from a windows path.
    // from node's lib/path.js
    var splitDeviceRe =
          /^([a-zA-Z]:|[\\\/]{2}[^\\\/]+[\\\/][^\\\/]+)?([\\\/])?/
      , result = splitDeviceRe.exec(p)
      , device = result[1] || ''
      , isUnc = device && device.charAt(1) !== ':'
      , isAbsolute = !!result[2] || isUnc // UNC paths are always absolute
  
    return isAbsolute
  }
  
  function absUnix (p) {
    return p.charAt(0) === "/" || p === ""
  }
  

  provide("which", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  module.exports = read
  
  var buffer = ""
    , tty = require("tty")
    , StringDecoder = require("string_decoder").StringDecoder
  
  function raw (mode) {
    try {
      process.stdin.setRawMode(mode)
    } catch (e) {
      tty.setRawMode(mode)
    }
  }
  
  function read (opts, cb) {
    if (!cb) cb = opts, opts = {}
  
    var p = opts.prompt || ""
      , def = opts.default
      , silent = opts.silent
      , timeout = opts.timeout
      , num = opts.num || null
      , delim = opts.delim || "\n"
  
    if (p && def) p += "("+(silent ? "<default hidden>" : def)+") "
  
    // switching into raw mode is a little bit painful.
    // avoid if possible.
    var r = silent || num || delim !== "\n" ? rawRead : normalRead
  
    if (timeout) {
      cb = (function (cb) {
        var called = false
        var t = setTimeout(function () {
          raw(false)
          process.stdout.write("\n")
          if (def) done(null, def)
          else done(new Error("timeout"))
        }, timeout)
  
        function done (er, data) {
          clearTimeout(t)
          if (called) return
          // stop reading!
          stdin.pause()
          called = true
          cb(er, data)
        }
  
        return done
      })(cb)
    }
  
    if (p && !process.stdout.write(p)) {
      process.stdout.on("drain", function D () {
        process.stdout.removeListener("drain", D)
        r(def, timeout, delim, silent, num, cb)
      })
    } else {
      process.nextTick(function () {
        r(def, timeout, delim, silent, num, cb)
      })
    }
  }
  
  function normalRead (def, timeout, delim, silent, num, cb) {
    var stdin = process.openStdin()
      , val = ""
      , decoder = new StringDecoder("utf8")
  
    stdin.resume()
    stdin.on("error", cb)
    stdin.on("data", function D (chunk) {
      // get the characters that are completed.
      val += buffer + decoder.write(chunk)
      buffer = ""
  
      // \r has no place here.
      // XXX But what if \r is the delim or something dumb like that?
      // Meh.  If anyone complains about this, deal with it.
      val = val.replace(/\r/g, "")
  
      // TODO Make delim configurable
      if (val.indexOf(delim) !== -1) {
        // pluck off any delims at the beginning.
        if (val !== delim) {
          var i, l
          for (i = 0, l = val.length; i < l; i ++) {
            if (val.charAt(i) !== delim) break
          }
          if (i !== 0) val = val.substr(i)
        }
  
        // buffer whatever might have come *after* the delimter
        var delimIndex = val.indexOf(delim)
        if (delimIndex !== -1) {
          buffer = val.substr(delimIndex)
          val = val.substr(0, delimIndex)
        } else {
          buffer = ""
        }
  
        stdin.pause()
        stdin.removeListener("data", D)
        stdin.removeListener("error", cb)
  
        // read(1) trims
        val = val.trim() || def
        cb(null, val)
      }
    })
  }
  
  function rawRead (def, timeout, delim, silent, num, cb) {
    var stdin = process.openStdin()
      , val = ""
      , decoder = new StringDecoder
  
    raw(true)
    stdin.resume()
    stdin.on("error", cb)
    stdin.on("data", function D (c) {
      // \r is my enemy.
      c = decoder.write(c).replace(/\r/g, "\n")
  
      switch (c) {
        case "": // probably just a \r that was ignored.
          break
  
        case "\u0004": // EOF
        case delim:
          raw(false)
          stdin.removeListener("data", D)
          stdin.removeListener("error", cb)
          val = val.trim() || def
          process.stdout.write("\n")
          stdin.pause()
          return cb(null, val)
  
        case "\u0003": case "\0": // ^C or other signal abort
          raw(false)
          stdin.removeListener("data", D)
          stdin.removeListener("error", cb)
          stdin.pause()
          return cb(new Error("cancelled"))
          break
  
        default: // just a normal char
          val += buffer + c
          buffer = ""
          if (!silent) process.stdout.write(c)
  
          // explicitly process a delim if we have enough chars.
          if (num && val.length >= num) D(delim)
          break
      }
    })
  }
  

  provide("read", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  // Copyright 2010-2012 Mikeal Rogers
  //
  //    Licensed under the Apache License, Version 2.0 (the "License");
  //    you may not use this file except in compliance with the License.
  //    You may obtain a copy of the License at
  //
  //        http://www.apache.org/licenses/LICENSE-2.0
  //
  //    Unless required by applicable law or agreed to in writing, software
  //    distributed under the License is distributed on an "AS IS" BASIS,
  //    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  //    See the License for the specific language governing permissions and
  //    limitations under the License.
  
  var http = require('http')
    , https = false
    , tls = false
    , url = require('url')
    , util = require('util')
    , stream = require('stream')
    , qs = require('querystring')
    , mimetypes = require('./mimetypes')
    , oauth = require('./oauth')
    , uuid = require('./uuid')
    , ForeverAgent = require('./forever')
    , Cookie = require('./vendor/cookie')
    , CookieJar = require('./vendor/cookie/jar')
    , cookieJar = new CookieJar
    , tunnel = require('./tunnel')
    ;
    
  if (process.logging) {
    var log = process.logging('request')
  }
  
  try {
    https = require('https')
  } catch (e) {}
  
  try {
    tls = require('tls')
  } catch (e) {}
  
  function toBase64 (str) {
    return (new Buffer(str || "", "ascii")).toString("base64")
  }
  
  // Hacky fix for pre-0.4.4 https
  if (https && !https.Agent) {
    https.Agent = function (options) {
      http.Agent.call(this, options)
    }
    util.inherits(https.Agent, http.Agent)
    https.Agent.prototype._getConnection = function(host, port, cb) {
      var s = tls.connect(port, host, this.options, function() {
        // do other checks here?
        if (cb) cb()
      })
      return s
    }
  }
  
  function isReadStream (rs) {
    if (rs.readable && rs.path && rs.mode) {
      return true
    }
  }
  
  function copy (obj) {
    var o = {}
    Object.keys(obj).forEach(function (i) {
      o[i] = obj[i]
    })
    return o
  }
  
  var isUrl = /^https?:/
  
  var globalPool = {}
  
  function Request (options) {
    stream.Stream.call(this)
    this.readable = true
    this.writable = true
  
    if (typeof options === 'string') {
      options = {uri:options}
    }
    
    var reserved = Object.keys(Request.prototype)
    for (var i in options) {
      if (reserved.indexOf(i) === -1) {
        this[i] = options[i]
      } else {
        if (typeof options[i] === 'function') {
          delete options[i]
        }
      }
    }
    options = copy(options)
    
    this.init(options)
  }
  util.inherits(Request, stream.Stream)
  Request.prototype.init = function (options) {
    var self = this
    
    if (!options) options = {}
    
    if (!self.pool) self.pool = globalPool
    self.dests = []
    self.__isRequestRequest = true
    
    // Protect against double callback
    if (!self._callback && self.callback) {
      self._callback = self.callback
      self.callback = function () {
        if (self._callbackCalled) return // Print a warning maybe?
        self._callback.apply(self, arguments)
        self._callbackCalled = true
      }
      self.on('error', self.callback.bind())
      self.on('complete', self.callback.bind(self, null))
    }
  
    if (self.url) {
      // People use this property instead all the time so why not just support it.
      self.uri = self.url
      delete self.url
    }
  
    if (!self.uri) {
      throw new Error("options.uri is a required argument")
    } else {
      if (typeof self.uri == "string") self.uri = url.parse(self.uri)
    }
    if (self.proxy) {
      if (typeof self.proxy == 'string') self.proxy = url.parse(self.proxy)
  
      // do the HTTP CONNECT dance using koichik/node-tunnel
      if (http.globalAgent && self.uri.protocol === "https:") {
        self.tunnel = true
        var tunnelFn = self.proxy.protocol === "http:"
                     ? tunnel.httpsOverHttp : tunnel.httpsOverHttps
  
        var tunnelOptions = { proxy: { host: self.proxy.hostname
                                     , port: +self.proxy.port 
                                     , proxyAuth: self.proxy.auth }
                            , ca: this.ca }
  
        self.agent = tunnelFn(tunnelOptions)
        self.tunnel = true
      }
    }
  
    self._redirectsFollowed = self._redirectsFollowed || 0
    self.maxRedirects = (self.maxRedirects !== undefined) ? self.maxRedirects : 10
    self.followRedirect = (self.followRedirect !== undefined) ? self.followRedirect : true
    self.followAllRedirects = (self.followAllRedirects !== undefined) ? self.followAllRedirects : false;
    if (self.followRedirect || self.followAllRedirects)
      self.redirects = self.redirects || []
  
    self.headers = self.headers ? copy(self.headers) : {}
  
    self.setHost = false
    if (!self.headers.host) {
      self.headers.host = self.uri.hostname
      if (self.uri.port) {
        if ( !(self.uri.port === 80 && self.uri.protocol === 'http:') &&
             !(self.uri.port === 443 && self.uri.protocol === 'https:') )
        self.headers.host += (':'+self.uri.port)
      }
      self.setHost = true
    }
    
    self.jar(self._jar || options.jar)
  
    if (!self.uri.pathname) {self.uri.pathname = '/'}
    if (!self.uri.port) {
      if (self.uri.protocol == 'http:') {self.uri.port = 80}
      else if (self.uri.protocol == 'https:') {self.uri.port = 443}
    }
  
    if (self.proxy && !self.tunnel) {
      self.port = self.proxy.port
      self.host = self.proxy.hostname
    } else {
      self.port = self.uri.port
      self.host = self.uri.hostname
    }
  
    self.clientErrorHandler = function (error) {
      if (self._aborted) return
      
      if (self.setHost) delete self.headers.host
      if (self.req._reusedSocket && error.code === 'ECONNRESET'
          && self.agent.addRequestNoreuse) {
        self.agent = { addRequest: self.agent.addRequestNoreuse.bind(self.agent) }
        self.start()
        self.req.end()
        return
      }
      if (self.timeout && self.timeoutTimer) {
        clearTimeout(self.timeoutTimer)
        self.timeoutTimer = null
      }
      self.emit('error', error)
    }
  
    if (options.form) {
      self.form(options.form)
    }
  
    if (options.oauth) {
      self.oauth(options.oauth)
    }
  
    if (self.uri.auth && !self.headers.authorization) {
      self.headers.authorization = "Basic " + toBase64(self.uri.auth.split(':').map(function(item){ return qs.unescape(item)}).join(':'))
    }
    if (self.proxy && self.proxy.auth && !self.headers['proxy-authorization'] && !self.tunnel) {
      self.headers['proxy-authorization'] = "Basic " + toBase64(self.proxy.auth.split(':').map(function(item){ return qs.unescape(item)}).join(':'))
    }
  
    if (options.qs) self.qs(options.qs)
  
    if (self.uri.path) {
      self.path = self.uri.path
    } else {
      self.path = self.uri.pathname + (self.uri.search || "")
    }
  
    if (self.path.length === 0) self.path = '/'
  
    if (self.proxy && !self.tunnel) self.path = (self.uri.protocol + '//' + self.uri.host + self.path)
  
    if (options.json) {
      self.json(options.json)
    } else if (options.multipart) {
      self.multipart(options.multipart)
    }
  
    if (self.body) {
      var length = 0
      if (!Buffer.isBuffer(self.body)) {
        if (Array.isArray(self.body)) {
          for (var i = 0; i < self.body.length; i++) {
            length += self.body[i].length
          }
        } else {
          self.body = new Buffer(self.body)
          length = self.body.length
        }
      } else {
        length = self.body.length
      }
      if (length) {
        self.headers['content-length'] = length
      } else {
        throw new Error('Argument error, options.body.')
      }
    }
  
    var protocol = self.proxy && !self.tunnel ? self.proxy.protocol : self.uri.protocol
      , defaultModules = {'http:':http, 'https:':https}
      , httpModules = self.httpModules || {}
      ;
    self.httpModule = httpModules[protocol] || defaultModules[protocol]
  
    if (!self.httpModule) throw new Error("Invalid protocol")
  
    if (options.ca) self.ca = options.ca
  
    if (!self.agent) {
      if (options.agentOptions) self.agentOptions = options.agentOptions
  
      if (options.agentClass) {
        self.agentClass = options.agentClass
      } else if (options.forever) {
        self.agentClass = protocol === 'http:' ? ForeverAgent : ForeverAgent.SSL
      } else {
        self.agentClass = self.httpModule.Agent
      }
    }
  
    if (self.pool === false) {
      self.agent = false
    } else {
      self.agent = self.agent || self.getAgent()
      if (self.maxSockets) {
        // Don't use our pooling if node has the refactored client
        self.agent.maxSockets = self.maxSockets
      }
      if (self.pool.maxSockets) {
        // Don't use our pooling if node has the refactored client
        self.agent.maxSockets = self.pool.maxSockets
      }
    }
  
    self.once('pipe', function (src) {
      if (self.ntick) throw new Error("You cannot pipe to this stream after the first nextTick() after creation of the request stream.")
      self.src = src
      if (isReadStream(src)) {
        if (!self.headers['content-type'] && !self.headers['Content-Type'])
          self.headers['content-type'] = mimetypes.lookup(src.path.slice(src.path.lastIndexOf('.')+1))
      } else {
        if (src.headers) {
          for (var i in src.headers) {
            if (!self.headers[i]) {
              self.headers[i] = src.headers[i]
            }
          }
        }
        if (src.method && !self.method) {
          self.method = src.method
        }
      }
  
      self.on('pipe', function () {
        console.error("You have already piped to this stream. Pipeing twice is likely to break the request.")
      })
    })
  
    process.nextTick(function () {
      if (self._aborted) return
      
      if (self.body) {
        if (Array.isArray(self.body)) {
          self.body.forEach(function(part) {
            self.write(part)
          })
        } else {
          self.write(self.body)
        }
        self.end()
      } else if (self.requestBodyStream) {
        console.warn("options.requestBodyStream is deprecated, please pass the request object to stream.pipe.")
        self.requestBodyStream.pipe(self)
      } else if (!self.src) {
        self.headers['content-length'] = 0
        self.end()
      }
      self.ntick = true
    })
  }
  
  Request.prototype.getAgent = function () {
    var Agent = this.agentClass
    var options = {}
    if (this.agentOptions) {
      for (var i in this.agentOptions) {
        options[i] = this.agentOptions[i]
      }
    }
    if (this.ca) options.ca = this.ca
  
    var poolKey = ''
  
    // different types of agents are in different pools
    if (Agent !== this.httpModule.Agent) {
      poolKey += Agent.name
    }
  
    if (!this.httpModule.globalAgent) {
      // node 0.4.x
      options.host = this.host
      options.port = this.port
      if (poolKey) poolKey += ':'
      poolKey += this.host + ':' + this.port
    }
  
    if (options.ca) {
      if (poolKey) poolKey += ':'
      poolKey += options.ca
    }
  
    if (!poolKey && Agent === this.httpModule.Agent && this.httpModule.globalAgent) {
      // not doing anything special.  Use the globalAgent
      return this.httpModule.globalAgent
    }
  
    // already generated an agent for this setting
    if (this.pool[poolKey]) return this.pool[poolKey]
  
    return this.pool[poolKey] = new Agent(options)
  }
  
  Request.prototype.start = function () {
    var self = this
    
    if (self._aborted) return
    
    self._started = true
    self.method = self.method || 'GET'
    self.href = self.uri.href
    if (log) log('%method %href', self)
    self.req = self.httpModule.request(self, function (response) {
      if (self._aborted) return
      if (self._paused) response.pause()
      
      self.response = response
      response.request = self
      response.toJSON = toJSON
  
      if (self.httpModule === https &&
          self.strictSSL &&
          !response.client.authorized) {
        var sslErr = response.client.authorizationError
        self.emit('error', new Error('SSL Error: '+ sslErr))
        return
      }
  
      if (self.setHost) delete self.headers.host
      if (self.timeout && self.timeoutTimer) {
        clearTimeout(self.timeoutTimer)
        self.timeoutTimer = null
      }  
      
      if (response.headers['set-cookie'] && (!self._disableCookies)) {
        response.headers['set-cookie'].forEach(function(cookie) {
          if (self._jar) self._jar.add(new Cookie(cookie))
          else cookieJar.add(new Cookie(cookie))
        })
      }
  
      if (response.statusCode >= 300 && response.statusCode < 400  &&
          (self.followAllRedirects ||
           (self.followRedirect && (self.method !== 'PUT' && self.method !== 'POST' && self.method !== 'DELETE'))) &&
          response.headers.location) {
        if (self._redirectsFollowed >= self.maxRedirects) {
          self.emit('error', new Error("Exceeded maxRedirects. Probably stuck in a redirect loop."))
          return
        }
        self._redirectsFollowed += 1
  
        if (!isUrl.test(response.headers.location)) {
          response.headers.location = url.resolve(self.uri.href, response.headers.location)
        }
        self.uri = response.headers.location
        self.redirects.push(
          { statusCode : response.statusCode
          , redirectUri: response.headers.location 
          }
        )
        if (self.followAllRedirects) self.method = 'GET'
        // self.method = 'GET'; // Force all redirects to use GET || commented out fixes #215
        delete self.req
        delete self.agent
        delete self._started
        delete self.body
        if (self.headers) {
          delete self.headers.host
        }
        if (log) log('Redirect to %uri', self)
        self.init()
        return // Ignore the rest of the response
      } else {
        self._redirectsFollowed = self._redirectsFollowed || 0
        // Be a good stream and emit end when the response is finished.
        // Hack to emit end on close because of a core bug that never fires end
        response.on('close', function () {
          if (!self._ended) self.response.emit('end')
        })
  
        if (self.encoding) {
          if (self.dests.length !== 0) {
            console.error("Ingoring encoding parameter as this stream is being piped to another stream which makes the encoding option invalid.")
          } else {
            response.setEncoding(self.encoding)
          }
        }
  
        self.dests.forEach(function (dest) {
          self.pipeDest(dest)
        })
  
        response.on("data", function (chunk) {
          self._destdata = true
          self.emit("data", chunk)
        })
        response.on("end", function (chunk) {
          self._ended = true
          self.emit("end", chunk)
        })
        response.on("close", function () {self.emit("close")})
  
        self.emit('response', response)
  
        if (self.callback) {
          var buffer = []
          var bodyLen = 0
          self.on("data", function (chunk) {
            buffer.push(chunk)
            bodyLen += chunk.length
          })
          self.on("end", function () {
            if (self._aborted) return
            
            if (buffer.length && Buffer.isBuffer(buffer[0])) {
              var body = new Buffer(bodyLen)
              var i = 0
              buffer.forEach(function (chunk) {
                chunk.copy(body, i, 0, chunk.length)
                i += chunk.length
              })
              if (self.encoding === null) {
                response.body = body
              } else {
                response.body = body.toString()
              }
            } else if (buffer.length) {
              response.body = buffer.join('')
            }
  
            if (self._json) {
              try {
                response.body = JSON.parse(response.body)
              } catch (e) {}
            }
            
            self.emit('complete', response, response.body)
          })
        }
      }
    })
  
    if (self.timeout && !self.timeoutTimer) {
      self.timeoutTimer = setTimeout(function() {
        self.req.abort()
        var e = new Error("ETIMEDOUT")
        e.code = "ETIMEDOUT"
        self.emit("error", e)
      }, self.timeout)
      
      // Set additional timeout on socket - in case if remote
      // server freeze after sending headers
      if (self.req.setTimeout) { // only works on node 0.6+
        self.req.setTimeout(self.timeout, function(){
          if (self.req) {
            self.req.abort()
            var e = new Error("ESOCKETTIMEDOUT")
            e.code = "ESOCKETTIMEDOUT"
            self.emit("error", e)
          }
        })
      }
    }
    
    self.req.on('error', self.clientErrorHandler)
    
    self.emit('request', self.req)
  }
  
  Request.prototype.abort = function() {
    this._aborted = true;
    
    if (this.req) {
      this.req.abort()
    }
    else if (this.response) {
      this.response.abort()
    }
    
    this.emit("abort")
  }
  
  Request.prototype.pipeDest = function (dest) {
    var response = this.response
    // Called after the response is received
    if (dest.headers) {
      dest.headers['content-type'] = response.headers['content-type']
      if (response.headers['content-length']) {
        dest.headers['content-length'] = response.headers['content-length']
      }
    }
    if (dest.setHeader) {
      for (var i in response.headers) {
        dest.setHeader(i, response.headers[i])
      }
      dest.statusCode = response.statusCode
    }
    if (this.pipefilter) this.pipefilter(response, dest)
  }
  
  // Composable API
  Request.prototype.setHeader = function (name, value, clobber) {
    if (clobber === undefined) clobber = true
    if (clobber || !this.headers.hasOwnProperty(name)) this.headers[name] = value
    else this.headers[name] += ',' + value
    return this
  }
  Request.prototype.setHeaders = function (headers) {
    for (i in headers) {this.setHeader(i, headers[i])}
    return this
  }
  Request.prototype.qs = function (q, clobber) {
    var base
    if (!clobber && this.uri.query) base = qs.parse(this.uri.query)
    else base = {}
    
    for (var i in q) {
      base[i] = q[i]
    }
    
    this.uri = url.parse(this.uri.href.split('?')[0] + '?' + qs.stringify(base))
    this.url = this.uri
    
    return this
  }
  Request.prototype.form = function (form) {
    this.headers['content-type'] = 'application/x-www-form-urlencoded; charset=utf-8'
    this.body = qs.stringify(form).toString('utf8')
    return this
  }
  Request.prototype.multipart = function (multipart) {
    var self = this
    self.body = []
  
    if (!self.headers['content-type']) {
      self.headers['content-type'] = 'multipart/related; boundary=frontier';
    } else {
      self.headers['content-type'] = self.headers['content-type'].split(';')[0] + '; boundary=frontier';
    }
  
    if (!multipart.forEach) throw new Error('Argument error, options.multipart.')
  
    multipart.forEach(function (part) {
      var body = part.body
      if(!body) throw Error('Body attribute missing in multipart.')
      delete part.body
      var preamble = '--frontier\r\n'
      Object.keys(part).forEach(function(key){
        preamble += key + ': ' + part[key] + '\r\n'
      })
      preamble += '\r\n'
      self.body.push(new Buffer(preamble))
      self.body.push(new Buffer(body))
      self.body.push(new Buffer('\r\n'))
    })
    self.body.push(new Buffer('--frontier--'))
    return self
  }
  Request.prototype.json = function (val) {
    this.setHeader('content-type', 'application/json')
    this.setHeader('accept', 'application/json')
    this._json = true
    if (typeof val === 'boolean') {
      if (typeof this.body === 'object') this.body = JSON.stringify(this.body)
    } else {
      this.body = JSON.stringify(val)
    }
    return this
  }
  Request.prototype.oauth = function (_oauth) {
    var form
    if (this.headers['content-type'] && 
        this.headers['content-type'].slice(0, 'application/x-www-form-urlencoded'.length) ===
          'application/x-www-form-urlencoded' 
       ) {
      form = qs.parse(this.body)
    }
    if (this.uri.query) {
      form = qs.parse(this.uri.query)
    } 
    if (!form) form = {}
    var oa = {}
    for (var i in form) oa[i] = form[i]
    for (var i in _oauth) oa['oauth_'+i] = _oauth[i]
    if (!oa.oauth_version) oa.oauth_version = '1.0'
    if (!oa.oauth_timestamp) oa.oauth_timestamp = Math.floor( (new Date()).getTime() / 1000 ).toString()
    if (!oa.oauth_nonce) oa.oauth_nonce = uuid().replace(/-/g, '')
    
    oa.oauth_signature_method = 'HMAC-SHA1'
    
    var consumer_secret = oa.oauth_consumer_secret
    delete oa.oauth_consumer_secret
    var token_secret = oa.oauth_token_secret
    delete oa.oauth_token_secret
    
    var baseurl = this.uri.protocol + '//' + this.uri.host + this.uri.pathname
    var signature = oauth.hmacsign(this.method, baseurl, oa, consumer_secret, token_secret)
    
    // oa.oauth_signature = signature
    for (var i in form) {
      if ( i.slice(0, 'oauth_') in _oauth) {
        // skip 
      } else {
        delete oa['oauth_'+i]
      }
    }
    this.headers.Authorization = 
      'OAuth '+Object.keys(oa).sort().map(function (i) {return i+'="'+oauth.rfc3986(oa[i])+'"'}).join(',')
    this.headers.Authorization += ',oauth_signature="'+oauth.rfc3986(signature)+'"'
    return this
  }
  Request.prototype.jar = function (jar) {
    var cookies
    
    if (this._redirectsFollowed === 0) {
      this.originalCookieHeader = this.headers.cookie
    }
    
    if (jar === false) {
      // disable cookies
      cookies = false;
      this._disableCookies = true;
    } else if (jar) {
      // fetch cookie from the user defined cookie jar
      cookies = jar.get({ url: this.uri.href })
    } else {
      // fetch cookie from the global cookie jar
      cookies = cookieJar.get({ url: this.uri.href })
    }
    
    if (cookies && cookies.length) {
      var cookieString = cookies.map(function (c) {
        return c.name + "=" + c.value
      }).join("; ")
  
      if (this.originalCookieHeader) {
        // Don't overwrite existing Cookie header
        this.headers.cookie = this.originalCookieHeader + '; ' + cookieString
      } else {
        this.headers.cookie = cookieString
      }
    }
    this._jar = jar
    return this
  }
  
  
  // Stream API
  Request.prototype.pipe = function (dest, opts) {
    if (this.response) {
      if (this._destdata) {
        throw new Error("You cannot pipe after data has been emitted from the response.")
      } else if (this._ended) {
        throw new Error("You cannot pipe after the response has been ended.")
      } else {
        stream.Stream.prototype.pipe.call(this, dest, opts)
        this.pipeDest(dest)
        return dest
      }
    } else {
      this.dests.push(dest)
      stream.Stream.prototype.pipe.call(this, dest, opts)
      return dest
    }
  }
  Request.prototype.write = function () {
    if (!this._started) this.start()
    this.req.write.apply(this.req, arguments)
  }
  Request.prototype.end = function (chunk) {
    if (chunk) this.write(chunk)
    if (!this._started) this.start()
    this.req.end()
  }
  Request.prototype.pause = function () {
    if (!this.response) this._paused = true
    else this.response.pause.apply(this.response, arguments)
  }
  Request.prototype.resume = function () {
    if (!this.response) this._paused = false
    else this.response.resume.apply(this.response, arguments)
  }
  Request.prototype.destroy = function () {
    if (!this._ended) this.end()
  }
  
  // organize params for post, put, head, del
  function initParams(uri, options, callback) {
    if ((typeof options === 'function') && !callback) callback = options;
    if (typeof options === 'object') {
      options.uri = uri;
    } else if (typeof uri === 'string') {
      options = {uri:uri};
    } else {
      options = uri;
      uri = options.uri;
    }
    return { uri: uri, options: options, callback: callback };
  }
  
  function request (uri, options, callback) {
    if (typeof uri === 'undefined') throw new Error('undefined is not a valid uri or options object.')
    if ((typeof options === 'function') && !callback) callback = options;
    if (typeof options === 'object') {
      options.uri = uri;
    } else if (typeof uri === 'string') {
      options = {uri:uri};
    } else {
      options = uri;
    }
  
    if (callback) options.callback = callback;
    var r = new Request(options)
    return r
  }
  
  module.exports = request
  
  request.defaults = function (options) {
    var def = function (method) {
      var d = function (uri, opts, callback) {
        var params = initParams(uri, opts, callback);
        for (var i in options) {
          if (params.options[i] === undefined) params.options[i] = options[i]
        }
        return method(params.options, params.callback)
      }
      return d
    }
    var de = def(request)
    de.get = def(request.get)
    de.post = def(request.post)
    de.put = def(request.put)
    de.head = def(request.head)
    de.del = def(request.del)
    de.cookie = def(request.cookie)
    de.jar = def(request.jar)
    return de
  }
  
  request.forever = function (agentOptions, optionsArg) {
    var options = {}
    if (optionsArg) {
      for (option in optionsArg) {
        options[option] = optionsArg[option]
      }
    }
    if (agentOptions) options.agentOptions = agentOptions
    options.forever = true
    return request.defaults(options)
  }
  
  request.get = request
  request.post = function (uri, options, callback) {
    var params = initParams(uri, options, callback);
    params.options.method = 'POST';
    return request(params.uri || null, params.options, params.callback)
  }
  request.put = function (uri, options, callback) {
    var params = initParams(uri, options, callback);
    params.options.method = 'PUT'
    return request(params.uri || null, params.options, params.callback)
  }
  request.head = function (uri, options, callback) {
    var params = initParams(uri, options, callback);
    params.options.method = 'HEAD'
    if (params.options.body || 
        params.options.requestBodyStream || 
        (params.options.json && typeof params.options.json !== 'boolean') || 
        params.options.multipart) {
      throw new Error("HTTP HEAD requests MUST NOT include a request body.")
    }
    return request(params.uri || null, params.options, params.callback)
  }
  request.del = function (uri, options, callback) {
    var params = initParams(uri, options, callback);
    params.options.method = 'DELETE'
    return request(params.uri || null, params.options, params.callback)
  }
  request.jar = function () {
    return new CookieJar
  }
  request.cookie = function (str) {
    if (str && str.uri) str = str.uri
    if (typeof str !== 'string') throw new Error("The cookie function only accepts STRING as param")
    return new Cookie(str)
  }
  
  // Safe toJSON
  
  function getSafe (self, uuid) {  
    if (typeof self === 'object' || typeof self === 'function') var safe = {}
    if (Array.isArray(self)) var safe = []
  
    var recurse = []
    
    Object.defineProperty(self, uuid, {})
    
    var attrs = Object.keys(self).filter(function (i) {
      if (i === uuid) return false 
      if ( (typeof self[i] !== 'object' && typeof self[i] !== 'function') || self[i] === null) return true
      return !(Object.getOwnPropertyDescriptor(self[i], uuid))
    })
    
    
    for (var i=0;i<attrs.length;i++) {
      if ( (typeof self[attrs[i]] !== 'object' && typeof self[attrs[i]] !== 'function') || 
            self[attrs[i]] === null
          ) {
        safe[attrs[i]] = self[attrs[i]]
      } else {
        recurse.push(attrs[i])
        Object.defineProperty(self[attrs[i]], uuid, {})
      }
    }
  
    for (var i=0;i<recurse.length;i++) {
      safe[recurse[i]] = getSafe(self[recurse[i]], uuid)
    }
    
    return safe
  }
  
  function toJSON () {
    return getSafe(this, (((1+Math.random())*0x10000)|0).toString(16))
  }
  
  Request.prototype.toJSON = toJSON
  
  

  provide("request", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  module.exports = function archy (obj, prefix, opts) {
      if (prefix === undefined) prefix = '';
      if (!opts) opts = {};
      var chr = function (s) {
          var chars = {
              '│' : '|',
              '└' : '`',
              '├' : '+',
              '─' : '-',
              '┬' : '-'
          };
          return opts.unicode === false ? chars[s] : s;
      };
      
      if (typeof obj === 'string') obj = { label : obj };
      
      var nodes = obj.nodes || [];
      var lines = (obj.label || '').split('\n');
      var splitter = '\n' + prefix + (nodes.length ? chr('│') : ' ') + ' ';
      
      return prefix
          + lines.join(splitter) + '\n'
          + nodes.map(function (node, ix) {
              var last = ix === nodes.length - 1;
              var more = node.nodes && node.nodes.length;
              var prefix_ = prefix + (last ? ' ' : chr('│')) + ' ';
              
              return prefix
                  + (last ? chr('└') : chr('├')) + chr('─')
                  + (more ? chr('┬') : chr('─')) + ' '
                  + archy(node, prefix_, opts).slice(prefix.length + 2)
              ;
          }).join('')
      ;
  };
  

  provide("archy", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  //     node-uuid/uuid.js
  //
  //     Copyright (c) 2010 Robert Kieffer
  //     Dual licensed under the MIT and GPL licenses.
  //     Documentation and details at https://github.com/broofa/node-uuid
  (function() {
    var _global = this;
  
    // Unique ID creation requires a high quality random # generator, but
    // Math.random() does not guarantee "cryptographic quality".  So we feature
    // detect for more robust APIs, normalizing each method to return 128-bits
    // (16 bytes) of random data.
    var mathRNG, nodeRNG, whatwgRNG;
  
    // Math.random()-based RNG.  All platforms, very fast, unknown quality
    var _rndBytes = new Array(16);
    mathRNG = function() {
      var r, b = _rndBytes, i = 0;
  
      for (var i = 0, r; i < 16; i++) {
        if ((i & 0x03) == 0) r = Math.random() * 0x100000000;
        b[i] = r >>> ((i & 0x03) << 3) & 0xff;
      }
  
      return b;
    }
  
    // WHATWG crypto-based RNG - http://wiki.whatwg.org/wiki/Crypto
    // WebKit only (currently), moderately fast, high quality
    if (_global.crypto && crypto.getRandomValues) {
      var _rnds = new Uint32Array(4);
      whatwgRNG = function() {
        crypto.getRandomValues(_rnds);
  
        for (var c = 0 ; c < 16; c++) {
          _rndBytes[c] = _rnds[c >> 2] >>> ((c & 0x03) * 8) & 0xff;
        }
        return _rndBytes;
      }
    }
  
    // Node.js crypto-based RNG - http://nodejs.org/docs/v0.6.2/api/crypto.html
    // Node.js only, moderately fast, high quality
    try {
      var _rb = require('crypto').randomBytes;
      nodeRNG = _rb && function() {
        return _rb(16);
      };
    } catch (e) {}
  
    // Select RNG with best quality
    var _rng = nodeRNG || whatwgRNG || mathRNG;
  
    // Buffer class to use
    var BufferClass = typeof(Buffer) == 'function' ? Buffer : Array;
  
    // Maps for number <-> hex string conversion
    var _byteToHex = [];
    var _hexToByte = {};
    for (var i = 0; i < 256; i++) {
      _byteToHex[i] = (i + 0x100).toString(16).substr(1);
      _hexToByte[_byteToHex[i]] = i;
    }
  
    // **`parse()` - Parse a UUID into it's component bytes**
    function parse(s, buf, offset) {
      var i = (buf && offset) || 0, ii = 0;
  
      buf = buf || [];
      s.toLowerCase().replace(/[0-9a-f]{2}/g, function(byte) {
        if (ii < 16) { // Don't overflow!
          buf[i + ii++] = _hexToByte[byte];
        }
      });
  
      // Zero out remaining bytes if string was short
      while (ii < 16) {
        buf[i + ii++] = 0;
      }
  
      return buf;
    }
  
    // **`unparse()` - Convert UUID byte array (ala parse()) into a string**
    function unparse(buf, offset) {
      var i = offset || 0, bth = _byteToHex;
      return  bth[buf[i++]] + bth[buf[i++]] +
              bth[buf[i++]] + bth[buf[i++]] + '-' +
              bth[buf[i++]] + bth[buf[i++]] + '-' +
              bth[buf[i++]] + bth[buf[i++]] + '-' +
              bth[buf[i++]] + bth[buf[i++]] + '-' +
              bth[buf[i++]] + bth[buf[i++]] +
              bth[buf[i++]] + bth[buf[i++]] +
              bth[buf[i++]] + bth[buf[i++]];
    }
  
    // **`v1()` - Generate time-based UUID**
    //
    // Inspired by https://github.com/LiosK/UUID.js
    // and http://docs.python.org/library/uuid.html
  
    // random #'s we need to init node and clockseq
    var _seedBytes = _rng();
  
    // Per 4.5, create and 48-bit node id, (47 random bits + multicast bit = 1)
    var _nodeId = [
      _seedBytes[0] | 0x01,
      _seedBytes[1], _seedBytes[2], _seedBytes[3], _seedBytes[4], _seedBytes[5]
    ];
  
    // Per 4.2.2, randomize (14 bit) clockseq
    var _clockseq = (_seedBytes[6] << 8 | _seedBytes[7]) & 0x3fff;
  
    // Previous uuid creation time
    var _lastMSecs = 0, _lastNSecs = 0;
  
    // See https://github.com/broofa/node-uuid for API details
    function v1(options, buf, offset) {
      var i = buf && offset || 0;
      var b = buf || [];
  
      options = options || {};
  
      var clockseq = options.clockseq != null ? options.clockseq : _clockseq;
  
      // UUID timestamps are 100 nano-second units since the Gregorian epoch,
      // (1582-10-15 00:00).  JSNumbers aren't precise enough for this, so
      // time is handled internally as 'msecs' (integer milliseconds) and 'nsecs'
      // (100-nanoseconds offset from msecs) since unix epoch, 1970-01-01 00:00.
      var msecs = options.msecs != null ? options.msecs : new Date().getTime();
  
      // Per 4.2.1.2, use count of uuid's generated during the current clock
      // cycle to simulate higher resolution clock
      var nsecs = options.nsecs != null ? options.nsecs : _lastNSecs + 1;
  
      // Time since last uuid creation (in msecs)
      var dt = (msecs - _lastMSecs) + (nsecs - _lastNSecs)/10000;
  
      // Per 4.2.1.2, Bump clockseq on clock regression
      if (dt < 0 && options.clockseq == null) {
        clockseq = clockseq + 1 & 0x3fff;
      }
  
      // Reset nsecs if clock regresses (new clockseq) or we've moved onto a new
      // time interval
      if ((dt < 0 || msecs > _lastMSecs) && options.nsecs == null) {
        nsecs = 0;
      }
  
      // Per 4.2.1.2 Throw error if too many uuids are requested
      if (nsecs >= 10000) {
        throw new Error('uuid.v1(): Can\'t create more than 10M uuids/sec');
      }
  
      _lastMSecs = msecs;
      _lastNSecs = nsecs;
      _clockseq = clockseq;
  
      // Per 4.1.4 - Convert from unix epoch to Gregorian epoch
      msecs += 12219292800000;
  
      // `time_low`
      var tl = ((msecs & 0xfffffff) * 10000 + nsecs) % 0x100000000;
      b[i++] = tl >>> 24 & 0xff;
      b[i++] = tl >>> 16 & 0xff;
      b[i++] = tl >>> 8 & 0xff;
      b[i++] = tl & 0xff;
  
      // `time_mid`
      var tmh = (msecs / 0x100000000 * 10000) & 0xfffffff;
      b[i++] = tmh >>> 8 & 0xff;
      b[i++] = tmh & 0xff;
  
      // `time_high_and_version`
      b[i++] = tmh >>> 24 & 0xf | 0x10; // include version
      b[i++] = tmh >>> 16 & 0xff;
  
      // `clock_seq_hi_and_reserved` (Per 4.2.2 - include variant)
      b[i++] = clockseq >>> 8 | 0x80;
  
      // `clock_seq_low`
      b[i++] = clockseq & 0xff;
  
      // `node`
      var node = options.node || _nodeId;
      for (var n = 0; n < 6; n++) {
        b[i + n] = node[n];
      }
  
      return buf ? buf : unparse(b);
    }
  
    // **`v4()` - Generate random UUID**
  
    // See https://github.com/broofa/node-uuid for API details
    function v4(options, buf, offset) {
      // Deprecated - 'format' argument, as supported in v1.2
      var i = buf && offset || 0;
  
      if (typeof(options) == 'string') {
        buf = options == 'binary' ? new BufferClass(16) : null;
        options = null;
      }
      options = options || {};
  
      var rnds = options.random || (options.rng || _rng)();
  
      // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`
      rnds[6] = (rnds[6] & 0x0f) | 0x40;
      rnds[8] = (rnds[8] & 0x3f) | 0x80;
  
      // Copy bytes to buffer, if provided
      if (buf) {
        for (var ii = 0; ii < 16; ii++) {
          buf[i + ii] = rnds[ii];
        }
      }
  
      return buf || unparse(rnds);
    }
  
    // Export public API
    var uuid = v4;
    uuid.v1 = v1;
    uuid.v4 = v4;
    uuid.parse = parse;
    uuid.unparse = unparse;
    uuid.BufferClass = BufferClass;
  
    // Export RNG options
    uuid.mathRNG = mathRNG;
    uuid.nodeRNG = nodeRNG;
    uuid.whatwgRNG = whatwgRNG;
  
    if (typeof(module) != 'undefined') {
      // Play nice with node.js
      module.exports = uuid;
    } else {
      // Play nice with browsers
      var _previousRoot = _global.uuid;
  
      // **`noConflict()` - (browser only) to reset global 'uuid' var**
      uuid.noConflict = function() {
        _global.uuid = _previousRoot;
        return uuid;
      }
      _global.uuid = uuid;
    }
  }());
  

  provide("node-uuid", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  exports.asyncMap = require("./async-map")
  exports.bindActor = require("./bind-actor")
  exports.chain = require("./chain")
  

  provide("slide", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  module.exports = exports = abbrev.abbrev = abbrev
  
  abbrev.monkeyPatch = monkeyPatch
  
  function monkeyPatch () {
    Array.prototype.abbrev = function () { return abbrev(this) }
    Object.prototype.abbrev = function () { return abbrev(Object.keys(this)) }
  }
  
  function abbrev (list) {
    if (arguments.length !== 1 || !Array.isArray(list)) {
      list = Array.prototype.slice.call(arguments, 0)
    }
    for (var i = 0, l = list.length, args = [] ; i < l ; i ++) {
      args[i] = typeof list[i] === "string" ? list[i] : String(list[i])
    }
  
    // sort them lexicographically, so that they're next to their nearest kin
    args = args.sort(lexSort)
  
    // walk through each, seeing how much it has in common with the next and previous
    var abbrevs = {}
      , prev = ""
    for (var i = 0, l = args.length ; i < l ; i ++) {
      var current = args[i]
        , next = args[i + 1] || ""
        , nextMatches = true
        , prevMatches = true
      if (current === next) continue
      for (var j = 0, cl = current.length ; j < cl ; j ++) {
        var curChar = current.charAt(j)
        nextMatches = nextMatches && curChar === next.charAt(j)
        prevMatches = prevMatches && curChar === prev.charAt(j)
        if (nextMatches || prevMatches) continue
        else {
          j ++
          break
        }
      }
      prev = current
      if (j === cl) {
        abbrevs[current] = current
        continue
      }
      for (var a = current.substr(0, j) ; j <= cl ; j ++) {
        abbrevs[a] = current
        a += current.charAt(j)
      }
    }
    return abbrevs
  }
  
  function lexSort (a, b) {
    return a === b ? 0 : a > b ? 1 : -1
  }
  
  
  // tests
  if (module === require.main) {
  
  var assert = require("assert")
    , sys
  sys = require("util")
  
  console.log("running tests")
  function test (list, expect) {
    var actual = abbrev(list)
    assert.deepEqual(actual, expect,
      "abbrev("+sys.inspect(list)+") === " + sys.inspect(expect) + "\n"+
      "actual: "+sys.inspect(actual))
    actual = abbrev.apply(exports, list)
    assert.deepEqual(abbrev.apply(exports, list), expect,
      "abbrev("+list.map(JSON.stringify).join(",")+") === " + sys.inspect(expect) + "\n"+
      "actual: "+sys.inspect(actual))
  }
  
  test([ "ruby", "ruby", "rules", "rules", "rules" ],
  { rub: 'ruby'
  , ruby: 'ruby'
  , rul: 'rules'
  , rule: 'rules'
  , rules: 'rules'
  })
  test(["fool", "foom", "pool", "pope"],
  { fool: 'fool'
  , foom: 'foom'
  , poo: 'pool'
  , pool: 'pool'
  , pop: 'pope'
  , pope: 'pope'
  })
  test(["a", "ab", "abc", "abcd", "abcde", "acde"],
  { a: 'a'
  , ab: 'ab'
  , abc: 'abc'
  , abcd: 'abcd'
  , abcde: 'abcde'
  , ac: 'acde'
  , acd: 'acde'
  , acde: 'acde'
  })
  
  console.log("pass")
  
  }
  

  provide("abbrev", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  module.exports = uidNumber
  
  // This module calls into get-uid-gid.js, which sets the
  // uid and gid to the supplied argument, in order to find out their
  // numeric value.  This can't be done in the main node process,
  // because otherwise node would be running as that user from this
  // point on.
  
  var child_process = require("child_process")
    , path = require("path")
    , uidSupport = process.getuid && process.setuid
    , uidCache = {}
    , gidCache = {}
  
  function uidNumber (uid, gid, cb) {
    if (!uidSupport) return cb()
    if (typeof cb !== "function") cb = gid, gid = null
    if (typeof cb !== "function") cb = uid, uid = null
    if (gid == null) gid = process.getgid()
    if (uid == null) uid = process.getuid()
    if (!isNaN(gid)) gid = uidCache[gid] = +gid
    if (!isNaN(uid)) uid = uidCache[uid] = +uid
  
    if (uidCache.hasOwnProperty(uid)) uid = uidCache[uid]
    if (gidCache.hasOwnProperty(gid)) gid = gidCache[gid]
  
    if (typeof gid === "number" && typeof uid === "number") {
      return process.nextTick(cb.bind(null, null, uid, gid))
    }
  
    var getter = require.resolve("./get-uid-gid.js")
  
    child_process.execFile( process.execPath
                          , [getter, uid, gid]
                          , function (code, out, err) {
      if (er) return cb(new Error("could not get uid/gid\n" + err))
      try {
        out = JSON.parse(out+"")
      } catch (ex) {
        return cb(ex)
      }
  
      if (out.error) {
        var er = new Error(out.error)
        er.errno = out.errno
        return cb(er)
      }
  
      if (isNaN(out.uid) || isNaN(out.gid)) return cb(new Error(
        "Could not get uid/gid: "+JSON.stringify(out)))
  
      cb(null, uidCache[uid] = +out.uid, uidCache[gid] = +out.gid)
    })
  }
  

  provide("uid-number", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  module.exports = rimraf
  rimraf.sync = rimrafSync
  
  var path = require("path")
    , fs
  
  try {
    // optional dependency
    fs = require("graceful-fs")
  } catch (er) {
    fs = require("fs")
  }
  
  var lstat = "lstat"
  if (process.platform === "win32") {
    // not reliable on windows prior to 0.7.9
    var v = process.version.replace(/^v/, '').split(/\.|-/).map(Number)
    if (v[0] === 0 && (v[1] < 7 || v[1] == 7 && v[2] < 9)) {
      lstat = "stat"
    }
  }
  if (!fs[lstat]) lstat = "stat"
  var lstatSync = lstat + "Sync"
  
  // for EMFILE handling
  var timeout = 0
  exports.EMFILE_MAX = 1000
  exports.BUSYTRIES_MAX = 3
  
  function rimraf (p, cb) {
  
    if (!cb) throw new Error("No callback passed to rimraf()")
  
    var busyTries = 0
  
    rimraf_(p, function CB (er) {
      if (er) {
        if (er.code === "EBUSY" && busyTries < exports.BUSYTRIES_MAX) {
          busyTries ++
          var time = busyTries * 100
          // try again, with the same exact callback as this one.
          return setTimeout(function () {
            rimraf_(p, CB)
          }, time)
        }
  
        // this one won't happen if graceful-fs is used.
        if (er.code === "EMFILE" && timeout < exports.EMFILE_MAX) {
          return setTimeout(function () {
            rimraf_(p, CB)
          }, timeout ++)
        }
  
        // already gone
        if (er.code === "ENOENT") er = null
      }
  
      timeout = 0
      cb(er)
    })
  }
  
  function rimraf_ (p, cb) {
    fs[lstat](p, function (er, s) {
      if (er) {
        // already gone
        if (er.code === "ENOENT") return cb()
        // some other kind of error, permissions, etc.
        return cb(er)
      }
  
      return rm_(p, s, false, cb)
    })
  }
  
  
  var myGid = function myGid () {
    var g = process.getuid && process.getgid()
    myGid = function myGid () { return g }
    return g
  }
  
  var myUid = function myUid () {
    var u = process.getuid && process.getuid()
    myUid = function myUid () { return u }
    return u
  }
  
  
  function writable (s) {
    var mode = s.mode || 0777
      , uid = myUid()
      , gid = myGid()
    return (mode & 0002)
        || (gid === s.gid && (mode & 0020))
        || (uid === s.uid && (mode & 0200))
  }
  
  function rm_ (p, s, didWritableCheck, cb) {
    if (!didWritableCheck && !writable(s)) {
      // make file writable
      // user/group/world, doesn't matter at this point
      // since it's about to get nuked.
      return fs.chmod(p, s.mode | 0222, function (er) {
        if (er) return cb(er)
        rm_(p, s, true, cb)
      })
    }
  
    if (!s.isDirectory()) {
      return fs.unlink(p, cb)
    }
  
    // directory
    fs.readdir(p, function (er, files) {
      if (er) return cb(er)
      asyncForEach(files.map(function (f) {
        return path.join(p, f)
      }), function (file, cb) {
        rimraf(file, cb)
      }, function (er) {
        if (er) return cb(er)
        fs.rmdir(p, cb)
      })
    })
  }
  
  function asyncForEach (list, fn, cb) {
    if (!list.length) cb()
    var c = list.length
      , errState = null
    list.forEach(function (item, i, list) {
      fn(item, function (er) {
        if (errState) return
        if (er) return cb(errState = er)
        if (-- c === 0) return cb()
      })
    })
  }
  
  // this looks simpler, but it will fail with big directory trees,
  // or on slow stupid awful cygwin filesystems
  function rimrafSync (p) {
    try {
      var s = fs[lstatSync](p)
    } catch (er) {
      if (er.code === "ENOENT") return
      throw er
    }
  
    if (!writable(s)) {
      fs.chmodSync(p, s.mode | 0222)
    }
  
    if (!s.isDirectory()) return fs.unlinkSync(p)
  
    fs.readdirSync(p).forEach(function (f) {
      rimrafSync(path.join(p, f))
    })
    fs.rmdirSync(p)
  }
  

  provide("rimraf", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  var EE = require('events').EventEmitter
  var log = exports = module.exports = new EE
  var util = require('util')
  
  var ansi = require('ansi')
  log.cursor = ansi(process.stderr)
  log.stream = process.stderr
  
  // by default, let ansi decide based on tty-ness.
  var colorEnabled = undefined
  log.enableColor = function () {
    colorEnabled = true
    this.cursor.enabled = true
  }
  log.disableColor = function () {
    colorEnabled = false
    this.cursor.enabled = false
  }
  
  // default level
  log.level = 'info'
  
  // temporarily stop emitting, but don't drop
  log.pause = function () {
    this._paused = true
  }
  
  log.resume = function () {
    if (!this._paused) return
    this._paused = false
  
    var b = this._buffer
    this._buffer = []
    b.forEach(function (m) {
      this.emitLog(m)
    }, this)
  }
  
  log._buffer = []
  
  var id = 0
  log.record = []
  log.maxRecordSize = 10000
  log.log = function (lvl, prefix, message) {
    var l = this.levels[lvl]
    if (l === undefined) {
      return this.emit('error', new Error(util.format(
        'Undefined log level: %j', lvl)))
    }
  
    var a = new Array(arguments.length - 2)
    var stack = null
    for (var i = 2; i < arguments.length; i ++) {
      var arg = a[i-2] = arguments[i]
  
      // resolve stack traces to a plain string.
      if (typeof arg === 'object' && arg &&
          (arg instanceof Error) && arg.stack) {
        arg.stack = stack = arg.stack + ''
      }
    }
    if (stack) a.unshift(stack + '\n')
    message = util.format.apply(util, a)
  
    var m = { id: id++,
              level: lvl,
              prefix: String(prefix || ''),
              message: message,
              messageRaw: a }
  
    this.emit('log', m)
    this.emit('log.' + lvl, m)
    if (m.prefix) this.emit(m.prefix, m)
  
    this.record.push(m)
    var mrs = this.maxRecordSize
    var n = this.record.length - mrs
    if (n > mrs / 10) {
      var newSize = Math.floor(mrs * 0.9)
      this.record = this.record.slice(-1 * newSize)
    }
  
    this.emitLog(m)
  }
  
  log.emitLog = function (m) {
    if (this._paused) {
      this._buffer.push(m)
      return
    }
    var l = this.levels[m.level]
    if (l === undefined) return
    if (l < this.levels[this.level]) return
    if (l > 0 && !isFinite(l)) return
  
    var style = log.style[m.level]
    var disp = log.disp[m.level] || m.level
    m.message.split(/\r?\n/).forEach(function (line) {
      if (this.heading) {
        this.write(this.heading, this.headingStyle)
        this.write(' ')
      }
      this.write(disp, log.style[m.level])
      var p = m.prefix || ''
      if (p) this.write(' ')
      this.write(p, this.prefixStyle)
      this.write(' ' + line + '\n')
    }, this)
  }
  
  log.write = function (msg, style) {
    if (!this.cursor) return
    if (this.stream !== this.cursor.stream) {
      this.cursor = ansi(this.stream, { enabled: colorEnabled })
    }
  
    style = style || {}
    if (style.fg) this.cursor.fg[style.fg]()
    if (style.bg) this.cursor.bg[style.bg]()
    if (style.bold) this.cursor.bold()
    if (style.underline) this.cursor.underline()
    if (style.inverse) this.cursor.inverse()
    if (style.beep) this.cursor.beep()
    this.cursor.write(msg).reset()
  }
  
  log.addLevel = function (lvl, n, style, disp) {
    if (!disp) disp = lvl
    this.levels[lvl] = n
    this.style[lvl] = style
    if (!this[lvl]) this[lvl] = function () {
      var a = new Array(arguments.length + 1)
      a[0] = lvl
      for (var i = 0; i < arguments.length; i ++) {
        a[i + 1] = arguments[i]
      }
      return this.log.apply(this, a)
    }
    this.disp[lvl] = disp
  }
  
  log.prefixStyle = { fg: 'magenta' }
  log.headingStyle = { fg: 'white', bg: 'black' }
  
  log.style = {}
  log.levels = {}
  log.disp = {}
  log.addLevel('silly', -Infinity, { inverse: true }, 'sill')
  log.addLevel('verbose', 1000, { fg: 'blue', bg: 'black' }, 'verb')
  log.addLevel('info', 2000, { fg: 'green' })
  log.addLevel('http', 3000, { fg: 'green', bg: 'black' })
  log.addLevel('warn', 4000, { fg: 'black', bg: 'red' }, 'WARN')
  log.addLevel('error', 5000, { fg: 'red', bg: 'black' }, 'ERR!')
  log.addLevel('silent', Infinity)
  

  provide("npmlog", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  // utilities for working with the js-registry site.
  
  module.exports = RegClient
  
  var fs = require('fs')
  , url = require('url')
  , path = require('path')
  , npmlog
  
  try {
    npmlog = require("npmlog")
  } catch (er) {
    npmlog = { error: noop, warn: noop, info: noop,
               verbose: noop, silly: noop, http: silly,
               pause: noop, resume: noop }
  }
  
  function noop () {}
  
  function RegClient (options) {
    // a registry url must be provided.
    var registry = url.parse(options.registry)
    if (!registry.protocol) throw new Error(
      'Invalid registry: ' + registry.url)
    this.registry = registry.href
  
    this.cache = options.cache
    if (!this.cache) throw new Error("Cache dir is required")
  
    this.alwaysAuth = options.alwaysAuth || false
  
    this.auth = options.auth || null
    if (this.auth) {
      var a = new Buffer(this.auth, "base64").toString()
      a = a.split(":")
      this.username = a.shift()
      this.password = a.join(":")
    }
    this.email = options.email || null
    this.defaultTag = options.tag || "latest"
  
    this.ca = options.ca || null
  
    this.strictSSL = options.strictSSL
    if (this.strictSSL === undefined) this.strictSSL = true
  
    this.userAgent = options.userAgent
    if (this.userAgent === undefined) {
      this.userAgent = 'node/' + process.version
    }
  
    this.cacheMin = options.cacheMin || 0
    this.cacheMax = options.cacheMax || Infinity
  
    this.proxy = options.proxy
    this.httpsProxy = options.httpsProxy || options.proxy
  
    this.log = options.log || npmlog
  }
  
  require('fs').readdirSync(__dirname + "/lib").forEach(function (f) {
    if (!f.match(/\.js$/)) return
    RegClient.prototype[f.replace(/\.js$/, '')] = require('./lib/' + f)
  })
  

  provide("npm-registry-client", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  exports.Abstract = require("./lib/abstract.js")
  exports.Reader = require("./lib/reader.js")
  exports.Writer = require("./lib/writer.js")
  
  exports.File =
    { Reader: require("./lib/file-reader.js")
    , Writer: require("./lib/file-writer.js") }
  
  exports.Dir = 
    { Reader : require("./lib/dir-reader.js")
    , Writer : require("./lib/dir-writer.js") }
  
  exports.Link =
    { Reader : require("./lib/link-reader.js")
    , Writer : require("./lib/link-writer.js") }
  
  exports.Proxy =
    { Reader : require("./lib/proxy-reader.js")
    , Writer : require("./lib/proxy-writer.js") }
  
  exports.Reader.Dir = exports.DirReader = exports.Dir.Reader
  exports.Reader.File = exports.FileReader = exports.File.Reader
  exports.Reader.Link = exports.LinkReader = exports.Link.Reader
  exports.Reader.Proxy = exports.ProxyReader = exports.Proxy.Reader
  
  exports.Writer.Dir = exports.DirWriter = exports.Dir.Writer
  exports.Writer.File = exports.FileWriter = exports.File.Writer
  exports.Writer.Link = exports.LinkWriter = exports.Link.Writer
  exports.Writer.Proxy = exports.ProxyWriter = exports.Proxy.Writer
  
  exports.collect = require("./lib/collect.js")
  

  provide("fstream", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  // Approach:
  //
  // 1. Get the minimatch set
  // 2. For each pattern in the set, PROCESS(pattern)
  // 3. Store matches per-set, then uniq them
  //
  // PROCESS(pattern)
  // Get the first [n] items from pattern that are all strings
  // Join these together.  This is PREFIX.
  //   If there is no more remaining, then stat(PREFIX) and
  //   add to matches if it succeeds.  END.
  // readdir(PREFIX) as ENTRIES
  //   If fails, END
  //   If pattern[n] is GLOBSTAR
  //     // handle the case where the globstar match is empty
  //     // by pruning it out, and testing the resulting pattern
  //     PROCESS(pattern[0..n] + pattern[n+1 .. $])
  //     // handle other cases.
  //     for ENTRY in ENTRIES (not dotfiles)
  //       // attach globstar + tail onto the entry
  //       PROCESS(pattern[0..n] + ENTRY + pattern[n .. $])
  //
  //   else // not globstar
  //     for ENTRY in ENTRIES (not dotfiles, unless pattern[n] is dot)
  //       Test ENTRY against pattern[n+1]
  //       If fails, continue
  //       If passes, PROCESS(pattern[0..n] + item + pattern[n+1 .. $])
  //
  // Caveat:
  //   Cache all stats and readdirs results to minimize syscall.  Since all
  //   we ever care about is existence and directory-ness, we can just keep
  //   `true` for files, and [children,...] for directories, or `false` for
  //   things that don't exist.
  
  
  
  module.exports = glob
  
  var fs = require("graceful-fs")
  , minimatch = require("minimatch")
  , Minimatch = minimatch.Minimatch
  , inherits = require("inherits")
  , EE = require("events").EventEmitter
  , path = require("path")
  , isDir = {}
  , assert = require("assert").ok
  , EOF = {}
  
  function glob (pattern, options, cb) {
    if (typeof options === "function") cb = options, options = {}
    if (!options) options = {}
  
    if (typeof options === "number") {
      deprecated()
      return
    }
  
    var g = new Glob(pattern, options, cb)
    return g.sync ? g.found : g
  }
  
  glob.fnmatch = deprecated
  
  function deprecated () {
    throw new Error("glob's interface has changed. Please see the docs.")
  }
  
  glob.sync = globSync
  function globSync (pattern, options) {
    if (typeof options === "number") {
      deprecated()
      return
    }
  
    options = options || {}
    options.sync = true
    return glob(pattern, options)
  }
  
  
  glob.Glob = Glob
  inherits(Glob, EE)
  function Glob (pattern, options, cb) {
    if (!(this instanceof Glob)) {
      return new Glob(pattern, options, cb)
    }
  
    if (typeof cb === "function") {
      this.on("error", cb)
      this.on("end", function (matches) {
        // console.error("cb with matches", matches)
        cb(null, matches)
      })
    }
  
    options = options || {}
  
    this.maxDepth = options.maxDepth || 1000
    this.maxLength = options.maxLength || Infinity
    this.statCache = options.statCache || {}
  
    this.changedCwd = false
    var cwd = process.cwd()
    if (!options.hasOwnProperty("cwd")) this.cwd = cwd
    else {
      this.cwd = options.cwd
      this.changedCwd = path.resolve(options.cwd) !== cwd
    }
  
    this.root = options.root || path.resolve(this.cwd, "/")
    this.root = path.resolve(this.root)
  
    this.nomount = !!options.nomount
  
    if (!pattern) {
      throw new Error("must provide pattern")
    }
  
    // base-matching: just use globstar for that.
    if (options.matchBase && -1 === pattern.indexOf("/")) {
      if (options.noglobstar) {
        throw new Error("base matching requires globstar")
      }
      pattern = "**/" + pattern
    }
  
    this.dot = !!options.dot
    this.mark = !!options.mark
    this.sync = !!options.sync
    this.nounique = !!options.nounique
    this.nonull = !!options.nonull
    this.nosort = !!options.nosort
    this.nocase = !!options.nocase
    this.stat = !!options.stat
    this.debug = !!options.debug || !!options.globDebug
    this.silent = !!options.silent
  
    var mm = this.minimatch = new Minimatch(pattern, options)
    this.options = mm.options
    pattern = this.pattern = mm.pattern
  
    this.error = null
    this.aborted = false
  
    EE.call(this)
  
    // process each pattern in the minimatch set
    var n = this.minimatch.set.length
  
    // The matches are stored as {<filename>: true,...} so that
    // duplicates are automagically pruned.
    // Later, we do an Object.keys() on these.
    // Keep them as a list so we can fill in when nonull is set.
    this.matches = new Array(n)
  
    this.minimatch.set.forEach(iterator.bind(this))
    function iterator (pattern, i, set) {
      this._process(pattern, 0, i, function (er) {
        if (er) this.emit("error", er)
        if (-- n <= 0) this._finish()
      })
    }
  }
  
  Glob.prototype._finish = function () {
    assert(this instanceof Glob)
  
    var nou = this.nounique
    , all = nou ? [] : {}
  
    for (var i = 0, l = this.matches.length; i < l; i ++) {
      var matches = this.matches[i]
      if (this.debug) console.error("matches[%d] =", i, matches)
      // do like the shell, and spit out the literal glob
      if (!matches) {
        if (this.nonull) {
          var literal = this.minimatch.globSet[i]
          if (nou) all.push(literal)
          else nou[literal] = true
        }
      } else {
        // had matches
        var m = Object.keys(matches)
        if (nou) all.push.apply(all, m)
        else m.forEach(function (m) {
          all[m] = true
        })
      }
    }
  
    if (!nou) all = Object.keys(all)
  
    if (!this.nosort) {
      all = all.sort(this.nocase ? alphasorti : alphasort)
    }
  
    if (this.mark) {
      // at *some* point we statted all of these
      all = all.map(function (m) {
        var sc = this.statCache[m]
        if (!sc) return m
        if (m.slice(-1) !== "/" && (Array.isArray(sc) || sc === 2)) {
          return m + "/"
        }
        if (m.slice(-1) === "/") {
          return m.replace(/\/$/, "")
        }
        return m
      }, this)
    }
  
    if (this.debug) console.error("emitting end", all)
  
    EOF = this.found = all
    this.emitMatch(EOF)
  }
  
  function alphasorti (a, b) {
    a = a.toLowerCase()
    b = b.toLowerCase()
    return alphasort(a, b)
  }
  
  function alphasort (a, b) {
    return a > b ? 1 : a < b ? -1 : 0
  }
  
  Glob.prototype.abort = function () {
    this.aborted = true
    this.emit("abort")
  }
  
  Glob.prototype.pause = function () {
    if (this.paused) return
    if (this.sync)
      this.emit("error", new Error("Can't pause/resume sync glob"))
    this.paused = true
    this.emit("pause")
  }
  
  Glob.prototype.resume = function () {
    if (!this.paused) return
    if (this.sync)
      this.emit("error", new Error("Can't pause/resume sync glob"))
    this.paused = false
    this.emit("resume")
  }
  
  
  Glob.prototype.emitMatch = function (m) {
    if (!this.paused) {
      this.emit(m === EOF ? "end" : "match", m)
      return
    }
  
    if (!this._emitQueue) {
      this._emitQueue = []
      this.once("resume", function () {
        var q = this._emitQueue
        this._emitQueue = null
        q.forEach(function (m) {
          this.emitMatch(m)
        }, this)
      })
    }
  
    this._emitQueue.push(m)
  
    //this.once("resume", this.emitMatch.bind(this, m))
  }
  
  
  
  Glob.prototype._process = function (pattern, depth, index, cb_) {
    assert(this instanceof Glob)
  
    var cb = function cb (er, res) {
      assert(this instanceof Glob)
      if (this.paused) {
        if (!this._processQueue) {
          this._processQueue = []
          this.once("resume", function () {
            var q = this._processQueue
            this._processQueue = null
            q.forEach(function (cb) { cb() })
          })
        }
        this._processQueue.push(cb_.bind(this, er, res))
      } else {
        cb_.call(this, er, res)
      }
    }.bind(this)
  
    if (this.aborted) return cb()
  
    if (depth > this.maxDepth) return cb()
  
    // Get the first [n] parts of pattern that are all strings.
    var n = 0
    while (typeof pattern[n] === "string") {
      n ++
    }
    // now n is the index of the first one that is *not* a string.
  
    // see if there's anything else
    var prefix
    switch (n) {
      // if not, then this is rather simple
      case pattern.length:
        prefix = pattern.join("/")
        this._stat(prefix, function (exists, isDir) {
          // either it's there, or it isn't.
          // nothing more to do, either way.
          if (exists) {
            if (prefix.charAt(0) === "/" && !this.nomount) {
              prefix = path.join(this.root, prefix)
            }
            this.matches[index] = this.matches[index] || {}
            this.matches[index][prefix] = true
            this.emitMatch(prefix)
          }
          return cb()
        })
        return
  
      case 0:
        // pattern *starts* with some non-trivial item.
        // going to readdir(cwd), but not include the prefix in matches.
        prefix = null
        break
  
      default:
        // pattern has some string bits in the front.
        // whatever it starts with, whether that's "absolute" like /foo/bar,
        // or "relative" like "../baz"
        prefix = pattern.slice(0, n)
        prefix = prefix.join("/")
        break
    }
  
    // get the list of entries.
    var read
    if (prefix === null) read = "."
    else if (isAbsolute(prefix)) {
      read = prefix = path.join("/", prefix)
      if (this.debug) console.error('absolute: ', prefix, this.root, pattern)
    } else read = prefix
  
    if (this.debug) console.error('readdir(%j)', read, this.cwd, this.root)
    return this._readdir(read, function (er, entries) {
      if (er) {
        // not a directory!
        // this means that, whatever else comes after this, it can never match
        return cb()
      }
  
      // globstar is special
      if (pattern[n] === minimatch.GLOBSTAR) {
        // test without the globstar, and with every child both below
        // and replacing the globstar.
        var s = [ pattern.slice(0, n).concat(pattern.slice(n + 1)) ]
        entries.forEach(function (e) {
          if (e.charAt(0) === "." && !this.dot) return
          // instead of the globstar
          s.push(pattern.slice(0, n).concat(e).concat(pattern.slice(n + 1)))
          // below the globstar
          s.push(pattern.slice(0, n).concat(e).concat(pattern.slice(n)))
        }, this)
  
        // now asyncForEach over this
        var l = s.length
        , errState = null
        s.forEach(function (gsPattern) {
          this._process(gsPattern, depth + 1, index, function (er) {
            if (errState) return
            if (er) return cb(errState = er)
            if (--l <= 0) return cb()
          })
        }, this)
  
        return
      }
  
      // not a globstar
      // It will only match dot entries if it starts with a dot, or if
      // dot is set.  Stuff like @(.foo|.bar) isn't allowed.
      var pn = pattern[n]
      if (typeof pn === "string") {
        var found = entries.indexOf(pn) !== -1
        entries = found ? entries[pn] : []
      } else {
        var rawGlob = pattern[n]._glob
        , dotOk = this.dot || rawGlob.charAt(0) === "."
  
        entries = entries.filter(function (e) {
          return (e.charAt(0) !== "." || dotOk) &&
                 (typeof pattern[n] === "string" && e === pattern[n] ||
                  e.match(pattern[n]))
        })
      }
  
      // If n === pattern.length - 1, then there's no need for the extra stat
      // *unless* the user has specified "mark" or "stat" explicitly.
      // We know that they exist, since the readdir returned them.
      if (n === pattern.length - 1 &&
          !this.mark &&
          !this.stat) {
        entries.forEach(function (e) {
          if (prefix) {
            if (prefix !== "/") e = prefix + "/" + e
            else e = prefix + e
          }
          if (e.charAt(0) === "/" && !this.nomount) {
            e = path.join(this.root, e)
          }
  
          this.matches[index] = this.matches[index] || {}
          this.matches[index][e] = true
          this.emitMatch(e)
        }, this)
        return cb.call(this)
      }
  
  
      // now test all the remaining entries as stand-ins for that part
      // of the pattern.
      var l = entries.length
      , errState = null
      if (l === 0) return cb() // no matches possible
      entries.forEach(function (e) {
        var p = pattern.slice(0, n).concat(e).concat(pattern.slice(n + 1))
        this._process(p, depth + 1, index, function (er) {
          if (errState) return
          if (er) return cb(errState = er)
          if (--l === 0) return cb.call(this)
        })
      }, this)
    })
  
  }
  
  Glob.prototype._stat = function (f, cb) {
    assert(this instanceof Glob)
    var abs = f
    if (f.charAt(0) === "/") {
      abs = path.join(this.root, f)
    } else if (this.changedCwd) {
      abs = path.resolve(this.cwd, f)
    }
    if (this.debug) console.error('stat', [this.cwd, f, '=', abs])
    if (f.length > this.maxLength) {
      var er = new Error("Path name too long")
      er.code = "ENAMETOOLONG"
      er.path = f
      return this._afterStat(f, abs, cb, er)
    }
  
    if (this.statCache.hasOwnProperty(f)) {
      var exists = this.statCache[f]
      , isDir = exists && (Array.isArray(exists) || exists === 2)
      if (this.sync) return cb.call(this, !!exists, isDir)
      return process.nextTick(cb.bind(this, !!exists, isDir))
    }
  
    if (this.sync) {
      var er, stat
      try {
        stat = fs.statSync(abs)
      } catch (e) {
        er = e
      }
      this._afterStat(f, abs, cb, er, stat)
    } else {
      fs.stat(abs, this._afterStat.bind(this, f, abs, cb))
    }
  }
  
  Glob.prototype._afterStat = function (f, abs, cb, er, stat) {
    var exists
    assert(this instanceof Glob)
    if (er || !stat) {
      exists = false
    } else {
      exists = stat.isDirectory() ? 2 : 1
    }
    this.statCache[f] = this.statCache[f] || exists
    cb.call(this, !!exists, exists === 2)
  }
  
  Glob.prototype._readdir = function (f, cb) {
    assert(this instanceof Glob)
    var abs = f
    if (f.charAt(0) === "/") {
      abs = path.join(this.root, f)
    } else if (isAbsolute(f)) {
      abs = f
    } else if (this.changedCwd) {
      abs = path.resolve(this.cwd, f)
    }
  
    if (this.debug) console.error('readdir', [this.cwd, f, abs])
    if (f.length > this.maxLength) {
      var er = new Error("Path name too long")
      er.code = "ENAMETOOLONG"
      er.path = f
      return this._afterReaddir(f, abs, cb, er)
    }
  
    if (this.statCache.hasOwnProperty(f)) {
      var c = this.statCache[f]
      if (Array.isArray(c)) {
        if (this.sync) return cb.call(this, null, c)
        return process.nextTick(cb.bind(this, null, c))
      }
  
      if (!c || c === 1) {
        // either ENOENT or ENOTDIR
        var code = c ? "ENOTDIR" : "ENOENT"
        , er = new Error((c ? "Not a directory" : "Not found") + ": " + f)
        er.path = f
        er.code = code
        if (this.debug) console.error(f, er)
        if (this.sync) return cb.call(this, er)
        return process.nextTick(cb.bind(this, er))
      }
  
      // at this point, c === 2, meaning it's a dir, but we haven't
      // had to read it yet, or c === true, meaning it's *something*
      // but we don't have any idea what.  Need to read it, either way.
    }
  
    if (this.sync) {
      var er, entries
      try {
        entries = fs.readdirSync(abs)
      } catch (e) {
        er = e
      }
      return this._afterReaddir(f, abs, cb, er, entries)
    }
  
    fs.readdir(abs, this._afterReaddir.bind(this, f, abs, cb))
  }
  
  Glob.prototype._afterReaddir = function (f, abs, cb, er, entries) {
    assert(this instanceof Glob)
    if (entries && !er) {
      this.statCache[f] = entries
      // if we haven't asked to stat everything for suresies, then just
      // assume that everything in there exists, so we can avoid
      // having to stat it a second time.  This also gets us one step
      // further into ELOOP territory.
      if (!this.mark && !this.stat) {
        entries.forEach(function (e) {
          if (f === "/") e = f + e
          else e = f + "/" + e
          this.statCache[e] = true
        }, this)
      }
  
      return cb.call(this, er, entries)
    }
  
    // now handle errors, and cache the information
    if (er) switch (er.code) {
      case "ENOTDIR": // totally normal. means it *does* exist.
        this.statCache[f] = 1
        return cb.call(this, er)
      case "ENOENT": // not terribly unusual
      case "ELOOP":
      case "ENAMETOOLONG":
      case "UNKNOWN":
        this.statCache[f] = false
        return cb.call(this, er)
      default: // some unusual error.  Treat as failure.
        this.statCache[f] = false
        if (this.strict) this.emit("error", er)
        if (!this.silent) console.error("glob error", er)
        return cb.call(this, er)
    }
  }
  
  var isAbsolute = process.platform === "win32" ? absWin : absUnix
  
  function absWin (p) {
    if (absUnix(p)) return true
    // pull off the device/UNC bit from a windows path.
    // from node's lib/path.js
    var splitDeviceRe =
          /^([a-zA-Z]:|[\\\/]{2}[^\\\/]+[\\\/][^\\\/]+)?([\\\/])?/
      , result = splitDeviceRe.exec(p)
      , device = result[1] || ''
      , isUnc = device && device.charAt(1) !== ':'
      , isAbsolute = !!result[2] || isUnc // UNC paths are always absolute
  
    return isAbsolute
  }
  
  function absUnix (p) {
    return p.charAt(0) === "/" || p === ""
  }
  

  provide("glob", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  ;(function (require, exports, module, platform) {
  
  if (module) module.exports = minimatch
  else exports.minimatch = minimatch
  
  if (!require) {
    require = function (id) {
      switch (id) {
        case "path": return { basename: function (f) {
          f = f.split(/[\/\\]/)
          var e = f.pop()
          if (!e) e = f.pop()
          return e
        }}
        case "lru-cache": return function LRUCache () {
          // not quite an LRU, but still space-limited.
          var cache = {}
          var cnt = 0
          this.set = function (k, v) {
            cnt ++
            if (cnt >= 100) cache = {}
            cache[k] = v
          }
          this.get = function (k) { return cache[k] }
        }
      }
    }
  }
  
  minimatch.Minimatch = Minimatch
  
  var LRU = require("lru-cache")
    , cache = minimatch.cache = new LRU(100)
    , GLOBSTAR = minimatch.GLOBSTAR = Minimatch.GLOBSTAR = {}
  
  var path = require("path")
    // any single thing other than /
    // don't need to escape / when using new RegExp()
    , qmark = "[^/]"
  
    // * => any number of characters
    , star = qmark + "*?"
  
    // ** when dots are allowed.  Anything goes, except .. and .
    // not (^ or / followed by one or two dots followed by $ or /),
    // followed by anything, any number of times.
    , twoStarDot = "(?:(?!(?:\\\/|^)(?:\\.{1,2})($|\\\/)).)*?"
  
    // not a ^ or / followed by a dot,
    // followed by anything, any number of times.
    , twoStarNoDot = "(?:(?!(?:\\\/|^)\\.).)*?"
  
    // characters that need to be escaped in RegExp.
    , reSpecials = charSet("().*{}+?[]^$\\!")
  
  // "abc" -> { a:true, b:true, c:true }
  function charSet (s) {
    return s.split("").reduce(function (set, c) {
      set[c] = true
      return set
    }, {})
  }
  
  // normalizes slashes.
  var slashSplit = /\/+/
  
  minimatch.monkeyPatch = monkeyPatch
  function monkeyPatch () {
    var desc = Object.getOwnPropertyDescriptor(String.prototype, "match")
    var orig = desc.value
    desc.value = function (p) {
      if (p instanceof Minimatch) return p.match(this)
      return orig.call(this, p)
    }
    Object.defineProperty(String.prototype, desc)
  }
  
  minimatch.filter = filter
  function filter (pattern, options) {
    options = options || {}
    return function (p, i, list) {
      return minimatch(p, pattern, options)
    }
  }
  
  function ext (a, b) {
    a = a || {}
    b = b || {}
    var t = {}
    Object.keys(b).forEach(function (k) {
      t[k] = b[k]
    })
    Object.keys(a).forEach(function (k) {
      t[k] = a[k]
    })
    return t
  }
  
  minimatch.defaults = function (def) {
    if (!def || !Object.keys(def).length) return minimatch
  
    var orig = minimatch
  
    var m = function minimatch (p, pattern, options) {
      return orig.minimatch(p, pattern, ext(def, options))
    }
  
    m.Minimatch = function Minimatch (pattern, options) {
      return new orig.Minimatch(pattern, ext(def, options))
    }
  
    return m
  }
  
  Minimatch.defaults = function (def) {
    if (!def || !Object.keys(def).length) return Minimatch
    return minimatch.defaults(def).Minimatch
  }
  
  
  function minimatch (p, pattern, options) {
    if (typeof pattern !== "string") {
      throw new TypeError("glob pattern string required")
    }
  
    if (!options) options = {}
  
    // shortcut: comments match nothing.
    if (!options.nocomment && pattern.charAt(0) === "#") {
      return false
    }
  
    // "" only matches ""
    if (pattern.trim() === "") return p === ""
  
    return new Minimatch(pattern, options).match(p)
  }
  
  function Minimatch (pattern, options) {
    if (!(this instanceof Minimatch)) {
      return new Minimatch(pattern, options, cache)
    }
  
    if (typeof pattern !== "string") {
      throw new TypeError("glob pattern string required")
    }
  
    if (!options) options = {}
    pattern = pattern.trim()
  
    // lru storage.
    // these things aren't particularly big, but walking down the string
    // and turning it into a regexp can get pretty costly.
    var cacheKey = pattern + "\n" + Object.keys(options).filter(function (k) {
      return options[k]
    }).join(":")
    var cached = minimatch.cache.get(cacheKey)
    if (cached) return cached
    minimatch.cache.set(cacheKey, this)
  
    this.options = options
    this.set = []
    this.pattern = pattern
    this.regexp = null
    this.negate = false
    this.comment = false
    this.empty = false
  
    // make the set of regexps etc.
    this.make()
  }
  
  Minimatch.prototype.make = make
  function make () {
    // don't do it more than once.
    if (this._made) return
  
    var pattern = this.pattern
    var options = this.options
  
    // empty patterns and comments match nothing.
    if (!options.nocomment && pattern.charAt(0) === "#") {
      this.comment = true
      return
    }
    if (!pattern) {
      this.empty = true
      return
    }
  
    // step 1: figure out negation, etc.
    this.parseNegate()
  
    // step 2: expand braces
    var set = this.globSet = this.braceExpand()
  
    if (options.debug) console.error(this.pattern, set)
  
    // step 3: now we have a set, so turn each one into a series of path-portion
    // matching patterns.
    // These will be regexps, except in the case of "**", which is
    // set to the GLOBSTAR object for globstar behavior,
    // and will not contain any / characters
    set = this.globParts = set.map(function (s) {
      return s.split(slashSplit)
    })
  
    if (options.debug) console.error(this.pattern, set)
  
    // glob --> regexps
    set = set.map(function (s, si, set) {
      return s.map(this.parse, this)
    }, this)
  
    if (options.debug) console.error(this.pattern, set)
  
    // filter out everything that didn't compile properly.
    set = set.filter(function (s) {
      return -1 === s.indexOf(false)
    })
  
    if (options.debug) console.error(this.pattern, set)
  
    this.set = set
  }
  
  Minimatch.prototype.parseNegate = parseNegate
  function parseNegate () {
    var pattern = this.pattern
      , negate = false
      , options = this.options
      , negateOffset = 0
  
    if (options.nonegate) return
  
    for ( var i = 0, l = pattern.length
        ; i < l && pattern.charAt(i) === "!"
        ; i ++) {
      negate = !negate
      negateOffset ++
    }
  
    if (negateOffset) this.pattern = pattern.substr(negateOffset)
    this.negate = negate
  }
  
  // Brace expansion:
  // a{b,c}d -> abd acd
  // a{b,}c -> abc ac
  // a{0..3}d -> a0d a1d a2d a3d
  // a{b,c{d,e}f}g -> abg acdfg acefg
  // a{b,c}d{e,f}g -> abdeg acdeg abdeg abdfg
  //
  // Invalid sets are not expanded.
  // a{2..}b -> a{2..}b
  // a{b}c -> a{b}c
  minimatch.braceExpand = function (pattern, options) {
    return new Minimatch(pattern, options).braceExpand()
  }
  
  Minimatch.prototype.braceExpand = braceExpand
  function braceExpand (pattern, options) {
    options = options || this.options
    pattern = typeof pattern === "undefined"
      ? this.pattern : pattern
  
    if (typeof pattern === "undefined") {
      throw new Error("undefined pattern")
    }
  
    if (options.nobrace ||
        !pattern.match(/\{.*\}/)) {
      // shortcut. no need to expand.
      return [pattern]
    }
  
    var escaping = false
  
    // examples and comments refer to this crazy pattern:
    // a{b,c{d,e},{f,g}h}x{y,z}
    // expected:
    // abxy
    // abxz
    // acdxy
    // acdxz
    // acexy
    // acexz
    // afhxy
    // afhxz
    // aghxy
    // aghxz
  
    // everything before the first \{ is just a prefix.
    // So, we pluck that off, and work with the rest,
    // and then prepend it to everything we find.
    if (pattern.charAt(0) !== "{") {
      // console.error(pattern)
      var prefix = null
      for (var i = 0, l = pattern.length; i < l; i ++) {
        var c = pattern.charAt(i)
        // console.error(i, c)
        if (c === "\\") {
          escaping = !escaping
        } else if (c === "{" && !escaping) {
          prefix = pattern.substr(0, i)
          break
        }
      }
  
      // actually no sets, all { were escaped.
      if (prefix === null) {
        // console.error("no sets")
        return [pattern]
      }
  
      var tail = braceExpand(pattern.substr(i), options)
      return tail.map(function (t) {
        return prefix + t
      })
    }
  
    // now we have something like:
    // {b,c{d,e},{f,g}h}x{y,z}
    // walk through the set, expanding each part, until
    // the set ends.  then, we'll expand the suffix.
    // If the set only has a single member, then'll put the {} back
  
    // first, handle numeric sets, since they're easier
    var numset = pattern.match(/^\{(-?[0-9]+)\.\.(-?[0-9]+)\}/)
    if (numset) {
      // console.error("numset", numset[1], numset[2])
      var suf = braceExpand(pattern.substr(numset[0].length), options)
        , start = +numset[1]
        , end = +numset[2]
        , inc = start > end ? -1 : 1
        , set = []
      for (var i = start; i != (end + inc); i += inc) {
        // append all the suffixes
        for (var ii = 0, ll = suf.length; ii < ll; ii ++) {
          set.push(i + suf[ii])
        }
      }
      return set
    }
  
    // ok, walk through the set
    // We hope, somewhat optimistically, that there
    // will be a } at the end.
    // If the closing brace isn't found, then the pattern is
    // interpreted as braceExpand("\\" + pattern) so that
    // the leading \{ will be interpreted literally.
    var i = 1 // skip the \{
      , depth = 1
      , set = []
      , member = ""
      , sawEnd = false
      , escaping = false
  
    function addMember () {
      set.push(member)
      member = ""
    }
  
    // console.error("Entering for")
    FOR: for (i = 1, l = pattern.length; i < l; i ++) {
      var c = pattern.charAt(i)
      // console.error("", i, c)
  
      if (escaping) {
        escaping = false
        member += "\\" + c
      } else {
        switch (c) {
          case "\\":
            escaping = true
            continue
  
          case "{":
            depth ++
            member += "{"
            continue
  
          case "}":
            depth --
            // if this closes the actual set, then we're done
            if (depth === 0) {
              addMember()
              // pluck off the close-brace
              i ++
              break FOR
            } else {
              member += c
              continue
            }
  
          case ",":
            if (depth === 1) {
              addMember()
            } else {
              member += c
            }
            continue
  
          default:
            member += c
            continue
        } // switch
      } // else
    } // for
  
    // now we've either finished the set, and the suffix is
    // pattern.substr(i), or we have *not* closed the set,
    // and need to escape the leading brace
    if (depth !== 0) {
      // console.error("didn't close", pattern)
      return braceExpand("\\" + pattern, options)
    }
  
    // x{y,z} -> ["xy", "xz"]
    // console.error("set", set)
    // console.error("suffix", pattern.substr(i))
    var suf = braceExpand(pattern.substr(i), options)
    // ["b", "c{d,e}","{f,g}h"] ->
    //   [["b"], ["cd", "ce"], ["fh", "gh"]]
    var addBraces = set.length === 1
    // console.error("set pre-expanded", set)
    set = set.map(function (p) {
      return braceExpand(p, options)
    })
    // console.error("set expanded", set)
  
  
    // [["b"], ["cd", "ce"], ["fh", "gh"]] ->
    //   ["b", "cd", "ce", "fh", "gh"]
    set = set.reduce(function (l, r) {
      return l.concat(r)
    })
  
    if (addBraces) {
      set = set.map(function (s) {
        return "{" + s + "}"
      })
    }
  
    // now attach the suffixes.
    var ret = []
    for (var i = 0, l = set.length; i < l; i ++) {
      for (var ii = 0, ll = suf.length; ii < ll; ii ++) {
        ret.push(set[i] + suf[ii])
      }
    }
    return ret
  }
  
  // parse a component of the expanded set.
  // At this point, no pattern may contain "/" in it
  // so we're going to return a 2d array, where each entry is the full
  // pattern, split on '/', and then turned into a regular expression.
  // A regexp is made at the end which joins each array with an
  // escaped /, and another full one which joins each regexp with |.
  //
  // Following the lead of Bash 4.1, note that "**" only has special meaning
  // when it is the *only* thing in a path portion.  Otherwise, any series
  // of * is equivalent to a single *.  Globstar behavior is enabled by
  // default, and can be disabled by setting options.noglobstar.
  Minimatch.prototype.parse = parse
  var SUBPARSE = {}
  function parse (pattern, isSub) {
    var options = this.options
  
    // shortcuts
    if (!options.noglobstar && pattern === "**") return GLOBSTAR
    if (pattern === "") return ""
  
    var re = ""
      , hasMagic = false
      , escaping = false
      // ? => one single character
      , patternListStack = []
      , plType
      , stateChar
      , inClass = false
      , reClassStart = -1
      , classStart = -1
      // . and .. never match anything that doesn't start with .,
      // even when options.dot is set.
      , patternStart = pattern.charAt(0) === "." ? "" // anything
        // not (start or / followed by . or .. followed by / or end)
        : options.dot ? "(?!(?:^|\\\/)\\.{1,2}(?:$|\\\/))"
        : "(?!\\.)"
  
    function clearStateChar () {
      if (stateChar) {
        // we had some state-tracking character
        // that wasn't consumed by this pass.
        switch (stateChar) {
          case "*":
            re += star
            hasMagic = true
            break
          case "?":
            re += qmark
            hasMagic = true
            break
          default:
            re += "\\"+stateChar
            break
        }
        stateChar = false
      }
    }
  
    for ( var i = 0, len = pattern.length, c
        ; (i < len) && (c = pattern.charAt(i))
        ; i ++ ) {
  
      if (options.debug) {
        console.error("%s\t%s %s %j", pattern, i, re, c)
      }
  
      // skip over any that are escaped.
      if (escaping && reSpecials[c]) {
        re += "\\" + c
        escaping = false
        continue
      }
  
      SWITCH: switch (c) {
        case "/":
          // completely not allowed, even escaped.
          // Should already be path-split by now.
          return false
  
        case "\\":
          clearStateChar()
          escaping = true
          continue
  
        // the various stateChar values
        // for the "extglob" stuff.
        case "?":
        case "*":
        case "+":
        case "@":
        case "!":
          if (options.debug) {
            console.error("%s\t%s %s %j <-- stateChar", pattern, i, re, c)
          }
  
          // all of those are literals inside a class, except that
          // the glob [!a] means [^a] in regexp
          if (inClass) {
            if (c === "!" && i === classStart + 1) c = "^"
            re += c
            continue
          }
  
          // if we already have a stateChar, then it means
          // that there was something like ** or +? in there.
          // Handle the stateChar, then proceed with this one.
          clearStateChar()
          stateChar = c
          // if extglob is disabled, then +(asdf|foo) isn't a thing.
          // just clear the statechar *now*, rather than even diving into
          // the patternList stuff.
          if (options.noext) clearStateChar()
          continue
  
        case "(":
          if (inClass) {
            re += "("
            continue
          }
  
          if (!stateChar) {
            re += "\\("
            continue
          }
  
          plType = stateChar
          patternListStack.push({ type: plType
                                , start: i - 1
                                , reStart: re.length })
          // negation is (?:(?!js)[^/]*)
          re += stateChar === "!" ? "(?:(?!" : "(?:"
          stateChar = false
          continue
  
        case ")":
          if (inClass || !patternListStack.length) {
            re += "\\)"
            continue
          }
  
          hasMagic = true
          re += ")"
          plType = patternListStack.pop().type
          // negation is (?:(?!js)[^/]*)
          // The others are (?:<pattern>)<type>
          switch (plType) {
            case "!":
              re += "[^/]*?)"
              break
            case "?":
            case "+":
            case "*": re += plType
            case "@": break // the default anyway
          }
          continue
  
        case "|":
          if (inClass || !patternListStack.length || escaping) {
            re += "\\|"
            escaping = false
            continue
          }
  
          re += "|"
          continue
  
        // these are mostly the same in regexp and glob
        case "[":
          // swallow any state-tracking char before the [
          clearStateChar()
  
          if (inClass) {
            re += "\\" + c
            continue
          }
  
          inClass = true
          classStart = i
          reClassStart = re.length
          re += c
          continue
  
        case "]":
          //  a right bracket shall lose its special
          //  meaning and represent itself in
          //  a bracket expression if it occurs
          //  first in the list.  -- POSIX.2 2.8.3.2
          if (i === classStart + 1 || !inClass) {
            re += "\\" + c
            escaping = false
            continue
          }
  
          // finish up the class.
          hasMagic = true
          inClass = false
          re += c
          continue
  
        default:
          // swallow any state char that wasn't consumed
          clearStateChar()
  
          if (escaping) {
            // no need
            escaping = false
          } else if (reSpecials[c]
                     && !(c === "^" && inClass)) {
            re += "\\"
          }
  
          re += c
  
      } // switch
    } // for
  
  
    // handle the case where we left a class open.
    // "[abc" is valid, equivalent to "\[abc"
    if (inClass) {
      // split where the last [ was, and escape it
      // this is a huge pita.  We now have to re-walk
      // the contents of the would-be class to re-translate
      // any characters that were passed through as-is
      var cs = pattern.substr(classStart + 1)
        , sp = this.parse(cs, SUBPARSE)
      re = re.substr(0, reClassStart) + "\\[" + sp[0]
      hasMagic = hasMagic || sp[1]
    }
  
    // handle the case where we had a +( thing at the *end*
    // of the pattern.
    // each pattern list stack adds 3 chars, and we need to go through
    // and escape any | chars that were passed through as-is for the regexp.
    // Go through and escape them, taking care not to double-escape any
    // | chars that were already escaped.
    var pl
    while (pl = patternListStack.pop()) {
      var tail = re.slice(pl.reStart + 3)
      // maybe some even number of \, then maybe 1 \, followed by a |
      tail = tail.replace(/((?:\\{2})*)(\\?)\|/g, function (_, $1, $2) {
        if (!$2) {
          // the | isn't already escaped, so escape it.
          $2 = "\\"
        }
  
        // need to escape all those slashes *again*, without escaping the
        // one that we need for escaping the | character.  As it works out,
        // escaping an even number of slashes can be done by simply repeating
        // it exactly after itself.  That's why this trick works.
        //
        // I am sorry that you have to see this.
        return $1 + $1 + $2 + "|"
      })
  
      // console.error("tail=%j\n   %s", tail, tail)
      var t = pl.type === "*" ? star
            : pl.type === "?" ? qmark
            : "\\" + pl.type
  
      hasMagic = true
      re = re.slice(0, pl.reStart)
         + t + "\\("
         + tail
    }
  
    // handle trailing things that only matter at the very end.
    clearStateChar()
    if (escaping) {
      // trailing \\
      re += "\\\\"
    }
  
    // only need to apply the nodot start if the re starts with
    // something that could conceivably capture a dot
    var addPatternStart = false
    switch (re.charAt(0)) {
      case ".":
      case "[":
      case "(": addPatternStart = true
    }
  
    // if the re is not "" at this point, then we need to make sure
    // it doesn't match against an empty path part.
    // Otherwise a/* will match a/, which it should not.
    if (re !== "" && hasMagic) re = "(?=.)" + re
  
    if (addPatternStart) re = patternStart + re
  
    // parsing just a piece of a larger pattern.
    if (isSub === SUBPARSE) {
      return [ re, hasMagic ]
    }
  
    // skip the regexp for non-magical patterns
    // unescape anything in it, though, so that it'll be
    // an exact match against a file etc.
    if (!hasMagic) {
      return globUnescape(pattern)
    }
  
    var flags = options.nocase ? "i" : ""
      , regExp = new RegExp("^" + re + "$", flags)
  
    regExp._glob = pattern
    regExp._src = re
  
    return regExp
  }
  
  minimatch.makeRe = function (pattern, options) {
    return new Minimatch(pattern, options || {}).makeRe()
  }
  
  Minimatch.prototype.makeRe = makeRe
  function makeRe () {
    if (this.regexp || this.regexp === false) return this.regexp
  
    // at this point, this.set is a 2d array of partial
    // pattern strings, or "**".
    //
    // It's better to use .match().  This function shouldn't
    // be used, really, but it's pretty convenient sometimes,
    // when you just want to work with a regex.
    var set = this.set
  
    if (!set.length) return this.regexp = false
    var options = this.options
  
    var twoStar = options.noglobstar ? star
        : options.dot ? twoStarDot
        : twoStarNoDot
      , flags = options.nocase ? "i" : ""
  
    var re = set.map(function (pattern) {
      return pattern.map(function (p) {
        return (p === GLOBSTAR) ? twoStar
             : (typeof p === "string") ? regExpEscape(p)
             : p._src
      }).join("\\\/")
    }).join("|")
  
    // must match entire pattern
    // ending in a * or ** will make it less strict.
    re = "^" + re + "$"
  
    // can match anything, as long as it's not this.
    if (this.negate) re = "^(?!" + re + ").*$"
  
    try {
      return this.regexp = new RegExp(re, flags)
    } catch (ex) {
      return this.regexp = false
    }
  }
  
  minimatch.match = function (list, pattern, options) {
    var mm = new Minimatch(pattern, options)
    list = list.filter(function (f) {
      return mm.match(f)
    })
    if (options.nonull && !list.length) {
      list.push(pattern)
    }
    return list
  }
  
  Minimatch.prototype.match = match
  function match (f, partial) {
    // console.error("match", f, this.pattern)
    // short-circuit in the case of busted things.
    // comments, etc.
    if (this.comment) return false
    if (this.empty) return f === ""
  
    if (f === "/" && partial) return true
  
    var options = this.options
  
    // windows: need to use /, not \
    // On other platforms, \ is a valid (albeit bad) filename char.
    if (platform === "win32") {
      f = f.split("\\").join("/")
    }
  
    // treat the test path as a set of pathparts.
    f = f.split(slashSplit)
    if (options.debug) {
      console.error(this.pattern, "split", f)
    }
  
    // just ONE of the pattern sets in this.set needs to match
    // in order for it to be valid.  If negating, then just one
    // match means that we have failed.
    // Either way, return on the first hit.
  
    var set = this.set
    // console.error(this.pattern, "set", set)
  
    for (var i = 0, l = set.length; i < l; i ++) {
      var pattern = set[i]
      var hit = this.matchOne(f, pattern, partial)
      if (hit) {
        if (options.flipNegate) return true
        return !this.negate
      }
    }
  
    // didn't get any hits.  this is success if it's a negative
    // pattern, failure otherwise.
    if (options.flipNegate) return false
    return this.negate
  }
  
  // set partial to true to test if, for example,
  // "/a/b" matches the start of "/*/b/*/d"
  // Partial means, if you run out of file before you run
  // out of pattern, then that's fine, as long as all
  // the parts match.
  Minimatch.prototype.matchOne = function (file, pattern, partial) {
    var options = this.options
  
    if (options.debug) {
      console.error("matchOne",
                    { "this": this
                    , file: file
                    , pattern: pattern })
    }
  
    if (options.matchBase && pattern.length === 1) {
      file = path.basename(file.join("/")).split("/")
    }
  
    if (options.debug) {
      console.error("matchOne", file.length, pattern.length)
    }
  
    for ( var fi = 0
            , pi = 0
            , fl = file.length
            , pl = pattern.length
        ; (fi < fl) && (pi < pl)
        ; fi ++, pi ++ ) {
  
      if (options.debug) {
        console.error("matchOne loop")
      }
      var p = pattern[pi]
        , f = file[fi]
  
      if (options.debug) {
        console.error(pattern, p, f)
      }
  
      // should be impossible.
      // some invalid regexp stuff in the set.
      if (p === false) return false
  
      if (p === GLOBSTAR) {
        // "**"
        // a/**/b/**/c would match the following:
        // a/b/x/y/z/c
        // a/x/y/z/b/c
        // a/b/x/b/x/c
        // a/b/c
        // To do this, take the rest of the pattern after
        // the **, and see if it would match the file remainder.
        // If so, return success.
        // If not, the ** "swallows" a segment, and try again.
        // This is recursively awful.
        // a/b/x/y/z/c
        // - a matches a
        // - doublestar
        //   - matchOne(b/x/y/z/c, b/**/c)
        //     - b matches b
        //     - doublestar
        //       - matchOne(x/y/z/c, c) -> no
        //       - matchOne(y/z/c, c) -> no
        //       - matchOne(z/c, c) -> no
        //       - matchOne(c, c) yes, hit
        var fr = fi
          , pr = pi + 1
        if (pr === pl) {
          // a ** at the end will just swallow the rest.
          // We have found a match.
          // however, it will not swallow /.x, unless
          // options.dot is set.
          // . and .. are *never* matched by **, for explosively
          // exponential reasons.
          for ( ; fi < fl; fi ++) {
            if (file[fi] === "." || file[fi] === ".." ||
                (!options.dot && file[fi].charAt(0) === ".")) return false
          }
          return true
        }
  
        // ok, let's see if we can swallow whatever we can.
        WHILE: while (fr < fl) {
          var swallowee = file[fr]
          if (swallowee === "." || swallowee === ".." ||
              (!options.dot && swallowee.charAt(0) === ".")) {
            // console.error("dot detected!")
            break WHILE
          }
  
          // XXX remove this slice.  Just pass the start index.
          if (this.matchOne(file.slice(fr), pattern.slice(pr), partial)) {
            // found a match.
            return true
          } else {
            // ** swallows a segment, and continue.
            fr ++
          }
        }
        // no match was found.
        // However, in partial mode, we can't say this is necessarily over.
        // If there's more *pattern* left, then 
        if (partial) {
          // ran out of file
          // console.error("\n>>> no match, partial?", file, fr, pattern, pr)
          if (fr === fl) return true
        }
        return false
      }
  
      // something other than **
      // non-magic patterns just have to match exactly
      // patterns with magic have been turned into regexps.
      var hit
      if (typeof p === "string") {
        if (options.nocase) {
          hit = f.toLowerCase() === p.toLowerCase()
        } else {
          hit = f === p
        }
        if (options.debug) {
          console.error("string match", p, f, hit)
        }
      } else {
        hit = f.match(p)
        if (options.debug) {
          console.error("pattern match", p, f, hit)
        }
      }
  
      if (!hit) return false
    }
  
    // Note: ending in / means that we'll get a final ""
    // at the end of the pattern.  This can only match a
    // corresponding "" at the end of the file.
    // If the file ends in /, then it can only match a
    // a pattern that ends in /, unless the pattern just
    // doesn't have any more for it. But, a/b/ should *not*
    // match "a/b/*", even though "" matches against the
    // [^/]*? pattern, except in partial mode, where it might
    // simply not be reached yet.
    // However, a/b/ should still satisfy a/*
  
    // now either we fell off the end of the pattern, or we're done.
    if (fi === fl && pi === pl) {
      // ran out of pattern and filename at the same time.
      // an exact hit!
      return true
    } else if (fi === fl) {
      // ran out of file, but still had pattern left.
      // this is ok if we're doing the match as part of
      // a glob fs traversal.
      return partial
    } else if (pi === pl) {
      // ran out of pattern, still have file left.
      // this is only acceptable if we're on the very last
      // empty segment of a file with a trailing slash.
      // a/* should match a/b/
      var emptyFileEnd = (fi === fl - 1) && (file[fi] === "")
      return emptyFileEnd
    }
  
    // should be unreachable.
    throw new Error("wtf?")
  }
  
  
  // replace stuff like \* with *
  function globUnescape (s) {
    return s.replace(/\\(.)/g, "$1")
  }
  
  
  function regExpEscape (s) {
    return s.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&")
  }
  
  })( typeof require === "function" ? require : null,
      this,
      typeof module === "object" ? module : null,
      typeof process === "object" ? process.platform : "win32"
    )
  

  provide("minimatch", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  // info about each config option.
  
  var debug = process.env.DEBUG_NOPT || process.env.NOPT_DEBUG
    ? function () { console.error.apply(console, arguments) }
    : function () {}
  
  var url = require("url")
    , path = require("path")
    , Stream = require("stream").Stream
    , abbrev = require("abbrev")
  
  module.exports = exports = nopt
  exports.clean = clean
  
  exports.typeDefs =
    { String  : { type: String,  validate: validateString  }
    , Boolean : { type: Boolean, validate: validateBoolean }
    , url     : { type: url,     validate: validateUrl     }
    , Number  : { type: Number,  validate: validateNumber  }
    , path    : { type: path,    validate: validatePath    }
    , Stream  : { type: Stream,  validate: validateStream  }
    , Date    : { type: Date,    validate: validateDate    }
    }
  
  function nopt (types, shorthands, args, slice) {
    args = args || process.argv
    types = types || {}
    shorthands = shorthands || {}
    if (typeof slice !== "number") slice = 2
  
    debug(types, shorthands, args, slice)
  
    args = args.slice(slice)
    var data = {}
      , key
      , remain = []
      , cooked = args
      , original = args.slice(0)
  
    parse(args, data, remain, types, shorthands)
    // now data is full
    clean(data, types, exports.typeDefs)
    data.argv = {remain:remain,cooked:cooked,original:original}
    data.argv.toString = function () {
      return this.original.map(JSON.stringify).join(" ")
    }
    return data
  }
  
  function clean (data, types, typeDefs) {
    typeDefs = typeDefs || exports.typeDefs
    var remove = {}
      , typeDefault = [false, true, null, String, Number]
  
    Object.keys(data).forEach(function (k) {
      if (k === "argv") return
      var val = data[k]
        , isArray = Array.isArray(val)
        , type = types[k]
      if (!isArray) val = [val]
      if (!type) type = typeDefault
      if (type === Array) type = typeDefault.concat(Array)
      if (!Array.isArray(type)) type = [type]
  
      debug("val=%j", val)
      debug("types=", type)
      val = val.map(function (val) {
        // if it's an unknown value, then parse false/true/null/numbers/dates
        if (typeof val === "string") {
          debug("string %j", val)
          val = val.trim()
          if ((val === "null" && ~type.indexOf(null))
              || (val === "true" &&
                 (~type.indexOf(true) || ~type.indexOf(Boolean)))
              || (val === "false" &&
                 (~type.indexOf(false) || ~type.indexOf(Boolean)))) {
            val = JSON.parse(val)
            debug("jsonable %j", val)
          } else if (~type.indexOf(Number) && !isNaN(val)) {
            debug("convert to number", val)
            val = +val
          } else if (~type.indexOf(Date) && !isNaN(Date.parse(val))) {
            debug("convert to date", val)
            val = new Date(val)
          }
        }
  
        if (!types.hasOwnProperty(k)) {
          return val
        }
  
        // allow `--no-blah` to set 'blah' to null if null is allowed
        if (val === false && ~type.indexOf(null) &&
            !(~type.indexOf(false) || ~type.indexOf(Boolean))) {
          val = null
        }
  
        var d = {}
        d[k] = val
        debug("prevalidated val", d, val, types[k])
        if (!validate(d, k, val, types[k], typeDefs)) {
          if (exports.invalidHandler) {
            exports.invalidHandler(k, val, types[k], data)
          } else if (exports.invalidHandler !== false) {
            debug("invalid: "+k+"="+val, types[k])
          }
          return remove
        }
        debug("validated val", d, val, types[k])
        return d[k]
      }).filter(function (val) { return val !== remove })
  
      if (!val.length) delete data[k]
      else if (isArray) {
        debug(isArray, data[k], val)
        data[k] = val
      } else data[k] = val[0]
  
      debug("k=%s val=%j", k, val, data[k])
    })
  }
  
  function validateString (data, k, val) {
    data[k] = String(val)
  }
  
  function validatePath (data, k, val) {
    data[k] = path.resolve(String(val))
    return true
  }
  
  function validateNumber (data, k, val) {
    debug("validate Number %j %j %j", k, val, isNaN(val))
    if (isNaN(val)) return false
    data[k] = +val
  }
  
  function validateDate (data, k, val) {
    debug("validate Date %j %j %j", k, val, Date.parse(val))
    var s = Date.parse(val)
    if (isNaN(s)) return false
    data[k] = new Date(val)
  }
  
  function validateBoolean (data, k, val) {
    if (val instanceof Boolean) val = val.valueOf()
    else if (typeof val === "string") {
      if (!isNaN(val)) val = !!(+val)
      else if (val === "null" || val === "false") val = false
      else val = true
    } else val = !!val
    data[k] = val
  }
  
  function validateUrl (data, k, val) {
    val = url.parse(String(val))
    if (!val.host) return false
    data[k] = val.href
  }
  
  function validateStream (data, k, val) {
    if (!(val instanceof Stream)) return false
    data[k] = val
  }
  
  function validate (data, k, val, type, typeDefs) {
    // arrays are lists of types.
    if (Array.isArray(type)) {
      for (var i = 0, l = type.length; i < l; i ++) {
        if (type[i] === Array) continue
        if (validate(data, k, val, type[i], typeDefs)) return true
      }
      delete data[k]
      return false
    }
  
    // an array of anything?
    if (type === Array) return true
  
    // NaN is poisonous.  Means that something is not allowed.
    if (type !== type) {
      debug("Poison NaN", k, val, type)
      delete data[k]
      return false
    }
  
    // explicit list of values
    if (val === type) {
      debug("Explicitly allowed %j", val)
      // if (isArray) (data[k] = data[k] || []).push(val)
      // else data[k] = val
      data[k] = val
      return true
    }
  
    // now go through the list of typeDefs, validate against each one.
    var ok = false
      , types = Object.keys(typeDefs)
    for (var i = 0, l = types.length; i < l; i ++) {
      debug("test type %j %j %j", k, val, types[i])
      var t = typeDefs[types[i]]
      if (t && type === t.type) {
        var d = {}
        ok = false !== t.validate(d, k, val)
        val = d[k]
        if (ok) {
          // if (isArray) (data[k] = data[k] || []).push(val)
          // else data[k] = val
          data[k] = val
          break
        }
      }
    }
    debug("OK? %j (%j %j %j)", ok, k, val, types[i])
  
    if (!ok) delete data[k]
    return ok
  }
  
  function parse (args, data, remain, types, shorthands) {
    debug("parse", args, data, remain)
  
    var key = null
      , abbrevs = abbrev(Object.keys(types))
      , shortAbbr = abbrev(Object.keys(shorthands))
  
    for (var i = 0; i < args.length; i ++) {
      var arg = args[i]
      debug("arg", arg)
  
      if (arg.match(/^-{2,}$/)) {
        // done with keys.
        // the rest are args.
        remain.push.apply(remain, args.slice(i + 1))
        args[i] = "--"
        break
      }
      if (arg.charAt(0) === "-") {
        if (arg.indexOf("=") !== -1) {
          var v = arg.split("=")
          arg = v.shift()
          v = v.join("=")
          args.splice.apply(args, [i, 1].concat([arg, v]))
        }
        // see if it's a shorthand
        // if so, splice and back up to re-parse it.
        var shRes = resolveShort(arg, shorthands, shortAbbr, abbrevs)
        debug("arg=%j shRes=%j", arg, shRes)
        if (shRes) {
          debug(arg, shRes)
          args.splice.apply(args, [i, 1].concat(shRes))
          if (arg !== shRes[0]) {
            i --
            continue
          }
        }
        arg = arg.replace(/^-+/, "")
        var no = false
        while (arg.toLowerCase().indexOf("no-") === 0) {
          no = !no
          arg = arg.substr(3)
        }
  
        if (abbrevs[arg]) arg = abbrevs[arg]
  
        var isArray = types[arg] === Array ||
          Array.isArray(types[arg]) && types[arg].indexOf(Array) !== -1
  
        var val
          , la = args[i + 1]
  
        var isBool = no ||
          types[arg] === Boolean ||
          Array.isArray(types[arg]) && types[arg].indexOf(Boolean) !== -1 ||
          (la === "false" &&
           (types[arg] === null ||
            Array.isArray(types[arg]) && ~types[arg].indexOf(null)))
  
        if (isBool) {
          // just set and move along
          val = !no
          // however, also support --bool true or --bool false
          if (la === "true" || la === "false") {
            val = JSON.parse(la)
            la = null
            if (no) val = !val
            i ++
          }
  
          // also support "foo":[Boolean, "bar"] and "--foo bar"
          if (Array.isArray(types[arg]) && la) {
            if (~types[arg].indexOf(la)) {
              // an explicit type
              val = la
              i ++
            } else if ( la === "null" && ~types[arg].indexOf(null) ) {
              // null allowed
              val = null
              i ++
            } else if ( !la.match(/^-{2,}[^-]/) &&
                        !isNaN(la) &&
                        ~types[arg].indexOf(Number) ) {
              // number
              val = +la
              i ++
            } else if ( !la.match(/^-[^-]/) && ~types[arg].indexOf(String) ) {
              // string
              val = la
              i ++
            }
          }
  
          if (isArray) (data[arg] = data[arg] || []).push(val)
          else data[arg] = val
  
          continue
        }
  
        if (la && la.match(/^-{2,}$/)) {
          la = undefined
          i --
        }
  
        val = la === undefined ? true : la
        if (isArray) (data[arg] = data[arg] || []).push(val)
        else data[arg] = val
  
        i ++
        continue
      }
      remain.push(arg)
    }
  }
  
  function resolveShort (arg, shorthands, shortAbbr, abbrevs) {
    // handle single-char shorthands glommed together, like
    // npm ls -glp, but only if there is one dash, and only if
    // all of the chars are single-char shorthands, and it's
    // not a match to some other abbrev.
    arg = arg.replace(/^-+/, '')
    if (abbrevs[arg] && !shorthands[arg]) {
      return null
    }
    if (shortAbbr[arg]) {
      arg = shortAbbr[arg]
    } else {
      var singles = shorthands.___singles
      if (!singles) {
        singles = Object.keys(shorthands).filter(function (s) {
          return s.length === 1
        }).reduce(function (l,r) { l[r] = true ; return l }, {})
        shorthands.___singles = singles
      }
      var chrs = arg.split("").filter(function (c) {
        return singles[c]
      })
      if (chrs.join("") === arg) return chrs.map(function (c) {
        return shorthands[c]
      }).reduce(function (l, r) {
        return l.concat(r)
      }, [])
    }
  
    if (shorthands[arg] && !Array.isArray(shorthands[arg])) {
      shorthands[arg] = shorthands[arg].split(/\s+/)
    }
    return shorthands[arg]
  }
  
  if (module === require.main) {
  var assert = require("assert")
    , util = require("util")
  
    , shorthands =
      { s : ["--loglevel", "silent"]
      , d : ["--loglevel", "info"]
      , dd : ["--loglevel", "verbose"]
      , ddd : ["--loglevel", "silly"]
      , noreg : ["--no-registry"]
      , reg : ["--registry"]
      , "no-reg" : ["--no-registry"]
      , silent : ["--loglevel", "silent"]
      , verbose : ["--loglevel", "verbose"]
      , h : ["--usage"]
      , H : ["--usage"]
      , "?" : ["--usage"]
      , help : ["--usage"]
      , v : ["--version"]
      , f : ["--force"]
      , desc : ["--description"]
      , "no-desc" : ["--no-description"]
      , "local" : ["--no-global"]
      , l : ["--long"]
      , p : ["--parseable"]
      , porcelain : ["--parseable"]
      , g : ["--global"]
      }
  
    , types =
      { aoa: Array
      , nullstream: [null, Stream]
      , date: Date
      , str: String
      , browser : String
      , cache : path
      , color : ["always", Boolean]
      , depth : Number
      , description : Boolean
      , dev : Boolean
      , editor : path
      , force : Boolean
      , global : Boolean
      , globalconfig : path
      , group : [String, Number]
      , gzipbin : String
      , logfd : [Number, Stream]
      , loglevel : ["silent","win","error","warn","info","verbose","silly"]
      , long : Boolean
      , "node-version" : [false, String]
      , npaturl : url
      , npat : Boolean
      , "onload-script" : [false, String]
      , outfd : [Number, Stream]
      , parseable : Boolean
      , pre: Boolean
      , prefix: path
      , proxy : url
      , "rebuild-bundle" : Boolean
      , registry : url
      , searchopts : String
      , searchexclude: [null, String]
      , shell : path
      , t: [Array, String]
      , tag : String
      , tar : String
      , tmp : path
      , "unsafe-perm" : Boolean
      , usage : Boolean
      , user : String
      , username : String
      , userconfig : path
      , version : Boolean
      , viewer: path
      , _exit : Boolean
      }
  
  ; [["-v", {version:true}, []]
    ,["---v", {version:true}, []]
    ,["ls -s --no-reg connect -d",
      {loglevel:"info",registry:null},["ls","connect"]]
    ,["ls ---s foo",{loglevel:"silent"},["ls","foo"]]
    ,["ls --registry blargle", {}, ["ls"]]
    ,["--no-registry", {registry:null}, []]
    ,["--no-color true", {color:false}, []]
    ,["--no-color false", {color:true}, []]
    ,["--no-color", {color:false}, []]
    ,["--color false", {color:false}, []]
    ,["--color --logfd 7", {logfd:7,color:true}, []]
    ,["--color=true", {color:true}, []]
    ,["--logfd=10", {logfd:10}, []]
    ,["--tmp=/tmp -tar=gtar",{tmp:"/tmp",tar:"gtar"},[]]
    ,["--tmp=tmp -tar=gtar",
      {tmp:path.resolve(process.cwd(), "tmp"),tar:"gtar"},[]]
    ,["--logfd x", {}, []]
    ,["a -true -- -no-false", {true:true},["a","-no-false"]]
    ,["a -no-false", {false:false},["a"]]
    ,["a -no-no-true", {true:true}, ["a"]]
    ,["a -no-no-no-false", {false:false}, ["a"]]
    ,["---NO-no-No-no-no-no-nO-no-no"+
      "-No-no-no-no-no-no-no-no-no"+
      "-no-no-no-no-NO-NO-no-no-no-no-no-no"+
      "-no-body-can-do-the-boogaloo-like-I-do"
     ,{"body-can-do-the-boogaloo-like-I-do":false}, []]
    ,["we are -no-strangers-to-love "+
      "--you-know the-rules --and so-do-i "+
      "---im-thinking-of=a-full-commitment "+
      "--no-you-would-get-this-from-any-other-guy "+
      "--no-gonna-give-you-up "+
      "-no-gonna-let-you-down=true "+
      "--no-no-gonna-run-around false "+
      "--desert-you=false "+
      "--make-you-cry false "+
      "--no-tell-a-lie "+
      "--no-no-and-hurt-you false"
     ,{"strangers-to-love":false
      ,"you-know":"the-rules"
      ,"and":"so-do-i"
      ,"you-would-get-this-from-any-other-guy":false
      ,"gonna-give-you-up":false
      ,"gonna-let-you-down":false
      ,"gonna-run-around":false
      ,"desert-you":false
      ,"make-you-cry":false
      ,"tell-a-lie":false
      ,"and-hurt-you":false
      },["we", "are"]]
    ,["-t one -t two -t three"
     ,{t: ["one", "two", "three"]}
     ,[]]
    ,["-t one -t null -t three four five null"
     ,{t: ["one", "null", "three"]}
     ,["four", "five", "null"]]
    ,["-t foo"
     ,{t:["foo"]}
     ,[]]
    ,["--no-t"
     ,{t:["false"]}
     ,[]]
    ,["-no-no-t"
     ,{t:["true"]}
     ,[]]
    ,["-aoa one -aoa null -aoa 100"
     ,{aoa:["one", null, 100]}
     ,[]]
    ,["-str 100"
     ,{str:"100"}
     ,[]]
    ,["--color always"
     ,{color:"always"}
     ,[]]
    ,["--no-nullstream"
     ,{nullstream:null}
     ,[]]
    ,["--nullstream false"
     ,{nullstream:null}
     ,[]]
    ,["--notadate 2011-01-25"
     ,{notadate: "2011-01-25"}
     ,[]]
    ,["--date 2011-01-25"
     ,{date: new Date("2011-01-25")}
     ,[]]
    ].forEach(function (test) {
      var argv = test[0].split(/\s+/)
        , opts = test[1]
        , rem = test[2]
        , actual = nopt(types, shorthands, argv, 0)
        , parsed = actual.argv
      delete actual.argv
      console.log(util.inspect(actual, false, 2, true), parsed.remain)
      for (var i in opts) {
        var e = JSON.stringify(opts[i])
          , a = JSON.stringify(actual[i] === undefined ? null : actual[i])
        if (e && typeof e === "object") {
          assert.deepEqual(e, a)
        } else {
          assert.equal(e, a)
        }
      }
      assert.deepEqual(rem, parsed.remain)
    })
  }
  

  provide("nopt", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  // field paths that every tar file must have.
  // header is padded to 512 bytes.
  var f = 0
    , fields = {}
    , path = fields.path = f++
    , mode = fields.mode = f++
    , uid = fields.uid = f++
    , gid = fields.gid = f++
    , size = fields.size = f++
    , mtime = fields.mtime = f++
    , cksum = fields.cksum = f++
    , type = fields.type = f++
    , linkpath = fields.linkpath = f++
    , headerSize = 512
    , blockSize = 512
    , fieldSize = []
  
  fieldSize[path] = 100
  fieldSize[mode] = 8
  fieldSize[uid] = 8
  fieldSize[gid] = 8
  fieldSize[size] = 12
  fieldSize[mtime] = 12
  fieldSize[cksum] = 8
  fieldSize[type] = 1
  fieldSize[linkpath] = 100
  
  // "ustar\0" may introduce another bunch of headers.
  // these are optional, and will be nulled out if not present.
  
  var ustar = fields.ustar = f++
    , ustarver = fields.ustarver = f++
    , uname = fields.uname = f++
    , gname = fields.gname = f++
    , devmaj = fields.devmaj = f++
    , devmin = fields.devmin = f++
    , prefix = fields.prefix = f++
    , fill = fields.fill = f++
  
  // terminate fields.
  fields[f] = null
  
  fieldSize[ustar] = 6
  fieldSize[ustarver] = 2
  fieldSize[uname] = 32
  fieldSize[gname] = 32
  fieldSize[devmaj] = 8
  fieldSize[devmin] = 8
  fieldSize[prefix] = 155
  fieldSize[fill] = 12
  
  // nb: prefix field may in fact be 130 bytes of prefix,
  // a null char, 12 bytes for atime, 12 bytes for ctime.
  //
  // To recognize this format:
  // 1. prefix[130] === ' ' or '\0'
  // 2. atime and ctime are octal numeric values
  // 3. atime and ctime have ' ' in their last byte
  
  var fieldEnds = {}
    , fieldOffs = {}
    , fe = 0
  for (var i = 0; i < f; i ++) {
    fieldOffs[i] = fe
    fieldEnds[i] = (fe += fieldSize[i])
  }
  
  // build a translation table of field paths.
  Object.keys(fields).forEach(function (f) {
    if (fields[f] !== null) fields[fields[f]] = f
  })
  
  // different values of the 'type' field
  // paths match the values of Stats.isX() functions, where appropriate
  var types =
    { 0: "File"
    , "\0": "OldFile" // like 0
    , 1: "Link"
    , 2: "SymbolicLink"
    , 3: "CharacterDevice"
    , 4: "BlockDevice"
    , 5: "Directory"
    , 6: "FIFO"
    , 7: "ContiguousFile" // like 0
    // posix headers
    , g: "GlobalExtendedHeader" // k=v for the rest of the archive
    , x: "ExtendedHeader" // k=v for the next file
    // vendor-specific stuff
    , A: "SolarisACL" // skip
    , D: "GNUDumpDir" // like 5, but with data, which should be skipped
    , I: "Inode" // metadata only, skip
    , K: "NextFileHasLongLinkpath" // data = link path of next file
    , L: "NextFileHasLongPath" // data = path of next file
    , M: "ContinuationFile" // skip
    , N: "OldGnuLongPath" // like L
    , S: "SparseFile" // skip
    , V: "TapeVolumeHeader" // skip
    , X: "OldExtendedHeader" // like x
    }
  
  Object.keys(types).forEach(function (t) {
    types[types[t]] = types[types[t]] || t
  })
  
  // values for the mode field
  var modes =
    { suid: 04000 // set uid on extraction
    , sgid: 02000 // set gid on extraction
    , svtx: 01000 // set restricted deletion flag on dirs on extraction
    , uread:  0400
    , uwrite: 0200
    , uexec:  0100
    , gread:  040
    , gwrite: 020
    , gexec:  010
    , oread:  4
    , owrite: 2
    , oexec:  1
    , all: 07777
    }
  
  var numeric =
    { mode: true
    , uid: true
    , gid: true
    , size: true
    , mtime: true
    , devmaj: true
    , devmin: true
    , cksum: true
    , atime: true
    , ctime: true
    , dev: true
    , ino: true
    , nlink: true
    }
  
  Object.keys(modes).forEach(function (t) {
    modes[modes[t]] = modes[modes[t]] || t
  })
  
  var knownExtended =
    { atime: true
    , charset: true
    , comment: true
    , ctime: true
    , gid: true
    , gname: true
    , linkpath: true
    , mtime: true
    , path: true
    , realtime: true
    , security: true
    , size: true
    , uid: true
    , uname: true }
  
  
  exports.fields = fields
  exports.fieldSize = fieldSize
  exports.fieldOffs = fieldOffs
  exports.fieldEnds = fieldEnds
  exports.types = types
  exports.modes = modes
  exports.numeric = numeric
  exports.headerSize = headerSize
  exports.blockSize = blockSize
  exports.knownExtended = knownExtended
  
  exports.Pack = require("./lib/pack.js")
  exports.Parse = require("./lib/parse.js")
  exports.Extract = require("./lib/extract.js")
  

  provide("tar", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  module.exports = exports = gyp
  
  /**
   * Module dependencies.
   */
  
  var fs = require('graceful-fs')
    , path = require('path')
    , nopt = require('nopt')
    , log = require('npmlog')
    , child_process = require('child_process')
    , EE = require('events').EventEmitter
    , inherits = require('util').inherits
    , commands = [
        // Module build commands
          'build'
        , 'clean'
        , 'configure'
        , 'rebuild'
        // Development Header File management commands
        , 'install'
        , 'list'
        , 'remove'
      ]
    , aliases = {
          'ls': 'list'
        , 'rm': 'remove'
      }
  
  log.heading = 'gyp'
  
  /**
   * The `gyp` function.
   */
  
  function gyp () {
    return new Gyp
  }
  
  function Gyp () {
    var self = this
  
    // set the dir where node-gyp dev files get installed
    // TODO: make this *more* configurable?
    //       see: https://github.com/TooTallNate/node-gyp/issues/21
    var homeDir = process.env.HOME || process.env.USERPROFILE
    this.devDir = path.resolve(homeDir, '.node-gyp')
  
    this.commands = {}
  
    commands.forEach(function (command) {
      self.commands[command] = function (argv, callback) {
        log.verbose('command', command, argv)
        return require('./' + command)(self, argv, callback)
      }
    })
  }
  inherits(Gyp, EE)
  exports.Gyp = Gyp
  var proto = Gyp.prototype
  
  /**
   * Export the contents of the package.json.
   */
  
  proto.package = require('../package')
  
  /**
   * nopt configuration definitions
   */
  
  proto.configDefs = {
      help: Boolean    // everywhere
    , arch: String     // 'configure'
    , debug: Boolean   // 'build'
    , directory: String // bin
    , msvs_version: String // 'configure'
    , ensure: Boolean  // 'install'
    , solution: String // 'build' (windows only)
    , proxy: String // 'install'
    , nodedir: String // 'configure'
    , loglevel: String // everywhere
  }
  
  /**
   * nopt shorthands
   */
  
  proto.shorthands = {
      release: '--no-debug'
    , C: '--directory'
    , debug: '--debug'
    , silly: '--loglevel=silly'
    , verbose: '--loglevel=verbose'
  }
  
  /**
   * expose the command aliases for the bin file to use.
   */
  
  proto.aliases = aliases
  
  /**
   * Parses the given argv array and sets the 'opts',
   * 'argv' and 'command' properties.
   */
  
  proto.parseArgv = function parseOpts (argv) {
    this.opts = nopt(this.configDefs, this.shorthands, argv)
    this.argv = this.opts.argv.remain.slice()
  
    var commands = []
    this.argv.slice().forEach(function (arg) {
      if (arg in this.commands || arg in this.aliases) {
        this.argv.splice(this.argv.indexOf(arg), 1)
        commands.push(arg)
      }
    }, this)
  
    this.todo = commands
  
    // support for inheriting config env variables from npm
    var npm_config_prefix = 'npm_config_'
    Object.keys(process.env).forEach(function (name) {
      if (name.indexOf(npm_config_prefix) !== 0) return
      var val = process.env[name]
      if (name === npm_config_prefix + 'loglevel') {
        log.level = val
      } else {
        // take the config name and check if it's one that node-gyp cares about
        name = name.substring(npm_config_prefix.length)
        if (name in this.configDefs) {
          this.opts[name] = val
        }
      }
    }, this)
  
    if (this.opts.loglevel) {
      log.level = this.opts.loglevel
    }
    log.resume()
  }
  
  /**
   * Spawns a child process and emits a 'spawn' event.
   */
  
  proto.spawn = function spawn (command, args, opts) {
    opts || (opts = {})
    if (!opts.silent && !opts.customFds) {
      opts.customFds = [ 0, 1, 2 ]
    }
    var cp = child_process.spawn(command, args, opts)
    log.info('spawn', command)
    log.info('spawn args', args)
    return cp
  }
  
  /**
   * Prints the usage instructions and then exits.
   */
  
  proto.usageAndExit = function usageAndExit () {
    var usage = [
        ''
      , '  Usage: node-gyp <command> [options]'
      , ''
      , '  where <command> is one of:'
      , commands.map(function (c) {
          return '    - ' + c + ' - ' + require('./' + c).usage
        }).join('\n')
      , ''
      , '  for specific command usage and options try:'
      , '    $ node-gyp <command> --help'
      , ''
      , 'node-gyp@' + this.version + '  ' + path.resolve(__dirname, '..')
      , 'node@' + process.versions.node
    ].join('\n')
  
    console.log(usage)
    process.exit(4)
  }
  
  /**
   * Version number proxy.
   */
  
  Object.defineProperty(proto, 'version', {
      get: function () {
        return this.package.version
      }
    , enumerable: true
  })
  
  

  provide("node-gyp", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  // write data to it, and it'll emit data in 512 byte blocks.
  // if you .end() or .flush(), it'll emit whatever it's got,
  // padded with nulls to 512 bytes.
  
  module.exports = BlockStream
  
  var Stream = require("stream").Stream
    , inherits = require("inherits")
    , assert = require("assert").ok
    , debug = process.env.DEBUG ? console.error : function () {}
  
  function BlockStream (size, opt) {
    this.writable = this.readable = true
    this._opt = opt || {}
    this._chunkSize = size || 512
    this._offset = 0
    this._buffer = []
    this._bufferLength = 0
    if (this._opt.nopad) this._zeroes = false
    else {
      this._zeroes = new Buffer(this._chunkSize)
      for (var i = 0; i < this._chunkSize; i ++) {
        this._zeroes[i] = 0
      }
    }
  }
  
  inherits(BlockStream, Stream)
  
  BlockStream.prototype.write = function (c) {
    // debug("   BS write", c)
    if (this._ended) throw new Error("BlockStream: write after end")
    if (c && !Buffer.isBuffer(c)) c = new Buffer(c + "")
    if (c.length) {
      this._buffer.push(c)
      this._bufferLength += c.length
    }
    // debug("pushed onto buffer", this._bufferLength)
    if (this._bufferLength >= this._chunkSize) {
      if (this._paused) {
        // debug("   BS paused, return false, need drain")
        this._needDrain = true
        return false
      }
      this._emitChunk()
    }
    return true
  }
  
  BlockStream.prototype.pause = function () {
    // debug("   BS pausing")
    this._paused = true
  }
  
  BlockStream.prototype.resume = function () {
    // debug("   BS resume")
    this._paused = false
    return this._emitChunk()
  }
  
  BlockStream.prototype.end = function (chunk) {
    // debug("end", chunk)
    if (typeof chunk === "function") cb = chunk, chunk = null
    if (chunk) this.write(chunk)
    this._ended = true
    this.flush()
  }
  
  BlockStream.prototype.flush = function () {
    this._emitChunk(true)
  }
  
  BlockStream.prototype._emitChunk = function (flush) {
    // debug("emitChunk flush=%j emitting=%j paused=%j", flush, this._emitting, this._paused)
  
    // emit a <chunkSize> chunk
    if (flush && this._zeroes) {
      // debug("    BS push zeroes", this._bufferLength)
      // push a chunk of zeroes
      var padBytes = (this._bufferLength % this._chunkSize)
      if (padBytes !== 0) padBytes = this._chunkSize - padBytes
      if (padBytes > 0) {
        // debug("padBytes", padBytes, this._zeroes.slice(0, padBytes))
        this._buffer.push(this._zeroes.slice(0, padBytes))
        this._bufferLength += padBytes
        // debug(this._buffer[this._buffer.length - 1].length, this._bufferLength)
      }
    }
  
    if (this._emitting || this._paused) return
    this._emitting = true
  
    // debug("    BS entering loops")
    var bufferIndex = 0
    while (this._bufferLength >= this._chunkSize &&
           (flush || !this._paused)) {
      // debug("     BS data emission loop", this._bufferLength)
  
      var out
        , outOffset = 0
        , outHas = this._chunkSize
  
      while (outHas > 0 && (flush || !this._paused) ) {
        // debug("    BS data inner emit loop", this._bufferLength)
        var cur = this._buffer[bufferIndex]
          , curHas = cur.length - this._offset
        // debug("cur=", cur)
        // debug("curHas=%j", curHas)
        // If it's not big enough to fill the whole thing, then we'll need
        // to copy multiple buffers into one.  However, if it is big enough,
        // then just slice out the part we want, to save unnecessary copying.
        // Also, need to copy if we've already done some copying, since buffers
        // can't be joined like cons strings.
        if (out || curHas < outHas) {
          out = out || new Buffer(this._chunkSize)
          cur.copy(out, outOffset,
                   this._offset, this._offset + Math.min(curHas, outHas))
        } else if (cur.length === outHas && this._offset === 0) {
          // shortcut -- cur is exactly long enough, and no offset.
          out = cur
        } else {
          // slice out the piece of cur that we need.
          out = cur.slice(this._offset, this._offset + outHas)
        }
  
        if (curHas > outHas) {
          // means that the current buffer couldn't be completely output
          // update this._offset to reflect how much WAS written
          this._offset += outHas
          outHas = 0
        } else {
          // output the entire current chunk.
          // toss it away
          outHas -= curHas
          outOffset += curHas
          bufferIndex ++
          this._offset = 0
        }
      }
  
      this._bufferLength -= this._chunkSize
      assert(out.length === this._chunkSize)
      // debug("emitting data", out)
      // debug("   BS emitting, paused=%j", this._paused, this._bufferLength)
      this.emit("data", out)
      out = null
    }
    // debug("    BS out of loops", this._bufferLength)
  
    // whatever is left, it's not enough to fill up a block, or we're paused
    this._buffer = this._buffer.slice(bufferIndex)
    if (this._paused) {
      // debug("    BS paused, leaving", this._bufferLength)
      this._needsDrain = true
      this._emitting = false
      return
    }
  
    // if flushing, and not using null-padding, then need to emit the last
    // chunk(s) sitting in the queue.  We know that it's not enough to
    // fill up a whole block, because otherwise it would have been emitted
    // above, but there may be some offset.
    var l = this._buffer.length
    if (flush && !this._zeroes && l) {
      if (l === 1) {
        if (this._offset) {
          this.emit("data", this._buffer[0].slice(this._offset))
        } else {
          this.emit("data", this._buffer[0])
        }
      } else {
        var outHas = this._bufferLength
          , out = new Buffer(outHas)
          , outOffset = 0
        for (var i = 0; i < l; i ++) {
          var cur = this._buffer[i]
            , curHas = cur.length - this._offset
          cur.copy(out, outOffset, this._offset)
          this._offset = 0
          outOffset += curHas
          this._bufferLength -= curHas
        }
        this.emit("data", out)
      }
      // truncate
      this._buffer.length = 0
      this._bufferLength = 0
      this._offset = 0
    }
  
    // now either drained or ended
    // debug("either draining, or ended", this._bufferLength, this._ended)
    // means that we've flushed out all that we can so far.
    if (this._needDrain) {
      // debug("emitting drain", this._bufferLength)
      this._needDrain = false
      this.emit("drain")
    }
  
    if ((this._bufferLength === 0) && this._ended && !this._endEmitted) {
      // debug("emitting end", this._bufferLength)
      this._endEmitted = true
      this.emit("end")
    }
  
    this._emitting = false
  
    // debug("    BS no longer emitting", flush, this._paused, this._emitting, this._bufferLength, this._chunkSize)
  }
  

  provide("block-stream", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  // vim: set softtabstop=16 shiftwidth=16:
  
  try {
                  readJson.log = require("npmlog")
  } catch (er) {
                  readJson.log = {
                                  info: function () {},
                                  verbose: function () {},
                                  warn: function () {}
                  }
  }
  
  
  try {
                  var fs = require("graceful-fs")
  } catch (er) {
                  var fs = require("fs")
  }
  
  
  module.exports = readJson
  
  var LRU = require("lru-cache")
  readJson.cache = new LRU(1000)
  var path = require("path")
  var glob = require("glob")
  var slide = require("slide")
  var asyncMap = slide.asyncMap
  var semver = require("semver")
  
  // put more stuff on here to customize.
  readJson.extraSet = [gypfile, wscript, serverjs, authors, readme, mans, bins]
  
  var typoWarned = {}
  // http://registry.npmjs.org/-/fields
  var typos = { "dependancies": "dependencies"
              , "dependecies": "dependencies"
              , "depdenencies": "dependencies"
              , "devEependencies": "devDependencies"
              , "depends": "dependencies"
              , "dev-dependencies": "devDependencies"
              , "devDependences": "devDependencies"
              , "devDepenencies": "devDependencies"
              , "devdependencies": "devDependencies"
              , "repostitory": "repository"
              , "prefereGlobal": "preferGlobal"
              , "hompage": "homepage"
              , "hampage": "homepage"
              , "autohr": "author"
              , "autor": "author"
              , "contributers": "contributors"
              , "publicationConfig": "publishConfig"
              }
  var bugsTypos = { "web": "url", "name": "url" }
  var scriptTypos = { "server": "start", "tests": "test" }
  var depTypes = [ "dependencies"
                 , "devDependencies"
                 , "optionalDependencies" ]
  
  
  function readJson (file, cb) {
                  var c = readJson.cache.get(file)
                  if (c) {
                                  readJson.log.verbose("from cache", file)
                                  cb = cb.bind(null, null, c)
                                  return process.nextTick(cb);
                  }
                  readJson.log.verbose("read json", file)
                  cb = (function (orig) { return function (er, data) {
                                  if (data) readJson.cache.set(file, data);
                                  return orig(er, data)
                  } })(cb)
                  readJson_(file, cb)
  }
  
  
  function readJson_ (file, cb) {
                  fs.readFile(file, "utf8", function (er, d) {
                                  if (er && er.code === "ENOENT") {
                                                  indexjs(file, er, cb)
                                                  return
                                  }
                                  if (er) return cb(er);
                                  try {
                                                  d = JSON.parse(d)
                                  } catch (er) {
                                                  er = parseError(er, file);
                                                  return cb(er);
                                  }
                                  extras(file, d, cb)
                  })
  }
  
  
  function indexjs (file, er, cb) {
                  if (path.basename(file) === "index.js") {
                                  return cb(er);
                  }
                  var index = path.resolve(path.dirname(file), "index.js")
                  fs.readFile(index, "utf8", function (er2, d) {
                                  if (er2) return cb(er);
                                  d = parseIndex(d)
                                  if (!d) return cb(er);
                                  extras(file, d, cb)
                  })
  }
  
  
  readJson.extras = extras
  function extras (file, data, cb) {
                  asyncMap(readJson.extraSet, function (fn, cb) {
                                  return fn(file, data, cb)
                  }, function (er) {
                                  if (er) return cb(er);
                                  final(file, data, cb)
                  })
  }
  
  function gypfile (file, data, cb) {
                  var dir = path.dirname(file)
                  var s = data.scripts || {}
                  if (s.install || s.preinstall) {
                                  return cb(null, data);
                  }
                  glob("*.gyp", { cwd: dir }, function (er, files) {
                                  if (er) return cb(er);
                                  gypfile_(file, data, files, cb)
                  })
  }
  
  function gypfile_ (file, data, files, cb) {
                  if (!files.length) return cb(null, data);
                  var s = data.scripts || {}
                  s.install = "node-gyp rebuild"
                  data.scripts = s
                  data.gypfile = true
                  return cb(null, data);
  }
  
  function wscript (file, data, cb) {
                  var dir = path.dirname(file)
                  var s = data.scripts || {}
                  if (s.install || s.preinstall) {
                                  return cb(null, data);
                  }
                  glob("wscript", { cwd: dir }, function (er, files) {
                                  if (er) return cb(er);
                                  wscript_(file, data, files, cb)
                  })
  }
  function wscript_ (file, data, files, cb) {
                  if (!files.length || data.gypfile) return cb(null, data);
                  var s = data.scripts || {}
                  s.install = "node-waf clean ; node-waf configure build"
                  data.scripts = s
                  return cb(null, data);
  }
  
  function serverjs (file, data, cb) {
                  var dir = path.dirname(file)
                  var s = data.scripts || {}
                  if (s.start) return cb(null, data)
                  glob("server.js", { cwd: dir }, function (er, files) {
                                  if (er) return cb(er);
                                  serverjs_(file, data, files, cb)
                  })
  }
  function serverjs_ (file, data, files, cb) {
                  if (!files.length) return cb(null, data);
                  var s = data.scripts || {}
                  s.start = "node server.js"
                  data.scripts = s
                  return cb(null, data)
  }
  
  function authors (file, data, cb) {
                  if (data.contributors) return cb(null, data);
                  var af = path.resolve(path.dirname(file), "AUTHORS")
                  fs.readFile(af, "utf8", function (er, ad) {
                                  // ignore error.  just checking it.
                                  if (er) return cb(null, data);
                                  authors_(file, data, ad, cb)
                  })
  }
  function authors_ (file, data, ad, cb) {
                  ad = ad.split(/\r?\n/g).map(function (line) {
                                  return line.replace(/^\s*#.*$/, '').trim()
                  }).filter(function (line) {
                                  return line
                  })
                  data.contributors = ad
                  return cb(null, data)
  }
  
  function readme (file, data, cb) {
                  if (data.readme) return cb(null, data);
                  var dir = path.dirname(file)
                  glob("README?(.*)", { cwd: dir }, function (er, files) {
                                  if (er) return cb(er);
                                  if (!files.length) return cb()
                                  var rm = path.resolve(dir, files[0])
                                  readme_(file, data, rm, cb)
                  })
  }
  function readme_(file, data, rm, cb) {
                  fs.readFile(rm, "utf8", function (er, rm) {
                                  data.readme = rm
                                  return cb(er, data)
                  })
  }
  
  function mans (file, data, cb) {
                  var m = data.directories && data.directories.man
                  if (data.man || !m) return cb(null, data);
                  m = path.resolve(path.dirname(file), m)
                  glob("**/*.[0-9]", { cwd: m }, function (er, mans) {
                                  if (er) return cb(er);
                                  mans_(file, data, mans, cb)
                  })
  }
  function mans_ (file, data, mans, cb) {
                  var m = data.directories && data.directories.man
                  data.man = mans.map(function (mf) {
                                  return path.resolve(m, mf)
                  })
                  return cb(null, data)
  }
  
  function bins (file, data, cb) {
                  var m = data.directories && data.directories.bin
                  if (data.bin || !m) return cb(null, data);
                  m = path.resolve(path.dirname(file), m)
                  glob("**", { cwd: m }, function (er, bins) {
                                  if (er) return cb(er);
                                  bins_(file, data, bins, cb)
                  })
  }
  function bins_ (file, data, bins, cb) {
                  var m = data.directories && data.directories.bin
                  data.bin = bins.map(function (mf) {
                                  return path.resolve(m, mf)
                  })
                  return cb(null, data)
  }
  
  function final (file, data, cb) {
                  var ret = validName(file, data)
                  if (ret !== true) return cb(ret);
                  ret = validVersion(file, data)
                  if (ret !== true) return cb(ret);
  
                  data._id = data.name + "@" + data.version
                  typoWarn(file, data)
                  validRepo(file, data)
                  validFiles(file, data)
                  validBin(file, data)
                  validMan(file, data)
                  validBundled(file, data)
                  objectifyDeps(file, data)
                  unParsePeople(file, data)
                  parsePeople(file, data)
  
                  readJson.cache.set(file, data)
                  cb(null, data)
  }
  
  
  // /**package { "name": "foo", "version": "1.2.3", ... } **/
  function parseIndex (data) {
                  data = data.split(/^\/\*\*package(?:\s|$)/m)
                  if (data.length < 2) return null
                  data = data[1]
                  data = data.split(/\*\*\/$/m)
                  if (data.length < 2) return null
                  data = data[0]
                  data = data.replace(/^\s*\*/mg, "")
                  try {
                                  return JSON.parse(data)
                  } catch (er) {
                                  return null
                  }
  }
  
  function parseError (ex, file) {
                  var e = new Error("Failed to parse json\n"+ex.message)
                  e.code = "EJSONPARSE"
                  e.file = file
                  return e
  }
  
  // a warning for deprecated or likely-incorrect fields
  function typoWarn (file, data) {
                  if (typoWarned[data._id]) return;
                  typoWarned[data._id] = true
                  if (data.modules) {
                                  warn(file, data,
                                       "'modules' is deprecated")
                                  delete data.modules
                  }
                  Object.keys(typos).forEach(function (d) {
                                  checkTypo(file, data, d)
                  })
                  bugsTypoWarn(file, data)
                  scriptTypoWarn(file, data)
  }
  
  function checkTypo (file, data, d) {
                  if (!data.hasOwnProperty(d)) return;
                  warn(file, data,
                       "'" + d + "' should probably be '" + typos[d] + "'" )
  }
  
  function bugsTypoWarn (file, data) {
                  var b = data.bugs
                  if (!b || typeof b !== "object") return
                  Object.keys(b).forEach(function (k) {
                                  if (bugsTypos[k]) {
                                                  b[bugsTypos[k]] = b[k]
                                                  delete b[k]
                                  }
                  })
  }
  
  function scriptTypoWarn (file, data) {
                  var s = data.scripts
                  if (!s || typeof s !== "object") return
                  Object.keys(s).forEach(function (k) {
                                  if (scriptTypos[k]) {
                                                  scriptWarn_(file, data, k)
                                  }
                  })
  }
  function scriptWarn_ (file, data, k) {
                  warn(file, data, "scripts['" + k + "'] should probably " +
                       "be scripts['" + scriptTypos[k] + "']")
  }
  
  function validRepo (file, data) {
                  if (data.repostories) {
                                  warnRepositories(file, data)
                  }
                  if (!data.repository) return;
                  if (typeof data.repository === "string") {
                                  data.repository = {
                                                  type: "git",
                                                  url: data.repository
                                  }
                  }
                  var r = data.repository.url || ""
                  // use the non-private urls
                  r = r.replace(/^(https?|git):\/\/[^\@]+\@github.com/,
                                '$1://github.com')
                  r = r.replace(/^https?:\/\/github.com/,
                                'git://github.com')
                  if (r.match(/github.com\/[^\/]+\/[^\/]+\.git\.git$/)) {
                                  warn(file, data, "Probably broken git " +
                                       "url: " + r)
                  }
  }
  function warnRepostories (file, data) {
                  warn(file, data,
                       "'repositories' (plural) Not supported.\n" +
                       "Please pick one as the 'repository' field");
                  data.repository = data.repositories[0]
  }
  
  function validFiles (file, data) {
                  var files = data.files
                  if (files && !Array.isArray(files)) {
                                  warn(file, data, "Invalid 'files' member")
                                  delete data.files
                  }
  }
  
  function validBin (file, data) {
                  if (!data.bin) return;
                  if (typeof data.bin === "string") {
                                  var b = {}
                                  b[data.name] = data.bin
                                  data.bin = b
                  }
  }
  
  function validMan (file, data) {
                  if (!data.man) return;
                  if (typeof data.man === "string") {
                                  data.man = [ data.man ]
                  }
  }
  
  function validBundled (file, data) {
                  var bdd = "bundledDependencies"
                  var bd = "bundleDependencies"
                  if (data[bdd] && !data[bd]) {
                                  data[bd] = data[bdd]
                                  delete data[bdd]
                  }
  
                  if (data[bd] && !Array.isArray(data[bd])) {
                                  warn(file, data, "bundleDependencies " +
                                       "must be an array")
                  }
  }
  
  function objectifyDeps (file, data) {
                  depTypes.forEach(function (d) {
                                  objectifyDep_(file, data, d)
                  })
  
                  var o = data.optionalDependencies
                  if (!o) return;
                  var d = data.dependencies || {}
                  Object.keys(o).forEach(function (k) {
                                  d[k] = o[k]
                  })
                  data.dependencies = d
  }
  function objectifyDep_ (file, data, type) {
                  if (!data[type]) return;
                  data[type] = depObjectify(file, data, data[type])
  }
  function depObjectify (file, data, deps) {
                  if (!deps) return {}
                  if (typeof deps === "string") {
                                  deps = deps.trim().split(/[\n\r\s\t ,]+/)
                  }
                  if (!Array.isArray(deps)) return deps
                  var o = {}
                  deps.forEach(function (d) {
                                  d = d.trim().split(/(:?[@\s><=])/)
                                  var dn = d.shift()
                                  var dv = d.join("")
                                  dv = dv.trim()
                                  dv = dv.replace(/^@/, "")
                                  o[dn] = dv
                  })
                  return o
  }
  
  
  function warn (f, d, m) {
                  readJson.log.warn("package.json", d._id, m)
  }
  
  
  function validName (file, data) {
                  if (!data.name) return new Error("No 'name' field")
                  data.name = data.name.trim()
                  if (data.name.charAt(0) === "." ||
                      data.name.match(/[\/@\s\+%:]/) ||
                      data.name.toLowerCase() === "node_modules" ||
                      data.name.toLowerCase() === "favicon.ico") {
                                  return new Error("Invalid name: " +
                                                   JSON.stringify(data.name))
                  }
                  return true
  }
  
  
  function parseKeywords (file, data) {
                  var kw = data.keywords
                  if (typeof kw === "string") {
                                  kw = kw.split(/,\s+/)
                                  data.keywords = kw
                  }
  }
  
  function validVersion (file, data) {
                  var v = data.version
                  if (!v) return new Error("no version");
                  if (!semver.valid(v)) {
                                  return new Error("invalid version: "+v)
                  }
                  data.version = semver.clean(data.version)
                  return true
  }
  function unParsePeople (file, data) {
                  return parsePeople(file, data, true)
  }
  
  function parsePeople (file, data, un) {
                  var fn = un ? unParsePerson : parsePerson
                  if (data.author) data.author = fn(data.author)
                  ;["maintainers", "contributors"].forEach(function (set) {
                                  if (!Array.isArray(data[set])) return;
                                  data[set] = data[set].map(fn)
                  })
                  return data
  }
  
  function unParsePerson (person) {
                  if (typeof person === "string") return person
                  var name = person.name || ""
                  var u = person.url || person.web
                  var url = u ? (" ("+u+")") : ""
                  var e = person.email || person.mail
                  var email = e ? (" <"+e+">") : ""
                  return name+email+url
  }
  
  function parsePerson (person) {
                  if (typeof person !== "string") return person
                  var name = person.match(/^([^\(<]+)/)
                  var url = person.match(/\(([^\)]+)\)/)
                  var email = person.match(/<([^>]+)>/)
                  var obj = {}
                  if (name && name[0].trim()) obj.name = name[0].trim()
                  if (email) obj.email = email[1];
                  if (url) obj.url = url[1];
                  return obj
  }
  

  provide("read-package-json", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  
  // Walk through the file-system "database" of installed
  // packages, and create a data object related to the
  // installed versions of each package.
  
  /*
  This will traverse through all node_modules folders,
  resolving the dependencies object to the object corresponding to
  the package that meets that dep, or just the version/range if
  unmet.
  
  Assuming that you had this folder structure:
  
  /path/to
  +-- package.json { name = "root" }
  `-- node_modules
      +-- foo {bar, baz, asdf}
      | +-- node_modules
      |   +-- bar { baz }
      |   `-- baz
      `-- asdf
  
  where "foo" depends on bar, baz, and asdf, bar depends on baz,
  and bar and baz are bundled with foo, whereas "asdf" is at
  the higher level (sibling to foo), you'd get this object structure:
  
  { <package.json data>
  , path: "/path/to"
  , parent: null
  , dependencies:
    { foo :
      { version: "1.2.3"
      , path: "/path/to/node_modules/foo"
      , parent: <Circular: root>
      , dependencies:
        { bar:
          { parent: <Circular: foo>
          , path: "/path/to/node_modules/foo/node_modules/bar"
          , version: "2.3.4"
          , dependencies: { baz: <Circular: foo.dependencies.baz> }
          }
        , baz: { ... }
        , asdf: <Circular: asdf>
        }
      }
    , asdf: { ... }
    }
  }
  
  Unmet deps are left as strings.
  Extraneous deps are marked with extraneous:true
  deps that don't meet a requirement are marked with invalid:true
  
  to READ(packagefolder, parentobj, name, reqver)
  obj = read package.json
  installed = ./node_modules/*
  if parentobj is null, and no package.json
    obj = {dependencies:{<installed>:"*"}}
  deps = Object.keys(obj.dependencies)
  obj.path = packagefolder
  obj.parent = parentobj
  if name, && obj.name !== name, obj.invalid = true
  if reqver, && obj.version !satisfies reqver, obj.invalid = true
  if !reqver && parentobj, obj.extraneous = true
  for each folder in installed
    obj.dependencies[folder] = READ(packagefolder+node_modules+folder,
                                    obj, folder, obj.dependencies[folder])
  # walk tree to find unmet deps
  for each dep in obj.dependencies not in installed
    r = obj.parent
    while r
      if r.dependencies[dep]
        if r.dependencies[dep].verion !satisfies obj.dependencies[dep]
          WARN
          r.dependencies[dep].invalid = true
        obj.dependencies[dep] = r.dependencies[dep]
        r = null
      else r = r.parent
  return obj
  
  
  TODO:
  1. Find unmet deps in parent directories, searching as node does up
  as far as the left-most node_modules folder.
  2. Ignore anything in node_modules that isn't a package folder.
  
  */
  
  try {
    var fs = require("graceful-fs")
  } catch (er) {
    var fs = require("fs")
  }
  
  try {
    var log = require("npmlog")
  } catch (_) {
    var log = { verbose: noop, info: noop, warn: noop, error: noop }
    function noop () {}
  }
  
  var path = require("path")
  var asyncMap = require("slide").asyncMap
  var semver = require("semver")
  var readJson = require("read-package-json")
  var url = require("url")
  
  module.exports = readInstalled
  
  function readInstalled (folder, depth, cb) {
    if (typeof cb !== "function") cb = depth, depth = Infinity
    readInstalled_(folder, null, null, null, 0, depth, function (er, obj) {
      if (er) return cb(er)
      // now obj has all the installed things, where they're installed
      // figure out the inheritance links, now that the object is built.
      resolveInheritance(obj)
      cb(null, obj)
    })
  }
  
  var rpSeen = {}
  function readInstalled_ (folder, parent, name, reqver, depth, maxDepth, cb) {
    //console.error(folder, name)
  
    var installed
      , obj
      , real
      , link
  
    fs.readdir(path.resolve(folder, "node_modules"), function (er, i) {
      // error indicates that nothing is installed here
      if (er) i = []
      installed = i.filter(function (f) { return f.charAt(0) !== "." })
      next()
    })
  
    readJson(path.resolve(folder, "package.json"), function (er, data) {
      obj = copy(data)
  
      if (!parent) {
        obj = obj || true
        er = null
      }
      return next(er)
    })
  
    fs.lstat(folder, function (er, st) {
      if (er) {
        if (!parent) real = true
        return next(er)
      }
      fs.realpath(folder, function (er, rp) {
        //console.error("realpath(%j) = %j", folder, rp)
        real = rp
        if (st.isSymbolicLink()) link = rp
        next(er)
      })
    })
  
    var errState = null
      , called = false
    function next (er) {
      if (errState) return
      if (er) {
        errState = er
        return cb(null, [])
      }
      //console.error('next', installed, obj && typeof obj, name, real)
      if (!installed || !obj || !real || called) return
      called = true
      if (rpSeen[real]) return cb(null, rpSeen[real])
      if (obj === true) {
        obj = {dependencies:{}, path:folder}
        installed.forEach(function (i) { obj.dependencies[i] = "*" })
      }
      if (name && obj.name !== name) obj.invalid = true
      obj.realName = name || obj.name
      obj.dependencies = obj.dependencies || {}
  
      // "foo":"http://blah" is always presumed valid
      if (reqver
          && semver.validRange(reqver)
          && !semver.satisfies(obj.version, reqver)) {
        obj.invalid = true
      }
  
      if (parent
          && !(name in parent.dependencies)
          && !(name in (parent.devDependencies || {}))) {
        obj.extraneous = true
      }
      obj.path = obj.path || folder
      obj.realPath = real
      obj.link = link
      if (parent && !obj.link) obj.parent = parent
      rpSeen[real] = obj
      obj.depth = depth
      //if (depth >= maxDepth) return cb(null, obj)
      asyncMap(installed, function (pkg, cb) {
        var rv = obj.dependencies[pkg]
        if (!rv && obj.devDependencies) rv = obj.devDependencies[pkg]
        if (depth >= maxDepth) {
          // just try to get the version number
          var pkgfolder = path.resolve(folder, "node_modules", pkg)
            , jsonFile = path.resolve(pkgfolder, "package.json")
          return readJson(jsonFile, function (er, depData) {
            // already out of our depth, ignore errors
            if (er || !depData || !depData.version) return cb(null, obj)
            obj.dependencies[pkg] = depData.version
            cb(null, obj)
          })
        }
  
        readInstalled_( path.resolve(folder, "node_modules/"+pkg)
                      , obj, pkg, obj.dependencies[pkg], depth + 1, maxDepth
                      , cb )
  
      }, function (er, installedData) {
        if (er) return cb(er)
        installedData.forEach(function (dep) {
          obj.dependencies[dep.realName] = dep
        })
  
        // any strings here are unmet things.  however, if it's
        // optional, then that's fine, so just delete it.
        if (obj.optionalDependencies) {
          Object.keys(obj.optionalDependencies).forEach(function (dep) {
            if (typeof obj.dependencies[dep] === "string") {
              delete obj.dependencies[dep]
            }
          })
        }
        return cb(null, obj)
      })
    }
  }
  
  // starting from a root object, call findUnmet on each layer of children
  var riSeen = []
  function resolveInheritance (obj) {
    if (typeof obj !== "object") return
    if (riSeen.indexOf(obj) !== -1) return
    riSeen.push(obj)
    if (typeof obj.dependencies !== "object") {
      obj.dependencies = {}
    }
    Object.keys(obj.dependencies).forEach(function (dep) {
      findUnmet(obj.dependencies[dep])
    })
    Object.keys(obj.dependencies).forEach(function (dep) {
      resolveInheritance(obj.dependencies[dep])
    })
  }
  
  // find unmet deps by walking up the tree object.
  // No I/O
  var fuSeen = []
  function findUnmet (obj) {
    if (fuSeen.indexOf(obj) !== -1) return
    fuSeen.push(obj)
    //console.error("find unmet", obj.name, obj.parent && obj.parent.name)
    var deps = obj.dependencies = obj.dependencies || {}
    //console.error(deps)
    Object.keys(deps)
      .filter(function (d) { return typeof deps[d] === "string" })
      .forEach(function (d) {
        //console.error("find unmet", obj.name, d, deps[d])
        var r = obj.parent
          , found = null
        while (r && !found && typeof deps[d] === "string") {
          // if r is a valid choice, then use that.
          found = r.dependencies[d]
          if (!found && r.realName === d) found = r
  
          if (!found) {
            r = r.link ? null : r.parent
            continue
          }
          if ( typeof deps[d] === "string"
              // url deps presumed innocent.
              && !url.parse(deps[d]).protocol
              && !semver.satisfies(found.version, deps[d])) {
            // the bad thing will happen
            log.warn("unmet dependency", obj.path + " requires "+d+"@'"+deps[d]
                    +"' but will load\n"
                    +found.path+",\nwhich is version "+found.version
                    )
            found.invalid = true
          }
          deps[d] = found
        }
  
      })
    log.verbose("readInstalled", "returning", obj._id)
    return obj
  }
  
  function copy (obj) {
    if (!obj || typeof obj !== 'object') return obj
    if (Array.isArray(obj)) return obj.map(copy)
  
    var o = {}
    for (var i in obj) o[i] = copy(obj[i])
    return o
  }
  

  provide("read-installed", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  // Essentially, this is a fstream.DirReader class, but with a
  // bit of special logic to read the specified sort of ignore files,
  // and a filter that prevents it from picking up anything excluded
  // by those files.
  
  var Minimatch = require("minimatch").Minimatch
  , fstream = require("fstream")
  , DirReader = fstream.DirReader
  , inherits = require("inherits")
  , path = require("path")
  , fs = require("fs")
  
  module.exports = IgnoreReader
  
  inherits(IgnoreReader, DirReader)
  
  function IgnoreReader (props) {
    if (!(this instanceof IgnoreReader)) {
      return new IgnoreReader(props)
    }
  
    // must be a Directory type
    if (typeof props === "string") {
      props = { path: path.resolve(props) }
    }
  
    props.type = "Directory"
    props.Directory = true
  
    if (!props.ignoreFiles) props.ignoreFiles = [".ignore"]
    this.ignoreFiles = props.ignoreFiles
  
    this.ignoreRules = null
  
    // ensure that .ignore files always show up at the top of the list
    // that way, they can be read before proceeding to handle other
    // entries in that same folder
    if (props.sort) {
      this._sort = props.sort === "alpha" ? alphasort : props.sort
      props.sort = null
    }
  
    this.on("entries", function () {
      // if there are any ignore files in the list, then
      // pause and add them.
      // then, filter the list based on our ignoreRules
  
      var hasIg = this.entries.some(this.isIgnoreFile, this)
  
      if (!hasIg) return this.filterEntries()
  
      this.addIgnoreFiles()
    })
  
    // we filter entries before we know what they are.
    // however, directories have to be re-tested against
    // rules with a "/" appended, because "a/b/" will only
    // match if "a/b" is a dir, and not otherwise.
    this.on("_entryStat", function (entry, props) {
      var t = entry.basename
      if (!this.applyIgnores(entry.basename,
                             entry.type === "Directory",
                             entry)) {
        entry.abort()
      }
    }.bind(this))
  
    DirReader.call(this, props)
  }
  
  
  IgnoreReader.prototype.addIgnoreFiles = function () {
    if (this._paused) {
      this.once("resume", this.addIgnoreFiles)
      return
    }
    if (this._ignoreFilesAdded) return
    this._ignoreFilesAdded = true
  
    var newIg = this.entries.filter(this.isIgnoreFile, this)
    , count = newIg.length
    , errState = null
  
    if (!count) return
  
    this.pause()
  
    var then = function then (er) {
      if (errState) return
      if (er) return this.emit("error", errState = er)
      if (-- count === 0) {
        this.filterEntries()
        this.resume()
      }
    }.bind(this)
  
    newIg.forEach(function (ig) {
      this.addIgnoreFile(ig, then)
    }, this)
  }
  
  
  IgnoreReader.prototype.isIgnoreFile = function (e) {
    return e !== "." &&
           e !== ".." &&
           -1 !== this.ignoreFiles.indexOf(e)
  }
  
  
  IgnoreReader.prototype.getChildProps = function (stat) {
    var props = DirReader.prototype.getChildProps.call(this, stat)
    props.ignoreFiles = this.ignoreFiles
  
    // Directories have to be read as IgnoreReaders
    // otherwise fstream.Reader will create a DirReader instead.
    if (stat.isDirectory()) {
      props.type = this.constructor
    }
    return props
  }
  
  
  IgnoreReader.prototype.addIgnoreFile = function (e, cb) {
    // read the file, and then call addIgnoreRules
    // if there's an error, then tell the cb about it.
  
    var ig = path.resolve(this.path, e)
    fs.readFile(ig, function (er, data) {
      if (er) return cb(er)
  
      this.emit("ignoreFile", e, data)
      var rules = this.readRules(data, e)
      this.addIgnoreRules(rules, e)
      cb()
    }.bind(this))
  }
  
  
  IgnoreReader.prototype.readRules = function (buf, e) {
    return buf.toString().split(/\r?\n/)
  }
  
  
  // Override this to do fancier things, like read the
  // "files" array from a package.json file or something.
  IgnoreReader.prototype.addIgnoreRules = function (set, e) {
    // filter out anything obvious
    set = set.filter(function (s) {
      s = s.trim()
      return s && !s.match(/^#/)
    })
  
    // no rules to add!
    if (!set.length) return
  
    // now get a minimatch object for each one of these.
    // Note that we need to allow dot files by default, and
    // not switch the meaning of their exclusion
    var mmopt = { matchBase: true, dot: true, flipNegate: true }
    , mm = set.map(function (s) {
      var m = new Minimatch(s, mmopt)
      m.ignoreFile = e
      return m
    })
  
    if (!this.ignoreRules) this.ignoreRules = []
    this.ignoreRules.push.apply(this.ignoreRules, mm)
  }
  
  
  IgnoreReader.prototype.filterEntries = function () {
    // this exclusion is at the point where we know the list of
    // entries in the dir, but don't know what they are.  since
    // some of them *might* be directories, we have to run the
    // match in dir-mode as well, so that we'll pick up partials
    // of files that will be included later.  Anything included
    // at this point will be checked again later once we know
    // what it is.
    this.entries = this.entries.filter(function (entry) {
      // at this point, we don't know if it's a dir or not.
      return this.applyIgnores(entry) || this.applyIgnores(entry, true)
    }, this)
  }
  
  
  IgnoreReader.prototype.applyIgnores = function (entry, partial, obj) {
    var included = true
  
    // this = /a/b/c
    // entry = d
    // parent /a/b sees c/d
    if (this.parent && this.parent.applyIgnores) {
      var pt = this.basename + "/" + entry
      included = this.parent.applyIgnores(pt, partial)
    }
  
    // Negated Rules
    // Since we're *ignoring* things here, negating means that a file
    // is re-included, if it would have been excluded by a previous
    // rule.  So, negated rules are only relevant if the file
    // has been excluded.
    //
    // Similarly, if a file has been excluded, then there's no point
    // trying it against rules that have already been applied
    //
    // We're using the "flipnegate" flag here, which tells minimatch
    // to set the "negate" for our information, but still report
    // whether the core pattern was a hit or a miss.
  
    if (!this.ignoreRules) {
      return included
    }
  
    this.ignoreRules.forEach(function (rule) {
      // negation means inclusion
      if (rule.negate && included ||
          !rule.negate && !included) {
        // unnecessary
        return
      }
  
      // first, match against /foo/bar
      var match = rule.match("/" + entry)
  
      if (!match) {
        // try with the leading / trimmed off the test
        // eg: foo/bar instead of /foo/bar
        match = rule.match(entry)
      }
  
      // if the entry is a directory, then it will match
      // with a trailing slash. eg: /foo/bar/ or foo/bar/
      if (!match && partial) {
        match = rule.match("/" + entry + "/") ||
                rule.match(entry + "/")
      }
  
      // When including a file with a negated rule, it's
      // relevant if a directory partially matches, since
      // it may then match a file within it.
      // Eg, if you ignore /a, but !/a/b/c
      if (!match && rule.negate && partial) {
        match = rule.match("/" + entry, true) ||
                rule.match(entry, true)
      }
  
      if (match) {
        included = rule.negate
      }
    }, this)
  
    return included
  }
  
  
  IgnoreReader.prototype.sort = function (a, b) {
    var aig = this.ignoreFiles.indexOf(a) !== -1
    , big = this.ignoreFiles.indexOf(b) !== -1
  
    if (aig && !big) return -1
    if (big && !aig) return 1
    return this._sort(a, b)
  }
  
  IgnoreReader.prototype._sort = function (a, b) {
    return 0
  }
  
  function alphasort (a, b) {
    return a === b ? 0
         : a.toLowerCase() > b.toLowerCase() ? 1
         : a.toLowerCase() < b.toLowerCase() ? -1
         : a > b ? 1
         : -1
  }
  

  provide("fstream-ignore", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  var Ignore = require("fstream-ignore")
  , inherits = require("inherits")
  , path = require("path")
  , fs = require("fs")
  
  module.exports = Packer
  
  inherits(Packer, Ignore)
  
  function Packer (props) {
    if (!(this instanceof Packer)) {
      return new Packer(props)
    }
  
    if (typeof props === "string") {
      props = { path: props }
    }
  
    props.ignoreFiles = [ ".npmignore",
                          ".gitignore",
                          "package.json" ]
  
    Ignore.call(this, props)
  
    this.bundled = props.bundled
    this.bundleLinks = props.bundleLinks
    this.package = props.package
  
    // only do the magic bundling stuff for the node_modules folder that
    // lives right next to a package.json file.
    this.bundleMagic = this.parent &&
                       this.parent.packageRoot &&
                       this.basename === "node_modules"
  
    // in a node_modules folder, resolve symbolic links to
    // bundled dependencies when creating the package.
    props.follow = this.follow = this.bundleMagic
    // console.error("follow?", this.path, props.follow)
  
    if (this === this.root ||
        this.parent &&
        this.parent.bundleMagic &&
        this.basename.charAt(0) !== ".") {
      this.readBundledLinks()
    }
  
  
    this.on("entryStat", function (entry, props) {
      // files should *always* get into tarballs
      // in a user-writable state, even if they're
      // being installed from some wackey vm-mounted
      // read-only filesystem.
      entry.mode = props.mode = props.mode | 0200
    })
  }
  
  Packer.prototype.readBundledLinks = function () {
    if (this._paused) {
      this.once("resume", this.addIgnoreFiles)
      return
    }
  
    this.pause()
    fs.readdir(this.path + "/node_modules", function (er, list) {
      // no harm if there's no bundle
      var l = list && list.length
      if (er || l === 0) return this.resume()
  
      var errState = null
      , then = function then (er) {
        if (errState) return
        if (er) return errState = er, this.resume()
        if (-- l === 0) return this.resume()
      }.bind(this)
  
      list.forEach(function (pkg) {
        if (pkg.charAt(0) === ".") return then()
        var pd = this.path + "/node_modules/" + pkg
        fs.realpath(pd, function (er, rp) {
          if (er) return then()
          this.bundleLinks = this.bundleLinks || {}
          this.bundleLinks[pkg] = rp
          then()
        }.bind(this))
      }, this)
    }.bind(this))
  }
  
  Packer.prototype.applyIgnores = function (entry, partial, entryObj) {
    // package.json files can never be ignored.
    if (entry === "package.json") return true
  
    // special rules.  see below.
    if (entry === "node_modules" && this.packageRoot) return true
  
    // some files are *never* allowed under any circumstances
    if (entry === ".git" ||
        entry === ".lock-wscript" ||
        entry.match(/^\.wafpickle-[0-9]+$/) ||
        entry === "CVS" ||
        entry === ".svn" ||
        entry === ".hg" ||
        entry.match(/^\..*\.swp$/) ||
        entry === ".DS_Store" ||
        entry.match(/^\._/) ||
        entry === "npm-debug.log"
      ) {
      return false
    }
  
    // in a node_modules folder, we only include bundled dependencies
    // also, prevent packages in node_modules from being affected
    // by rules set in the containing package, so that
    // bundles don't get busted.
    // Also, once in a bundle, everything is installed as-is
    // To prevent infinite cycles in the case of cyclic deps that are
    // linked with npm link, even in a bundle, deps are only bundled
    // if they're not already present at a higher level.
    if (this.bundleMagic) {
      // bubbling up.  stop here and allow anything the bundled pkg allows
      if (entry.indexOf("/") !== -1) return true
  
      // never include the .bin.  It's typically full of platform-specific
      // stuff like symlinks and .cmd files anyway.
      if (entry === ".bin") return false
  
      var shouldBundle = false
      // the package root.
      var p = this.parent
      // the package before this one.
      var pp = p && p.parent
  
      // if this entry has already been bundled, and is a symlink,
      // and it is the *same* symlink as this one, then exclude it.
      if (pp && pp.bundleLinks && this.bundleLinks &&
          pp.bundleLinks[entry] === this.bundleLinks[entry]) {
        return false
      }
  
      // since it's *not* a symbolic link, if we're *already* in a bundle,
      // then we should include everything.
      if (pp && pp.package) {
        return true
      }
  
      // only include it at this point if it's a bundleDependency
      var bd = this.package && this.package.bundleDependencies
      var shouldBundle = bd && bd.indexOf(entry) !== -1
      // if we're not going to bundle it, then it doesn't count as a bundleLink
      // if (this.bundleLinks && !shouldBundle) delete this.bundleLinks[entry]
      return shouldBundle
    }
    // if (this.bundled) return true
  
    return Ignore.prototype.applyIgnores.call(this, entry, partial, entryObj)
  }
  
  Packer.prototype.addIgnoreFiles = function () {
    var entries = this.entries
    // if there's a .npmignore, then we do *not* want to
    // read the .gitignore.
    if (-1 !== entries.indexOf(".npmignore")) {
      var i = entries.indexOf(".gitignore")
      if (i !== -1) {
        entries.splice(i, 1)
      }
    }
  
    this.entries = entries
  
    Ignore.prototype.addIgnoreFiles.call(this)
  }
  
  
  Packer.prototype.readRules = function (buf, e) {
    if (e !== "package.json") {
      return Ignore.prototype.readRules.call(this, buf, e)
    }
  
    buf = buf.toString().trim()
  
    if (buf.length === 0) return []
  
    try {
      var p = this.package = JSON.parse(buf)
    } catch (er) {
      er.file = path.resolve(this.path, e)
      this.error(er)
      return
    }
  
    if (this === this.root) {
      this.bundleLinks = this.bundleLinks || {}
      this.bundleLinks[p.name] = this._path
    }
  
    this.packageRoot = true
    this.emit("package", p)
  
    // make bundle deps predictable
    if (p.bundledDependencies && !p.bundleDependencies) {
      p.bundleDependencies = p.bundledDependencies
      delete p.bundledDependencies
    }
  
    if (!p.files || !Array.isArray(p.files)) return []
  
    // ignore everything except what's in the files array.
    return ["*"].concat(p.files.map(function (f) {
      return "!" + f
    })).concat(p.files.map(function (f) {
      return "!" + f.replace(/\/+$/, "") + "/**"
    }))
  }
  
  Packer.prototype.getChildProps = function (stat) {
    var props = Ignore.prototype.getChildProps.call(this, stat)
  
    props.package = this.package
  
    props.bundled = this.bundled && this.bundled.slice(0)
    props.bundleLinks = this.bundleLinks &&
      Object.create(this.bundleLinks)
  
    // Directories have to be read as Packers
    // otherwise fstream.Reader will create a DirReader instead.
    if (stat.isDirectory()) {
      props.type = this.constructor
    }
  
    // only follow symbolic links directly in the node_modules folder.
    props.follow = false
    return props
  }
  
  
  var order =
    [ "package.json"
    , ".npmignore"
    , ".gitignore"
    , /^README(\.md)?$/
    , "LICENCE"
    , "LICENSE"
    , /\.js$/ ]
  
  Packer.prototype.sort = function (a, b) {
    for (var i = 0, l = order.length; i < l; i ++) {
      var o = order[i]
      if (typeof o === "string") {
        if (a === o) return -1
        if (b === o) return 1
      } else {
        if (a.match(o)) return -1
        if (b.match(o)) return 1
      }
    }
  
    // deps go in the back
    if (a === "node_modules") return 1
    if (b === "node_modules") return -1
  
    return Ignore.prototype.sort.call(this, a, b)
  }
  
  
  
  Packer.prototype.emitEntry = function (entry) {
    if (this._paused) {
      this.once("resume", this.emitEntry.bind(this, entry))
      return
    }
  
    // if there is a .gitignore, then we're going to
    // rename it to .npmignore in the output.
    if (entry.basename === ".gitignore") {
      entry.basename = ".npmignore"
      entry.path = path.resolve(entry.dirname, entry.basename)
    }
  
    // all *.gyp files are renamed to binding.gyp for node-gyp
    // but only when they are in the same folder as a package.json file.
    if (entry.basename.match(/\.gyp$/) &&
        this.entries.indexOf("package.json") !== -1) {
      entry.basename = "binding.gyp"
      entry.path = path.resolve(entry.dirname, entry.basename)
    }
  
    // skip over symbolic links
    if (entry.type === "SymbolicLink") {
      entry.abort()
      return
    }
  
    if (entry.type !== "Directory") {
      // make it so that the folder in the tarball is named "package"
      var h = path.dirname((entry.root || entry).path)
      , t = entry.path.substr(h.length + 1).replace(/^[^\/\\]+/, "package")
      , p = h + "/" + t
  
      entry.path = p
      entry.dirname = path.dirname(p)
      return Ignore.prototype.emitEntry.call(this, entry)
    }
  
    // we don't want empty directories to show up in package
    // tarballs.
    // don't emit entry events for dirs, but still walk through
    // and read them.  This means that we need to proxy up their
    // entry events so that those entries won't be missed, since
    // .pipe() doesn't do anythign special with "child" events, on
    // with "entry" events.
    var me = this
    entry.on("entry", function (e) {
      if (e.parent === entry) {
        e.parent = me
        me.emit("entry", e)
      }
    })
    entry.on("package", this.emit.bind(this, "package"))
  }
  

  provide("fstream-npm", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  ;(function(){
  // windows: running "npm blah" in this folder will invoke WSH, not node.
  if (typeof WScript !== "undefined") {
    WScript.echo("npm does not work when run\n"
                +"with the Windows Scripting Host\n\n"
                +"'cd' to a different directory,\n"
                +"or type 'npm.cmd <args>',\n"
                +"or type 'node npm <args>'.")
    WScript.quit(1)
    return
  }
  
  
  // FIXME there really ought to be a path.split in node core
  require("path").SPLIT_CHAR = process.platform === "win32" ? "\\" : "/"
  
  var EventEmitter = require("events").EventEmitter
    , npm = module.exports = new EventEmitter
    , config = require("./config.js")
    , ini = require("./utils/ini.js")
    , log = require("npmlog")
    , fs = require("graceful-fs")
    , path = require("path")
    , abbrev = require("abbrev")
    , which = require("which")
    , semver = require("semver")
    , findPrefix = require("./utils/find-prefix.js")
    , getUid = require("uid-number")
    , mkdirp = require("mkdirp")
    , slide = require("slide")
    , chain = slide.chain
    , RegClient = require("npm-registry-client")
  
  // /usr/local is often a read-only fs, which is not
  // well handled by node or mkdirp.  Just double-check
  // in the case of errors when making the prefix dirs.
  function mkdir (p, cb) {
    mkdirp(p, function (er, made) {
      // it could be that we couldn't create it, because it
      // already exists, and is on a read-only fs.
      if (er) {
        return fs.stat(p, function (er2, st) {
          if (er2 || !st.isDirectory()) return cb(er)
          return cb(null, made)
        })
      }
      return cb(er, made)
    })
  }
  
  npm.commands = {}
  
  try {
    // startup, ok to do this synchronously
    var j = JSON.parse(fs.readFileSync(
      path.join(__dirname, "../package.json"))+"")
    npm.version = j.version
    npm.nodeVersionRequired = j.engines.node
    if (!semver.satisfies(process.version, j.engines.node)) {
      log.error("unsupported version", [""
                ,"npm requires node version: "+j.engines.node
                ,"And you have: "+process.version
                ,"which is not satisfactory."
                ,""
                ,"Bad things will likely happen.  You have been warned."
                ,""].join("\n"))
    }
  } catch (ex) {
    try {
      log.info("error reading version", ex)
    } catch (er) {}
    npm.version = ex
  }
  
  var commandCache = {}
    // short names for common things
    , aliases = { "rm" : "uninstall"
                , "r" : "uninstall"
                , "un" : "uninstall"
                , "unlink" : "uninstall"
                , "remove" : "uninstall"
                , "rb" : "rebuild"
                , "list" : "ls"
                , "la" : "ls"
                , "ll" : "ls"
                , "ln" : "link"
                , "i" : "install"
                , "up" : "update"
                , "c" : "config"
                , "info" : "view"
                , "show" : "view"
                , "find" : "search"
                , "s" : "search"
                , "se" : "search"
                , "author" : "owner"
                , "home" : "docs"
                , "unstar": "star" // same function
                , "apihelp" : "help"
                , "login": "adduser"
                , "add-user": "adduser"
                }
  
    , aliasNames = Object.keys(aliases)
    // these are filenames in .
    , cmdList = [ "install"
                , "uninstall"
                , "cache"
                , "config"
                , "set"
                , "get"
                , "update"
                , "outdated"
                , "prune"
                , "submodule"
                , "pack"
  
                , "rebuild"
                , "link"
  
                , "publish"
                , "star"
                , "tag"
                , "adduser"
                , "unpublish"
                , "owner"
                , "deprecate"
                , "shrinkwrap"
  
                , "help"
                , "help-search"
                , "ls"
                , "search"
                , "view"
                , "init"
                , "version"
                , "edit"
                , "explore"
                , "docs"
                , "bugs"
                , "faq"
                , "root"
                , "prefix"
                , "bin"
                , "whoami"
  
                , "test"
                , "stop"
                , "start"
                , "restart"
                , "run-script"
                , "completion"
                ]
    , plumbing = [ "build"
                 , "unbuild"
                 , "xmas"
                 , "substack"
                 ]
    , fullList = npm.fullList = cmdList.concat(aliasNames).filter(function (c) {
        return plumbing.indexOf(c) === -1
      })
    , abbrevs = abbrev(fullList)
  
  Object.keys(abbrevs).concat(plumbing).forEach(function addCommand (c) {
    Object.defineProperty(npm.commands, c, { get : function () {
      if (!loaded) throw new Error(
        "Call npm.load(conf, cb) before using this command.\n"+
        "See the README.md or cli.js for example usage.")
      var a = npm.deref(c)
      if (c === "la" || c === "ll") {
        npm.config.set("long", true)
      }
      npm.command = c
      if (commandCache[a]) return commandCache[a]
      var cmd = require(__dirname+"/"+a+".js")
      commandCache[a] = function () {
        var args = Array.prototype.slice.call(arguments, 0)
        if (typeof args[args.length - 1] !== "function") {
          args.push(defaultCb)
        }
        if (args.length === 1) args.unshift([])
        cmd.apply(npm, args)
      }
      Object.keys(cmd).forEach(function (k) {
        commandCache[a][k] = cmd[k]
      })
      return commandCache[a]
    }, enumerable: fullList.indexOf(c) !== -1 })
  
    // make css-case commands callable via camelCase as well
    if (c.match(/\-([a-z])/)) {
      addCommand(c.replace(/\-([a-z])/g, function (a, b) {
        return b.toUpperCase()
      }))
    }
  })
  
  function defaultCb (er, data) {
    if (er) console.error(er.stack || er.message)
    else console.log(data)
  }
  
  npm.deref = function (c) {
    if (!c) return ""
    if (c.match(/[A-Z]/)) c = c.replace(/([A-Z])/g, function (m) {
      return "-" + m.toLowerCase()
    })
    if (plumbing.indexOf(c) !== -1) return c
    var a = abbrevs[c]
    if (aliases[a]) a = aliases[a]
    return a
  }
  
  var loaded = false
    , loading = false
    , loadErr = null
    , loadListeners = []
  
  function loadCb (er) {
    loadListeners.forEach(function (cb) {
      process.nextTick(cb.bind(npm, er, npm))
    })
    loadListeners.length = 0
  }
  
  
  npm.load = function (conf, cb_) {
    if (!cb_ && typeof conf === "function") cb_ = conf , conf = {}
    if (!cb_) cb_ = function () {}
    if (!conf) conf = {}
    loadListeners.push(cb_)
    if (loaded || loadErr) return cb(loadErr)
    if (loading) return
    loading = true
    var onload = true
  
    function cb (er) {
      if (loadErr) return
      loaded = true
      loadCb(loadErr = er)
      if (onload = onload && npm.config.get("onload-script")) {
        require(onload)
        onload = false
      }
    }
  
    log.pause()
  
    load(npm, conf, cb)
  }
  
  
  function load (npm, conf, cb) {
    which(process.argv[0], function (er, node) {
      if (!er && node.toUpperCase() !== process.execPath.toUpperCase()) {
        log.verbose("node symlink", node)
        process.execPath = node
        process.installPrefix = path.resolve(node, "..", "..")
      }
  
      // look up configs
      //console.error("about to look up configs")
  
      ini.resolveConfigs(conf, function (er) {
        log.level = npm.config.get("loglevel")
        log.heading = "npm"
        switch (npm.config.get("color")) {
          case "always": log.enableColor(); break
          case false: log.disableColor(); break
        }
        log.resume()
  
        if (er) return cb(er)
  
        // at this point the configs are all set.
        // go ahead and spin up the registry client.
        npm.registry = new RegClient(
          { registry: npm.config.get("registry")
          , cache: npm.config.get("cache")
          , auth: npm.config.get("_auth")
          , alwaysAuth: npm.config.get("always-auth")
          , email: npm.config.get("email")
          , tag: npm.config.get("tag")
          , ca: npm.config.get("ca")
          , strictSSL: npm.config.get("strict-ssl")
          , userAgent: npm.config.get("user-agent")
          , E404: npm.E404
          , EPUBLISHCONFLICT: npm.EPUBLISHCONFLICT
          , log: log
          })
  
        var umask = parseInt(conf.umask, 8)
        npm.modes = { exec: 0777 & (~umask)
                    , file: 0666 & (~umask)
                    , umask: umask }
  
        chain([ [ loadPrefix, npm, conf ]
              , [ setUser, ini.configList, ini.defaultConfig ]
              , [ loadUid, npm, conf ]
              ], cb)
      })
    })
  }
  
  function loadPrefix (npm, conf, cb) {
    // try to guess at a good node_modules location.
    var p
      , gp
    if (!conf.hasOwnProperty("prefix")) {
      p = process.cwd()
    } else {
      p = npm.config.get("prefix")
    }
    gp = npm.config.get("prefix")
  
    findPrefix(p, function (er, p) {
      Object.defineProperty(npm, "localPrefix",
        { get : function () { return p }
        , set : function (r) { return p = r }
        , enumerable : true
        })
      // the prefix MUST exist, or else nothing works.
      if (!npm.config.get("global")) {
        mkdir(p, next)
      } else {
        next(er)
      }
    })
  
    findPrefix(gp, function (er, gp) {
      Object.defineProperty(npm, "globalPrefix",
        { get : function () { return gp }
        , set : function (r) { return gp = r }
        , enumerable : true
        })
      // the prefix MUST exist, or else nothing works.
      mkdir(gp, next)
    })
  
    var i = 2
      , errState = null
    function next (er) {
      if (errState) return
      if (er) return cb(errState = er)
      if (--i === 0) return cb()
    }
  }
  
  
  function loadUid (npm, conf, cb) {
    // if we're not in unsafe-perm mode, then figure out who
    // to run stuff as.  Do this first, to support `npm update npm -g`
    if (!npm.config.get("unsafe-perm")) {
      getUid(npm.config.get("user"), npm.config.get("group"), cb)
    } else {
      process.nextTick(cb)
    }
  }
  
  function setUser (cl, dc, cb) {
    // If global, leave it as-is.
    // If not global, then set the user to the owner of the prefix folder.
    // Just set the default, so it can be overridden.
    if (cl.get("global")) return cb()
    if (process.env.SUDO_UID) {
      dc.user = +(process.env.SUDO_UID)
      return cb()
    }
  
    var prefix = path.resolve(cl.get("prefix"))
    mkdir(prefix, function (er) {
      if (er) {
        log.error("could not create prefix dir", prefix)
        return cb(er)
      }
      fs.stat(prefix, function (er, st) {
        dc.user = st && st.uid
        return cb(er)
      })
    })
  }
  
  
  npm.config =
    { get : function (key) { return ini.get(key) }
    , set : function (key, val) { return ini.set(key, val, "cli") }
    , del : function (key, val) { return ini.del(key, val, "cli") }
    }
  
  Object.defineProperty(npm, "prefix",
    { get : function () {
        return npm.config.get("global") ? npm.globalPrefix : npm.localPrefix
      }
    , set : function (r) {
        var k = npm.config.get("global") ? "globalPrefix" : "localPrefix"
        return npm[k] = r
      }
    , enumerable : true
    })
  
  Object.defineProperty(npm, "bin",
    { get : function () {
        if (npm.config.get("global")) return npm.globalBin
        return path.resolve(npm.root, ".bin")
      }
    , enumerable : true
    })
  
  Object.defineProperty(npm, "globalBin",
    { get : function () {
        var b = npm.globalPrefix
        if (process.platform !== "win32") b = path.resolve(b, "bin")
        return b
      }
    })
  
  Object.defineProperty(npm, "dir",
    { get : function () {
        if (npm.config.get("global")) return npm.globalDir
        return path.resolve(npm.prefix, "node_modules")
      }
    , enumerable : true
    })
  
  Object.defineProperty(npm, "globalDir",
    { get : function () {
        return (process.platform !== "win32")
             ? path.resolve(npm.globalPrefix, "lib", "node_modules")
             : path.resolve(npm.globalPrefix, "node_modules")
      }
    , enumerable : true
    })
  
  Object.defineProperty(npm, "root",
    { get : function () { return npm.dir } })
  
  Object.defineProperty(npm, "cache",
    { get : function () { return npm.config.get("cache") }
    , set : function (r) { return npm.config.set("cache", r) }
    , enumerable : true
    })
  
  var tmpFolder
  Object.defineProperty(npm, "tmp",
    { get : function () {
        if (!tmpFolder) tmpFolder = "npm-"+Date.now()
        return path.resolve(npm.config.get("tmp"), tmpFolder)
      }
    , enumerable : true
    })
  
  // the better to repl you with
  Object.getOwnPropertyNames(npm.commands).forEach(function (n) {
    if (npm.hasOwnProperty(n)) return
  
    Object.defineProperty(npm, n, { get: function () {
      return function () {
        var args = Array.prototype.slice.call(arguments, 0)
          , cb = defaultCb
  
        if (args.length === 1 && Array.isArray(args[0])) {
          args = args[0]
        }
  
        if (typeof args[args.length - 1] === "function") {
          cb = args.pop()
        }
  
        npm.commands[n](args, cb)
      }
    }, enumerable: false, configurable: true })
  })
  
  if (require.main === module) {
    require("../bin/npm-cli.js")
  }
  })()
  

  provide("npm", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  // Ender: open module JavaScript framework
  // copyright @ded and @fat
  // https://ender.no.de
  // License MIT
  // ==============
  
  process.title = "Ender"
  
  // for Node 0.7+ compatibility
  ;('exists' in require('fs')) && (function () {
    require('path').exists     = require('fs').exists
    require('path').existsSync = require('fs').existsSync
  }())
  
  var colors = require('colors')
    , fs = require('fs')
    , path = require('path')
    , async = require('async')
    , context = null
  
  // ENDER OBJECTS
  // =============
  
    , ENDER = { cmd: require('./ender.cmd')
      , file: require('./ender.file')
      , npm: require('./ender.npm')
      , search: require('./ender.search')
      , get: require('./ender.get')
      , util: require('./ender.util')
      , docs: require('./ender.docs')
      , closure: require('./ender.closure')
      }
  
  // ENDER'S API DEFINITION
  // ======================
  
   , API = module.exports = {
  
        search: ENDER.search
  
      , welcome: function () {
          console.log("Welcome to ENDER - The no-library library".red)
          console.log("-----------------------------------------")
        }
  
      , build: function (packages, options, callback) {
          packages = options.sans ? packages : ENDER.get.special(options).concat(packages)
          packages = ENDER.util.unique(packages)
  
          async.waterfall([
            async.apply(ENDER.npm.install, packages, options)
          , async.apply(ENDER.file.assemble, packages, options, writeSource)
          , writeSource
          ])
  
          function writeSource(err, source) {
            async.parallel([
              async.apply(ENDER.file.output, source, options.output, context, options)
            , async.apply(ENDER.file.uglify, source, options.output, context, options)
            ], callback)
          }
        }
  
      , add: function (newPackages, options, callback) {
          if (!newPackages.length) {
            return console.log('Error: you must specify a package to add.'.yellow)
          }
  
          newPackages = options.sans ? newPackages : ENDER.get.special(options).concat(newPackages)
          newPackages = ENDER.util.unique(newPackages)
  
          async.waterfall([
            async.apply(ENDER.get.buildHistory, options.use)
          , ENDER.cmd.process
          , determinePackagesToAdd
          ])
  
          function determinePackagesToAdd(type, activePackages, activeOptions) {
            options = ENDER.util.merge(activeOptions, options)
            activePackages = ENDER.util.unique(ENDER.get.special(options).concat(activePackages))
  
            async.waterfall([
              async.apply(ENDER.file.constructDependencyTree, activePackages, 'node_modules')
            , function (tree, callback) { ENDER.file.flattenDependencyTree(tree, null, callback) }
            , ENDER.file.validatePaths
            , function (activePackages, uniqueActivePackageNames) { installPackages(type, activePackages, uniqueActivePackageNames) }
            ])
          }
  
          function installPackages(type, activePackages, uniqueActivePackageNames) {
            uniqueActivePackageNames = uniqueActivePackageNames.concat(ENDER.get.special(options))
            newPackages = ENDER.util.reject(newPackages, uniqueActivePackageNames)
            newPackages = ENDER.util.unique(newPackages)
  
            if (!newPackages.length) {
              return console.log('Specified packages already installed.')
            }
  
            uniqueActivePackageNames = ENDER.util.unique(uniqueActivePackageNames.concat(newPackages))
            context = ENDER.cmd.getContext(type, uniqueActivePackageNames, options.context)
  
            async.waterfall([
              async.apply(ENDER.npm.install, newPackages, options)
            , async.apply(ENDER.file.assemble, uniqueActivePackageNames, options)
            , writeSource
            ])
  
            function writeSource(source) {
              async.parallel([
                async.apply(ENDER.file.output, source, options.output, context, options)
              , async.apply(ENDER.file.uglify, source, options.output, context, options)
              ], callback)
            }
          }
  
        }
  
      , remove: function (packagesForRemoval, options, callback) {
          if (!packagesForRemoval.length) {
            return console.log('Error: you must specify a package to remove.'.yellow)
          }
  
          packagesForRemoval = options.sans ? packagesForRemoval : ENDER.get.special(options).concat(packagesForRemoval)
  
          async.waterfall([
            async.apply(ENDER.get.buildHistory, options.use)
          , ENDER.cmd.process
          , removePackages
          ])
  
          function removePackages(type, activePackages, activeOptions) {
            options = ENDER.util.merge(activeOptions, options)
            packagesForRemoval = ENDER.npm.stripVersions(packagesForRemoval)
            activePackages = ENDER.npm.stripVersions(ENDER.util.unique(ENDER.get.special(options).concat(activePackages)))
            packagesForRemoval = ENDER.util.unique(ENDER.util.keep(packagesForRemoval, activePackages))
            packagesForRemoval = ENDER.util.reject(packagesForRemoval, ENDER.get.special(options))
  
            if (!packagesForRemoval.length) {
              console.log('Nothing to uninstall.')
              return callback && callback()
            }
  
            activePackages = ENDER.util.reject(activePackages, packagesForRemoval, true)
            context = ENDER.cmd.getContext(type, ENDER.util.unique(ENDER.cmd.normalize(activePackages), options.context))
  
            async.waterfall([
              async.apply(ENDER.npm.uninstall, packagesForRemoval)
            , async.apply(ENDER.file.assemble, activePackages, options)
            , writeSource
            ])
          }
  
          function writeSource(source) {
            async.parallel([
              async.apply(ENDER.file.output, source, options.output, context, options)
            , async.apply(ENDER.file.uglify, source, options.output, context, options)
            ], callback)
          }
        }
  
      , info: function (packages, options) {
          async.waterfall([
            async.apply(ENDER.get.buildHistory, options.use)
          , ENDER.cmd.process
          , analyzePackages
          ])
  
          function analyzePackages(type, activePackages, activeOptions) {
            options = ENDER.util.merge(activeOptions, options)
            activePackages = ENDER.util.unique(activePackages)
  
            async.series([
              async.apply(ENDER.file.prettyPrintEnderSize, type, options.use)
            , async.apply(ENDER.npm.prettyPrintDependencies, activePackages)
            ])
          }
        }
  
      , refresh: function (type, options) {
          console.log('refreshing build...')
  
          async.waterfall([
            async.apply(ENDER.get.buildHistory, options.use)
          , ENDER.cmd.process
          , refreshBuild
          ])
  
          function refreshBuild(activeType, activePackages, activeOptions) {
             options = ENDER.util.merge(activeOptions, options)
             type = typeof type == 'string' ? type : activeType
             context = ENDER.cmd.getContext(type, activePackages, options.context)
             API[type](activePackages, options);
          }
        }
  
      , help: function (type) {
          if (type.length) {
            console.log(ENDER.docs[type[0]])
          } else {
            console.log(ENDER.docs.overview)
          }
        }
  
      , version: function (args, options, callback) {
          fs.readFile(path.resolve(__dirname, '../package.json'), 'utf-8', function (err, data) {
            if (err)
              throw err
            console.log('Active Version: v' + JSON.parse(data).version)
            callback && callback()
          })
        }
  
      , compile: function (files, options) {
          var enderfile = options.use ? options.use.replace(/(\.js)?$/, '.js') : 'ender.js'
            , outfile = options.output ? options.output.replace(/(\.js)?$/, '.js')
                : enderfile.replace(/\.js$/, '-app.js')
  
          console.log('Compiling', enderfile, 'with', files.join(' '))
          console.log('This might take a minute...'.yellow)
  
          files.unshift(enderfile)
  
          async.waterfall([
            async.apply(ENDER.closure.compile, files, outfile)
          , async.apply(fs.readFile, outfile)
          , ENDER.file.gzip
          , function (data) {
              var size = (Math.round((data.length / 1024) * 10) / 10) + ' kB';
              console.log('Success! Your compiled source is', (size).cyan, 'and available at', outfile.green)
            }
          ])
        }
  
    }
  
  // ALIAS CLI WITH EXTRA METHODS
  // ============================
  
  ENDER.util.merge(API, {
    'set': API.add
  , 'rm': API.remove
  , 'list': API.info
  , 'ls': API.info
  })
  
  // EXPOSE EXEC FOR CLI
  // ===================
  
  module.exports.exec = function (cmd, callback) {
  
    API.welcome()
  
    ENDER.cmd.process(cmd, function(err, type, args, options) {
  
      if (options.help) {
        args = [type]
        type = 'help'
      } else if (type == 'build' && !args.length) {
        args.push('.')
      }
  
      context = ENDER.cmd.getContext(type, args, options.context)
  
      if (API[type]) {
        API[type](args, options, callback)
      } else {
        console.log('sorry, but the method ' + type.yellow + ' doesn\'t exist ' + ':('.cyan)
      }
  
    });
  
  }
  

  provide("ender", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  module.exports = require("./app/wallet-app");

  provide("ender-wallet", module.exports);

  $.ender(module.exports);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*!
    * bean.js - copyright Jacob Thornton 2011
    * https://github.com/fat/bean
    * MIT License
    * special thanks to:
    * dean edwards: http://dean.edwards.name/
    * dperini: https://github.com/dperini/nwevents
    * the entire mootools team: github.com/mootools/mootools-core
    */
  !function (name, context, definition) {
    if (typeof module !== 'undefined') module.exports = definition(name, context);
    else if (typeof define === 'function' && typeof define.amd  === 'object') define(definition);
    else context[name] = definition(name, context);
  }('bean', this, function (name, context) {
    var win = window
      , old = context[name]
      , overOut = /over|out/
      , namespaceRegex = /[^\.]*(?=\..*)\.|.*/
      , nameRegex = /\..*/
      , addEvent = 'addEventListener'
      , attachEvent = 'attachEvent'
      , removeEvent = 'removeEventListener'
      , detachEvent = 'detachEvent'
      , ownerDocument = 'ownerDocument'
      , targetS = 'target'
      , qSA = 'querySelectorAll'
      , doc = document || {}
      , root = doc.documentElement || {}
      , W3C_MODEL = root[addEvent]
      , eventSupport = W3C_MODEL ? addEvent : attachEvent
      , slice = Array.prototype.slice
      , mouseTypeRegex = /click|mouse(?!(.*wheel|scroll))|menu|drag|drop/i
      , mouseWheelTypeRegex = /mouse.*(wheel|scroll)/i
      , textTypeRegex = /^text/i
      , touchTypeRegex = /^touch|^gesture/i
      , ONE = {} // singleton for quick matching making add() do one()
  
      , nativeEvents = (function (hash, events, i) {
          for (i = 0; i < events.length; i++)
            hash[events[i]] = 1
          return hash
        }({}, (
            'click dblclick mouseup mousedown contextmenu ' +                  // mouse buttons
            'mousewheel mousemultiwheel DOMMouseScroll ' +                     // mouse wheel
            'mouseover mouseout mousemove selectstart selectend ' +            // mouse movement
            'keydown keypress keyup ' +                                        // keyboard
            'orientationchange ' +                                             // mobile
            'focus blur change reset select submit ' +                         // form elements
            'load unload beforeunload resize move DOMContentLoaded '+          // window
            'readystatechange message ' +                                      // window
            'error abort scroll ' +                                            // misc
            (W3C_MODEL ? // element.fireEvent('onXYZ'... is not forgiving if we try to fire an event
                         // that doesn't actually exist, so make sure we only do these on newer browsers
              'show ' +                                                          // mouse buttons
              'input invalid ' +                                                 // form elements
              'touchstart touchmove touchend touchcancel ' +                     // touch
              'gesturestart gesturechange gestureend ' +                         // gesture
              'readystatechange pageshow pagehide popstate ' +                   // window
              'hashchange offline online ' +                                     // window
              'afterprint beforeprint ' +                                        // printing
              'dragstart dragenter dragover dragleave drag drop dragend ' +      // dnd
              'loadstart progress suspend emptied stalled loadmetadata ' +       // media
              'loadeddata canplay canplaythrough playing waiting seeking ' +     // media
              'seeked ended durationchange timeupdate play pause ratechange ' +  // media
              'volumechange cuechange ' +                                        // media
              'checking noupdate downloading cached updateready obsolete ' +     // appcache
              '' : '')
          ).split(' ')
        ))
  
      , customEvents = (function () {
          var cdp = 'compareDocumentPosition'
            , isAncestor = cdp in root
                ? function (element, container) {
                    return container[cdp] && (container[cdp](element) & 16) === 16
                  }
                : 'contains' in root
                  ? function (element, container) {
                      container = container.nodeType === 9 || container === window ? root : container
                      return container !== element && container.contains(element)
                    }
                  : function (element, container) {
                      while (element = element.parentNode) if (element === container) return 1
                      return 0
                    }
  
          function check(event) {
            var related = event.relatedTarget
            return !related
              ? related === null
              : (related !== this && related.prefix !== 'xul' && !/document/.test(this.toString()) && !isAncestor(related, this))
          }
  
          return {
              mouseenter: { base: 'mouseover', condition: check }
            , mouseleave: { base: 'mouseout', condition: check }
            , mousewheel: { base: /Firefox/.test(navigator.userAgent) ? 'DOMMouseScroll' : 'mousewheel' }
          }
        }())
  
      , fixEvent = (function () {
          var commonProps = 'altKey attrChange attrName bubbles cancelable ctrlKey currentTarget detail eventPhase getModifierState isTrusted metaKey relatedNode relatedTarget shiftKey srcElement target timeStamp type view which'.split(' ')
            , mouseProps = commonProps.concat('button buttons clientX clientY dataTransfer fromElement offsetX offsetY pageX pageY screenX screenY toElement'.split(' '))
            , mouseWheelProps = mouseProps.concat('wheelDelta wheelDeltaX wheelDeltaY wheelDeltaZ axis'.split(' ')) // 'axis' is FF specific
            , keyProps = commonProps.concat('char charCode key keyCode keyIdentifier keyLocation'.split(' '))
            , textProps = commonProps.concat(['data'])
            , touchProps = commonProps.concat('touches targetTouches changedTouches scale rotation'.split(' '))
            , messageProps = commonProps.concat(['data', 'origin', 'source'])
            , preventDefault = 'preventDefault'
            , createPreventDefault = function (event) {
                return function () {
                  if (event[preventDefault])
                    event[preventDefault]()
                  else
                    event.returnValue = false
                }
              }
            , stopPropagation = 'stopPropagation'
            , createStopPropagation = function (event) {
                return function () {
                  if (event[stopPropagation])
                    event[stopPropagation]()
                  else
                    event.cancelBubble = true
                }
              }
            , createStop = function (synEvent) {
                return function () {
                  synEvent[preventDefault]()
                  synEvent[stopPropagation]()
                  synEvent.stopped = true
                }
              }
            , copyProps = function (event, result, props) {
                var i, p
                for (i = props.length; i--;) {
                  p = props[i]
                  if (!(p in result) && p in event) result[p] = event[p]
                }
              }
  
          return function (event, isNative) {
            var result = { originalEvent: event, isNative: isNative }
            if (!event)
              return result
  
            var props
              , type = event.type
              , target = event[targetS] || event.srcElement
  
            result[preventDefault] = createPreventDefault(event)
            result[stopPropagation] = createStopPropagation(event)
            result.stop = createStop(result)
            result[targetS] = target && target.nodeType === 3 ? target.parentNode : target
  
            if (isNative) { // we only need basic augmentation on custom events, the rest is too expensive
              if (type.indexOf('key') !== -1) {
                props = keyProps
                result.keyCode = event.keyCode || event.which
              } else if (mouseTypeRegex.test(type)) {
                props = mouseProps
                result.rightClick = event.which === 3 || event.button === 2
                result.pos = { x: 0, y: 0 }
                if (event.pageX || event.pageY) {
                  result.clientX = event.pageX
                  result.clientY = event.pageY
                } else if (event.clientX || event.clientY) {
                  result.clientX = event.clientX + doc.body.scrollLeft + root.scrollLeft
                  result.clientY = event.clientY + doc.body.scrollTop + root.scrollTop
                }
                if (overOut.test(type))
                  result.relatedTarget = event.relatedTarget || event[(type === 'mouseover' ? 'from' : 'to') + 'Element']
              } else if (touchTypeRegex.test(type)) {
                props = touchProps
              } else if (mouseWheelTypeRegex.test(type)) {
                props = mouseWheelProps
              } else if (textTypeRegex.test(type)) {
                props = textProps
              } else if (type === 'message') {
                props = messageProps
              }
              copyProps(event, result, props || commonProps)
            }
            return result
          }
        }())
  
        // if we're in old IE we can't do onpropertychange on doc or win so we use doc.documentElement for both
      , targetElement = function (element, isNative) {
          return !W3C_MODEL && !isNative && (element === doc || element === win) ? root : element
        }
  
        // we use one of these per listener, of any type
      , RegEntry = (function () {
          function entry(element, type, handler, original, namespaces) {
            var isNative = this.isNative = nativeEvents[type] && element[eventSupport]
            this.element = element
            this.type = type
            this.handler = handler
            this.original = original
            this.namespaces = namespaces
            this.custom = customEvents[type]
            this.eventType = W3C_MODEL || isNative ? type : 'propertychange'
            this.customType = !W3C_MODEL && !isNative && type
            this[targetS] = targetElement(element, isNative)
            this[eventSupport] = this[targetS][eventSupport]
          }
  
          entry.prototype = {
              // given a list of namespaces, is our entry in any of them?
              inNamespaces: function (checkNamespaces) {
                var i, j
                if (!checkNamespaces)
                  return true
                if (!this.namespaces)
                  return false
                for (i = checkNamespaces.length; i--;) {
                  for (j = this.namespaces.length; j--;) {
                    if (checkNamespaces[i] === this.namespaces[j])
                      return true
                  }
                }
                return false
              }
  
              // match by element, original fn (opt), handler fn (opt)
            , matches: function (checkElement, checkOriginal, checkHandler) {
                return this.element === checkElement &&
                  (!checkOriginal || this.original === checkOriginal) &&
                  (!checkHandler || this.handler === checkHandler)
              }
          }
  
          return entry
        }())
  
      , registry = (function () {
          // our map stores arrays by event type, just because it's better than storing
          // everything in a single array. uses '$' as a prefix for the keys for safety
          var map = {}
  
            // generic functional search of our registry for matching listeners,
            // `fn` returns false to break out of the loop
            , forAll = function (element, type, original, handler, fn) {
                if (!type || type === '*') {
                  // search the whole registry
                  for (var t in map) {
                    if (t.charAt(0) === '$')
                      forAll(element, t.substr(1), original, handler, fn)
                  }
                } else {
                  var i = 0, l, list = map['$' + type], all = element === '*'
                  if (!list)
                    return
                  for (l = list.length; i < l; i++) {
                    if (all || list[i].matches(element, original, handler))
                      if (!fn(list[i], list, i, type))
                        return
                  }
                }
              }
  
            , has = function (element, type, original) {
                // we're not using forAll here simply because it's a bit slower and this
                // needs to be fast
                var i, list = map['$' + type]
                if (list) {
                  for (i = list.length; i--;) {
                    if (list[i].matches(element, original, null))
                      return true
                  }
                }
                return false
              }
  
            , get = function (element, type, original) {
                var entries = []
                forAll(element, type, original, null, function (entry) { return entries.push(entry) })
                return entries
              }
  
            , put = function (entry) {
                (map['$' + entry.type] || (map['$' + entry.type] = [])).push(entry)
                return entry
              }
  
            , del = function (entry) {
                forAll(entry.element, entry.type, null, entry.handler, function (entry, list, i) {
                  list.splice(i, 1)
                  if (list.length === 0)
                    delete map['$' + entry.type]
                  return false
                })
              }
  
              // dump all entries, used for onunload
            , entries = function () {
                var t, entries = []
                for (t in map) {
                  if (t.charAt(0) === '$')
                    entries = entries.concat(map[t])
                }
                return entries
              }
  
          return { has: has, get: get, put: put, del: del, entries: entries }
        }())
  
      , selectorEngine = doc[qSA]
          ? function (s, r) {
              return r[qSA](s)
            }
          : function () {
              throw new Error('Bean: No selector engine installed') // eeek
            }
  
      , setSelectorEngine = function (e) {
          selectorEngine = e
        }
  
        // add and remove listeners to DOM elements
      , listener = W3C_MODEL ? function (element, type, fn, add) {
          element[add ? addEvent : removeEvent](type, fn, false)
        } : function (element, type, fn, add, custom) {
          if (custom && add && element['_on' + custom] === null)
            element['_on' + custom] = 0
          element[add ? attachEvent : detachEvent]('on' + type, fn)
        }
  
      , nativeHandler = function (element, fn, args) {
          var beanDel = fn.__beanDel
            , handler = function (event) {
            event = fixEvent(event || ((this[ownerDocument] || this.document || this).parentWindow || win).event, true)
            if (beanDel) // delegated event, fix the fix
              event.currentTarget = beanDel.ft(event[targetS], element)
            return fn.apply(element, [event].concat(args))
          }
          handler.__beanDel = beanDel
          return handler
        }
  
      , customHandler = function (element, fn, type, condition, args, isNative) {
          var beanDel = fn.__beanDel
            , handler = function (event) {
            var target = beanDel ? beanDel.ft(event[targetS], element) : this // deleated event
            if (condition ? condition.apply(target, arguments) : W3C_MODEL ? true : event && event.propertyName === '_on' + type || !event) {
              if (event) {
                event = fixEvent(event || ((this[ownerDocument] || this.document || this).parentWindow || win).event, isNative)
                event.currentTarget = target
              }
              fn.apply(element, event && (!args || args.length === 0) ? arguments : slice.call(arguments, event ? 0 : 1).concat(args))
            }
          }
          handler.__beanDel = beanDel
          return handler
        }
  
      , once = function (rm, element, type, fn, originalFn) {
          // wrap the handler in a handler that does a remove as well
          return function () {
            rm(element, type, originalFn)
            fn.apply(this, arguments)
          }
        }
  
      , removeListener = function (element, orgType, handler, namespaces) {
          var i, l, entry
            , type = (orgType && orgType.replace(nameRegex, ''))
            , handlers = registry.get(element, type, handler)
  
          for (i = 0, l = handlers.length; i < l; i++) {
            if (handlers[i].inNamespaces(namespaces)) {
              if ((entry = handlers[i])[eventSupport])
                listener(entry[targetS], entry.eventType, entry.handler, false, entry.type)
              // TODO: this is problematic, we have a registry.get() and registry.del() that
              // both do registry searches so we waste cycles doing this. Needs to be rolled into
              // a single registry.forAll(fn) that removes while finding, but the catch is that
              // we'll be splicing the arrays that we're iterating over. Needs extra tests to
              // make sure we don't screw it up. @rvagg
              registry.del(entry)
            }
          }
        }
  
      , addListener = function (element, orgType, fn, originalFn, args) {
          var entry
            , type = orgType.replace(nameRegex, '')
            , namespaces = orgType.replace(namespaceRegex, '').split('.')
  
          if (registry.has(element, type, fn))
            return element // no dupe
          if (type === 'unload')
            fn = once(removeListener, element, type, fn, originalFn) // self clean-up
          if (customEvents[type]) {
            if (customEvents[type].condition)
              fn = customHandler(element, fn, type, customEvents[type].condition, args, true)
            type = customEvents[type].base || type
          }
          entry = registry.put(new RegEntry(element, type, fn, originalFn, namespaces[0] && namespaces))
          entry.handler = entry.isNative ?
            nativeHandler(element, entry.handler, args) :
            customHandler(element, entry.handler, type, false, args, false)
          if (entry[eventSupport])
            listener(entry[targetS], entry.eventType, entry.handler, true, entry.customType)
        }
  
      , del = function (selector, fn, $) {
              //TODO: findTarget (therefore $) is called twice, once for match and once for
              // setting e.currentTarget, fix this so it's only needed once
          var findTarget = function (target, root) {
                var i, array = typeof selector === 'string' ? $(selector, root) : selector
                for (; target && target !== root; target = target.parentNode) {
                  for (i = array.length; i--;) {
                    if (array[i] === target)
                      return target
                  }
                }
              }
            , handler = function (e) {
                var match = findTarget(e[targetS], this)
                match && fn.apply(match, arguments)
              }
  
          handler.__beanDel = {
              ft: findTarget // attach it here for customEvents to use too
            , selector: selector
            , $: $
          }
          return handler
        }
  
      , remove = function (element, typeSpec, fn) {
          var k, type, namespaces, i
            , rm = removeListener
            , isString = typeSpec && typeof typeSpec === 'string'
  
          if (isString && typeSpec.indexOf(' ') > 0) {
            // remove(el, 't1 t2 t3', fn) or remove(el, 't1 t2 t3')
            typeSpec = typeSpec.split(' ')
            for (i = typeSpec.length; i--;)
              remove(element, typeSpec[i], fn)
            return element
          }
          type = isString && typeSpec.replace(nameRegex, '')
          if (type && customEvents[type])
            type = customEvents[type].type
          if (!typeSpec || isString) {
            // remove(el) or remove(el, t1.ns) or remove(el, .ns) or remove(el, .ns1.ns2.ns3)
            if (namespaces = isString && typeSpec.replace(namespaceRegex, ''))
              namespaces = namespaces.split('.')
            rm(element, type, fn, namespaces)
          } else if (typeof typeSpec === 'function') {
            // remove(el, fn)
            rm(element, null, typeSpec)
          } else {
            // remove(el, { t1: fn1, t2, fn2 })
            for (k in typeSpec) {
              if (typeSpec.hasOwnProperty(k))
                remove(element, k, typeSpec[k])
            }
          }
          return element
        }
  
        // 5th argument, $=selector engine, is deprecated and will be removed
      , add = function (element, events, fn, delfn, $) {
          var type, types, i, args
            , originalFn = fn
            , isDel = fn && typeof fn === 'string'
  
          if (events && !fn && typeof events === 'object') {
            for (type in events) {
              if (events.hasOwnProperty(type))
                add.apply(this, [ element, type, events[type] ])
            }
          } else {
            args = arguments.length > 3 ? slice.call(arguments, 3) : []
            types = (isDel ? fn : events).split(' ')
            isDel && (fn = del(events, (originalFn = delfn), $ || selectorEngine)) && (args = slice.call(args, 1))
            // special case for one()
            this === ONE && (fn = once(remove, element, events, fn, originalFn))
            for (i = types.length; i--;) addListener(element, types[i], fn, originalFn, args)
          }
          return element
        }
  
      , one = function () {
          return add.apply(ONE, arguments)
        }
  
      , fireListener = W3C_MODEL ? function (isNative, type, element) {
          var evt = doc.createEvent(isNative ? 'HTMLEvents' : 'UIEvents')
          evt[isNative ? 'initEvent' : 'initUIEvent'](type, true, true, win, 1)
          element.dispatchEvent(evt)
        } : function (isNative, type, element) {
          element = targetElement(element, isNative)
          // if not-native then we're using onpropertychange so we just increment a custom property
          isNative ? element.fireEvent('on' + type, doc.createEventObject()) : element['_on' + type]++
        }
  
      , fire = function (element, type, args) {
          var i, j, l, names, handlers
            , types = type.split(' ')
  
          for (i = types.length; i--;) {
            type = types[i].replace(nameRegex, '')
            if (names = types[i].replace(namespaceRegex, ''))
              names = names.split('.')
            if (!names && !args && element[eventSupport]) {
              fireListener(nativeEvents[type], type, element)
            } else {
              // non-native event, either because of a namespace, arguments or a non DOM element
              // iterate over all listeners and manually 'fire'
              handlers = registry.get(element, type)
              args = [false].concat(args)
              for (j = 0, l = handlers.length; j < l; j++) {
                if (handlers[j].inNamespaces(names))
                  handlers[j].handler.apply(element, args)
              }
            }
          }
          return element
        }
  
      , clone = function (element, from, type) {
          var i = 0
            , handlers = registry.get(from, type)
            , l = handlers.length
            , args, beanDel
  
          for (;i < l; i++) {
            if (handlers[i].original) {
              beanDel = handlers[i].handler.__beanDel
              if (beanDel) {
                args = [ element, beanDel.selector, handlers[i].type, handlers[i].original, beanDel.$]
              } else
                args = [ element, handlers[i].type, handlers[i].original ]
              add.apply(null, args)
            }
          }
          return element
        }
  
      , bean = {
            add: add
          , one: one
          , remove: remove
          , clone: clone
          , fire: fire
          , setSelectorEngine: setSelectorEngine
          , noConflict: function () {
              context[name] = old
              return this
            }
        }
  
    if (win[attachEvent]) {
      // for IE, clean up on unload to avoid leaks
      var cleanup = function () {
        var i, entries = registry.entries()
        for (i in entries) {
          if (entries[i].type && entries[i].type !== 'unload')
            remove(entries[i].element, entries[i].type)
        }
        win[detachEvent]('onunload', cleanup)
        win.CollectGarbage && win.CollectGarbage()
      }
      win[attachEvent]('onunload', cleanup)
    }
  
    return bean
  })
  

  provide("bean", module.exports);

  !function ($) {
    var b = require('bean')
      , integrate = function (method, type, method2) {
          var _args = type ? [type] : []
          return function () {
            for (var i = 0, l = this.length; i < l; i++) {
              if (!arguments.length && method == 'add' && type) method = 'fire'
              b[method].apply(this, [this[i]].concat(_args, Array.prototype.slice.call(arguments, 0)))
            }
            return this
          }
        }
      , add = integrate('add')
      , remove = integrate('remove')
      , fire = integrate('fire')
  
      , methods = {
            on: add // NOTE: .on() is likely to change in the near future, don't rely on this as-is see https://github.com/fat/bean/issues/55
          , addListener: add
          , bind: add
          , listen: add
          , delegate: add
  
          , one: integrate('one')
  
          , off: remove
          , unbind: remove
          , unlisten: remove
          , removeListener: remove
          , undelegate: remove
  
          , emit: fire
          , trigger: fire
  
          , cloneEvents: integrate('clone')
  
          , hover: function (enter, leave, i) { // i for internal
              for (i = this.length; i--;) {
                b.add.call(this, this[i], 'mouseenter', enter)
                b.add.call(this, this[i], 'mouseleave', leave)
              }
              return this
            }
        }
  
      , shortcuts =
           ('blur change click dblclick error focus focusin focusout keydown keypress '
          + 'keyup load mousedown mouseenter mouseleave mouseout mouseover mouseup '
          + 'mousemove resize scroll select submit unload').split(' ')
  
    for (var i = shortcuts.length; i--;) {
      methods[shortcuts[i]] = integrate('add', shortcuts[i])
    }
  
    b.setSelectorEngine($)
  
    $.ender(methods, true)
  }(ender)
  

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  /*
      http://www.JSON.org/json2.js
      2011-02-23
  
      Public Domain.
  
      NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
  
      See http://www.JSON.org/js.html
  
  
      This code should be minified before deployment.
      See http://javascript.crockford.com/jsmin.html
  
      USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
      NOT CONTROL.
  
  
      This file creates a global JSON object containing two methods: stringify
      and parse.
  
          JSON.stringify(value, replacer, space)
              value       any JavaScript value, usually an object or array.
  
              replacer    an optional parameter that determines how object
                          values are stringified for objects. It can be a
                          function or an array of strings.
  
              space       an optional parameter that specifies the indentation
                          of nested structures. If it is omitted, the text will
                          be packed without extra whitespace. If it is a number,
                          it will specify the number of spaces to indent at each
                          level. If it is a string (such as '\t' or '&nbsp;'),
                          it contains the characters used to indent at each level.
  
              This method produces a JSON text from a JavaScript value.
  
              When an object value is found, if the object contains a toJSON
              method, its toJSON method will be called and the result will be
              stringified. A toJSON method does not serialize: it returns the
              value represented by the name/value pair that should be serialized,
              or undefined if nothing should be serialized. The toJSON method
              will be passed the key associated with the value, and this will be
              bound to the value
  
              For example, this would serialize Dates as ISO strings.
  
                  Date.prototype.toJSON = function (key) {
                      function f(n) {
                          // Format integers to have at least two digits.
                          return n < 10 ? '0' + n : n;
                      }
  
                      return this.getUTCFullYear()   + '-' +
                           f(this.getUTCMonth() + 1) + '-' +
                           f(this.getUTCDate())      + 'T' +
                           f(this.getUTCHours())     + ':' +
                           f(this.getUTCMinutes())   + ':' +
                           f(this.getUTCSeconds())   + 'Z';
                  };
  
              You can provide an optional replacer method. It will be passed the
              key and value of each member, with this bound to the containing
              object. The value that is returned from your method will be
              serialized. If your method returns undefined, then the member will
              be excluded from the serialization.
  
              If the replacer parameter is an array of strings, then it will be
              used to select the members to be serialized. It filters the results
              such that only members with keys listed in the replacer array are
              stringified.
  
              Values that do not have JSON representations, such as undefined or
              functions, will not be serialized. Such values in objects will be
              dropped; in arrays they will be replaced with null. You can use
              a replacer function to replace those with JSON values.
              JSON.stringify(undefined) returns undefined.
  
              The optional space parameter produces a stringification of the
              value that is filled with line breaks and indentation to make it
              easier to read.
  
              If the space parameter is a non-empty string, then that string will
              be used for indentation. If the space parameter is a number, then
              the indentation will be that many spaces.
  
              Example:
  
              text = JSON.stringify(['e', {pluribus: 'unum'}]);
              // text is '["e",{"pluribus":"unum"}]'
  
  
              text = JSON.stringify(['e', {pluribus: 'unum'}], null, '\t');
              // text is '[\n\t"e",\n\t{\n\t\t"pluribus": "unum"\n\t}\n]'
  
              text = JSON.stringify([new Date()], function (key, value) {
                  return this[key] instanceof Date ?
                      'Date(' + this[key] + ')' : value;
              });
              // text is '["Date(---current time---)"]'
  
  
          JSON.parse(text, reviver)
              This method parses a JSON text to produce an object or array.
              It can throw a SyntaxError exception.
  
              The optional reviver parameter is a function that can filter and
              transform the results. It receives each of the keys and values,
              and its return value is used instead of the original value.
              If it returns what it received, then the structure is not modified.
              If it returns undefined then the member is deleted.
  
              Example:
  
              // Parse the text. Values that look like ISO date strings will
              // be converted to Date objects.
  
              myData = JSON.parse(text, function (key, value) {
                  var a;
                  if (typeof value === 'string') {
                      a =
  /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                      if (a) {
                          return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],
                              +a[5], +a[6]));
                      }
                  }
                  return value;
              });
  
              myData = JSON.parse('["Date(09/09/2001)"]', function (key, value) {
                  var d;
                  if (typeof value === 'string' &&
                          value.slice(0, 5) === 'Date(' &&
                          value.slice(-1) === ')') {
                      d = new Date(value.slice(5, -1));
                      if (d) {
                          return d;
                      }
                  }
                  return value;
              });
  
  
      This is a reference implementation. You are free to copy, modify, or
      redistribute.
  */
  
  /*jslint evil: true, strict: false, regexp: false */
  
  /*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", apply,
      call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,
      getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,
      lastIndex, length, parse, prototype, push, replace, slice, stringify,
      test, toJSON, toString, valueOf
  */
  
  
  // Create a JSON object only if one does not already exist. We create the
  // methods in a closure to avoid creating global variables.
  
  !function (context) {
      "use strict";
  
      var JSON;
      if (context.JSON) {
          return;
      } else {
          context['JSON'] = JSON = {};
      }
  
      function f(n) {
          // Format integers to have at least two digits.
          return n < 10 ? '0' + n : n;
      }
  
      if (typeof Date.prototype.toJSON !== 'function') {
  
          Date.prototype.toJSON = function (key) {
  
              return isFinite(this.valueOf()) ?
                  this.getUTCFullYear()     + '-' +
                  f(this.getUTCMonth() + 1) + '-' +
                  f(this.getUTCDate())      + 'T' +
                  f(this.getUTCHours())     + ':' +
                  f(this.getUTCMinutes())   + ':' +
                  f(this.getUTCSeconds())   + 'Z' : null;
          };
  
          String.prototype.toJSON      =
              Number.prototype.toJSON  =
              Boolean.prototype.toJSON = function (key) {
                  return this.valueOf();
              };
      }
  
      var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
          escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
          gap,
          indent,
          meta = {    // table of character substitutions
              '\b': '\\b',
              '\t': '\\t',
              '\n': '\\n',
              '\f': '\\f',
              '\r': '\\r',
              '"' : '\\"',
              '\\': '\\\\'
          },
          rep;
  
  
      function quote(string) {
  
  // If the string contains no control characters, no quote characters, and no
  // backslash characters, then we can safely slap some quotes around it.
  // Otherwise we must also replace the offending characters with safe escape
  // sequences.
  
          escapable.lastIndex = 0;
          return escapable.test(string) ? '"' + string.replace(escapable, function (a) {
              var c = meta[a];
              return typeof c === 'string' ? c :
                  '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
          }) + '"' : '"' + string + '"';
      }
  
  
      function str(key, holder) {
  
  // Produce a string from holder[key].
  
          var i,          // The loop counter.
              k,          // The member key.
              v,          // The member value.
              length,
              mind = gap,
              partial,
              value = holder[key];
  
  // If the value has a toJSON method, call it to obtain a replacement value.
  
          if (value && typeof value === 'object' &&
                  typeof value.toJSON === 'function') {
              value = value.toJSON(key);
          }
  
  // If we were called with a replacer function, then call the replacer to
  // obtain a replacement value.
  
          if (typeof rep === 'function') {
              value = rep.call(holder, key, value);
          }
  
  // What happens next depends on the value's type.
  
          switch (typeof value) {
          case 'string':
              return quote(value);
  
          case 'number':
  
  // JSON numbers must be finite. Encode non-finite numbers as null.
  
              return isFinite(value) ? String(value) : 'null';
  
          case 'boolean':
          case 'null':
  
  // If the value is a boolean or null, convert it to a string. Note:
  // typeof null does not produce 'null'. The case is included here in
  // the remote chance that this gets fixed someday.
  
              return String(value);
  
  // If the type is 'object', we might be dealing with an object or an array or
  // null.
  
          case 'object':
  
  // Due to a specification blunder in ECMAScript, typeof null is 'object',
  // so watch out for that case.
  
              if (!value) {
                  return 'null';
              }
  
  // Make an array to hold the partial results of stringifying this object value.
  
              gap += indent;
              partial = [];
  
  // Is the value an array?
  
              if (Object.prototype.toString.apply(value) === '[object Array]') {
  
  // The value is an array. Stringify every element. Use null as a placeholder
  // for non-JSON values.
  
                  length = value.length;
                  for (i = 0; i < length; i += 1) {
                      partial[i] = str(i, value) || 'null';
                  }
  
  // Join all of the elements together, separated with commas, and wrap them in
  // brackets.
  
                  v = partial.length === 0 ? '[]' : gap ?
                      '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']' :
                      '[' + partial.join(',') + ']';
                  gap = mind;
                  return v;
              }
  
  // If the replacer is an array, use it to select the members to be stringified.
  
              if (rep && typeof rep === 'object') {
                  length = rep.length;
                  for (i = 0; i < length; i += 1) {
                      if (typeof rep[i] === 'string') {
                          k = rep[i];
                          v = str(k, value);
                          if (v) {
                              partial.push(quote(k) + (gap ? ': ' : ':') + v);
                          }
                      }
                  }
              } else {
  
  // Otherwise, iterate through all of the keys in the object.
  
                  for (k in value) {
                      if (Object.prototype.hasOwnProperty.call(value, k)) {
                          v = str(k, value);
                          if (v) {
                              partial.push(quote(k) + (gap ? ': ' : ':') + v);
                          }
                      }
                  }
              }
  
  // Join all of the member texts together, separated with commas,
  // and wrap them in braces.
  
              v = partial.length === 0 ? '{}' : gap ?
                  '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}' :
                  '{' + partial.join(',') + '}';
              gap = mind;
              return v;
          }
      }
  
  // If the JSON object does not yet have a stringify method, give it one.
  
      if (typeof JSON.stringify !== 'function') {
          JSON.stringify = function (value, replacer, space) {
  
  // The stringify method takes a value and an optional replacer, and an optional
  // space parameter, and returns a JSON text. The replacer can be a function
  // that can replace values, or an array of strings that will select the keys.
  // A default replacer method can be provided. Use of the space parameter can
  // produce text that is more easily readable.
  
              var i;
              gap = '';
              indent = '';
  
  // If the space parameter is a number, make an indent string containing that
  // many spaces.
  
              if (typeof space === 'number') {
                  for (i = 0; i < space; i += 1) {
                      indent += ' ';
                  }
  
  // If the space parameter is a string, it will be used as the indent string.
  
              } else if (typeof space === 'string') {
                  indent = space;
              }
  
  // If there is a replacer, it must be a function or an array.
  // Otherwise, throw an error.
  
              rep = replacer;
              if (replacer && typeof replacer !== 'function' &&
                      (typeof replacer !== 'object' ||
                      typeof replacer.length !== 'number')) {
                  throw new Error('JSON.stringify');
              }
  
  // Make a fake root object containing our value under the key of ''.
  // Return the result of stringifying the value.
  
              return str('', {'': value});
          };
      }
  
  
  // If the JSON object does not yet have a parse method, give it one.
  
      if (typeof JSON.parse !== 'function') {
          JSON.parse = function (text, reviver) {
  
  // The parse method takes a text and an optional reviver function, and returns
  // a JavaScript value if the text is a valid JSON text.
  
              var j;
  
              function walk(holder, key) {
  
  // The walk method is used to recursively walk the resulting structure so
  // that modifications can be made.
  
                  var k, v, value = holder[key];
                  if (value && typeof value === 'object') {
                      for (k in value) {
                          if (Object.prototype.hasOwnProperty.call(value, k)) {
                              v = walk(value, k);
                              if (v !== undefined) {
                                  value[k] = v;
                              } else {
                                  delete value[k];
                              }
                          }
                      }
                  }
                  return reviver.call(holder, key, value);
              }
  
  
  // Parsing happens in four stages. In the first stage, we replace certain
  // Unicode characters with escape sequences. JavaScript handles many characters
  // incorrectly, either silently deleting them, or treating them as line endings.
  
              text = String(text);
              cx.lastIndex = 0;
              if (cx.test(text)) {
                  text = text.replace(cx, function (a) {
                      return '\\u' +
                          ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                  });
              }
  
  // In the second stage, we run the text against regular expressions that look
  // for non-JSON patterns. We are especially concerned with '()' and 'new'
  // because they can cause invocation, and '=' because it can cause mutation.
  // But just to be safe, we want to reject all unexpected forms.
  
  // We split the second stage into 4 regexp operations in order to work around
  // crippling inefficiencies in IE's and Safari's regexp engines. First we
  // replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
  // replace all simple value tokens with ']' characters. Third, we delete all
  // open brackets that follow a colon or comma or that begin the text. Finally,
  // we look to see that the remaining characters are only whitespace or ']' or
  // ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.
  
              if (/^[\],:{}\s]*$/
                      .test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                          .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                          .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
  
  // In the third stage we use the eval function to compile the text into a
  // JavaScript structure. The '{' operator is subject to a syntactic ambiguity
  // in JavaScript: it can begin a block or an object literal. We wrap the text
  // in parens to eliminate the ambiguity.
  
                  j = eval('(' + text + ')');
  
  // In the optional fourth stage, we recursively walk the new structure, passing
  // each name/value pair to a reviver function for possible transformation.
  
                  return typeof reviver === 'function' ?
                      walk({'': j}, '') : j;
              }
  
  // If the text is not JSON parseable, then a SyntaxError is thrown.
  
              throw new SyntaxError('JSON.parse');
          };
      }
  }(this);

  provide("ender-json", module.exports);

  !function($) {
      return $.ender({
          JSON: JSON
      });
  }(ender);

}());

(function () {

  var module = { exports: {} }, exports = module.exports;

  // Generated by CoffeeScript 1.3.3
  (function() {
    var $, Formwatcher, Watcher, inputSelector,
      __slice = [].slice,
      __hasProp = {}.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };
  
    $ = ender;
  
    $.ender({
      fwData: function(name, value) {
        var formwatcherAttributes;
        if (this.data("_formwatcher") == null) {
          this.data("_formwatcher", {});
        }
        if (name == null) {
          return this;
        }
        formwatcherAttributes = this.data("_formwatcher");
        if (value != null) {
          formwatcherAttributes[name] = value;
          this.data("_formwatcher", formwatcherAttributes);
          return this;
        } else {
          return formwatcherAttributes[name];
        }
      }
    }, true);
  
    inputSelector = "input, textarea, select, button";
  
    Formwatcher = {
      version: "2.1.9",
      debugging: false,
      debug: function() {
        if (this.debugging && ((typeof console !== "undefined" && console !== null ? console.debug : void 0) != null)) {
          return console.debug.apply(console, arguments);
        }
      },
      getErrorsElement: function(elements, createIfNotFound) {
        var errors, input;
        input = elements.input;
        if (input.attr("name")) {
          errors = $("#" + (input.attr('name')) + "-errors");
        }
        if (!((errors != null ? errors.length : void 0) || !input.attr("id"))) {
          errors = $("#" + (input.attr('id')) + "-errors");
        }
        if (!errors || !errors.length) {
          errors = $.create("<small />");
          if (input.attr("name")) {
            errors.attr("id", input.attr("name") + "-errors");
          }
          errors.insertAfter(input);
        }
        return errors.hide().addClass("errors").addClass("fw-errors");
      },
      deepExtend: function() {
        var extenders, key, object, other, val, _i, _len;
        object = arguments[0], extenders = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
        if (object == null) {
          return {};
        }
        for (_i = 0, _len = extenders.length; _i < _len; _i++) {
          other = extenders[_i];
          for (key in other) {
            if (!__hasProp.call(other, key)) continue;
            val = other[key];
            if (!((object[key] != null) && typeof val === "object")) {
              object[key] = val;
            } else {
              object[key] = this.deepExtend(object[key], val);
            }
          }
        }
        return object;
      },
      getLabel: function(elements, automatchLabel) {
        var input, label, parent;
        input = elements.input;
        if (input.attr("id")) {
          label = $("label[for=" + input.attr("id") + "]");
          if (!label.length) {
            label = undefined;
          }
        }
        if (!label && automatchLabel) {
          parent = input.parent();
          if (parent.get(0).nodeName === "LABEL") {
            label = $("span", parent).first();
            if (label.length === 0) {
              label = void 0;
            }
          } else {
            label = input.previous();
            if (!label.length || label.get(0).nodeName !== "LABEL" || (label.attr("for") != null)) {
              label = void 0;
            }
          }
        }
        return label;
      },
      changed: function(elements, watcher) {
        var input;
        input = elements.input;
        if (!input.fwData("forceValidationOnChange") && (input.attr("type") === "checkbox" && input.fwData("previouslyChecked") === !!input[0].checked) || (input.fwData("previousValue") === input.val())) {
          return;
        }
        input.fwData("forceValidationOnChange", false);
        this.setPreviousValueToCurrentValue(elements);
        if ((input.attr("type") === "checkbox") && (input.fwData("initialyChecked") !== !!input[0].checked) || (input.attr("type") !== "checkbox") && (input.fwData("initialValue") !== input.val())) {
          Formwatcher.setChanged(elements, watcher);
        } else {
          Formwatcher.unsetChanged(elements, watcher);
        }
        if (watcher.options.validate) {
          return watcher.validateElements(elements);
        }
      },
      setChanged: function(elements, watcher) {
        var element, i, input;
        input = elements.input;
        if (input.fwData("changed")) {
          return;
        }
        for (i in elements) {
          if (!__hasProp.call(elements, i)) continue;
          element = elements[i];
          element.addClass("changed");
        }
        input.fwData("changed", true);
        if (!watcher.options.submitUnchanged) {
          Formwatcher.restoreName(elements);
        }
        if (watcher.options.submitOnChange && watcher.options.ajax) {
          return watcher.submitForm();
        }
      },
      unsetChanged: function(elements, watcher) {
        var element, i, input;
        input = elements.input;
        if (!input.fwData("changed")) {
          return;
        }
        for (i in elements) {
          if (!__hasProp.call(elements, i)) continue;
          element = elements[i];
          element.removeClass("changed");
        }
        input.fwData("changed", false);
        if (!watcher.options.submitUnchanged) {
          return Formwatcher.removeName(elements);
        }
      },
      storeInitialValue: function(elements) {
        var input;
        input = elements.input;
        if (input.attr("type") === "checkbox") {
          input.fwData("initialyChecked", !!input[0].checked);
        } else {
          input.fwData("initialValue", input.val());
        }
        return this.setPreviousValueToInitialValue(elements);
      },
      restoreInitialValue: function(elements) {
        var input;
        input = elements.input;
        if (input.attr("type") === "checkbox") {
          input.attr("checked", input.fwData("initialyChecked"));
        } else {
          input.val(input.fwData("initialValue"));
        }
        return this.setPreviousValueToInitialValue(elements);
      },
      setPreviousValueToInitialValue: function(elements) {
        var input;
        input = elements.input;
        if (input.attr("type") === "checkbox") {
          return input.fwData("previouslyChecked", input.fwData("initialyChecked"));
        } else {
          return input.fwData("previousValue", input.fwData("initialValue"));
        }
      },
      setPreviousValueToCurrentValue: function(elements) {
        var input;
        input = elements.input;
        if (input.attr("type") === "checkbox") {
          return input.fwData("previouslyChecked", !!input[0].checked);
        } else {
          return input.fwData("previousValue", input.val());
        }
      },
      removeName: function(elements) {
        var input;
        input = elements.input;
        if (input.attr("type") === "checkbox") {
          return;
        }
        if (!input.fwData("name")) {
          input.fwData("name", input.attr("name") || "");
        }
        return input.attr("name", "");
      },
      restoreName: function(elements) {
        var input;
        input = elements.input;
        if (input.attr("type") === "checkbox") {
          return;
        }
        return input.attr("name", input.fwData("name"));
      },
      decorators: [],
      decorate: function(watcher, input) {
        var dec, decorator, _i, _len, _ref;
        decorator = null;
        _ref = watcher.decorators;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          dec = _ref[_i];
          if (dec.accepts(input)) {
            decorator = dec;
            break;
          }
        }
        if (decorator) {
          Formwatcher.debug("Decorator \"" + decorator.name + "\" found for input field \"" + input.attr("name") + "\".");
          return decorator.decorate(input);
        } else {
          return {
            input: input
          };
        }
      },
      validators: [],
      currentWatcherId: 0,
      watchers: [],
      add: function(watcher) {
        return this.watchers[watcher.id] = watcher;
      },
      get: function(id) {
        return this.watchers[id];
      },
      getAll: function() {
        return this.watchers;
      },
      scanDocument: function() {
        var formId, handleForm, _results,
          _this = this;
        handleForm = function(form) {
          var domOptions, formId, options;
          form = $(form);
          if (form.fwData("watcher")) {
            return;
          }
          formId = form.attr("id");
          options = (formId != null) && (Formwatcher.options[formId] != null) ? Formwatcher.options[formId] : {};
          domOptions = form.data("fw");
          if (domOptions) {
            options = _this.deepExtend(options, JSON.parse(domOptions));
          }
          return new Watcher(form, options);
        };
        $("form[data-fw]").each(function(form) {
          return handleForm(form);
        });
        _results = [];
        for (formId in Formwatcher.options) {
          _results.push(handleForm($("#" + formId)));
        }
        return _results;
      },
      watch: function(form, options) {
        return $.domReady(function() {
          return new Watcher(form, options);
        });
      }
    };
  
    Formwatcher._ElementWatcher = (function() {
  
      _ElementWatcher.prototype.name = "No name";
  
      _ElementWatcher.prototype.description = "No description";
  
      _ElementWatcher.prototype.nodeNames = null;
  
      _ElementWatcher.prototype.classNames = [];
  
      _ElementWatcher.prototype.defaultOptions = {};
  
      _ElementWatcher.prototype.options = null;
  
      function _ElementWatcher(watcher) {
        var _ref;
        this.watcher = watcher;
        this.options = Formwatcher.deepExtend({}, this.defaultOptions, (_ref = watcher.options[this.name]) != null ? _ref : {});
      }
  
      _ElementWatcher.prototype.accepts = function(input) {
        var className, correctClassNames, correctNodeName, inputNodeName, nodeName, _i, _j, _len, _len1, _ref, _ref1;
        if ((this.watcher.options[this.name] != null) && this.watcher.options[this.name] === false) {
          return false;
        }
        correctNodeName = false;
        inputNodeName = input.get(0).nodeName;
        _ref = this.nodeNames;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          nodeName = _ref[_i];
          if (inputNodeName === nodeName) {
            correctNodeName = true;
            break;
          }
        }
        if (!correctNodeName) {
          return false;
        }
        correctClassNames = true;
        _ref1 = this.classNames;
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
          className = _ref1[_j];
          if (!input.hasClass(className)) {
            correctClassNames = false;
            break;
          }
        }
        return correctClassNames;
      };
  
      return _ElementWatcher;
  
    })();
  
    Formwatcher.Decorator = (function(_super) {
  
      __extends(Decorator, _super);
  
      function Decorator() {
        return Decorator.__super__.constructor.apply(this, arguments);
      }
  
      Decorator.prototype.decorate = function(watcher, input) {
        return {
          input: input
        };
      };
  
      return Decorator;
  
    })(Formwatcher._ElementWatcher);
  
    Formwatcher.Validator = (function(_super) {
  
      __extends(Validator, _super);
  
      function Validator() {
        return Validator.__super__.constructor.apply(this, arguments);
      }
  
      Validator.prototype.nodeNames = ["INPUT", "TEXTAREA", "SELECT"];
  
      Validator.prototype.validate = function(sanitizedValue, input) {
        return true;
      };
  
      Validator.prototype.sanitize = function(value) {
        return value;
      };
  
      return Validator;
  
    })(Formwatcher._ElementWatcher);
  
    Formwatcher.defaultOptions = {
      ajax: false,
      ajaxMethod: null,
      validate: true,
      submitOnChange: false,
      submitUnchanged: true,
      submitFormIfAllUnchanged: false,
      resetFormAfterSubmit: false,
      automatchLabel: true,
      responseCheck: function(data) {
        return !data;
      },
      onSubmit: function() {},
      onSuccess: function(data) {},
      onError: function(data) {
        return alert(data);
      },
      onComplete: function(data) {}
    };
  
    Formwatcher.options = {};
  
    Watcher = (function() {
  
      function Watcher(form, options) {
        var Decorator, Validator, hiddenSubmitButtonElement, submitButtons, _i, _j, _len, _len1, _ref, _ref1, _ref2,
          _this = this;
        this.form = typeof form === "string" ? $("#" + form) : $(form);
        if (this.form.length < 1) {
          throw "Form element not found.";
        } else if (this.form.length > 1) {
          throw "More than one form was found.";
        } else if (this.form.get(0).nodeName !== "FORM") {
          throw "The element was not a form.";
        }
        this.allElements = [];
        this.id = Formwatcher.currentWatcherId++;
        Formwatcher.add(this);
        this.observers = {};
        this.form.fwData("watcher", this);
        this.form.fwData("originalAction", this.form.attr("action") || "").attr("action", "javascript:undefined;");
        this.options = Formwatcher.deepExtend({}, Formwatcher.defaultOptions, options || {});
        this.decorators = [];
        this.validators = [];
        _ref = Formwatcher.decorators;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          Decorator = _ref[_i];
          this.decorators.push(new Decorator(this));
        }
        _ref1 = Formwatcher.validators;
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
          Validator = _ref1[_j];
          this.validators.push(new Validator(this));
        }
        if (this.options.ajaxMethod === null) {
          this.options.ajaxMethod = (_ref2 = this.form.attr("method")) != null ? _ref2.toLowerCase() : void 0;
        }
        switch (this.options.ajaxMethod) {
          case "post":
          case "put":
          case "delete":
            break;
          default:
            this.options.ajaxMethod = "get";
        }
        this.observe("submit", this.options.onSubmit);
        this.observe("success", this.options.onSuccess);
        this.observe("error", this.options.onError);
        this.observe("complete", this.options.onComplete);
        $(inputSelector, this.form).each(function(input) {
          var element, elements, errorsElement, i, label, onchangeFunction, validateElementsFunction, validator, _k, _len2, _ref3, _results;
          input = $(input);
          if (!input.fwData("initialized")) {
            if (input.attr("type") === "hidden") {
              return input.fwData("forceSubmission", true);
            } else {
              elements = Formwatcher.decorate(_this, input);
              if (elements.input.get() !== input.get()) {
                elements.input.attr("class", input.attr("class"));
                input = elements.input;
              }
              if (!elements.label) {
                label = Formwatcher.getLabel(elements, _this.options.automatchLabel);
                if (label) {
                  elements.label = label;
                }
              }
              if (!elements.errors) {
                errorsElement = Formwatcher.getErrorsElement(elements, true);
                elements.errors = errorsElement;
              }
              _this.allElements.push(elements);
              input.fwData("validators", []);
              _ref3 = _this.validators;
              for (_k = 0, _len2 = _ref3.length; _k < _len2; _k++) {
                validator = _ref3[_k];
                if (validator.accepts(input, _this)) {
                  Formwatcher.debug("Validator \"" + validator.name + "\" found for input field \"" + input.attr("name") + "\".");
                  input.fwData("validators").push(validator);
                }
              }
              Formwatcher.storeInitialValue(elements);
              if (input.val() === null || !input.val()) {
                for (i in elements) {
                  element = elements[i];
                  element.addClass("empty");
                }
              }
              if (!_this.options.submitUnchanged) {
                Formwatcher.removeName(elements);
              }
              onchangeFunction = function() {
                return Formwatcher.changed(elements, _this);
              };
              validateElementsFunction = function() {
                return _this.validateElements(elements, true);
              };
              _results = [];
              for (i in elements) {
                element = elements[i];
                _results.push((function(element) {
                  var _this = this;
                  element.on("focus", function() {
                    return element.addClass("focus");
                  });
                  element.on("blur", function() {
                    return element.removeClass("focus");
                  });
                  element.on("change", onchangeFunction);
                  element.on("blur", onchangeFunction);
                  return element.on("keyup", validateElementsFunction);
                })(element));
              }
              return _results;
            }
          }
        });
        submitButtons = $("input[type=submit], button[type=''], button[type='submit'], button:not([type])", this.form);
        hiddenSubmitButtonElement = $.create('<input type="hidden" name="" value="" />');
        this.form.append(hiddenSubmitButtonElement);
        submitButtons.each(function(element) {
          element = $(element);
          return element.click(function(e) {
            var elementValue, tmpElementText, _ref3, _ref4;
            if (element[0].tagName === "BUTTON") {
              tmpElementText = element.text();
              element.text("");
              elementValue = (_ref3 = element.val()) != null ? _ref3 : "";
              element.text(tmpElementText);
            } else {
              elementValue = (_ref4 = element.val()) != null ? _ref4 : "";
            }
            hiddenSubmitButtonElement.attr("name", element.attr("name") || "").val(elementValue);
            _this.submitForm();
            return e.stopPropagation();
          });
        });
      }
  
      Watcher.prototype.callObservers = function() {
        var args, eventName, observer, _i, _len, _ref, _results;
        eventName = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
        _ref = this.observers[eventName];
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          observer = _ref[_i];
          _results.push(observer.apply(this, args));
        }
        return _results;
      };
  
      Watcher.prototype.observe = function(eventName, func) {
        if (this.observers[eventName] === undefined) {
          this.observers[eventName] = [];
        }
        this.observers[eventName].push(func);
        return this;
      };
  
      Watcher.prototype.stopObserving = function(eventName, func) {
        var observer;
        this.observers[eventName] = (function() {
          var _i, _len, _ref, _results;
          _ref = this.observers[eventName];
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            observer = _ref[_i];
            if (observer !== func) {
              _results.push(observer);
            }
          }
          return _results;
        }).call(this);
        return this;
      };
  
      Watcher.prototype.enableForm = function() {
        return $(inputSelector, this.form).removeAttr("disabled");
      };
  
      Watcher.prototype.disableForm = function() {
        return $(inputSelector, this.form).attr("disabled", "disabled");
      };
  
      Watcher.prototype.submitForm = function(e) {
        var _this = this;
        if (!this.options.validate || this.validateForm()) {
          this.callObservers("submit");
          this.form.addClass("submitting");
          if (this.options.ajax) {
            this.disableForm();
            return this.submitAjax();
          } else {
            this.form.attr("action", this.form.fwData("originalAction"));
            setTimeout(function() {
              _this.form.submit();
              return _this.disableForm();
            }, 1);
            return false;
          }
        }
      };
  
      Watcher.prototype.validateForm = function() {
        var elements, validated, _i, _len, _ref;
        validated = true;
        _ref = this.allElements;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          elements = _ref[_i];
          if (!this.validateElements(elements)) {
            validated = false;
          }
        }
        return validated;
      };
  
      Watcher.prototype.validateElements = function(elements, inlineValidating) {
        var element, i, input, sanitizedValue, validated, validationOutput, validator, _i, _j, _len, _len1, _ref, _ref1;
        input = elements.input;
        validated = true;
        if (input.fwData("validators").length) {
          if (!inlineValidating || !input.fwData("lastValidatedValue") || input.fwData("lastValidatedValue") !== input.val()) {
            input.fwData("lastValidatedValue", input.val());
            Formwatcher.debug("Validating input " + input.attr("name"));
            input.fwData("validationErrors", []);
            _ref = input.fwData("validators");
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              validator = _ref[_i];
              if (input.val() === "" && validator.name !== "Required") {
                Formwatcher.debug("Validating " + validator.name + ". Field was empty so continuing.");
                continue;
              }
              Formwatcher.debug("Validating " + validator.name);
              validationOutput = validator.validate(validator.sanitize(input.val()), input);
              if (validationOutput !== true) {
                validated = false;
                input.fwData("validationErrors").push(validationOutput);
                break;
              }
            }
            if (validated) {
              elements.errors.html("").hide();
              for (i in elements) {
                if (!__hasProp.call(elements, i)) continue;
                element = elements[i];
                element.addClass("validated");
                element.removeClass("error");
              }
              if (inlineValidating) {
                elements.input.fwData("forceValidationOnChange", true);
              }
            } else {
              for (i in elements) {
                if (!__hasProp.call(elements, i)) continue;
                element = elements[i];
                element.removeClass("validated");
              }
              if (!inlineValidating) {
                elements.errors.html(input.fwData("validationErrors").join("<br />")).show();
                for (i in elements) {
                  if (!__hasProp.call(elements, i)) continue;
                  element = elements[i];
                  element.addClass("error");
                }
              }
            }
          }
          if (!inlineValidating && validated) {
            sanitizedValue = input.fwData("lastValidatedValue");
            _ref1 = input.fwData("validators");
            for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
              validator = _ref1[_j];
              sanitizedValue = validator.sanitize(sanitizedValue);
            }
            input.val(sanitizedValue);
          }
        } else {
          for (i in elements) {
            if (!__hasProp.call(elements, i)) continue;
            element = elements[i];
            element.addClass("validated");
          }
        }
        return validated;
      };
  
      Watcher.prototype.submitAjax = function() {
        var fieldCount, fields, i,
          _this = this;
        Formwatcher.debug("Submitting form via AJAX.");
        fields = {};
        fieldCount = 0;
        i = 0;
        $(inputSelector, this.form).each(function(input, i) {
          var attributeName, _ref;
          input = $(input);
          if (input[0].nodeName === "BUTTON" || (input[0].nodeName === "INPUT" && (input.attr("type").toLowerCase() === "submit" || input.attr("type").toLowerCase() === "button"))) {
            return;
          }
          if (input.fwData("forceSubmission") || (input.attr("type") && input.attr("type").toLowerCase() === "checkbox") || input.fwData('changed') || _this.options.submitUnchanged) {
            if (input.attr("type") !== "checkbox" || input.get(0).checked) {
              fieldCount++;
              attributeName = (_ref = input.attr("name")) != null ? _ref : "unnamedInput_" + i;
              return fields[attributeName] = input.val();
            }
          }
        });
        if (fieldCount === 0 && !this.options.submitFormIfAllUnchanged) {
          return setTimeout(function() {
            _this.enableForm();
            return _this.ajaxSuccess();
          }, 1);
        } else {
          return $.ajax({
            url: this.form.fwData("originalAction"),
            method: this.options.ajaxMethod,
            data: fields,
            type: "text",
            error: function(request) {
              return _this.callObservers("error", request.response);
            },
            success: function(request) {
              _this.enableForm();
              if (!_this.options.responseCheck(request.response)) {
                return _this.callObservers("error", request.response);
              } else {
                _this.callObservers("success", request.response);
                return _this.ajaxSuccess();
              }
            },
            complete: function(request) {
              _this.form.removeClass("submitting");
              return _this.callObservers("complete", request.response);
            }
          });
        }
      };
  
      Watcher.prototype.ajaxSuccess = function() {
        var element, elements, i, isEmpty, _i, _len, _ref, _results;
        _ref = this.allElements;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          elements = _ref[_i];
          Formwatcher.unsetChanged(elements, this);
          if (this.options.resetFormAfterSubmit) {
            Formwatcher.restoreInitialValue(elements);
          } else {
            Formwatcher.storeInitialValue(elements);
          }
          isEmpty = elements.input.val() === null || !elements.input.val();
          _results.push((function() {
            var _results1;
            _results1 = [];
            for (i in elements) {
              element = elements[i];
              if (isEmpty) {
                _results1.push(element.addClass("empty"));
              } else {
                _results1.push(element.removeClass("empty"));
              }
            }
            return _results1;
          })());
        }
        return _results;
      };
  
      return Watcher;
  
    })();
  
    if (typeof window !== "undefined" && window !== null) {
      window.Formwatcher = Formwatcher;
      window.Watcher = Watcher;
    }
  
    $.domReady(function() {
      return Formwatcher.scanDocument();
    });
  
  }).call(this);
  
  
  // Generated by CoffeeScript 1.3.3
  (function() {
    var trim,
      __hasProp = {}.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };
  
    trim = function(string) {
      return string.replace(/^\s.*\s$/, "");
    };
  
    Formwatcher.validators.push((function(_super) {
  
      __extends(_Class, _super);
  
      function _Class() {
        return _Class.__super__.constructor.apply(this, arguments);
      }
  
      _Class.prototype.name = "Integer";
  
      _Class.prototype.description = "Makes sure a value is an integer";
  
      _Class.prototype.classNames = ["validate-integer"];
  
      _Class.prototype.validate = function(value) {
        if (value.replace(/\d*/, "") !== "") {
          return "Has to be a number.";
        }
        return true;
      };
  
      _Class.prototype.sanitize = function(value) {
        return trim(value);
      };
  
      return _Class;
  
    })(Formwatcher.Validator));
  
    Formwatcher.validators.push((function(_super) {
  
      __extends(_Class, _super);
  
      function _Class() {
        return _Class.__super__.constructor.apply(this, arguments);
      }
  
      _Class.prototype.name = "Required";
  
      _Class.prototype.description = "Makes sure the value is not blank (nothing or spaces).";
  
      _Class.prototype.classNames = ["required"];
  
      _Class.prototype.validate = function(value, input) {
        if ((input.attr("type") === "checkbox" && !input.is(":checked")) || !trim(value)) {
          return "Can not be blank.";
        }
        return true;
      };
  
      return _Class;
  
    })(Formwatcher.Validator));
  
    Formwatcher.validators.push((function(_super) {
  
      __extends(_Class, _super);
  
      function _Class() {
        return _Class.__super__.constructor.apply(this, arguments);
      }
  
      _Class.prototype.name = "NotZero";
  
      _Class.prototype.description = "Makes sure the value is not 0.";
  
      _Class.prototype.classNames = ["not-zero"];
  
      _Class.prototype.validate = function(value) {
        var intValue;
        intValue = parseInt(value);
        if (!isNaN(intValue) && intValue === 0) {
          return "Can not be 0.";
        }
        return true;
      };
  
      return _Class;
  
    })(Formwatcher.Validator));
  
    Formwatcher.validators.push((function(_super) {
  
      __extends(_Class, _super);
  
      function _Class() {
        return _Class.__super__.constructor.apply(this, arguments);
      }
  
      _Class.prototype.name = "Email";
  
      _Class.prototype.description = "Makes sure the value is an email.";
  
      _Class.prototype.classNames = ["validate-email"];
  
      _Class.prototype.validate = function(value) {
        var emailRegEx;
        emailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!value.match(emailRegEx)) {
          return "Must be a valid email address.";
        }
        return true;
      };
  
      _Class.prototype.sanitize = function(value) {
        return trim(value);
      };
  
      return _Class;
  
    })(Formwatcher.Validator));
  
    Formwatcher.validators.push((function(_super) {
  
      __extends(_Class, _super);
  
      function _Class() {
        return _Class.__super__.constructor.apply(this, arguments);
      }
  
      _Class.prototype.name = "Float";
  
      _Class.prototype.description = "Makes sure a value is a float";
  
      _Class.prototype.classNames = ["validate-float"];
  
      _Class.prototype.defaultOptions = {
        decimalMark: ","
      };
  
      _Class.prototype.validate = function(value) {
        var regex;
        regex = new RegExp("\\d+(\\" + this.options.decimalMark + "\\d+)?");
        if (value.replace(regex, "") !== "") {
          return "Has to be a number.";
        }
        return true;
      };
  
      _Class.prototype.sanitize = function(value) {
        if (value.indexOf(".") >= 0 && value.indexOf(",") >= 0) {
          if (value.lastIndexOf(",") > value.lastIndexOf(".")) {
            value = value.replace(/\./g, "");
          } else {
            value = value.replace(/\,/g, "");
          }
        }
        if (value.indexOf(",") >= 0) {
          value = value.replace(/\,/g, ".");
        }
        if (value.indexOf(".") !== value.lastIndexOf(".")) {
          value = value.replace(/\./g, "");
        }
        value = value.replace(/\./g, this.options.decimalMark);
        return trim(value);
      };
  
      return _Class;
  
    })(Formwatcher.Validator));
  
  }).call(this);
  

  provide("formwatcher", module.exports);

}());