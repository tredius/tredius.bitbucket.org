(function() {

  jQuery(document).ready(function($) {
    var isEmail, isPlaats, isPostcode, isTelefoon;
    isEmail = function(emailAdres) {
      var patroon;
      patroon = new RegExp(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/);
      return patroon.test(emailAdres);
    };
    isPostcode = function(postCode) {
      var patroon;
      patroon = new RegExp(/^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i);
      return patroon.test(postCode);
    };
    isTelefoon = function(telefoon) {
      var patroon;
      patroon = new RegExp(/^\d{7,}$/);
      return patroon.test(telefoon.replace(/[\s()+\-\.]|ext/g, ""));
    };
    isPlaats = function(plaats) {
      var patroon;
      patroon = new RegExp(/^(([2][e][[:space:]]|['][ts][-[:space:]]))?([ëéÉËa-zA-Z]{2,}|[A-Z]\.)((\s|[-](\s)?)[ëéÉËü\.a-zA-Z]{2,})*$/);
      return patroon.test(plaats);
    };
    $("[id^=ulier] input").each(function() {
      return $(this).after("<mark class=\"validate\"></mark>");
    });
    $(document).on('focusout', "#col1, #col2, input[placeholder*='naam'], input[placeholder~='persoon']", function() {
      if (!$(this).val()) {
        return $(this).addClass("fout").parent().find("mark").removeClass("valid").addClass("fout");
      } else {
        return $(this).removeClass("fout").parent().find("mark").removeClass("fout").addClass("valid");
      }
    });
    $(document).on('focusout', "input[name*='mail']", function() {
      if (!$(this).val() || !isEmail($(this).val())) {
        return $(this).addClass("fout").parent().find("mark").removeClass("valid").addClass("fout");
      } else {
        return $(this).removeClass("fout").parent().find("mark").removeClass("fout").addClass("valid");
      }
    });
    $(document).on('focusout', "input[name*='tel'], #col6", function() {
      if (!$(this).val() || !isTelefoon($(this).val())) {
        return $(this).addClass("fout").parent().find("mark").removeClass("valid").addClass("fout");
      } else {
        return $(this).removeClass("fout").parent().find("mark").removeClass("fout").addClass("valid");
      }
    });
    $(document)('focusout', "#col13, #postcode", function() {
      if (!$(this).val() || !isPostcode($(this).val())) {
        return $(this).addClass("fout").parent().find("mark").removeClass("valid").addClass("fout");
      } else {
        return $(this).removeClass("fout").parent().find("mark").removeClass("fout").addClass("valid");
      }
    });
    $(document).on('focusout', "#col11, #col9, input[name*='plaats']", function() {
      if (!$(this).val() || !isWoonplaats($(this).val())) {
        $(this).addClass("fout").parent().find("mark").removeClass("valid").addClass("fout");
        return alert('foo');
      } else {
        return $(this).removeClass("fout").parent().find("mark").removeClass("fout").addClass("valid");
      }
    });
    return $("#verzenden").click(function() {
      return alert('foo');
    });
  });

}).call(this);
