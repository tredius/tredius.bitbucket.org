(function() {

  $(document).ready(function() {
    window.Swipe = function(element, options) {
      var _this;
      if (!element) {
        return null;
      }
      _this = this;
      this.options = options || {};
      this.index = this.options.startSlide || 0;
      this.speed = this.options.speed || 300;
      this.slidesPerPage = 2;
      this.callback = this.options.callback || function() {};
      this.delay = this.options.auto || 0;
      this.container = element;
      this.element = this.container.children[0];
      this.container.style.overflow = "hidden";
      this.element.style.listStyle = "none";
      this.element.style.margin = 0;
      this.setup();
      this.begin();
      if (this.element.addEventListener) {
        this.element.addEventListener("touchstart", this, false);
        this.element.addEventListener("touchmove", this, false);
        this.element.addEventListener("touchend", this, false);
        this.element.addEventListener("touchcancel", this, false);
        this.element.addEventListener("webkitTransitionEnd", this, false);
        this.element.addEventListener("msTransitionEnd", this, false);
        this.element.addEventListener("oTransitionEnd", this, false);
        this.element.addEventListener("transitionend", this, false);
        return window.addEventListener("resize", this, false);
      }
    };
    return Swipe.prototype = {
      setup: function() {
        var el, index, w;
        this.slides = this.element.children;
        this.length = this.slides.length;
        if (this.length < 2) {
          return null;
        }
        this.width = Math.ceil(("getBoundingClientRect" in this.container ? this.container.getBoundingClientRect().width : this.container.offsetWidth));
        if (this.width === 0 && typeof window.getComputedStyle === "function") {
          this.width = window.getComputedStyle(this.container, null).width.replace("px", "");
        }
        if (!this.width) {
          return null;
        }
        this.container.style.visibility = "hidden";
        this.element.style.width = Math.ceil(this.slides.length * this.width) + "px";
        index = this.slides.length;
        while (index--) {
          el = this.slides[index];
          el.style.width = w = (this.width / 2) + "px";
          el.style.display = "table-cell";
          el.style.verticalAlign = "top";
        }
        this.slide(this.index, 0);
        return this.container.style.visibility = "visible";
      },
      slide: function(index, duration) {
        var style;
        style = this.element.style;
        if (duration === undefined) {
          duration = this.speed;
        }
        style.webkitTransitionDuration = style.MozTransitionDuration = style.msTransitionDuration = style.OTransitionDuration = style.transitionDuration = duration + "ms";
        style.MozTransform = style.webkitTransform = "translate3d(" + -(index * this.width) + "px,0,0)";
        style.msTransform = style.OTransform = "translateX(" + -(index * this.width) + "px)";
        return this.index = index;
      },
      getPos: function() {
        return this.index;
      },
      vorige: function(delay) {
        this.delay = delay || 0;
        clearTimeout(this.interval);
        if (this.index) {
          return this.slide(this.index - 1, this.speed);
        }
      },
      volgende: function(delay) {
        this.delay = delay || 0;
        clearTimeout(this.interval);
        if (this.index < this.length - 1) {
          return this.slide(this.index + 1, this.speed);
        } else {
          return this.slide(0, this.speed);
        }
      },
      begin: function() {
        var _this;
        _this = this;
        return this.interval = (this.delay ? setTimeout(function() {
          return _this.volgende(_this.delay);
        }, this.delay) : 0);
      },
      stop: function() {
        this.delay = 0;
        return clearTimeout(this.interval);
      },
      resume: function() {
        this.delay = this.options.auto || 0;
        return this.begin();
      },
      handleEvent: function(e) {
        switch (e.type) {
          case "touchstart":
            return this.onTouchStart(e);
          case "touchmove":
            return this.onTouchMove(e);
          case "touchcancel":
          case "touchend":
            return this.onTouchEnd(e);
          case "webkitTransitionEnd":
          case "msTransitionEnd":
          case "oTransitionEnd":
          case "transitionend":
            return this.transitionEnd(e);
          case "resize":
            return this.setup();
        }
      },
      transitionEnd: function(e) {
        if (this.delay) {
          this.begin();
        }
        return this.callback(e, this.index, this.slides[this.index]);
      },
      onTouchStart: function(e) {
        this.start = {
          pageX: e.touches[0].pageX,
          pageY: e.touches[0].pageY,
          time: Number(new Date())
        };
        this.isScrolling = undefined;
        this.deltaX = 0;
        this.element.style.MozTransitionDuration = this.element.style.webkitTransitionDuration = 0;
        return e.stopPropagation();
      },
      onTouchMove: function(e) {
        if (e.touches.length > 1 || e.scale && e.scale !== 1) {
          return;
        }
        this.deltaX = e.touches[0].pageX - this.start.pageX;
        if (typeof this.isScrolling === "undefined") {
          this.isScrolling = !!(this.isScrolling || Math.abs(this.deltaX) < Math.abs(e.touches[0].pageY - this.start.pageY));
        }
        if (!this.isScrolling) {
          e.preventDefault();
          clearTimeout(this.interval);
          this.deltaX = this.deltaX / (!this.index && this.deltaX > 0 || this.index === this.length - 1 && this.deltaX < 0 ? Math.abs(this.deltaX) / this.width + 1 : 1);
          this.element.style.MozTransform = this.element.style.webkitTransform = "translate3d(" + (this.deltaX - this.index * this.width) + "px,0,0)";
          return e.stopPropagation();
        }
      },
      onTouchEnd: function(e) {
        var isPastBounds, isValidSlide;
        isValidSlide = Number(new Date()) - this.start.time < 250 && Math.abs(this.deltaX) > 20 || Math.abs(this.deltaX) > this.width / 2;
        isPastBounds = !this.index && this.deltaX > 0 || this.index === this.length - 1 && this.deltaX < 0;
        if (!this.isScrolling) {
          this.slide(this.index + (isValidSlide && !isPastBounds ? (this.deltaX < 0 ? 1 : -1) : 0), this.speed);
        }
        return e.stopPropagation();
      }
    };
  });

}).call(this);
