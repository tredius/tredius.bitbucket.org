(function() {

  jQuery(document).ready(function($) {
    var isEmail, isPlaats, isPostcode, isTelefoon;
    isEmail = function(emailAdres) {
      var patroon;
      patroon = new RegExp(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/);
      return patroon.test(emailAdres);
    };
    isPostcode = function(postCode) {
      var patroon;
      patroon = new RegExp(/^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i);
      return patroon.test(postCode);
    };
    isTelefoon = function(telefoon) {
      var patroon;
      patroon = new RegExp(/^\d{7,}$/);
      return patroon.test(telefoon.replace(/[\s()+\-\.]|ext/g, ""));
    };
    isPlaats = function(plaats) {
      var patroon;
      patroon = new RegExp(/^(([2][e][[:space:]]|['][ts][-[:space:]]))?([ëéÉËa-zA-Z]{2,}|[A-Z]\.)((\s|[-](\s)?)[ëéÉËü\.a-zA-Z]{2,})*$/);
      return patroon.test(plaats);
    };
    $("#form_api input[type=text], #form_api input[type=email], #form_api input[type=tel], #form_api select, #form_api textarea").each(function() {
      return $(this).after("<mark class=\"validate\"></mark>");
    });
    $("#col1,#col2,input[placeholder*='naam'], input[placeholder~='persoon']").focusout(function() {
      if (!$(this).val()) {
        return $(this).addClass("fout").parent().find("mark").removeClass("valid").addClass("fout");
      } else {
        return $(this).removeClass("fout").parent().find("mark").removeClass("fout").addClass("valid");
      }
    });
    $("input[name~='mail'], input[placeholder~='\@']").focusout(function() {
      if (!$(this).val() || !isEmail($(this).val())) {
        return $(this).addClass("fout").parent().find("mark").removeClass("valid").addClass("fout");
      } else {
        return $(this).removeClass("fout").parent().find("mark").removeClass("fout").addClass("valid");
      }
    });
    $("input[name~='tel'], #col6").focusout(function() {
      if (!$(this).val() || !isTelefoon($(this).val())) {
        return $(this).addClass("fout").parent().find("mark").removeClass("valid").addClass("fout");
      } else {
        return $(this).removeClass("fout").parent().find("mark").removeClass("fout").addClass("valid");
      }
    });
    $("#col13, #postcode").focusout(function() {
      if (!$(this).val() || !isPostcode($(this).val())) {
        return $(this).addClass("fout").parent().find("mark").removeClass("valid").addClass("fout");
      } else {
        return $(this).removeClass("fout").parent().find("mark").removeClass("fout").addClass("valid");
      }
    });
    return $("#col11, #col9").focusout(function() {
      if (!$(this).val() || !isWoonplaats($(this).val())) {
        return $(this).addClass("fout").parent().find("mark").removeClass("valid").addClass("fout");
      } else {
        return $(this).removeClass("fout").parent().find("mark").removeClass("fout").addClass("valid");
      }
    });
  });

}).call(this);
