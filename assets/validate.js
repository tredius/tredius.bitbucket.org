(function() {

  $.domReady(function() {
    var isGeldig;
    $("input[type!='radio']").each(function() {
      return $(this).after("<mark class='validate'></mark>");
    });
    isGeldig = function(veld) {
      var patroon, soort, waarde;
      waarde = veld.val();
      soort = veld.attr('id');
      switch (soort) {
        case 'email':
          patroon = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          break;
        case 'postcode':
          patroon = /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i;
          break;
        case 'telefoon':
          patroon = /^\d{7,}$/;
          break;
        default:
          patroon = /^(([2][e][[:space:]]|['][ts][-[:space:]]))?([ëéÉËa-zA-Z]{2,}|[A-Z]\.)((\s|[-](\s)?)[ëéÉËü\.a-zA-Z]{2,})*$/;
      }
      if (soort === 'telefoon') {
        return RegExp(patroon).test(waarde.replace(/[\s()+\-\.]|ext/g, ""));
      } else {
        return RegExp(patroon).test(waarde);
      }
    };
    $(document).on('focusout', "input[type!='radio']", function(e) {
      if (!$(this).val() || !isGeldig($(this))) {
        return $(this).addClass("fout").parent().find("mark").removeClass("valid").addClass("fout");
      } else {
        return $(this).removeClass("fout").parent().find("mark").removeClass("fout").addClass("valid");
      }
    });
    return $(document).on('click', "form a.versturen", function(e) {
      var actie, api, formulier;
      formulier = $(this).parents('form');
      formulier.find("input[type!='radio']").each(function() {
        return $(this).trigger('focusout');
      });
      if ($("form mark.fout").length > 0) {
        return $("span.bericht").text(" Enkele gegevens zijn niet aanwezig of onjuist ingevuld");
      } else {
        $("span.bericht").text("");
        actie = formulier.attr('action');
        api = actie + '&SucessReturnUrl=http://' + window.location.host + '/overig/succes.html' + '&ErrorReturnUrl=http://' + window.location.host + '/overig/faal.html';
        formulier.attr('action', api);
        return formulier.trigger('submit');
      }
    });
  });

}).call(this);
