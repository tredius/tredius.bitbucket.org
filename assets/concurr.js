(function() {
  var exec, word, _i, _len, _ref;

  exec = require('child_process').exec;

  _ref = ["Enjoy", "Rosetta", "Code"];
  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    word = _ref[_i];
    exec("echo " + word, function(err, stdout) {
      return console.log(stdout);
    });
  }

}).call(this);
