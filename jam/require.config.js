var jam = {
    "packages": [
        {
            "name": "hammer.js",
            "location": "jam/hammer.js",
            "main": "hammer.js"
        },
        {
            "name": "lodash",
            "location": "jam/lodash",
            "main": "./lodash.js"
        },
        {
            "name": "async",
            "location": "jam/async",
            "main": "./lib/async"
        },
        {
            "name": "jquery",
            "location": "jam/jquery",
            "main": "jquery.js"
        },
        {
            "name": "jquerypp",
            "location": "jam/jquerypp",
            "main": "index.js"
        }
    ],
    "version": "0.2.11",
    "shim": {
        "jquerypp": {
            "deps": [
                "jquery"
            ]
        }
    }
};

if (typeof require !== "undefined" && require.config) {
    require.config({packages: jam.packages, shim: jam.shim});
}
else {
    var require = {packages: jam.packages, shim: jam.shim};
}

if (typeof exports !== "undefined" && typeof module !== "undefined") {
    module.exports = jam;
}