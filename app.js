(function() {
  var chunkie;

  chunkie = function(arr, len) {
    var chunks, i, n;
    chunks = [];
    i = 0;
    n = arr.length;
    while (i < n) {
      chunks.push(arr.slice(i, i += len));
    }
    return chunks;
  };

}).call(this);
